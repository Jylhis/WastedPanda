import mealSets from './data/mealSets'
import allRecipes from './data/all-main-recipes'

const PRIMARY_KEY=""
//const SECONDARY_KEY=""
const API_URL="https://kesko.azure-api.net"
const LOCAL_URL="http://localhost:3000"

const getFunc = async (url='', data={}) => {
    const response = await fetch(url, {
        method: 'GET',
        headers: {
            'Access-Control-Allow-Origin': '*',
            'Ocp-Apim-Subscription-Key': PRIMARY_KEY,
            'Content-Type': 'application/json',
            
        },
    });
    const myJson = await response.json()
    return myJson
}
export function getRecipeDetail(id) {
    //const response = await fetch(LOCAL_URL+"/data/all-main-recipes.json");
    //const json = await response.json();
    //const results = json.results

    const result = allRecipes.results.find(x => x['Id'] === id)
    return result
}

export function getMealSets() {
    //const response = await fetch(LOCAL_URL+"/data/mealSets.json");
    //const json = await response.json();
    //console.log(json)
    return mealSets
}