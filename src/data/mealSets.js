export default {
    "sets" : [
        {
            "title": "Family meal",
            "description": "Food for one week!",
            "meals": [
                {
                    "Name": "Gluteeniton perunalaatikko",
                    "image": "https://k-file-storage-qa.imgix.net/f/k-ruoka/recipe/10628?w=1000&h=1000&fit=clip",
                    "description": "Gluteeniton perunalaatikko maistuu juuri sopivan makealta eli siltä miltä perunalaatikon kuuluukin! Voit pakastaa laatikon raakana joulua varten.",
                    "Id": "10628",
                    "co2": 0.5,
                    "kcal": 400
                },

                {
                    "Name": "Gyūdon eli japanilainen lihakulho",
                    "image": "https://k-file-storage-qa.imgix.net/f/k-ruoka/recipe/10551?w=1000&h=1000&fit=clip",
                    "description": "Gyūdon on japanilainen arkiruoka. Lihakulho kootaan maukkaan riisin päälle.",
                    "Id": "10551",
                    "co2": 0.6,
                    "kcal": 200
                },

                {
                    "Name": "Kalacurry",
                    "image": "https://k-file-storage-qa.imgix.net/f/k-ruoka/recipe/10550?w=1000&h=1000&fit=clip",
                    "description": "Helppo thaimaalaisittain maustettu kalacurry valmistuu hetkessä. Lisää curryyn tulisuutta tuoreella chilillä.",
                    "Id": "10550",
                    "co2": 0.4,
                    "kcal": 300
                },

                {
                    "Name": "Teriyaki-jauhelihanuudelit",
                    "image": "https://k-file-storage-qa.imgix.net/f/k-ruoka/recipe/10540?w=1000&h=1000&fit=clip",
                    "description": "Teriyaki-jauhelihanuudelit ovat oikea arjen pikaruoka, joka valmistuu vikkelästi.",
                    "Id": "10540",
                    "co2": 1.5,
                    "kcal": 350
                },

                {
                    "Name": "Nopea kanacouscous",
                    "image": "https://k-file-storage-qa.imgix.net/f/k-ruoka/recipe/10538?w=1000&h=1000&fit=clip",
                    "description": "Kanacouscous on oikea pikaruoka, ja tiskiäkään ei juuri synny. Vaikka valmistus vie vain vartin, ruoka on pullollaan makuja: sumakkia, kanelia ja appelsiinia.",
                    "Id": "10538",
                    "co2": 0.9,
                    "kcal": 250
                },

                {
                    "Name": "Japanilainen katkarapukulho",
                    "image": "https://k-file-storage-qa.imgix.net/f/k-ruoka/recipe/10533?w=1000&h=1000&fit=clip",
                    "description": "Kun kaipaat sushin makuja mutta ilman näpertelyä, kokeile tätä. Sushiriisi, edamamepavut ja katkaravut vain ladotaan kulhoon, päälle vielä korianterikastiketta.",
                    "Id": "10533",
                    "co2": 1.2,
                    "kcal": 310
                },

                {
                    "Name": "Pestolihapullat tomaattikastikkeessa",
                    "image": "https://k-file-storage-qa.imgix.net/f/k-ruoka/recipe/10524?w=1000&h=1000&fit=clip",
                    "description": "Maailman helpoimman lihapullakastikkeen lykkäät uuniin  10 minuutissa. Tiskiäkään ei tule paria kippoa enempää.",
                    "Id": "10524",
                    "co2": 1.1,
                    "kcal": 290
                }

            ]
        }
    ]
}
