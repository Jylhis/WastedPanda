export default {
    "totalHits": 3757,
    "results": [{
      "Id": "10628",
      "Name": "Gluteeniton perunalaatikko",
      "PieceSize": {
        "Unit": "g",
        "Amount": "200"
      },
      "Portions": {
        "Unit": "annosta",
        "Amount": "6"
      },
      "PreparationTime": {
        "Description": "yli 60 min",
        "TimeRange": {
          "MinTime": 60
        }
      },
      "UserPortions": {
        "$": {
          "Unit": "henkilölle"
        }
      },
      "Categories": [{
        "MainName": "Pääraaka-aine",
        "MainId": "14",
        "SubName": "Peruna",
        "SubId": "207"
      }, {
        "MainName": "Makuja maailmalta",
        "MainId": "2",
        "SubName": "Suomi",
        "SubId": "106"
      }, {
        "MainName": "Sesonki",
        "MainId": "1",
        "SubName": "Joulu",
        "SubId": "130"
      }, {
        "MainName": "Ruokalaji",
        "MainId": "16",
        "SubName": "Uuniruoat",
        "SubId": "141"
      }, {
        "MainName": "Asiakassegmentointi (K-asiakkaat)",
        "MainId": "17",
        "SubName": "Nautiskelijat",
        "SubId": "146"
      }, {
        "MainName": "Asiakassegmentointi (K-asiakkaat)",
        "MainId": "17",
        "SubName": "Tiedostavat",
        "SubId": "148"
      }, {
        "MainName": "Asiakassegmentointi (K-asiakkaat)",
        "MainId": "17",
        "SubName": "Vakiintuneet",
        "SubId": "150"
      }, {
        "MainName": "Pääruoat",
        "MainId": "4",
        "SubName": "Uuniruoat",
        "SubId": "30"
      }, {
        "MainName": "Sesonki",
        "MainId": "1",
        "SubName": "Joulun ruoat",
        "SubId": "75"
      }],
      "Pictures": ["glut-perunalaatikko-19"],
      "PictureUrls": [{
        "Id": "glut-perunalaatikko-19",
        "Normal": "https://k-file-storage-qa.imgix.net/f/k-ruoka/recipe/10628?w=1000&h=1000&fit=clip",
        "Original": "https://k-file-storage-qa.imgix.net/f/k-ruoka/recipe/10628"
      }],
      "Ingredients": [{
        "SubSectionHeading": "_",
        "SubSectionIngredients": [
          [{
            "Name": "jauhoisia perunoita  (punainen pussi)",
            "Amount": "1",
            "PackageRecipe": "false",
            "IngredientType": "6520",
            "Unit": "kg",
            "IngredientTypeName": "Peruna, jauhoinen"
          }],
          [{
            "Name": "tummaa siirappia",
            "Amount": "4-5",
            "PackageRecipe": "false",
            "IngredientType": "6898",
            "Unit": "rkl",
            "IngredientTypeName": "Siirappi, tumma"
          }],
          [{
            "Name": "Pirkka täysmaitoa",
            "Ean": "6410405113559",
            "Amount": "4 1/2",
            "PackageRecipe": "false",
            "IngredientType": "7218",
            "Unit": "dl",
            "IngredientTypeName": "Täysmaito"
          }],
          [{
            "Name": "voita",
            "Amount": "50",
            "PackageRecipe": "false",
            "IngredientType": "15824",
            "Unit": "g",
            "IngredientTypeName": "Pirkka voi 400g"
          }],
          [{
            "Name": "suolaa",
            "Amount": "1",
            "PackageRecipe": "false",
            "IngredientType": "7049",
            "Unit": "tl",
            "IngredientTypeName": "Suola"
          }],
          [{
            "Name": "jauhettua muskottipähkinää",
            "PackageRecipe": "false",
            "IngredientType": "11630",
            "Unit": "ripaus",
            "IngredientTypeName": "Muskottipähkinä, jauhettu"
          }]
        ]
      }],
      "Instructions": "# Keitä perunat kuorineen kypsiksi. Älä lisää suolaa keitinveteen.\r\n# Kuori perunat ja survo ne sileäksi soseeksi. Sekoita joukkoon siirappi.\r\n# Mittaa kattilaan maito ja voi, kuumenna kunnes voi on sulanut. Lisää suola ja muskottipähkinä.\r\n# Sekoita maito perunasoseen joukkoon ja kaada seos voideltuun uunivuokaan (tilavuus 2 l).\r\n# Paista 150-asteisessa uunissa hieman keskitasoa alemmalla tasolla 2-2 ½ tuntia. Peitä pinta leivinpaperilla, jos se tummuu liikaa.",
      "EndNote": "Vinkki! Jos haluat pakastaa laatikon, tee se ennen kypsennystä.",
      "Description": "Gluteeniton perunalaatikko maistuu juuri sopivan makealta eli siltä miltä perunalaatikon kuuluukin! Voit pakastaa laatikon raakana joulua varten.",
      "DateCreated": "2019-09-26T15:35:08",
      "DateModified": "2019-10-30T12:37:30",
      "Stamp": {
        "Name": "Testattu K-koekeittiössä",
        "Id": "1"
      },
      "RecipeUse": {
        "Name": "k-ruoka.fi",
        "Id": "1"
      },
      "resourceType": "recipe",
      "Url": "https://www.k-ruoka.fi/reseptit/gluteeniton-perunalaatikko",
      "UrlSlug": "gluteeniton-perunalaatikko",
      "Sort": [1, 1, 10628]
    }, {
      "Id": "10568",
      "Name": "Nakki-perunasalaatti",
      "PieceSize": {
        "Unit": "g",
        "Amount": "320"
      },
      "Portions": {
        "Unit": "annosta",
        "Amount": "5-6"
      },
      "PreparationTime": {
        "Description": "alle 30 min",
        "TimeRange": {
          "MinTime": 15,
          "MaxTime": 30
        }
      },
      "UserPortions": {
        "$": {
          "Unit": "henkilölle"
        }
      },
      "Categories": [{
        "MainName": "Teema",
        "MainId": "15",
        "SubName": "Lasten makuun",
        "SubId": "165"
      }, {
        "MainName": "Asiakassegmentointi (K-asiakkaat)",
        "MainId": "17",
        "SubName": "Mukavuudenhaluiset ",
        "SubId": "149"
      }, {
        "MainName": "Asiakassegmentointi (K-asiakkaat)",
        "MainId": "17",
        "SubName": "Vakiintuneet",
        "SubId": "150"
      }, {
        "MainName": "Apukategoriat",
        "MainId": "18",
        "SubName": "Helppo",
        "SubId": "153"
      }, {
        "MainName": "Apukategoriat",
        "MainId": "18",
        "SubName": "Puolivalmisteita sisältävä",
        "SubId": "158"
      }, {
        "MainName": "Teema",
        "MainId": "15",
        "SubName": "Arkiruoka",
        "SubId": "159"
      }, {
        "MainName": "Teema",
        "MainId": "15",
        "SubName": "Halpa",
        "SubId": "161"
      }, {
        "MainName": "Pääruoat",
        "MainId": "4",
        "SubName": "Uuniruoat",
        "SubId": "30"
      }, {
        "MainName": "Sesonki",
        "MainId": "1",
        "SubName": "Lasten reseptit",
        "SubId": "77"
      }, {
        "MainName": "Sesonki",
        "MainId": "1",
        "SubName": "Uusivuosi",
        "SubId": "81"
      }, {
        "MainName": "Pääraaka-aine",
        "MainId": "14",
        "SubName": "Makkara",
        "SubId": "113"
      }, {
        "MainName": "Ruokalaji",
        "MainId": "16",
        "SubName": "Salaatit",
        "SubId": "137"
      }, {
        "MainName": "Ruokalaji",
        "MainId": "16",
        "SubName": "Uuniruoat",
        "SubId": "141"
      }, {
        "MainName": "Sesonki",
        "MainId": "1",
        "SubName": "Vappu",
        "SubId": "3"
      }, {
        "MainName": "Pääruoat",
        "MainId": "4",
        "SubName": "Makkararuoat",
        "SubId": "25"
      }, {
        "MainName": "Pääruoat",
        "MainId": "4",
        "SubName": "Salaatit",
        "SubId": "28"
      }],
      "Pictures": ["nakki_perunasal_PI12_19"],
      "PictureUrls": [{
        "Id": "nakki_perunasal_PI12_19",
        "Normal": "https://k-file-storage-qa.imgix.net/f/k-ruoka/recipe/10568?w=1000&h=1000&fit=clip",
        "Original": "https://k-file-storage-qa.imgix.net/f/k-ruoka/recipe/10568"
      }],
      "Ingredients": [{
        "SubSectionHeading": "_",
        "SubSectionIngredients": [
          [{
            "Name": "Pirkka pestyjä yleisperunoita",
            "Amount": "1",
            "PackageRecipe": "false",
            "IngredientType": "7368",
            "Unit": "kg",
            "IngredientTypeName": "Yleisperuna"
          }],
          [{
            "Name": "rypsiöljyä",
            "Amount": "3",
            "PackageRecipe": "false",
            "IngredientType": "6807",
            "Unit": "rkl",
            "IngredientTypeName": "Rypsiöljy"
          }],
          [{
            "Name": "grillimaustetta",
            "Amount": "3",
            "PackageRecipe": "false",
            "IngredientType": "16159",
            "Unit": "tl",
            "IngredientTypeName": "Grillimauste"
          }],
          [{
            "Name": "Pirkka lihaisia herkkunakkeja",
            "Ean": "6410405179081",
            "Amount": "1",
            "AmountInfo": "(500 g)",
            "PackageRecipe": "false",
            "IngredientType": "16331",
            "Unit": "pkt",
            "IngredientTypeName": "Herkkunakki, lihaisa"
          }],
          [{
            "Name": "Pirkka ohuita vihreitä papuja (pakaste)",
            "Ean": "6410405045713",
            "Amount": "1",
            "AmountInfo": "(300 g)",
            "PackageRecipe": "false",
            "IngredientType": "12308",
            "Unit": "ps",
            "IngredientTypeName": "Papu, vihreä, pakaste"
          }],
          [{
            "Name": "kevätsipulia",
            "Amount": "1",
            "PackageRecipe": "false",
            "IngredientType": "5792",
            "Unit": "dl",
            "IngredientTypeName": "Kevätsipuli"
          }, {
            "Name": "ruohosipulia",
            "PackageRecipe": "false",
            "IngredientType": "6772",
            "IngredientTypeName": "Ruohosipuli, tuore"
          }]
        ]
      }, {
        "SubSectionHeading": "Piimäkastike",
        "SubSectionIngredients": [
          [{
            "Name": "piimää",
            "Amount": "1",
            "PackageRecipe": "false",
            "IngredientType": "6543",
            "Unit": "dl",
            "IngredientTypeName": "Piimä"
          }],
          [{
            "Name": "majoneesia",
            "Amount": "1/2",
            "PackageRecipe": "false",
            "IngredientType": "6058",
            "Unit": "dl",
            "IngredientTypeName": "Majoneesi"
          }],
          [{
            "Name": "Maille Dijon kokojyväsinappia",
            "Amount": "1",
            "PackageRecipe": "false",
            "IngredientType": "5394",
            "Unit": "rkl",
            "IngredientTypeName": "Dijonsinappi, kokojyvä"
          }]
        ]
      }],
      "Instructions": "# Huuhtele perunat ja leikkaa ne puoliksi tai lohkoiksi leivinpaperin päälle uunipellille. Lisää 2 rkl öljyä ja 2 tl grillimaustetta päälle, kääntele sekaisin. Kypsennä 225-asteisessa uunissa noin 25 minuuttia.\r\n# Paloittele nakit. Kuumenna 1 rkl öljyä paistinpannussa ja ruskista nakit. Siirrä hetkeksi lautaselle odottamaan. Lisää pavut ja 1/2 dl vettä pannuun ja hauduta noin 5 minuuttia, kunnes vesi on haihtunut. Mausta 1 tl:lla grillausmaustetta. Lisää nakit joukkoon ja nosta pannu liedeltä.\r\n# Yhdistä kastikkeen ainekset.\r\n# Sekoita nakit ja pavut kypsien perunoiden joukkoon pellillä tai tarjoilukulhossa. Hienonna kevätsipuli päälle. Tarjoa salaatti kuumana kastikkeen kanssa.",
      "EndNote": "Vinkki! Voit käyttää piimäkastikkeessa myös muuta sinappia. *Annoshinnat on laskettu K-citymarket-ketjun hintatason mukaan 11/2019. Tuoretuotteiden hinnat vaihtelevat sesongeittain.",
      "Description": "Nakkisalaatti on nimeään tyylikkäämpi ruoka. Kuuman salaatin maustaa sinappisen kirpakka piimäkastike. Helppo kimpparuoka rentoihin kemuihin ja  lapsenkin suosikki! Noin 1,20€/annos*",
      "DateCreated": "2019-08-20T14:49:36",
      "DateModified": "2019-11-06T16:06:41",
      "Stamp": {
        "Name": "Testattu K-koekeittiössä",
        "Id": "1"
      },
      "RecipeUse": {
        "Name": "k-ruoka.fi",
        "Id": "1"
      },
      "resourceType": "recipe",
      "Url": "https://www.k-ruoka.fi/reseptit/nakki-perunasalaatti",
      "UrlSlug": "nakki-perunasalaatti",
      "Sort": [1, 1, 10568]
    }, {
      "Id": "10552",
      "Name": "Helppo kanakorma",
      "PieceSize": {
        "Unit": "g",
        "Amount": "410"
      },
      "Portions": {
        "Unit": "annosta",
        "Amount": "4"
      },
      "PreparationTime": {
        "Description": "yli 60 min",
        "TimeRange": {
          "MinTime": 60
        }
      },
      "UserPortions": {
        "$": {
          "Unit": "henkilölle"
        }
      },
      "Categories": [{
        "MainName": "Makuja maailmalta",
        "MainId": "2",
        "SubName": "Intia",
        "SubId": "9"
      }, {
        "MainName": "Pääruoat",
        "MainId": "4",
        "SubName": "Broileri- ja linturuoat",
        "SubId": "21"
      }, {
        "MainName": "Pääraaka-aine",
        "MainId": "14",
        "SubName": "Broileri ja linnut",
        "SubId": "118"
      }, {
        "MainName": "Apukategoriat",
        "MainId": "18",
        "SubName": "Helppo",
        "SubId": "153"
      }, {
        "MainName": "Apukategoriat",
        "MainId": "18",
        "SubName": "Puolivalmisteita sisältävä",
        "SubId": "158"
      }, {
        "MainName": "Teema",
        "MainId": "15",
        "SubName": "Arkiruoka",
        "SubId": "159"
      }],
      "Pictures": ["helppo_kanakorma_6_19"],
      "PictureUrls": [{
        "Id": "helppo_kanakorma_6_19",
        "Normal": "https://k-file-storage-qa.imgix.net/f/k-ruoka/recipe/10552?w=1000&h=1000&fit=clip",
        "Original": "https://k-file-storage-qa.imgix.net/f/k-ruoka/recipe/10552"
      }],
      "Ingredients": [{
        "SubSectionHeading": "_",
        "SubSectionIngredients": [
          [{
            "Name": "Kariniemen kananpojan uunivalmis kanakorma kanapataa",
            "Amount": "1",
            "AmountInfo": "(700 g)",
            "PackageRecipe": "false",
            "Unit": "pkt"
          }],
          [{
            "Name": "Pirkka kookosmaitoa",
            "Ean": "6410402020669",
            "Amount": "1/2",
            "AmountInfo": "(à 400 ml)",
            "PackageRecipe": "false",
            "IngredientType": "5847",
            "Unit": "tlk",
            "IngredientTypeName": "Kookosmaito"
          }],
          [{
            "Name": "maissitärkkelysjauhoja (Maizena)",
            "Amount": "2",
            "PackageRecipe": "false",
            "IngredientType": "6035",
            "Unit": "tl",
            "IngredientTypeName": "Maissitärkkelysjauho"
          }]
        ]
      }, {
        "SubSectionHeading": "Korianteririisi",
        "SubSectionIngredients": [
          [{
            "Name": "Pirkka itämaista riisisekoitusta",
            "Ean": "6410405046130",
            "Amount": "1/2",
            "AmountInfo": "(à 500 g)",
            "PackageRecipe": "false",
            "IngredientType": "6738",
            "Unit": "pkt",
            "IngredientTypeName": "Riisisekoitus, itämainen"
          }],
          [{
            "Name": "suolaa",
            "Amount": "1",
            "PackageRecipe": "false",
            "IngredientType": "7049",
            "Unit": "tl",
            "IngredientTypeName": "Suola"
          }],
          [{
            "Name": "korianteria hienonnettuna",
            "Amount": "2",
            "PackageRecipe": "false",
            "IngredientType": "5852",
            "Unit": "dl",
            "IngredientTypeName": "Korianteri, tuore"
          }],
          [{
            "Name": "Pirkka mieto punainen chili",
            "Ean": "6410405091857",
            "Amount": "1",
            "PackageRecipe": "false",
            "IngredientType": "5355",
            "Unit": "",
            "IngredientTypeName": "Chili, punainen"
          }],
          [{
            "Name": "Pirkka mantelilastuja",
            "Ean": "6410405126542",
            "Amount": "1/2",
            "PackageRecipe": "false",
            "IngredientType": "6119",
            "Unit": "dl",
            "IngredientTypeName": "Manteli, lastu"
          }]
        ]
      }],
      "Instructions": "# Poista kanapadasta muovikalvo. Sekoita kookosmaito lusikalla tasaiseksi ennen puolittamista. Sekoita kookosmaito ja maissitärkkelysjauhot keskenään. Kaada seos kanapadan päälle. Paista 175-asteisessa uunissa noin 1 tunti.\r\n# Nosta kanapalat pois uunivuoasta. Soseuta liemi sauvasekoittimella tasaiseksi. Lisää kanapalat takaisin vuokaan.\r\n# Keitä riisisekoitus pakkauksen ohjeen mukaan. Sekoita riisin joukkoon suola ja korianteri. Viipaloi chili.\r\n# Ripottele chiliviipaleet ja mantelilastut kanakorman päälle. Tarjoa kanapata korianteririisin kanssa.",
      "EndNote": "*Annoshinnat on laskettu K-citymarket-ketjun hintatason mukaan 10/2019. Tuoretuotteiden hinnat vaihtelevat sesongeittain.",
      "Description": "Helppo kanakorma syntyy uunivalmiista kanapadasta. Lisää ruokaan chiliä ja korianteria oman makusi mukaan. Noin 2,85 €/annos*",
      "DateCreated": "2019-07-31T10:01:16",
      "DateModified": "2019-10-07T10:16:46",
      "TvDate": "2019-11-18T00:00:00",
      "Stamp": {
        "Name": "Testattu K-koekeittiössä",
        "Id": "1"
      },
      "RecipeUse": {
        "Name": "k-ruoka.fi",
        "Id": "1"
      },
      "resourceType": "recipe",
      "Url": "https://www.k-ruoka.fi/reseptit/helppo-kanakorma",
      "UrlSlug": "helppo-kanakorma",
      "Sort": [1, 1, 10552]
    }, {
      "Id": "10551",
      "Name": "Gyūdon eli japanilainen lihakulho",
      "PieceSize": {
        "Unit": "g",
        "Amount": "365"
      },
      "Portions": {
        "Unit": "annosta",
        "Amount": "3"
      },
      "PreparationTime": {
        "Description": "30 - 60 min",
        "TimeRange": {
          "MinTime": 30,
          "MaxTime": 60
        }
      },
      "UserPortions": {
        "$": {
          "Unit": "henkilölle"
        }
      },
      "Categories": [{
        "MainName": "Pääruoat",
        "MainId": "4",
        "SubName": "Liharuoat",
        "SubId": "24"
      }, {
        "MainName": "Makuja maailmalta",
        "MainId": "2",
        "SubName": "Japani",
        "SubId": "103"
      }, {
        "MainName": "Pääraaka-aine",
        "MainId": "14",
        "SubName": "Liha",
        "SubId": "114"
      }, {
        "MainName": "Teema",
        "MainId": "15",
        "SubName": "Arkiruoka",
        "SubId": "159"
      }, {
        "MainName": "Makuja maailmalta",
        "MainId": "2",
        "SubName": "Aasia",
        "SubId": "7"
      }],
      "Pictures": ["gydon_jap_lihak_6_19"],
      "PictureUrls": [{
        "Id": "gydon_jap_lihak_6_19",
        "Normal": "https://k-file-storage-qa.imgix.net/f/k-ruoka/recipe/10551?w=1000&h=1000&fit=clip",
        "Original": "https://k-file-storage-qa.imgix.net/f/k-ruoka/recipe/10551"
      }],
      "Ingredients": [{
        "SubSectionHeading": "_",
        "SubSectionIngredients": [
          [{
            "Name": "naudan paistisuikaleita, maustamaton",
            "Amount": "300",
            "PackageRecipe": "false",
            "IngredientType": "6322",
            "Unit": "g",
            "IngredientTypeName": "Naudan paistisuikale"
          }],
          [{
            "Name": "Pirkka japanilaista soijakastiketta",
            "Ean": "6410400048825",
            "Amount": "3",
            "PackageRecipe": "false",
            "IngredientType": "6953",
            "Unit": "rkl",
            "IngredientTypeName": "Soijakastike"
          }],
          [{
            "Name": "Pirkka riisiviinietikkaa",
            "Ean": "6410405192738",
            "Amount": "3",
            "PackageRecipe": "false",
            "IngredientType": "6740",
            "Unit": "rkl",
            "IngredientTypeName": "Riisiviinietikka"
          }],
          [{
            "Name": "tuoretta inkivääriä hienonnettuna",
            "Amount": "2",
            "PackageRecipe": "false",
            "IngredientType": "5567",
            "Unit": "rkl",
            "IngredientTypeName": "Inkivääri, tuore"
          }],
          [{
            "Name": "sokeria",
            "Amount": "1 1/2",
            "PackageRecipe": "false",
            "IngredientType": "6961",
            "Unit": "rkl",
            "IngredientTypeName": "Sokeri"
          }],
          [{
            "Name": "sipuli",
            "Amount": "1",
            "AmountInfo": "(n. 100 g)",
            "PackageRecipe": "false",
            "IngredientType": "6932",
            "Unit": "",
            "IngredientTypeName": "Sipuli"
          }],
          [{
            "Name": "öljyä",
            "Amount": "2",
            "PackageRecipe": "false",
            "IngredientType": "6807",
            "Unit": "rkl",
            "IngredientTypeName": "Rypsiöljy"
          }],
          [{
            "Name": "kananmunaa",
            "Amount": "3",
            "PackageRecipe": "false",
            "IngredientType": "5683",
            "Unit": "",
            "IngredientTypeName": "Kananmuna"
          }],
          [{
            "Name": "kevätsipulia hienonnettuna",
            "Amount": "2",
            "PackageRecipe": "false",
            "IngredientType": "5792",
            "Unit": "",
            "IngredientTypeName": "Kevätsipuli"
          }],
          [{
            "Name": "sushi nori merilevää",
            "Amount": "1",
            "PackageRecipe": "false",
            "IngredientType": "6220",
            "Unit": "levy",
            "IngredientTypeName": "Merilevä"
          }]
        ]
      }, {
        "SubSectionHeading": "Japanilainen riisi",
        "SubSectionIngredients": [
          [{
            "Name": "Pirkka Parhaat sushiriisiä",
            "Ean": "6410405210739",
            "Amount": "250",
            "AmountInfo": "(n. 3 dl)",
            "PackageRecipe": "false",
            "IngredientType": "7061",
            "Unit": "g",
            "IngredientTypeName": "Sushiriisi"
          }],
          [{
            "Name": "vettä",
            "Amount": "4",
            "PackageRecipe": "false",
            "IngredientType": "15792",
            "Unit": "dl",
            "IngredientTypeName": "Vesi"
          }],
          [{
            "Name": "Pirkka riisiviinietikkaa",
            "Ean": "6410405192738",
            "Amount": "2-3",
            "PackageRecipe": "false",
            "IngredientType": "6740",
            "Unit": "rkl",
            "IngredientTypeName": "Riisiviinietikka"
          }],
          [{
            "Name": "sokeria",
            "Amount": "1",
            "PackageRecipe": "false",
            "IngredientType": "6961",
            "Unit": "rkl",
            "IngredientTypeName": "Sokeri"
          }],
          [{
            "Name": "suolaa",
            "Amount": "3/4",
            "PackageRecipe": "false",
            "IngredientType": "7049",
            "Unit": "tl",
            "IngredientTypeName": "Suola"
          }]
        ]
      }],
      "Instructions": "# Ota liha huoneenlämpöön noin 30 minuuttia ennen valmistamista. Sekoita soijakastike, etikka, inkivääri ja sokeri keskenään. Kuori ja suikaloi sipuli.\r\n# Paista sipulit pannulla öljyssä. Lisää lihasuikaleet ja kastike. Hauduta pari minuuttia, kunnes liha ei ole enää punaista. Älä kypsennä liikaa, jotta liha pysyy mehevänä.\r\n# Paista kananmunat erikseen pannulla öljyssä.\r\n# Valmista riisi. Huuhdo riisiä siivilässä juoksevan kylmän veden alla, kunnes vesi on kirkasta. Kaada riisi kattilaan ja lisää kylmää vettä niin, että riisi peittyy. Anna seistä 20 minuuttia ja siivilöi vesi pois kattilasta.\r\n# Lisää 4 dl vettä ja kuumenna kiehuvaksi ilman kantta. Hauduta riisiä kannen alla miedolla lämmöllä 10 minuuttia, kunnes kaikki vesi on imeytynyt riisiin. Vältä kannen nostamista, äläkä sekoita riisiä keittämisen aikana.\r\n# Ota kattila pois liedeltä. Sekoita riisiviinietikka, sokeri ja suola keskenään. Mausta riisi etikkaseoksella.\r\n# Jaa riisi annoskulhoihin. Lisää päälle lihasuikaleet, paistettu kananmuna sekä kevätsipulia. Koristele murustetulla tai suikaloidulla merilevällä.",
      "EndNote": "Vinkki! Voit käyttää myös puuroriisiä. *Annoshinnat on laskettu K-citymarket-ketjun hintatason mukaan 10/2019. Tuoretuotteiden hinnat vaihtelevat sesongeittain.",
      "Description": "Gyūdon on japanilainen arkiruoka. Lihakulho kootaan maukkaan riisin päälle. Noin 3,85 €/annos*",
      "DateCreated": "2019-07-31T09:59:13",
      "DateModified": "2019-10-07T10:23:40",
      "TvDate": "2019-11-14T00:00:00",
      "Stamp": {
        "Name": "Testattu K-koekeittiössä",
        "Id": "1"
      },
      "RecipeUse": {
        "Name": "k-ruoka.fi",
        "Id": "1"
      },
      "resourceType": "recipe",
      "Url": "https://www.k-ruoka.fi/reseptit/gyudon-eli-japanilainen-lihakulho",
      "UrlSlug": "gyudon-eli-japanilainen-lihakulho",
      "Sort": [1, 1, 10551]
    }, {
      "Id": "10550",
      "Name": "Kalacurry",
      "PieceSize": {
        "Unit": "g",
        "Amount": "275"
      },
      "Portions": {
        "Unit": "annosta",
        "Amount": "4"
      },
      "PreparationTime": {
        "Description": "30 - 60 min",
        "TimeRange": {
          "MinTime": 30,
          "MaxTime": 60
        }
      },
      "UserPortions": {
        "$": {
          "Unit": "henkilölle"
        }
      },
      "Categories": [{
        "MainName": "Asiakassegmentointi (K-asiakkaat)",
        "MainId": "17",
        "SubName": "Nautiskelijat",
        "SubId": "146"
      }, {
        "MainName": "Apukategoriat",
        "MainId": "18",
        "SubName": "Helppo",
        "SubId": "153"
      }, {
        "MainName": "Apukategoriat",
        "MainId": "18",
        "SubName": "Puolivalmisteita sisältävä",
        "SubId": "158"
      }, {
        "MainName": "Teema",
        "MainId": "15",
        "SubName": "Arkiruoka",
        "SubId": "159"
      }, {
        "MainName": "Pääruoat",
        "MainId": "4",
        "SubName": "Kalaruoat",
        "SubId": "20"
      }, {
        "MainName": "Makuja maailmalta",
        "MainId": "2",
        "SubName": "Thaimaa",
        "SubId": "107"
      }, {
        "MainName": "Pääraaka-aine",
        "MainId": "14",
        "SubName": "Kala",
        "SubId": "116"
      }],
      "Pictures": ["kalacurry_6_19"],
      "PictureUrls": [{
        "Id": "kalacurry_6_19",
        "Normal": "https://k-file-storage-qa.imgix.net/f/k-ruoka/recipe/10550?w=1000&h=1000&fit=clip",
        "Original": "https://k-file-storage-qa.imgix.net/f/k-ruoka/recipe/10550"
      }],
      "VideoUrl": "https://youtu.be/sN14Iy3Tpf4",
      "VideoEmbedUrl": "<iframe width=\"1136\" height=\"639\" src=\"https://www.youtube.com/embed/sN14Iy3Tpf4\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>",
      "Ingredients": [{
        "SubSectionHeading": "_",
        "SubSectionIngredients": [
          [{
            "Name": "Pirkka alaskanseitifileepaloja (pakaste)",
            "Ean": "6410405067661",
            "Amount": "1",
            "AmountInfo": "(400 g)",
            "PackageRecipe": "false",
            "IngredientType": "5208",
            "Unit": "ps",
            "IngredientTypeName": "Alaskanseiti, fileepala, pakaste"
          }],
          [{
            "Name": "suolaa",
            "Amount": "1/2",
            "PackageRecipe": "false",
            "IngredientType": "7049",
            "Unit": "tl",
            "IngredientTypeName": "Suola"
          }],
          [{
            "Name": "keltaista paprikaa",
            "Amount": "1/2",
            "AmountInfo": "(100 g)",
            "PackageRecipe": "false",
            "IngredientType": "6445",
            "Unit": "",
            "IngredientTypeName": "Paprika, keltainen"
          }],
          [{
            "Name": "sipulia",
            "Amount": "1/2",
            "AmountInfo": "(50 g)",
            "PackageRecipe": "false",
            "IngredientType": "6932",
            "Unit": "",
            "IngredientTypeName": "Sipuli"
          }],
          [{
            "Name": "öljyä",
            "Amount": "1",
            "PackageRecipe": "false",
            "IngredientType": "6807",
            "Unit": "rkl",
            "IngredientTypeName": "Rypsiöljy"
          }],
          [{
            "Name": "Pirkka punaista currytahnaa",
            "Ean": "6410405153289",
            "Amount": "1 1/2",
            "PackageRecipe": "false",
            "IngredientType": "5386",
            "Unit": "rkl",
            "IngredientTypeName": "Currytahna, punainen"
          }],
          [{
            "Name": "Pirkka kookosmaitoa",
            "Ean": "6410402020669",
            "Amount": "1",
            "AmountInfo": "(400 ml)",
            "PackageRecipe": "false",
            "IngredientType": "5847",
            "Unit": "tlk",
            "IngredientTypeName": "Kookosmaito"
          }],
          [{
            "Name": "Pirkka ohuita vihreitä papuja (pakaste)",
            "Ean": "6410405045713",
            "Amount": "100",
            "PackageRecipe": "false",
            "IngredientType": "12308",
            "Unit": "g",
            "IngredientTypeName": "Papu, vihreä, pakaste"
          }],
          [{
            "Name": "Pirkka kalakastiketta",
            "Ean": "6410405096449",
            "Amount": "1",
            "PackageRecipe": "false",
            "IngredientType": "5656",
            "Unit": "rkl",
            "IngredientTypeName": "Kalakastike"
          }],
          [{
            "Name": "limetinmehua",
            "Amount": "2",
            "PackageRecipe": "false",
            "IngredientType": "11290",
            "Unit": "tl",
            "IngredientTypeName": "Mehu, limetti"
          }],
          [{
            "Name": "sokeria",
            "Amount": "1",
            "PackageRecipe": "false",
            "IngredientType": "6961",
            "Unit": "tl",
            "IngredientTypeName": "Sokeri"
          }]
        ]
      }],
      "Instructions": "# Sulata kalapalat pakkauksen ohjeen mukaan. Ripottele kalojen pintaan suolaa.\r\n# Kuutioi paprika ja kuorittu sipuli. Kuullota kasviksia öljyssä pannulla, kunnes ne hieman pehmenevät. Lisää currytahna ja kookosmaito. Kuumenna.\r\n# Paloittele sulaneet kalat reiluiksi annospaloiksi. Lisää kalat sekä pavut kuuman kastikkeen joukkoon. Hauduta muutama minuutti, kunnes kala on kypsää.\r\n# Mausta curry kalakastikkeella, limetinmehulla ja sokerilla. Maista, ja lisää mausteita tarvittaessa. Tarjoa kalacurry jasmiiniriisin kanssa.",
      "EndNote": "*Annoshinnat on laskettu K-citymarket-ketjun hintatason mukaan 10/2019. Tuoretuotteiden hinnat vaihtelevat sesongeittain.",
      "Description": "Helppo thaimaalaisittain maustettu kalacurry valmistuu hetkessä. Lisää curryyn tulisuutta tuoreella chilillä. Noin 1,45 €/annos*",
      "DateCreated": "2019-07-31T09:44:05",
      "DateModified": "2019-11-03T20:02:56",
      "TvDate": "2019-11-05T00:00:00",
      "Stamp": {
        "Name": "Testattu K-koekeittiössä",
        "Id": "1"
      },
      "RecipeUse": {
        "Name": "k-ruoka.fi",
        "Id": "1"
      },
      "resourceType": "recipe",
      "Url": "https://www.k-ruoka.fi/reseptit/kalacurry",
      "UrlSlug": "kalacurry",
      "Sort": [1, 1, 10550]
    }, {
      "Id": "10540",
      "Name": "Teriyaki-jauhelihanuudelit",
      "PieceSize": {
        "Unit": "g"
      },
      "Portions": {
        "Unit": "annosta",
        "Amount": "4"
      },
      "PreparationTime": {
        "Description": "alle 30 min",
        "TimeRange": {
          "MinTime": 15,
          "MaxTime": 30
        }
      },
      "UserPortions": {
        "$": {
          "Unit": "henkilölle"
        }
      },
      "Categories": [{
        "MainName": "Makuja maailmalta",
        "MainId": "2",
        "SubName": "Aasia",
        "SubId": "7"
      }, {
        "MainName": "Pääruoat",
        "MainId": "4",
        "SubName": "Jauheliharuoat",
        "SubId": "23"
      }, {
        "MainName": "Pääraaka-aine",
        "MainId": "14",
        "SubName": "Jauheliha",
        "SubId": "117"
      }, {
        "MainName": "Asiakassegmentointi (K-asiakkaat)",
        "MainId": "17",
        "SubName": "Innostujat",
        "SubId": "147"
      }, {
        "MainName": "Asiakassegmentointi (K-asiakkaat)",
        "MainId": "17",
        "SubName": "Mukavuudenhaluiset ",
        "SubId": "149"
      }],
      "Pictures": ["jauhelihanuudelit_19"],
      "PictureUrls": [{
        "Id": "jauhelihanuudelit_19",
        "Normal": "https://k-file-storage-qa.imgix.net/f/k-ruoka/recipe/10540?w=1000&h=1000&fit=clip",
        "Original": "https://k-file-storage-qa.imgix.net/f/k-ruoka/recipe/10540"
      }],
      "SpecialDiets": [{
        "Id": "1",
        "Name": "Vähälaktoosinen"
      }, {
        "Id": "2",
        "Name": "Laktoositon"
      }],
      "Ingredients": [{
        "SubSectionHeading": "_",
        "SubSectionIngredients": [
          [{
            "Name": "Pirkka munanuudeleita",
            "Ean": "6410405173331",
            "Amount": "350",
            "PackageRecipe": "false",
            "IngredientType": "6352",
            "Unit": "g",
            "IngredientTypeName": "Munanuudeli"
          }],
          [{
            "Name": "seesamiöljyä",
            "Amount": "1",
            "PackageRecipe": "false",
            "IngredientType": "6873",
            "Unit": "rkl",
            "IngredientTypeName": "Seesamiöljy"
          }],
          [{
            "Name": "rypsiöljyä",
            "Amount": "1",
            "PackageRecipe": "false",
            "IngredientType": "6807",
            "Unit": "rkl",
            "IngredientTypeName": "Rypsiöljy"
          }],
          [{
            "Name": "sika-nautajauhelihaa",
            "Amount": "1",
            "AmountInfo": "(400 g)",
            "PackageRecipe": "false",
            "IngredientType": "6903",
            "Unit": "rs",
            "IngredientTypeName": "Sika-nauta jauheliha"
          }],
          [{
            "Name": "Pirkka teriyakikastiketta",
            "Ean": "6410405192691",
            "Amount": "1",
            "PackageRecipe": "false",
            "IngredientType": "7110",
            "Unit": "dl",
            "IngredientTypeName": "Teriyakikastike"
          }],
          [{
            "Name": "(soijakastiketta)",
            "PackageRecipe": "false",
            "IngredientType": "6953",
            "IngredientTypeName": "Soijakastike"
          }],
          [{
            "Name": "porkkanaa",
            "Amount": "2",
            "PackageRecipe": "false",
            "IngredientType": "6582",
            "Unit": "",
            "IngredientTypeName": "Porkkana"
          }],
          [{
            "Name": "kurkku",
            "Amount": "1/2",
            "PackageRecipe": "false",
            "IngredientType": "5895",
            "Unit": "",
            "IngredientTypeName": "Kurkku"
          }],
          [{
            "Name": "kevätsipulin vartta",
            "Amount": "3",
            "PackageRecipe": "false",
            "IngredientType": "5792",
            "Unit": "",
            "IngredientTypeName": "Kevätsipuli"
          }],
          [{
            "Name": "mustapippuria",
            "PackageRecipe": "false",
            "IngredientType": "6271",
            "IngredientTypeName": "Mustapippuri, rouhittu"
          }]
        ]
      }],
      "Instructions": "# Valmista nuudelit pakkauksen ohjeen mukaan. Valuta ja lisää joukkoon seesamiöljy. Pidä lämpiminä.\r\n# Ruskista jauheliha rypsiöljyssä. Lisää lopuksi teriyakikastike ja kuumenna. Tarkista maku ja lisää tarpeen mukaan mustapippuria, soijakastiketta tai mustapippuria.\r\n# Kuori porkkanat ja leikkaa ne pitkiksi ohuiksi suikaleiksi julienne-raastimella. Leikkaa kurkusta samanlaisia pitkiä soiroja kuin porkkanasta. Suikaloi kevätsipuli.\r\n# Kokoa annos kulhoon nuudeleista ja jauhelihasta. Voit myös halutessasi pyöräyttää nuudelit sekaisin jauhelihan kanssa pannulla, jolloin ne kuumenevat samalla. Laita annoksen pinnalle porkkanaa, kurkkua ja viimeistele kevätsipulilla.",
      "EndNote": "Jos haluat annokseen tulisuutta, lisää pinnalle Pirkka srirachakastiketta.",
      "Description": "Teriyaki-jauhelihanuudelit ovat oikea arjen pikaruoka, joka valmistuu vikkelästi. Resepti: Nanna Rintala / Kaikki äitini reseptit.",
      "DateCreated": "2019-07-11T12:22:55",
      "DateModified": "2019-07-12T07:37:36",
      "Stamp": {
        "Name": "Bloggaaja",
        "Id": "2"
      },
      "RecipeUse": {
        "Name": "k-ruoka.fi",
        "Id": "1"
      },
      "resourceType": "recipe",
      "Url": "https://www.k-ruoka.fi/reseptit/teriyaki-jauhelihanuudelit",
      "UrlSlug": "teriyaki-jauhelihanuudelit",
      "Sort": [1, 1, 10540]
    }, {
      "Id": "10538",
      "Name": "Nopea kanacouscous",
      "PieceSize": {
        "Unit": "g",
        "Amount": "410"
      },
      "Portions": {
        "Unit": "annosta",
        "Amount": "4-5"
      },
      "PreparationTime": {
        "Description": "alle 30 min",
        "TimeRange": {
          "MinTime": 15,
          "MaxTime": 30
        }
      },
      "UserPortions": {
        "$": {
          "Unit": "henkilölle"
        }
      },
      "Categories": [{
        "MainName": "Teema",
        "MainId": "15",
        "SubName": "Mitä tänään syötäisiin?",
        "SubId": "144"
      }, {
        "MainName": "Asiakassegmentointi (K-asiakkaat)",
        "MainId": "17",
        "SubName": "Innostujat",
        "SubId": "147"
      }, {
        "MainName": "Asiakassegmentointi (K-asiakkaat)",
        "MainId": "17",
        "SubName": "Mukavuudenhaluiset ",
        "SubId": "149"
      }, {
        "MainName": "Apukategoriat",
        "MainId": "18",
        "SubName": "Helppo",
        "SubId": "153"
      }, {
        "MainName": "Apukategoriat",
        "MainId": "18",
        "SubName": "Puolivalmisteita sisältävä",
        "SubId": "158"
      }, {
        "MainName": "Teema",
        "MainId": "15",
        "SubName": "Arkiruoka",
        "SubId": "159"
      }, {
        "MainName": "Pääruoat",
        "MainId": "4",
        "SubName": "Broileri- ja linturuoat",
        "SubId": "21"
      }, {
        "MainName": "Makuja maailmalta",
        "MainId": "2",
        "SubName": "Lähi-Itä ja Afrikka",
        "SubId": "104"
      }, {
        "MainName": "Pääraaka-aine",
        "MainId": "14",
        "SubName": "Broileri ja linnut",
        "SubId": "118"
      }],
      "Pictures": ["nopea_kanacous_PI11_19"],
      "PictureUrls": [{
        "Id": "nopea_kanacous_PI11_19",
        "Normal": "https://k-file-storage-qa.imgix.net/f/k-ruoka/recipe/10538?w=1000&h=1000&fit=clip",
        "Original": "https://k-file-storage-qa.imgix.net/f/k-ruoka/recipe/10538"
      }],
      "VideoUrl": "https://youtu.be/fmtiGUSovOo",
      "VideoEmbedUrl": "<iframe width=\"1136\" height=\"639\" src=\"https://www.youtube.com/embed/fmtiGUSovOo\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>",
      "SpecialDiets": [{
        "Id": "7",
        "Name": "Kananmunaton"
      }],
      "Ingredients": [{
        "SubSectionHeading": "_",
        "SubSectionIngredients": [
          [{
            "Name": "Kariniemen kananpojan paistettuja broilerin fileepaloja",
            "Amount": "1",
            "AmountInfo": "(200 g)",
            "PackageRecipe": "false",
            "IngredientType": "16467",
            "Unit": "pkt",
            "IngredientTypeName": "broilerinfileepala"
          }],
          [{
            "Name": "oliiviöljyä",
            "Amount": "3",
            "PackageRecipe": "false",
            "IngredientType": "5405",
            "Unit": "rkl",
            "IngredientTypeName": "Ekstra neitsytoliiviöljy"
          }],
          [{
            "Name": "Pirkka pehmeitä taateleita",
            "Ean": "6408641083033",
            "Amount": "1",
            "AmountInfo": "(200 g)",
            "PackageRecipe": "false",
            "IngredientType": "7073",
            "Unit": "ps",
            "IngredientTypeName": "Taateli, kuivattu"
          }],
          [{
            "Name": "Pirkka ohuita vihreitä papuja (pakaste)",
            "Ean": "6410405045713",
            "Amount": "1",
            "AmountInfo": "(300 g)",
            "PackageRecipe": "false",
            "IngredientType": "12308",
            "Unit": "ps",
            "IngredientTypeName": "Papu, vihreä, pakaste"
          }],
          [{
            "Name": "couscousia",
            "Amount": "2",
            "PackageRecipe": "false",
            "IngredientType": "5369",
            "Unit": "dl",
            "IngredientTypeName": "Couscous"
          }],
          [{
            "Name": "vettä",
            "Amount": "4",
            "PackageRecipe": "false",
            "IngredientType": "15792",
            "Unit": "dl",
            "IngredientTypeName": "Vesi"
          }],
          [{
            "Name": "sumakkia",
            "Amount": "2",
            "PackageRecipe": "false",
            "IngredientType": "16468",
            "Unit": "tl",
            "IngredientTypeName": "sumakki"
          }],
          [{
            "Name": "kanelia",
            "Ean": "6410405055644",
            "Amount": "2",
            "PackageRecipe": "false",
            "IngredientType": "5689",
            "Unit": "tl",
            "IngredientTypeName": "Kaneli, jauhettu"
          }],
          [{
            "Name": "kurkumaa",
            "Amount": "1",
            "PackageRecipe": "false",
            "IngredientType": "10200",
            "Unit": "tl",
            "IngredientTypeName": "Kurkuma, jauhettu"
          }],
          [{
            "Name": "suolaa",
            "Amount": "1-1 1/2",
            "PackageRecipe": "false",
            "IngredientType": "7049",
            "Unit": "tl",
            "IngredientTypeName": "Suola"
          }]
        ]
      }, {
        "SubSectionHeading": "Appelsiinijogurtti",
        "SubSectionIngredients": [
          [{
            "Name": "appelsiini",
            "Amount": "1",
            "AmountInfo": "(300 g)",
            "PackageRecipe": "false",
            "IngredientType": "7532",
            "Unit": "",
            "IngredientTypeName": "Appelsiini, tuore"
          }],
          [{
            "Name": "Pirkka turkkilaista jogurttia",
            "Ean": "6410405169617",
            "Amount": "1",
            "AmountInfo": "(200 g)",
            "PackageRecipe": "false",
            "IngredientType": "7201",
            "Unit": "tlk",
            "IngredientTypeName": "Turkkilainen jogurtti"
          }],
          [{
            "Name": "juoksevaa hunajaa",
            "Amount": "1",
            "PackageRecipe": "false",
            "IngredientType": "5550",
            "Unit": "rkl",
            "IngredientTypeName": "Hunaja, juokseva"
          }]
        ]
      }],
      "Instructions": "# Kuumenna broilerinpalat kasarissa öljyssä. Lisää taatelit, pavut, couscous, vesi ja mausteet.\r\n# Keitä kannen alla 3 minuuttia. Nosta kasari sitten liedeltä ja anna tekeytyä 5 minuuttia.\r\n# Sekoita couscous ilmavaksi. Ripottele vielä sumakkia koristeeksi.\r\n# Tee appelsiinijogurtti. Raasta hyvin pestyn appelsiinin kuori kulhoon. Sekoita jogurtti ja hunaja joukkoon.\r\n# Leikkaa appelsiini kuorettomiksi lohkoiksi couscousin päälle. Tarjoa couscous appelsiinijogurtin kanssa.",
      "EndNote": "Voit halutessasi korvata sumakin juustokuminalla. *Annoshinnat on laskettu K-citymarket-ketjun hintatason mukaan 10/2019. Tuoretuotteiden hinnat vaihtelevat sesongeittain.",
      "Description": "Kanacouscous on oikea pikaruoka, ja tiskiäkään ei juuri synny. Vaikka valmistus vie vain vartin, ruoka on pullollaan makuja: sumakkia, kanelia ja appelsiinia. Noin 2,45 €/annos*",
      "DateCreated": "2019-07-11T10:45:30",
      "DateModified": "2019-11-08T13:50:14",
      "TvDate": "2019-10-28T00:00:00",
      "Stamp": {
        "Name": "Testattu K-koekeittiössä",
        "Id": "1"
      },
      "RecipeUse": {
        "Name": "k-ruoka.fi",
        "Id": "1"
      },
      "resourceType": "recipe",
      "Url": "https://www.k-ruoka.fi/reseptit/nopea-kanacouscous",
      "UrlSlug": "nopea-kanacouscous",
      "Sort": [1, 1, 10538]
    }, {
      "Id": "10534",
      "Name": "Kananachopelti",
      "PieceSize": {
        "Unit": "g",
        "Amount": "350"
      },
      "Portions": {
        "Unit": "annosta",
        "Amount": "4-5"
      },
      "PreparationTime": {
        "Description": "alle 30 min",
        "TimeRange": {
          "MinTime": 15,
          "MaxTime": 30
        }
      },
      "UserPortions": {
        "$": {
          "Unit": "henkilölle"
        }
      },
      "Categories": [{
        "MainName": "Apukategoriat",
        "MainId": "18",
        "SubName": "Lyhyt aktiivinen valmistusaika ",
        "SubId": "157"
      }, {
        "MainName": "Apukategoriat",
        "MainId": "18",
        "SubName": "Puolivalmisteita sisältävä",
        "SubId": "158"
      }, {
        "MainName": "Teema",
        "MainId": "15",
        "SubName": "Lasten juhlat",
        "SubId": "163"
      }, {
        "MainName": "Teema",
        "MainId": "15",
        "SubName": "Juhlat",
        "SubId": "164"
      }, {
        "MainName": "Iltapalat",
        "MainId": "21",
        "SubName": "Muut",
        "SubId": "197"
      }, {
        "MainName": "Ruokalaji",
        "MainId": "16",
        "SubName": "Uuniruoat",
        "SubId": "141"
      }, {
        "MainName": "Asiakassegmentointi (K-asiakkaat)",
        "MainId": "17",
        "SubName": "Nautiskelijat",
        "SubId": "146"
      }, {
        "MainName": "Asiakassegmentointi (K-asiakkaat)",
        "MainId": "17",
        "SubName": "Innostujat",
        "SubId": "147"
      }, {
        "MainName": "Asiakassegmentointi (K-asiakkaat)",
        "MainId": "17",
        "SubName": "Mukavuudenhaluiset ",
        "SubId": "149"
      }, {
        "MainName": "Asiakassegmentointi (K-asiakkaat)",
        "MainId": "17",
        "SubName": "Vakiintuneet",
        "SubId": "150"
      }, {
        "MainName": "Apukategoriat",
        "MainId": "18",
        "SubName": "Helppo",
        "SubId": "153"
      }, {
        "MainName": "Makuja maailmalta",
        "MainId": "2",
        "SubName": "Meksiko ja texmex",
        "SubId": "12"
      }, {
        "MainName": "Pääruoat",
        "MainId": "4",
        "SubName": "Broileri- ja linturuoat",
        "SubId": "21"
      }, {
        "MainName": "Sesonki",
        "MainId": "1",
        "SubName": "Uusivuosi",
        "SubId": "81"
      }, {
        "MainName": "Pääraaka-aine",
        "MainId": "14",
        "SubName": "Broileri ja linnut",
        "SubId": "118"
      }, {
        "MainName": "Sesonki",
        "MainId": "1",
        "SubName": "Isänpäivä",
        "SubId": "127"
      }, {
        "MainName": "Teema",
        "MainId": "15",
        "SubName": "Brunssi",
        "SubId": "131"
      }, {
        "MainName": "Sesonki",
        "MainId": "1",
        "SubName": "Vappu",
        "SubId": "3"
      }],
      "Pictures": ["kananachopelti_PI12_19"],
      "PictureUrls": [{
        "Id": "kananachopelti_PI12_19",
        "Normal": "https://k-file-storage-qa.imgix.net/f/k-ruoka/recipe/10534?w=1000&h=1000&fit=clip",
        "Original": "https://k-file-storage-qa.imgix.net/f/k-ruoka/recipe/10534"
      }],
      "Ingredients": [{
        "SubSectionHeading": "_",
        "SubSectionIngredients": [
          [{
            "Name": "Kariniemen kananpojan fajitassuikaleita",
            "Amount": "1",
            "AmountInfo": "(450 g)",
            "PackageRecipe": "false",
            "IngredientType": "16463",
            "Unit": "pkt",
            "IngredientTypeName": "Kariniemen fajitassuikale"
          }],
          [{
            "Name": "rypsiöljyä",
            "Amount": "1",
            "PackageRecipe": "false",
            "IngredientType": "6807",
            "Unit": "rkl",
            "IngredientTypeName": "Rypsiöljy"
          }],
          [{
            "Name": "Pirkka kikherneitä suolaliemessä",
            "Ean": "6410405088383",
            "Amount": "1",
            "AmountInfo": "(380 g/230 g)",
            "PackageRecipe": "false",
            "IngredientType": "5797",
            "Unit": "tlk",
            "IngredientTypeName": "Kikherne, säilyke"
          }],
          [{
            "Name": "Pirkka tortillachipsejä",
            "Ean": "6410405084415",
            "Amount": "1",
            "AmountInfo": "(200 g)",
            "PackageRecipe": "false",
            "IngredientType": "16015",
            "Unit": "ps",
            "IngredientTypeName": "Nacho"
          }],
          [{
            "Name": "Pirkka Parhaat cheddaria",
            "Ean": "6410405230904",
            "Amount": "1",
            "AmountInfo": "(200 g)",
            "PackageRecipe": "false",
            "IngredientType": "5349",
            "Unit": "pkt",
            "IngredientTypeName": "Cheddarjuusto"
          }],
          [{
            "Name": "tomaattia",
            "Amount": "2",
            "AmountInfo": "(200 g)",
            "PackageRecipe": "false",
            "IngredientType": "7124",
            "Unit": "",
            "IngredientTypeName": "Tomaatti"
          }],
          [{
            "Name": "kurkkua",
            "Amount": "1/2",
            "AmountInfo": "(200 g)",
            "PackageRecipe": "false",
            "IngredientType": "5895",
            "Unit": "",
            "IngredientTypeName": "Kurkku"
          }],
          [{
            "Name": "jäävuorisalaattia",
            "Amount": "1/2",
            "AmountInfo": "(150 g)",
            "PackageRecipe": "false",
            "IngredientType": "5635",
            "Unit": "",
            "IngredientTypeName": "Jäävuorisalaatti"
          }],
          [{
            "Name": "korianteria",
            "Amount": "1",
            "PackageRecipe": "false",
            "IngredientType": "5852",
            "Unit": "ruukku",
            "IngredientTypeName": "Korianteri, tuore"
          }],
          [{
            "Name": "Pirkka vihreitä jalapenoviipaleita",
            "Ean": "6410405092632",
            "Amount": "1/2",
            "AmountInfo": "(à 225 g/125 g)",
            "PackageRecipe": "false",
            "IngredientType": "5573",
            "Unit": "prk",
            "IngredientTypeName": "Jalapeno, viipale"
          }],
          [{
            "Name": "Pirkka crème fraîchea (ranskankermaa, 18 %)",
            "Ean": "6410405164629",
            "Amount": "1",
            "AmountInfo": "(150 g)",
            "PackageRecipe": "false",
            "IngredientType": "7954",
            "Unit": "prk",
            "IngredientTypeName": "Creme fraiche, 18 %, laktoositon"
          }],
          [{
            "Name": "Pirkka meksikolaista salsa chilpotle -kastiketta",
            "Ean": "6410405150257",
            "Amount": "1/2",
            "AmountInfo": "(à 230 g)",
            "PackageRecipe": "false",
            "IngredientType": "6857",
            "Unit": "prk",
            "IngredientTypeName": "Salsakastike, chipotle"
          }]
        ]
      }],
      "Instructions": "# Kuumenna öljy paistinpannussa. Paista broilerisuikaleita muutama minuutti. Huuhtele kikherneet ja lisää pannuun. Jatka paistamista muutama minuutti.\r\n# Levitä maissilastut leivinpaperin päälle uunipellille. Lisää broilerit ja kikherneet. Raasta juusto ja ripottele se pinnalle.\r\n# Kypsennä nachopeltiä 225-asteisessa uunissa noin 10 minuuttia, kunnes juusto on sulanut.\r\n# Kuutioi tomaatti ja kurkku. Leikkaa salaatti lohkoiksi. Hienonna korianteri. Valuta jalapenot. Laita ranskankerma ja salsa kuppeihin tarjolle.\r\n# Nostele kasvikset kuuman nachopellin päälle. Tarjoa dippien kanssa.",
      "EndNote": "*Annoshinnat on laskettu K-citymarket-ketjun hintatason mukaan 11/2019. Tuoretuotteiden hinnat vaihtelevat sesongeittain.",
      "Description": "Kananachopelti on näyttävä kimpparuoka. Sulan cheddarin alla on kanan lisäksi kikherneitä. Peltiruoka ei ole liian tuhti, kun päälle lohkoo reiluja paloja salaattia. Noin 3,80€/annos*",
      "DateCreated": "2019-07-10T13:23:40",
      "DateModified": "2019-11-06T16:05:59",
      "Stamp": {
        "Name": "Testattu K-koekeittiössä",
        "Id": "1"
      },
      "RecipeUse": {
        "Name": "k-ruoka.fi",
        "Id": "1"
      },
      "resourceType": "recipe",
      "Url": "https://www.k-ruoka.fi/reseptit/kananachopelti",
      "UrlSlug": "kananachopelti",
      "Sort": [1, 1, 10534]
    }, {
      "Id": "10533",
      "Name": "Japanilainen katkarapukulho",
      "PieceSize": {
        "Unit": "g",
        "Amount": "350"
      },
      "Portions": {
        "Unit": "annosta",
        "Amount": "4"
      },
      "PreparationTime": {
        "Description": "alle 30 min",
        "TimeRange": {
          "MinTime": 15,
          "MaxTime": 30
        }
      },
      "UserPortions": {
        "$": {
          "Unit": "henkilölle"
        }
      },
      "Categories": [{
        "MainName": "Pääruoat",
        "MainId": "4",
        "SubName": "Muut pääruoat",
        "SubId": "32"
      }, {
        "MainName": "Makuja maailmalta",
        "MainId": "2",
        "SubName": "Japani",
        "SubId": "103"
      }, {
        "MainName": "Pääraaka-aine",
        "MainId": "14",
        "SubName": "Äyriäiset",
        "SubId": "111"
      }, {
        "MainName": "Apukategoriat",
        "MainId": "18",
        "SubName": "Puolivalmisteita sisältävä",
        "SubId": "158"
      }, {
        "MainName": "Pääraaka-aine",
        "MainId": "14",
        "SubName": "Kasvis",
        "SubId": "115"
      }, {
        "MainName": "Asiakassegmentointi (K-asiakkaat)",
        "MainId": "17",
        "SubName": "Innostujat",
        "SubId": "147"
      }, {
        "MainName": "Asiakassegmentointi (K-asiakkaat)",
        "MainId": "17",
        "SubName": "Mukavuudenhaluiset ",
        "SubId": "149"
      }, {
        "MainName": "Apukategoriat",
        "MainId": "18",
        "SubName": "Helppo",
        "SubId": "153"
      }, {
        "MainName": "Apukategoriat",
        "MainId": "18",
        "SubName": "Trendikäs ",
        "SubId": "156"
      }, {
        "MainName": "Apukategoriat",
        "MainId": "18",
        "SubName": "Lyhyt aktiivinen valmistusaika ",
        "SubId": "157"
      }],
      "Pictures": ["japanil_katkar_PI11_19"],
      "PictureUrls": [{
        "Id": "japanil_katkar_PI11_19",
        "Normal": "https://k-file-storage-qa.imgix.net/f/k-ruoka/recipe/10533?w=1000&h=1000&fit=clip",
        "Original": "https://k-file-storage-qa.imgix.net/f/k-ruoka/recipe/10533"
      }],
      "Ingredients": [{
        "SubSectionHeading": "_",
        "SubSectionIngredients": [
          [{
            "Name": "Pirkka Parhaat sushiriisiä",
            "Ean": "6410405210739",
            "Amount": "2 1/2",
            "AmountInfo": "(250 g)",
            "PackageRecipe": "false",
            "IngredientType": "7061",
            "Unit": "dl",
            "IngredientTypeName": "Sushiriisi"
          }],
          [{
            "Name": "vettä",
            "Amount": "3 1/2",
            "PackageRecipe": "false",
            "Unit": "dl"
          }],
          [{
            "Name": "Pirkka riisiviinietikkaa",
            "Ean": "6410405192738",
            "Amount": "3",
            "PackageRecipe": "false",
            "IngredientType": "6740",
            "Unit": "rkl",
            "IngredientTypeName": "Riisiviinietikka"
          }],
          [{
            "Name": "sokeria",
            "Amount": "2",
            "PackageRecipe": "false",
            "IngredientType": "6961",
            "Unit": "tl",
            "IngredientTypeName": "Sokeri"
          }],
          [{
            "Name": "suolaa",
            "Amount": "1/4",
            "PackageRecipe": "false",
            "IngredientType": "7049",
            "Unit": "tl",
            "IngredientTypeName": "Suola"
          }],
          [{
            "Name": "Pirkka Parhaat chili-seesamikatkarapuja",
            "Ean": "6410405230539",
            "Amount": "1",
            "AmountInfo": "(200 g)",
            "PackageRecipe": "false",
            "IngredientType": "16462",
            "Unit": "prk",
            "IngredientTypeName": "Pirkka Parhaat chili-seesamikatkarapu"
          }],
          [{
            "Name": "Pirkka soijapapuja (pakaste)",
            "Ean": "6410405149312",
            "Amount": "1",
            "AmountInfo": "(250 g)",
            "PackageRecipe": "false",
            "IngredientType": "6957",
            "Unit": "ps",
            "IngredientTypeName": "Soijapapu, pakaste"
          }],
          [{
            "Name": "avokadoa",
            "Amount": "2",
            "AmountInfo": "(400 g)",
            "PackageRecipe": "false",
            "IngredientType": "7605",
            "Unit": "",
            "IngredientTypeName": "Avokado, tuore"
          }],
          [{
            "Name": "Pirkka sushi inkiväärilastuja",
            "Ean": "6410405192752",
            "Amount": "1/2",
            "AmountInfo": "(à 100 g/50 g)",
            "PackageRecipe": "false",
            "IngredientType": "5568",
            "Unit": "prk",
            "IngredientTypeName": "Inkiväärilastu, säilyke"
          }],
          [{
            "Name": "korianteria",
            "Amount": "1/2",
            "PackageRecipe": "false",
            "IngredientType": "5852",
            "Unit": "ruukkua",
            "IngredientTypeName": "Korianteri, tuore"
          }]
        ]
      }, {
        "SubSectionHeading": "Kastike",
        "SubSectionIngredients": [
          [{
            "Name": "Pirkka japanilaista soijakastiketta",
            "Ean": "6410400048825",
            "Amount": "3",
            "PackageRecipe": "false",
            "IngredientType": "6953",
            "Unit": "rkl",
            "IngredientTypeName": "Soijakastike"
          }],
          [{
            "Name": "Pirkka riisiviinietikkaa",
            "Ean": "6410405192738",
            "Amount": "2",
            "PackageRecipe": "false",
            "IngredientType": "6740",
            "Unit": "rkl",
            "IngredientTypeName": "Riisiviinietikka"
          }],
          [{
            "Name": "limetin mehu",
            "Amount": "1",
            "AmountInfo": "(2 rkl)",
            "PackageRecipe": "false",
            "IngredientType": "5997",
            "Unit": "",
            "IngredientTypeName": "Limetti"
          }],
          [{
            "Name": "Pirkka mieto punainen chili hienonnettuna",
            "Ean": "6410405091857",
            "Amount": "1",
            "PackageRecipe": "false",
            "IngredientType": "5355",
            "Unit": "",
            "IngredientTypeName": "Chili, punainen"
          }],
          [{
            "Name": "korianteria hienonnettuna",
            "Amount": "1/2",
            "PackageRecipe": "false",
            "IngredientType": "5852",
            "Unit": "ruukkua",
            "IngredientTypeName": "Korianteri, tuore"
          }]
        ]
      }],
      "Instructions": "# Mittaa riisi siivilään. Huuhdo juoksevan kylmän veden alla, kunnes vesi on kirkasta. Kaada riisi kattilaan ja lisää kylmää vettä niin, että riisi peittyy. Anna seistä 20 minuuttia ja siivilöi vesi pois kattilasta.\r\n# Lisää 3,5 dl vettä ja kuumenna kiehuvaksi. Hauduta riisiä kannen alla miedolla lämmöllä 10 minuuttia, kunnes kaikki vesi on imeytynyt riisiin. Vältä kannen nostamista äläkä sekoita riisiä keittämisen aikana.\r\n# Ota kattila pois liedeltä ja anna vetäytyä 10 minuuttia. Pirskota riisiviinietikka riisin päälle ja kääntele riisi varovasti ilmavaksi.\r\n# Riisin kypsentämisen lomassa keitä soijapapuja 3-4 minuuttia. Valuta pavut.\r\n# Yhdistä kastikkeen ainekset, korianteri varsineen. Halkaise ja viipaloi avokadot.\r\n# Kokoa annokset: jaa riisi kulhoihin, nostele papuja, avokadoa, katkarapuja ja inkivääriä päälle. Laita runsaasti korianteria koristeeksi. Tarjoa kulhot kastikkeen kanssa.",
      "EndNote": "*Annoshinnat on laskettu K-citymarket-ketjun hintatason mukaan 10/2019. Tuoretuotteiden hinnat vaihtelevat sesongeittain.",
      "Description": "Kun kaipaat sushin makuja mutta ilman näpertelyä, kokeile tätä. Sushiriisi, edamamepavut ja katkaravut vain ladotaan kulhoon, päälle vielä korianterikastiketta. Noin 3,50 €/annos*",
      "DateCreated": "2019-07-10T13:10:46",
      "DateModified": "2019-10-02T11:34:13",
      "Stamp": {
        "Name": "Testattu K-koekeittiössä",
        "Id": "1"
      },
      "RecipeUse": {
        "Name": "k-ruoka.fi",
        "Id": "1"
      },
      "resourceType": "recipe",
      "Url": "https://www.k-ruoka.fi/reseptit/japanilainen-katkarapukulho",
      "UrlSlug": "japanilainen-katkarapukulho",
      "Sort": [1, 1, 10533]
    }, {
      "Id": "10524",
      "Name": "Pestolihapullat tomaattikastikkeessa",
      "PieceSize": {
        "Unit": "g",
        "Amount": "410"
      },
      "Portions": {
        "Unit": "annosta",
        "Amount": "4-5"
      },
      "PreparationTime": {
        "Description": "alle 30 min",
        "TimeRange": {
          "MinTime": 15,
          "MaxTime": 30
        }
      },
      "UserPortions": {
        "$": {
          "Unit": "henkilölle"
        }
      },
      "Categories": [{
        "MainName": "Apukategoriat",
        "MainId": "18",
        "SubName": "K-Marketin valikoima",
        "SubId": "181"
      }, {
        "MainName": "Teema",
        "MainId": "15",
        "SubName": "K-Marketin valikoima",
        "SubId": "151"
      }, {
        "MainName": "Apukategoriat",
        "MainId": "18",
        "SubName": "Helppo",
        "SubId": "153"
      }, {
        "MainName": "Apukategoriat",
        "MainId": "18",
        "SubName": "Lyhyt aktiivinen valmistusaika ",
        "SubId": "157"
      }, {
        "MainName": "Apukategoriat",
        "MainId": "18",
        "SubName": "Puolivalmisteita sisältävä",
        "SubId": "158"
      }, {
        "MainName": "Teema",
        "MainId": "15",
        "SubName": "Arkiruoka",
        "SubId": "159"
      }, {
        "MainName": "Teema",
        "MainId": "15",
        "SubName": "Lasten makuun",
        "SubId": "165"
      }, {
        "MainName": "Sesonki",
        "MainId": "1",
        "SubName": "Lasten reseptit",
        "SubId": "77"
      }, {
        "MainName": "Pääraaka-aine",
        "MainId": "14",
        "SubName": "Jauheliha",
        "SubId": "117"
      }, {
        "MainName": "Ruokalaji",
        "MainId": "16",
        "SubName": "Pastat",
        "SubId": "138"
      }, {
        "MainName": "Ruokalaji",
        "MainId": "16",
        "SubName": "Uuniruoat",
        "SubId": "141"
      }, {
        "MainName": "Asiakassegmentointi (K-asiakkaat)",
        "MainId": "17",
        "SubName": "Mukavuudenhaluiset ",
        "SubId": "149"
      }, {
        "MainName": "Asiakassegmentointi (K-asiakkaat)",
        "MainId": "17",
        "SubName": "Vakiintuneet",
        "SubId": "150"
      }, {
        "MainName": "Pääruoat",
        "MainId": "4",
        "SubName": "Jauheliharuoat",
        "SubId": "23"
      }, {
        "MainName": "Pääruoat",
        "MainId": "4",
        "SubName": "Uuniruoat",
        "SubId": "30"
      }, {
        "MainName": "Pääruoat",
        "MainId": "4",
        "SubName": "Pastaruoat",
        "SubId": "31"
      }],
      "Pictures": ["pestolihapullat_6_19"],
      "PictureUrls": [{
        "Id": "pestolihapullat_6_19",
        "Normal": "https://k-file-storage-qa.imgix.net/f/k-ruoka/recipe/10524?w=1000&h=1000&fit=clip",
        "Original": "https://k-file-storage-qa.imgix.net/f/k-ruoka/recipe/10524"
      }],
      "VideoUrl": "https://youtu.be/SIC3zAKA7G8",
      "VideoEmbedUrl": "<iframe width=\"1136\" height=\"639\" src=\"https://www.youtube.com/embed/SIC3zAKA7G8\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>",
      "Ingredients": [{
        "SubSectionHeading": "_",
        "SubSectionIngredients": [
          [{
            "Name": "Pirkka naudan jauhelihaa (17 %)",
            "Ean": "6410405220097",
            "Amount": "1",
            "AmountInfo": "(400 g)",
            "PackageRecipe": "false",
            "IngredientType": "6307",
            "Unit": "rs",
            "IngredientTypeName": "Naudan jauheliha, 17 %"
          }],
          [{
            "Name": "Pirkka punaista pestoa",
            "Ean": "6410402008551",
            "Amount": "1/2",
            "AmountInfo": "(à 185 g)",
            "PackageRecipe": "false",
            "IngredientType": "6538",
            "Unit": "prk",
            "IngredientTypeName": "Pesto, punainen"
          }],
          [{
            "Name": "kananmuna",
            "Amount": "1",
            "PackageRecipe": "false",
            "IngredientType": "5683",
            "Unit": "",
            "IngredientTypeName": "Kananmuna"
          }],
          [{
            "Name": "suolaa",
            "Amount": "1/2",
            "PackageRecipe": "false",
            "IngredientType": "7049",
            "Unit": "tl",
            "IngredientTypeName": "Suola"
          }]
        ]
      }, {
        "SubSectionHeading": "Tomaattikastike",
        "SubSectionIngredients": [
          [{
            "Name": "Pirkka tomaattimurskaa",
            "Amount": "1",
            "AmountInfo": "(500 g)",
            "PackageRecipe": "false",
            "IngredientType": "7135",
            "Unit": "tlk",
            "IngredientTypeName": "Tomaattimurska"
          }],
          [{
            "Name": "Pirkka punaista pestoa",
            "Ean": "6410402008551",
            "Amount": "1/2",
            "AmountInfo": "(à 185 g)",
            "PackageRecipe": "false",
            "IngredientType": "6538",
            "Unit": "prk",
            "IngredientTypeName": "Pesto, punainen"
          }],
          [{
            "Name": "Pirkka kapriksia",
            "Ean": "6410405070654",
            "Amount": "1",
            "AmountInfo": "(35 g/50 g)",
            "PackageRecipe": "false",
            "IngredientType": "5695",
            "Unit": "prk",
            "IngredientTypeName": "Kapris"
          }],
          [{
            "Name": "Pirkka Parhaat parmesaania raastettuna",
            "Ean": "6410405102959",
            "Amount": "50",
            "PackageRecipe": "false",
            "IngredientType": "6468",
            "Unit": "g",
            "IngredientTypeName": "Parmesaani"
          }],
          [{
            "Name": "kuivattua oreganoa tai basilikaa",
            "Amount": "2",
            "PackageRecipe": "false",
            "IngredientType": "6409",
            "Unit": "tl",
            "IngredientTypeName": "Oregano, kuivattu"
          }],
          [{
            "Name": "mustapippuria",
            "Amount": "1/2",
            "PackageRecipe": "false",
            "IngredientType": "6167",
            "Unit": "tl",
            "IngredientTypeName": "Maustemylly, mustapippuri"
          }]
        ]
      }, {
        "SubSectionHeading": "Lisäksi",
        "SubSectionIngredients": [
          [{
            "Name": "Pirkka spagettia",
            "Ean": "6410405046840",
            "Amount": "350",
            "PackageRecipe": "false",
            "IngredientType": "6989",
            "Unit": "g",
            "IngredientTypeName": "Spagetti"
          }],
          [{
            "Name": "Pirkka Parhaat parmesaania",
            "Ean": "6410405102959",
            "Amount": "50",
            "PackageRecipe": "false",
            "IngredientType": "6468",
            "Unit": "g",
            "IngredientTypeName": "Parmesaani"
          }]
        ]
      }],
      "Instructions": "# Sekoita tomaattikastikkeen ainekset uunivuoassa, kaprikset liemineen.\r\n# Sekoita lihapullien ainekset tasaiseksi massaksi. Muotoile massa pulliksi tomaattikastikkeen päälle.\r\n# Kypsennä lihapullia 225-asteisessa uunissa noin 20 minuuttia, kunnes pullat ovat kypsiä.\r\n# Tarjoa keitetyn spagetin ja raastetun parmesaanin kanssa.",
      "EndNote": "*Annoshinnat on laskettu K-citymarket-ketjun hintatason mukaan 10/2019. Tuoretuotteiden hinnat vaihtelevat sesongeittain.",
      "Description": "Maailman helpoimman lihapullakastikkeen lykkäät uuniin  10 minuutissa. Tiskiäkään ei tule paria kippoa enempää. Noin 3,20 €/annos*",
      "DateCreated": "2019-07-04T13:33:16",
      "DateModified": "2019-11-03T20:01:32",
      "TvDate": "2019-11-04T00:00:00",
      "Stamp": {
        "Name": "Testattu K-koekeittiössä",
        "Id": "1"
      },
      "RecipeUse": {
        "Name": "k-ruoka.fi",
        "Id": "1"
      },
      "resourceType": "recipe",
      "Url": "https://www.k-ruoka.fi/reseptit/pestolihapullat-tomaattikastikkeessa",
      "UrlSlug": "pestolihapullat-tomaattikastikkeessa",
      "Sort": [1, 1, 10524]
    }, {
      "Id": "10522",
      "Name": "Peltirosolli",
      "PieceSize": {
        "Unit": "g",
        "Amount": "325"
      },
      "Portions": {
        "Unit": "annosta",
        "Amount": "4-6"
      },
      "PreparationTime": {
        "Description": "30 - 60 min",
        "TimeRange": {
          "MinTime": 30,
          "MaxTime": 60
        }
      },
      "UserPortions": {
        "$": {
          "Unit": "henkilölle"
        }
      },
      "Categories": [{
        "MainName": "Pääruoat",
        "MainId": "4",
        "SubName": "Kasvisruoat",
        "SubId": "22"
      }, {
        "MainName": "Lisäkkeet",
        "MainId": "6",
        "SubName": "Kasvislisäkkeet",
        "SubId": "42"
      }, {
        "MainName": "Sesonki",
        "MainId": "1",
        "SubName": "Joulun ruoat",
        "SubId": "75"
      }, {
        "MainName": "Makuja maailmalta",
        "MainId": "2",
        "SubName": "Suomi",
        "SubId": "106"
      }, {
        "MainName": "Pääraaka-aine",
        "MainId": "14",
        "SubName": "Kasvis",
        "SubId": "115"
      }, {
        "MainName": "Apukategoriat",
        "MainId": "18",
        "SubName": "Helppo",
        "SubId": "153"
      }, {
        "MainName": "Apukategoriat",
        "MainId": "18",
        "SubName": "Trendikäs ",
        "SubId": "156"
      }, {
        "MainName": "Teema",
        "MainId": "15",
        "SubName": "Arkiruoka",
        "SubId": "159"
      }, {
        "MainName": "Teema",
        "MainId": "15",
        "SubName": "Viikonloppu",
        "SubId": "160"
      }, {
        "MainName": "Sesonki",
        "MainId": "1",
        "SubName": "Joulu",
        "SubId": "130"
      }, {
        "MainName": "Ruokalaji",
        "MainId": "16",
        "SubName": "Uuniruoat",
        "SubId": "141"
      }, {
        "MainName": "Asiakassegmentointi (K-asiakkaat)",
        "MainId": "17",
        "SubName": "Innostujat",
        "SubId": "147"
      }, {
        "MainName": "Asiakassegmentointi (K-asiakkaat)",
        "MainId": "17",
        "SubName": "Tiedostavat",
        "SubId": "148"
      }, {
        "MainName": "Asiakassegmentointi (K-asiakkaat)",
        "MainId": "17",
        "SubName": "Mukavuudenhaluiset ",
        "SubId": "149"
      }, {
        "MainName": "Asiakassegmentointi (K-asiakkaat)",
        "MainId": "17",
        "SubName": "Vakiintuneet",
        "SubId": "150"
      }],
      "Pictures": ["peltirosolli_KR6_19"],
      "PictureUrls": [{
        "Id": "peltirosolli_KR6_19",
        "Normal": "https://k-file-storage-qa.imgix.net/f/k-ruoka/recipe/10522?w=1000&h=1000&fit=clip",
        "Original": "https://k-file-storage-qa.imgix.net/f/k-ruoka/recipe/10522"
      }],
      "SpecialDiets": [{
        "Id": "4",
        "Name": "Gluteeniton"
      }, {
        "Id": "6",
        "Name": "Kasvis (lakto-ovo)"
      }, {
        "Id": "7",
        "Name": "Kananmunaton"
      }, {
        "Id": "9",
        "Name": "Viljaton"
      }],
      "Ingredients": [{
        "SubSectionHeading": "_",
        "SubSectionIngredients": [
          [{
            "Name": "porkkanaa",
            "Amount": "3",
            "AmountInfo": "(300 g)",
            "PackageRecipe": "false",
            "IngredientType": "6582",
            "Unit": "",
            "IngredientTypeName": "Porkkana"
          }],
          [{
            "Name": "Pirkka yleisperunoita (keltainen pussi)",
            "Amount": "3",
            "AmountInfo": "(400 g)",
            "PackageRecipe": "false",
            "IngredientType": "7368",
            "Unit": "",
            "IngredientTypeName": "Yleisperuna"
          }],
          [{
            "Name": "Pirkka punajuurta",
            "Ean": "6410405097248",
            "Amount": "3",
            "AmountInfo": "(400 g)",
            "PackageRecipe": "false",
            "IngredientType": "6655",
            "Unit": "",
            "IngredientTypeName": "Punajuuri"
          }],
          [{
            "Name": "punasipuli",
            "Amount": "1",
            "AmountInfo": "(120 g)",
            "PackageRecipe": "false",
            "IngredientType": "6667",
            "Unit": "",
            "IngredientTypeName": "Punasipuli"
          }]
        ]
      }, {
        "SubSectionHeading": "Vinegretti",
        "SubSectionIngredients": [
          [{
            "Name": "rypsiöljyä",
            "Amount": "1",
            "PackageRecipe": "false",
            "IngredientType": "6807",
            "Unit": "dl",
            "IngredientTypeName": "Rypsiöljy"
          }],
          [{
            "Name": "omenaviinietikkaa",
            "Amount": "3",
            "PackageRecipe": "false",
            "IngredientType": "6408",
            "Unit": "rkl",
            "IngredientTypeName": "Omenaviinietikka"
          }],
          [{
            "Name": "tummaa siirappia",
            "Amount": "3",
            "PackageRecipe": "false",
            "IngredientType": "6898",
            "Unit": "rkl",
            "IngredientTypeName": "Siirappi, tumma"
          }],
          [{
            "Name": "suolaa",
            "Amount": "1",
            "PackageRecipe": "false",
            "IngredientType": "7049",
            "Unit": "tl",
            "IngredientTypeName": "Suola"
          }],
          [{
            "Name": "mustapippuria",
            "Amount": "1/2",
            "PackageRecipe": "false",
            "IngredientType": "6167",
            "Unit": "tl",
            "IngredientTypeName": "Maustemylly, mustapippuri"
          }]
        ]
      }, {
        "SubSectionHeading": "Halloumimuru",
        "SubSectionIngredients": [
          [{
            "Name": "Pirkka Parhaat halloumia",
            "Ean": "6410405196828",
            "Amount": "1",
            "AmountInfo": "(250 g)",
            "PackageRecipe": "false",
            "IngredientType": "5490",
            "Unit": "pkt",
            "IngredientTypeName": "Halloumi"
          }],
          [{
            "Name": "Pirkka Luomu kuorellisia manteleita",
            "Ean": "6410405115416",
            "Amount": "1",
            "AmountInfo": "(100 g)",
            "PackageRecipe": "false",
            "IngredientType": "6117",
            "Unit": "ps",
            "IngredientTypeName": "Manteli, luomu"
          }]
        ]
      }, {
        "SubSectionHeading": "Pinkki kermavaahto",
        "SubSectionIngredients": [
          [{
            "Name": "Pirkka kuohukermaa",
            "Ean": "6410405142597",
            "Amount": "1",
            "AmountInfo": "(2 dl)",
            "PackageRecipe": "false",
            "IngredientType": "5891",
            "Unit": "tlk",
            "IngredientTypeName": "Kuohukerma"
          }],
          [{
            "Name": "punajuurilohkoa",
            "Amount": "2",
            "PackageRecipe": "false",
            "IngredientType": "6655",
            "Unit": "",
            "IngredientTypeName": "Punajuuri"
          }],
          [{
            "Name": "omenaviinietikkaa",
            "Amount": "1 1/2",
            "PackageRecipe": "false",
            "IngredientType": "6408",
            "Unit": "tl",
            "IngredientTypeName": "Omenaviinietikka"
          }],
          [{
            "Name": "sokeria",
            "Amount": "2",
            "PackageRecipe": "false",
            "IngredientType": "6961",
            "Unit": "tl",
            "IngredientTypeName": "Sokeri"
          }]
        ]
      }],
      "Instructions": "# Kuori ja lohko porkkanat, perunat, punajuuret ja punasipuli. Laita kasvikset uunipellille leivinpaperin päälle osioittain, punajuuret reunimmaiseksi. Säästä pari punajuurilohkoa kermavaahdon värjäämistä varten.\r\n# Yhdistä vinegrettin ainekset keskenään ja kääntele puolet seoksesta kasvisten joukkoon. Paista kasviksia 200-asteisessa uunissa ensin 30 minuuttia.\r\n# Raasta halloumi karkeaksi. Rouhi mantelit veitsellä ja sekoita halloumin joukkoon. Ripottele seos kasvisten päälle. Jatka kypsentämistä noin 15 minuuttia, kunnes punajuuretkin ovat kypsiä. Valuta loppu vinegretti peltirosollin päälle.\r\n# Rosollin kypsyessä viipaloi jäljelle jääneet punajuuret ohuen ohuiksi viipaleiksi kulhoon. Lisää kerma ja heiluttele kulhoa niin, että kerma värjääntyy pinkiksi. Kalasta punajuuren palat haarukalla pois. Vaahdota kerma pehmeäksi vaahdoksi. Mausta etikalla ja sokerilla.\r\n# Tarjoa peltirosolli kuumana pinkin kermavaahdon kanssa.",
      "EndNote": "*Annoshinnat on laskettu K-citymarket-ketjun hintatason mukaan 10/2019. Tuoretuotteiden hinnat vaihtelevat sesongeittain.",
      "Description": "Peltirosolli on loistoruoka pikkujouluihin ja joulukuun tunnelmointiin. Pellillä tarjottava kimpparuoka sopii myös aaton rentoon buffapöytään. Noin 2,55 €/annos*",
      "DateCreated": "2019-07-04T12:52:53",
      "DateModified": "2019-10-07T09:38:14",
      "TvDate": "2019-11-20T00:00:00",
      "Stamp": {
        "Name": "Testattu K-koekeittiössä",
        "Id": "1"
      },
      "RecipeUse": {
        "Name": "k-ruoka.fi",
        "Id": "1"
      },
      "resourceType": "recipe",
      "Url": "https://www.k-ruoka.fi/reseptit/peltirosolli",
      "UrlSlug": "peltirosolli",
      "Sort": [1, 1, 10522]
    }, {
      "Id": "10520",
      "Name": "Omenainen kanapata",
      "PieceSize": {
        "Unit": "g"
      },
      "Portions": {
        "Unit": "annosta",
        "Amount": "5"
      },
      "PreparationTime": {
        "Description": "yli 60 min",
        "TimeRange": {
          "MinTime": 60
        }
      },
      "UserPortions": {
        "$": {
          "Unit": "henkilölle"
        }
      },
      "Categories": [{
        "MainName": "Apukategoriat",
        "MainId": "18",
        "SubName": "Helppo",
        "SubId": "153"
      }, {
        "MainName": "Sesongin raaka-aineet",
        "MainId": "19",
        "SubName": "Omena",
        "SubId": "212"
      }, {
        "MainName": "Pääraaka-aine",
        "MainId": "14",
        "SubName": "Broileri ja linnut",
        "SubId": "118"
      }, {
        "MainName": "Sesonki",
        "MainId": "1",
        "SubName": "Sadonkorjuu",
        "SubId": "124"
      }, {
        "MainName": "Ruokalaji",
        "MainId": "16",
        "SubName": "Pataruoat",
        "SubId": "142"
      }, {
        "MainName": "Teema",
        "MainId": "15",
        "SubName": "Mitä tänään syötäisiin?",
        "SubId": "144"
      }, {
        "MainName": "Asiakassegmentointi (K-asiakkaat)",
        "MainId": "17",
        "SubName": "Innostujat",
        "SubId": "147"
      }, {
        "MainName": "Asiakassegmentointi (K-asiakkaat)",
        "MainId": "17",
        "SubName": "Mukavuudenhaluiset ",
        "SubId": "149"
      }, {
        "MainName": "Pääruoat",
        "MainId": "4",
        "SubName": "Broileri- ja linturuoat",
        "SubId": "21"
      }, {
        "MainName": "Pääruoat",
        "MainId": "4",
        "SubName": "Pataruoat",
        "SubId": "29"
      }],
      "Pictures": ["omenainenkanapata-2019"],
      "PictureUrls": [{
        "Id": "omenainenkanapata-2019",
        "Normal": "https://k-file-storage-qa.imgix.net/f/k-ruoka/recipe/10520?w=1000&h=1000&fit=clip",
        "Original": "https://k-file-storage-qa.imgix.net/f/k-ruoka/recipe/10520"
      }],
      "VideoUrl": "https://youtu.be/8EmwZUGnu2w",
      "VideoEmbedUrl": "<iframe width=\"1136\" height=\"639\" src=\"https://www.youtube.com/embed/8EmwZUGnu2w\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>",
      "SpecialDiets": [{
        "Id": "2",
        "Name": "Laktoositon"
      }, {
        "Id": "3",
        "Name": "Maidoton"
      }, {
        "Id": "4",
        "Name": "Gluteeniton"
      }, {
        "Id": "7",
        "Name": "Kananmunaton"
      }, {
        "Id": "9",
        "Name": "Viljaton"
      }],
      "Ingredients": [{
        "SubSectionHeading": "_",
        "SubSectionIngredients": [
          [{
            "Name": "Kariniemen kananpojan fileepihvejä, omena-pippuri",
            "Amount": "1",
            "AmountInfo": "(1 kg)",
            "PackageRecipe": "false",
            "IngredientType": "5285",
            "Unit": "pkt",
            "IngredientTypeName": "Broilerin fileepihvi, marinoitu"
          }],
          [{
            "Name": "Pirkka rypsiöljyä",
            "Amount": "1",
            "PackageRecipe": "false",
            "IngredientType": "6807",
            "Unit": "rkl",
            "IngredientTypeName": "Rypsiöljy"
          }],
          [{
            "Name": "HK Amerikan pekonia",
            "Amount": "1",
            "AmountInfo": "(170 g)",
            "PackageRecipe": "false",
            "IngredientType": "6498",
            "Unit": "pkt",
            "IngredientTypeName": "Pekoni"
          }],
          [{
            "Name": "Pirkka salottisipulia",
            "Ean": "6410405057655",
            "Amount": "4",
            "AmountInfo": "(n.175 g)",
            "PackageRecipe": "false",
            "IngredientType": "6851",
            "Unit": "",
            "IngredientTypeName": "Salottisipuli"
          }],
          [{
            "Name": "valkosipulinkynttä",
            "Amount": "3",
            "PackageRecipe": "false",
            "IngredientType": "15809",
            "Unit": "",
            "IngredientTypeName": "Valkosipuli, tuore"
          }],
          [{
            "Name": "Pirkka Parhaat Cidre Brut Rose -omenasiideriä",
            "Ean": "6410405232588",
            "Amount": "1",
            "AmountInfo": "(0,33 l)",
            "PackageRecipe": "false",
            "IngredientType": "6888",
            "Unit": "plo",
            "IngredientTypeName": "Siideri, omena, kuiva"
          }],
          [{
            "Name": "vettä",
            "Amount": "2",
            "PackageRecipe": "false",
            "IngredientType": "15792",
            "Unit": "dl",
            "IngredientTypeName": "Vesi"
          }],
          [{
            "Name": "Knorr Fond \"du Chef\" kana-annosfondi",
            "Amount": "1",
            "PackageRecipe": "false",
            "IngredientType": "5225",
            "Unit": "",
            "IngredientTypeName": "Annosfondi, kana"
          }],
          [{
            "Name": "laakerinlehteä",
            "Amount": "2",
            "PackageRecipe": "false",
            "IngredientType": "5918",
            "Unit": "",
            "IngredientTypeName": "Laakerinlehti"
          }],
          [{
            "Name": "tuoretta timjamia hienonnettuna",
            "Amount": "2",
            "PackageRecipe": "false",
            "IngredientType": "7119",
            "Unit": "rkl",
            "IngredientTypeName": "Timjami, tuore"
          }],
          [{
            "Name": "hapahkoa omenaa (kotimainen tai Jonagold)",
            "Amount": "3",
            "AmountInfo": "(600 g)",
            "PackageRecipe": "false",
            "IngredientType": "12050",
            "Unit": "",
            "IngredientTypeName": "Omena, vihreä, Granny Smith"
          }],
          [{
            "Name": "rouhaisu mustapippuria myllystä",
            "PackageRecipe": "false",
            "IngredientType": "11644",
            "IngredientTypeName": "Mustapippuri, jauhettu"
          }]
        ]
      }],
      "Instructions": "# Kuori ja lohko salottisipulit neljään osaan. Viipaloi valkoipulinkynnet. Suikaloi pekoni.\r\n# Nosta broilerin fieepihvit pois marinadista.\r\n# Kuumenna paistokasari tai laakea pata ja ruskista broilerin fileepihvit öljysssä muutamassa erässä. Siirrä fileepihvit odottamaan lautaselle. Ruskista seuraavaksi pekonit. Lisää sipulit, ja pienennä lämpöä.\r\n# Siirrä ruskistetut fileepihvit paistokasariin tai pataan pekonien ja sipulien joukkoon.\r\n# Lisää pataan siideri, vesi ja annosfondi, laakerinlehdet sekä ruokalusikallinen timjamilla.\r\n# Kuumenna pata kiehuvaksi ja jätä pata porisemaan miedolle lämmölle noin 15 minuutiksi kannenn alle. Pese ja kuutioi sillä välin omenat reiluiksi kuutioiksi.\r\n# Lisää omenat kanapataan ja keitä vielä toiset 15 minuuttia välillä sekoitellen. Mausta lopuksi mustapippurilla sekä koristele timjamilla.",
      "EndNote": "",
      "Description": "Omenainen kanapata on herkullinen viikonloppuruoka, joka maistuu erityisen hyvältä viilenevinä iltoina.",
      "DateCreated": "2019-07-03T11:40:17",
      "DateModified": "2019-10-17T12:34:00",
      "TvDate": "2019-10-15T00:00:00",
      "Stamp": {
        "Name": "Testattu K-koekeittiössä",
        "Id": "1"
      },
      "RecipeUse": {
        "Name": "k-ruoka.fi",
        "Id": "1"
      },
      "resourceType": "recipe",
      "Url": "https://www.k-ruoka.fi/reseptit/omenainen-kanapata",
      "UrlSlug": "omenainen-kanapata",
      "Sort": [1, 1, 10520]
    }, {
      "Id": "10517",
      "Name": "Poropizza",
      "PieceSize": {
        "Unit": "g",
        "Amount": "342"
      },
      "Portions": {
        "Unit": "annosta",
        "Amount": "4"
      },
      "PreparationTime": {
        "Description": "30 - 60 min",
        "TimeRange": {
          "MinTime": 30,
          "MaxTime": 60
        }
      },
      "UserPortions": {
        "$": {
          "Unit": "henkilölle"
        }
      },
      "Categories": [{
        "MainName": "Asiakassegmentointi (K-asiakkaat)",
        "MainId": "17",
        "SubName": "Nautiskelijat",
        "SubId": "146"
      }, {
        "MainName": "Asiakassegmentointi (K-asiakkaat)",
        "MainId": "17",
        "SubName": "Innostujat",
        "SubId": "147"
      }, {
        "MainName": "Asiakassegmentointi (K-asiakkaat)",
        "MainId": "17",
        "SubName": "Tiedostavat",
        "SubId": "148"
      }, {
        "MainName": "Suolaiset leivonnaiset",
        "MainId": "8",
        "SubName": "Pizzat",
        "SubId": "56"
      }, {
        "MainName": "Pääruoat",
        "MainId": "4",
        "SubName": "Pizzat",
        "SubId": "87"
      }, {
        "MainName": "Makuja maailmalta",
        "MainId": "2",
        "SubName": "Suomi",
        "SubId": "106"
      }, {
        "MainName": "Pääraaka-aine",
        "MainId": "14",
        "SubName": "Liha",
        "SubId": "114"
      }, {
        "MainName": "Ruokalaji",
        "MainId": "16",
        "SubName": "Pizzat",
        "SubId": "139"
      }],
      "Pictures": ["poropizza_PI12_19"],
      "PictureUrls": [{
        "Id": "poropizza_PI12_19",
        "Normal": "https://k-file-storage-qa.imgix.net/f/k-ruoka/recipe/10517?w=1000&h=1000&fit=clip",
        "Original": "https://k-file-storage-qa.imgix.net/f/k-ruoka/recipe/10517"
      }],
      "Ingredients": [{
        "SubSectionHeading": "Täyte",
        "SubSectionIngredients": [
          [{
            "Name": "Pirkka poronkäristystä (pakaste)",
            "Ean": "6410405129208",
            "Amount": "1",
            "AmountInfo": "(240 g)",
            "PackageRecipe": "false",
            "IngredientType": "6592",
            "Unit": "pkt",
            "IngredientTypeName": "Poronkäristys, pakaste"
          }],
          [{
            "Name": "punasipuli",
            "Amount": "1",
            "AmountInfo": "(60 g)",
            "PackageRecipe": "false",
            "IngredientType": "6667",
            "Unit": "",
            "IngredientTypeName": "Punasipuli"
          }],
          [{
            "Name": "voita",
            "Amount": "2",
            "PackageRecipe": "false",
            "IngredientType": "15824",
            "Unit": "rkl",
            "IngredientTypeName": "Pirkka voi 400g"
          }],
          [{
            "Name": "suolaa",
            "Amount": "1/2",
            "PackageRecipe": "false",
            "IngredientType": "7049",
            "Unit": "tl",
            "IngredientTypeName": "Suola"
          }],
          [{
            "Name": "mustapippuria",
            "Amount": "1/2",
            "PackageRecipe": "false",
            "IngredientType": "6269",
            "Unit": "tl",
            "IngredientTypeName": "Mustapippuri, kokonainen"
          }],
          [{
            "Name": "vettä",
            "Amount": "3",
            "PackageRecipe": "false",
            "IngredientType": "15792",
            "Unit": "dl",
            "IngredientTypeName": "Vesi"
          }],
          [{
            "Name": "Arla Posketon pizzajuustoraastetta",
            "Amount": "1/2",
            "AmountInfo": "(à 500 g)",
            "PackageRecipe": "false",
            "IngredientType": "9163",
            "Unit": "ps",
            "IngredientTypeName": "Juustoraaste, pizza"
          }]
        ]
      }, {
        "SubSectionHeading": "Pohja",
        "SubSectionIngredients": [
          [{
            "Name": "Pirkka perunasoseaineksia (pakaste)",
            "Ean": "6410405056948",
            "Amount": "1",
            "AmountInfo": "(500 g)",
            "PackageRecipe": "false",
            "IngredientType": "6532",
            "Unit": "ps",
            "IngredientTypeName": "Perunasoseaines, pakaste"
          }],
          [{
            "Name": "maitoa",
            "Amount": "2",
            "PackageRecipe": "false",
            "IngredientType": "9812",
            "Unit": "dl",
            "IngredientTypeName": "Kevytmaito"
          }],
          [{
            "Name": "voita",
            "Amount": "3",
            "PackageRecipe": "false",
            "IngredientType": "15824",
            "Unit": "rkl",
            "IngredientTypeName": "Pirkka voi 400g"
          }],
          [{
            "Name": "Pirkka ruisjauhoja",
            "Ean": "6410400025253",
            "Amount": "1",
            "PackageRecipe": "false",
            "IngredientType": "5459",
            "Unit": "dl",
            "IngredientTypeName": "Grahamjauho, luomu"
          }],
          [{
            "Name": "Pirkka vehnäjauhoja",
            "Ean": "6410405193414",
            "Amount": "1",
            "PackageRecipe": "false",
            "IngredientType": "7293",
            "Unit": "dl",
            "IngredientTypeName": "Vehnäjauho"
          }],
          [{
            "Name": "leivinjauhetta",
            "Amount": "1",
            "PackageRecipe": "false",
            "IngredientType": "5967",
            "Unit": "tl",
            "IngredientTypeName": "Leivinjauhe"
          }],
          [{
            "Name": "suolaa",
            "PackageRecipe": "false",
            "IngredientType": "7049",
            "Unit": "ripaus",
            "IngredientTypeName": "Suola"
          }]
        ]
      }, {
        "SubSectionHeading": "Lisäksi",
        "SubSectionIngredients": [
          [{
            "Name": "tuoretta timjamia",
            "Amount": "2",
            "PackageRecipe": "false",
            "IngredientType": "7119",
            "Unit": "rkl",
            "IngredientTypeName": "Timjami, tuore"
          }],
          [{
            "Name": "puolukkahilloa tai -survosta",
            "Amount": "1",
            "PackageRecipe": "false",
            "IngredientType": "6672",
            "Unit": "dl",
            "IngredientTypeName": "Puolukkahillo"
          }]
        ]
      }],
      "Instructions": "# Kaada poronkäristys pannulle ja kuumenna. Hienonna kuorittu sipuli. Lisää se voin kanssa sulan käristyksen joukkoon pannulle. Mausta suolalla ja pippurilla. Lisää pannulle vesi ja anna käristyksen hautua kannen alla miedolla lämmöllä noin 30 minuuttia.\r\n# Sulata perunasoseainekset mikrossa maidon kanssa pakkauksen ohjeen mukaan. Lisää pehmeä voi. Sekoita jauhoihin leivinjauhe ja suola, lisää kuivat aineet muusiin ja vaivaa taikina tasaiseksi. Taputtele taikina jauhojen avulla kahdeksi soikeaksi pohjaksi leivinpaperin päälle uunipellille. Paista 200-asteisessa uunissa 15 minuuttia.\r\n# Levitä pizzapohjalle juustoraaste ja sen päälle käristys. Paista 200-asteisessa uunissa 15 minuuttia.\r\n# Koristele pizza timjamilla ja tarjoa puolukkahillon tai -survoksen kanssa.",
      "EndNote": "Vinkki! Voit myös keittää ja soseuttaa pohjaa varten 8 jauhoista perunaa (noin 500 g). *Annoshinnat on laskettu K-citymarket-ketjun hintatason mukaan 11/2019. Tuoretuotteiden hinnat vaihtelevat sesongeittain.",
      "Description": "Poropizza on kuin poronkäristys minikoossa! Perunapohja ja käristystäyte maistuvat mainiolta puolukkahillon kanssa. Noin 3,75€/annos*",
      "DateCreated": "2019-06-28T17:28:33",
      "DateModified": "2019-11-06T16:03:54",
      "Stamp": {
        "Name": "Testattu K-koekeittiössä",
        "Id": "1"
      },
      "RecipeUse": {
        "Name": "k-ruoka.fi",
        "Id": "1"
      },
      "resourceType": "recipe",
      "Url": "https://www.k-ruoka.fi/reseptit/poropizza",
      "UrlSlug": "poropizza",
      "Sort": [1, 1, 10517]
    }, {
      "Id": "10516",
      "Name": "Shahi paneer",
      "PieceSize": {
        "Unit": "g"
      },
      "Portions": {
        "Unit": "annosta",
        "Amount": "4"
      },
      "PreparationTime": {
        "Description": "alle 30 min",
        "TimeRange": {
          "MinTime": 15,
          "MaxTime": 30
        }
      },
      "UserPortions": {
        "$": {
          "Unit": "henkilölle"
        }
      },
      "Categories": [{
        "MainName": "Pääruoat",
        "MainId": "4",
        "SubName": "Kastikkeet",
        "SubId": "26"
      }, {
        "MainName": "Teema",
        "MainId": "15",
        "SubName": "Mitä tänään syötäisiin?",
        "SubId": "144"
      }, {
        "MainName": "Asiakassegmentointi (K-asiakkaat)",
        "MainId": "17",
        "SubName": "Nautiskelijat",
        "SubId": "146"
      }, {
        "MainName": "Asiakassegmentointi (K-asiakkaat)",
        "MainId": "17",
        "SubName": "Innostujat",
        "SubId": "147"
      }, {
        "MainName": "Asiakassegmentointi (K-asiakkaat)",
        "MainId": "17",
        "SubName": "Tiedostavat",
        "SubId": "148"
      }, {
        "MainName": "Teema",
        "MainId": "15",
        "SubName": "Arkiruoka",
        "SubId": "159"
      }, {
        "MainName": "Makuja maailmalta",
        "MainId": "2",
        "SubName": "Intia",
        "SubId": "9"
      }, {
        "MainName": "Pääruoat",
        "MainId": "4",
        "SubName": "Kasvisruoat",
        "SubId": "22"
      }],
      "Pictures": ["shahi_paneer_PI11_19"],
      "PictureUrls": [{
        "Id": "shahi_paneer_PI11_19",
        "Normal": "https://k-file-storage-qa.imgix.net/f/k-ruoka/recipe/10516?w=1000&h=1000&fit=clip",
        "Original": "https://k-file-storage-qa.imgix.net/f/k-ruoka/recipe/10516"
      }],
      "VideoUrl": "https://youtu.be/qwgfp_y4OH8",
      "VideoEmbedUrl": "<iframe width=\"1136\" height=\"639\" src=\"https://www.youtube.com/embed/qwgfp_y4OH8\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>",
      "Ingredients": [{
        "SubSectionHeading": "_",
        "SubSectionIngredients": [
          [{
            "Name": "Apetina Paneer -juustoa",
            "Ean": "5711953062599",
            "Amount": "1",
            "AmountInfo": "(225 g)",
            "PackageRecipe": "false",
            "IngredientType": "10087",
            "Unit": "pkt",
            "IngredientTypeName": "Kotijuusto, laktoositon"
          }],
          [{
            "Name": "sipulia",
            "Amount": "2",
            "AmountInfo": "(150 g)",
            "PackageRecipe": "false",
            "IngredientType": "6932",
            "Unit": "",
            "IngredientTypeName": "Sipuli"
          }],
          [{
            "Name": "valkosipulinkynttä",
            "Amount": "2",
            "PackageRecipe": "false",
            "IngredientType": "15809",
            "Unit": "",
            "IngredientTypeName": "Valkosipuli, tuore"
          }],
          [{
            "Name": "tuoretta inkivääriä",
            "Amount": "1",
            "PackageRecipe": "false",
            "IngredientType": "5567",
            "Unit": "rkl",
            "IngredientTypeName": "Inkivääri, tuore"
          }],
          [{
            "Name": "Pirkka mieto punainen chili",
            "Ean": "6410405091857",
            "Amount": "1",
            "PackageRecipe": "false",
            "IngredientType": "5355",
            "Unit": "",
            "IngredientTypeName": "Chili, punainen"
          }],
          [{
            "Name": "Pirkka ekstra-neitsytoliiviöljyä",
            "Ean": "6410402024759",
            "Amount": "2",
            "PackageRecipe": "false",
            "IngredientType": "5405",
            "Unit": "rkl",
            "IngredientTypeName": "Ekstra neitsytoliiviöljy"
          }],
          [{
            "Name": "kurkumaa",
            "Amount": "1",
            "PackageRecipe": "false",
            "IngredientType": "10200",
            "Unit": "tl",
            "IngredientTypeName": "Kurkuma, jauhettu"
          }],
          [{
            "Name": "chilimausteseosta",
            "Amount": "1/2",
            "PackageRecipe": "false",
            "IngredientType": "6176",
            "Unit": "tl",
            "IngredientTypeName": "Mausteseos, chili"
          }],
          [{
            "Name": "Pirkka tomaattimurskaa",
            "Ean": "6410402018161",
            "Amount": "1",
            "AmountInfo": "(500 g)",
            "PackageRecipe": "false",
            "IngredientType": "7135",
            "Unit": "tlk",
            "IngredientTypeName": "Tomaattimurska"
          }],
          [{
            "Name": "vettä",
            "Amount": "2",
            "PackageRecipe": "false",
            "IngredientType": "15792",
            "Unit": "dl",
            "IngredientTypeName": "Vesi"
          }],
          [{
            "Name": "Pirkka cashewpähkinöitä",
            "Amount": "1",
            "AmountInfo": "(125 g)",
            "PackageRecipe": "false",
            "IngredientType": "5346",
            "Unit": "ps",
            "IngredientTypeName": "Cashewpähkinä"
          }],
          [{
            "Name": "suolaa",
            "PackageRecipe": "false",
            "IngredientType": "6221",
            "Unit": "ripaus",
            "IngredientTypeName": "Merisuola"
          }],
          [{
            "Name": "sokeria",
            "PackageRecipe": "false",
            "IngredientType": "6961",
            "Unit": "ripaus",
            "IngredientTypeName": "Sokeri"
          }]
        ]
      }, {
        "SubSectionHeading": "Lisäksi",
        "SubSectionIngredients": [
          [{
            "Name": "tuoretta korianteria",
            "Amount": "1",
            "PackageRecipe": "false",
            "IngredientType": "5852",
            "Unit": "ruukku",
            "IngredientTypeName": "Korianteri, tuore"
          }],
          [{
            "Name": "Pirkka turkkilaista jogurttia",
            "Ean": "6410405169617",
            "Amount": "1",
            "AmountInfo": "(200 g)",
            "PackageRecipe": "false",
            "IngredientType": "7201",
            "Unit": "prk",
            "IngredientTypeName": "Turkkilainen jogurtti"
          }],
          [{
            "Name": "keitettyä riisiä",
            "PackageRecipe": "false",
            "IngredientType": "5272",
            "IngredientTypeName": "Basmatiriisi"
          }]
        ]
      }],
      "Instructions": "# Leikkaa juusto vajaan sentin viipaleiksi. Ruskista palat pannulla ja siirrä sivuun odottamaan\r\n# Hienonna kuoritut sipulit, valkosipulinkynnet, inkivääri ja chili. Kuumenna öljy kattilassa, lisää kasvikset, kurkuma sekä chilimauste ja kuullota. Kaada kattilaan tomaattimurska. Huuhtele tomaattimurskapurkki vedellä ja kaada neste kattilaan. Lisää cashewpähkinät ja anna hautua 15 minuuttia. Soseuta kastike ja mausta suolalla ja sokerilla.\r\n# Lisää kastikkeeseen juustopalat. Tarjoa tuoreen korianterin, jogurtin ja keitetyn riisin kanssa.",
      "EndNote": "*Annoshinnat on laskettu K-citymarket-ketjun hintatason mukaan 10/2019. Tuoretuotteiden hinnat vaihtelevat sesongeittain.",
      "Description": "Shahi paneer tarkoittaa juustoa tomaatti-pähkinäkastikkeessa. Kokkaa intialaisesta ravintolasta tuttua klassikkoa nyt myös kotona!\r\nNoin 2,55 €/annos*",
      "DateCreated": "2019-06-28T17:02:02",
      "DateModified": "2019-11-03T20:04:08",
      "TvDate": "2019-11-06T00:00:00",
      "Stamp": {
        "Name": "Testattu K-koekeittiössä",
        "Id": "1"
      },
      "RecipeUse": {
        "Name": "k-ruoka.fi",
        "Id": "1"
      },
      "resourceType": "recipe",
      "Url": "https://www.k-ruoka.fi/reseptit/shahi-paneer",
      "UrlSlug": "shahi-paneer",
      "Sort": [1, 1, 10516]
    }, {
      "Id": "10515",
      "Name": "Chimichurrilla täytetty naudanfilee",
      "PieceSize": {
        "Unit": "g",
        "Amount": "100"
      },
      "Portions": {
        "Unit": "annosta",
        "Amount": "10"
      },
      "PreparationTime": {
        "Description": "30 - 60 min",
        "TimeRange": {
          "MinTime": 30,
          "MaxTime": 60
        }
      },
      "UserPortions": {
        "$": {
          "Unit": "henkilölle"
        }
      },
      "Categories": [{
        "MainName": "Sesonki",
        "MainId": "1",
        "SubName": "Isänpäivä",
        "SubId": "127"
      }, {
        "MainName": "Teema",
        "MainId": "15",
        "SubName": "Mitä tänään syötäisiin?",
        "SubId": "144"
      }, {
        "MainName": "Asiakassegmentointi (K-asiakkaat)",
        "MainId": "17",
        "SubName": "Innostujat",
        "SubId": "147"
      }, {
        "MainName": "Asiakassegmentointi (K-asiakkaat)",
        "MainId": "17",
        "SubName": "Vakiintuneet",
        "SubId": "150"
      }, {
        "MainName": "Pääruoat",
        "MainId": "4",
        "SubName": "Liharuoat",
        "SubId": "24"
      }, {
        "MainName": "Makuja maailmalta",
        "MainId": "2",
        "SubName": "Etelä- ja Väli-Amerikka",
        "SubId": "102"
      }, {
        "MainName": "Pääraaka-aine",
        "MainId": "14",
        "SubName": "Liha",
        "SubId": "114"
      }],
      "Pictures": ["chimich_naudanf_PI11_19"],
      "PictureUrls": [{
        "Id": "chimich_naudanf_PI11_19",
        "Normal": "https://k-file-storage-qa.imgix.net/f/k-ruoka/recipe/10515?w=1000&h=1000&fit=clip",
        "Original": "https://k-file-storage-qa.imgix.net/f/k-ruoka/recipe/10515"
      }],
      "Ingredients": [{
        "SubSectionHeading": "_",
        "SubSectionIngredients": [
          [{
            "Name": "naudan sisäfileetä",
            "Amount": "1",
            "PackageRecipe": "false",
            "IngredientType": "6326",
            "Unit": "kg",
            "IngredientTypeName": "Naudan sisäfilee"
          }],
          [{
            "Name": "suolaa",
            "Amount": "1",
            "PackageRecipe": "false",
            "IngredientType": "7049",
            "Unit": "tl",
            "IngredientTypeName": "Suola"
          }],
          [{
            "Name": "mustapippuria",
            "Amount": "1/2",
            "PackageRecipe": "false",
            "IngredientType": "6269",
            "Unit": "tl",
            "IngredientTypeName": "Mustapippuri, kokonainen"
          }],
          [{
            "Name": "oliiviöljyä",
            "Amount": "2",
            "PackageRecipe": "false",
            "IngredientType": "6386",
            "Unit": "rkl",
            "IngredientTypeName": "Oliiviöljy"
          }],
          [{
            "Name": "voita",
            "Amount": "2",
            "PackageRecipe": "false",
            "IngredientType": "15824",
            "Unit": "rkl",
            "IngredientTypeName": "Pirkka voi 400g"
          }]
        ]
      }, {
        "SubSectionHeading": "Chimichurri",
        "SubSectionIngredients": [
          [{
            "Name": "tuoretta lehtipersiljaa",
            "Ean": "6410402026630",
            "Amount": "1",
            "PackageRecipe": "false",
            "IngredientType": "6517",
            "Unit": "ruukku",
            "IngredientTypeName": "Persilja, tuore"
          }],
          [{
            "Name": "tuoretta korianteria",
            "Amount": "1",
            "PackageRecipe": "false",
            "IngredientType": "5852",
            "Unit": "ruukku",
            "IngredientTypeName": "Korianteri, tuore"
          }],
          [{
            "Name": "Pirkka ekstra-neitsytoliiviöljyä",
            "Ean": "6410402024759",
            "Amount": "1/2",
            "PackageRecipe": "false",
            "IngredientType": "5405",
            "Unit": "dl",
            "IngredientTypeName": "Ekstra neitsytoliiviöljy"
          }],
          [{
            "Name": "punaviinietikkaa",
            "Amount": "3",
            "PackageRecipe": "false",
            "IngredientType": "6668",
            "Unit": "rkl",
            "IngredientTypeName": "Punaviinietikka"
          }],
          [{
            "Name": "Pirkka valkosipulinkynttä",
            "Ean": "6410405057570",
            "Amount": "2",
            "PackageRecipe": "false",
            "IngredientType": "7262",
            "Unit": "",
            "IngredientTypeName": "Valkosipuli"
          }],
          [{
            "Name": "Pirkka mieto punainen chili",
            "Ean": "6410405091857",
            "Amount": "1",
            "PackageRecipe": "false",
            "IngredientType": "5355",
            "Unit": "",
            "IngredientTypeName": "Chili, punainen"
          }],
          [{
            "Name": "sokeria",
            "Amount": "1",
            "PackageRecipe": "false",
            "IngredientType": "6961",
            "Unit": "tl",
            "IngredientTypeName": "Sokeri"
          }],
          [{
            "Name": "suolaa",
            "Amount": "1",
            "PackageRecipe": "false",
            "IngredientType": "7049",
            "Unit": "tl",
            "IngredientTypeName": "Suola"
          }]
        ]
      }, {
        "SubSectionHeading": "Lisäksi",
        "SubSectionIngredients": [
          [{
            "Name": "Pirkka crème fraîchea (ranskankermaa, 18 %)",
            "Ean": "6410405164629",
            "Amount": "1",
            "AmountInfo": "(à 150 g)",
            "PackageRecipe": "false",
            "IngredientType": "7954",
            "Unit": "prk",
            "IngredientTypeName": "Creme fraiche, 18 %, laktoositon"
          }]
        ]
      }],
      "Instructions": "# Ota naudanfilee huoneenlämpöön noin 30 minuuttia ennen paistamista.\r\n# Mittaa kaikki chimichurrin ainekset tehosekoittimeen. Sekoita kunnes seos on melko tasaista. Tarkista maku. Ota pari lusikallista chimichurria talteen erilliseen kulhoon kastiketta varten.\r\n# Leikkaa fileen keskelle terävällä veitsellä syvä tasku ensin sivusuunnassa ja sitten pystysuunnassa. Varo halkaisemasta fileetä ja jätä fileen päät leikkaamatta, jotta täyte pysyy sisällä. Mausta liha suolalla ja pippurilla. Lusikoi chimichurri viiltoon.\r\n# Sido filee kiinni paistinarulla.\r\n# Paista fileeseen kaunis väri pannussa öljy-voiseoksessa. Mausta fileen ulkopinta suolalla ja pippurilla ja siirrä filee uunipannuun. Työnnä paistolämpömittari fileen paksuimpaan kohtaan.\r\n# Kypsennä fileetä 175-asteisessa uunissa, medium-kypsyiseksi 55-58 asteeseen (noin 20 minuuttia) tai kypsemmäksi 64 asteeseen.\r\n# Kääri liha folioon ja anna levätä noin 15 minuuttia ennen leikkaamista.\r\n# Sekoita chimichurri ranskankermaan. Tarjoa täytetty sisäfilee ranskankermakastikkeen ja lohkoperunoiden kanssa.",
      "EndNote": "*Annoshinnat on laskettu K-citymarket-ketjun hintatason mukaan 10/2019. Tuoretuotteiden hinnat vaihtelevat sesongeittain.",
      "Description": "Chimichurrilla täytetty naudanfilee on näyttävä, mutta yllättävän helppo juhlaruoka. Tällä ruoalla hurmaat perheen ja vieraat! Noin 4,20 €/annos*",
      "DateCreated": "2019-06-28T16:50:51",
      "DateModified": "2019-10-11T14:10:40",
      "TvDate": "2019-11-01T00:00:00",
      "Stamp": {
        "Name": "Testattu K-koekeittiössä",
        "Id": "1"
      },
      "RecipeUse": {
        "Name": "k-ruoka.fi",
        "Id": "1"
      },
      "resourceType": "recipe",
      "Url": "https://www.k-ruoka.fi/reseptit/chimichurrilla-taytetty-naudanfilee",
      "UrlSlug": "chimichurrilla-taytetty-naudanfilee",
      "Sort": [1, 1, 10515]
    }, {
      "Id": "10507",
      "Name": "Porkkanalaatikkopihvit",
      "PieceSize": {
        "Unit": "g",
        "Amount": "80"
      },
      "Portions": {
        "Unit": "kpl",
        "Amount": "10"
      },
      "PreparationTime": {
        "Description": "alle 30 min",
        "TimeRange": {
          "MinTime": 15,
          "MaxTime": 30
        }
      },
      "UserPortions": {
        "$": {
          "Unit": "henkilölle"
        }
      },
      "Categories": [{
        "MainName": "Pääruoat",
        "MainId": "4",
        "SubName": "Kasvisruoat",
        "SubId": "22"
      }, {
        "MainName": "Asiakassegmentointi (K-asiakkaat)",
        "MainId": "17",
        "SubName": "Mukavuudenhaluiset ",
        "SubId": "149"
      }, {
        "MainName": "Asiakassegmentointi (K-asiakkaat)",
        "MainId": "17",
        "SubName": "Vakiintuneet",
        "SubId": "150"
      }, {
        "MainName": "Apukategoriat",
        "MainId": "18",
        "SubName": "Helppo",
        "SubId": "153"
      }, {
        "MainName": "Apukategoriat",
        "MainId": "18",
        "SubName": "Puolivalmisteita sisältävä",
        "SubId": "158"
      }, {
        "MainName": "Sesonki",
        "MainId": "1",
        "SubName": "Joulun ruoat",
        "SubId": "75"
      }, {
        "MainName": "Makuja maailmalta",
        "MainId": "2",
        "SubName": "Suomi",
        "SubId": "106"
      }, {
        "MainName": "Pääraaka-aine",
        "MainId": "14",
        "SubName": "Kasvis",
        "SubId": "115"
      }, {
        "MainName": "Sesonki",
        "MainId": "1",
        "SubName": "Joulu",
        "SubId": "130"
      }, {
        "MainName": "Ruokalaji",
        "MainId": "16",
        "SubName": "Pihvit",
        "SubId": "143"
      }, {
        "MainName": "Asiakassegmentointi (K-asiakkaat)",
        "MainId": "17",
        "SubName": "Tiedostavat",
        "SubId": "148"
      }],
      "Pictures": ["porkkanalaat_pihv_6_19"],
      "PictureUrls": [{
        "Id": "porkkanalaat_pihv_6_19",
        "Normal": "https://k-file-storage-qa.imgix.net/f/k-ruoka/recipe/10507?w=1000&h=1000&fit=clip",
        "Original": "https://k-file-storage-qa.imgix.net/f/k-ruoka/recipe/10507"
      }],
      "Ingredients": [{
        "SubSectionHeading": "_",
        "SubSectionIngredients": [
          [{
            "Name": "Saarioinen porkkanalaatikkoa",
            "Amount": "1",
            "AmountInfo": "(400 g)",
            "PackageRecipe": "false",
            "IngredientType": "12848",
            "Unit": "rs",
            "IngredientTypeName": "Porkkanalaatikko"
          }],
          [{
            "Name": "Pirkka salaattijuustokuutioita öljymarinadissa",
            "Ean": "6410405224095",
            "Amount": "1",
            "AmountInfo": "(340 g/200 g)",
            "PackageRecipe": "false",
            "IngredientType": "6824",
            "Unit": "prk",
            "IngredientTypeName": "Salaattijuustokuutio öljymarinadissa"
          }],
          [{
            "Name": "lehtipersiljaa",
            "Amount": "1",
            "PackageRecipe": "false",
            "IngredientType": "5954",
            "Unit": "dl",
            "IngredientTypeName": "Lehtipersilja"
          }],
          [{
            "Name": "ruohosipulia",
            "Amount": "1",
            "PackageRecipe": "false",
            "IngredientType": "6772",
            "Unit": "dl",
            "IngredientTypeName": "Ruohosipuli, tuore"
          }],
          [{
            "Name": "valkosipulinkynttä",
            "Amount": "2",
            "PackageRecipe": "false",
            "IngredientType": "15809",
            "Unit": "",
            "IngredientTypeName": "Valkosipuli, tuore"
          }],
          [{
            "Name": "kananmunaa",
            "Amount": "3",
            "PackageRecipe": "false",
            "IngredientType": "5683",
            "Unit": "",
            "IngredientTypeName": "Kananmuna"
          }],
          [{
            "Name": "korppujauhoja",
            "Amount": "1",
            "PackageRecipe": "false",
            "IngredientType": "5872",
            "Unit": "dl",
            "IngredientTypeName": "Korppujauho"
          }],
          [{
            "Name": "mustapippuria",
            "PackageRecipe": "false",
            "IngredientType": "6269",
            "Unit": "ripaus",
            "IngredientTypeName": "Mustapippuri, kokonainen"
          }]
        ]
      }, {
        "SubSectionHeading": "Paistamiseen",
        "SubSectionIngredients": [
          [{
            "Name": "rypsiöljyä",
            "Amount": "3",
            "PackageRecipe": "false",
            "IngredientType": "6807",
            "Unit": "rkl",
            "IngredientTypeName": "Rypsiöljy"
          }]
        ]
      }, {
        "SubSectionHeading": "Lisäksi",
        "SubSectionIngredients": [
          [{
            "Name": "Pirkka perunasoseaineksia (pakaste)",
            "Ean": "6410405056948",
            "Amount": "1",
            "AmountInfo": "(500 g)",
            "PackageRecipe": "false",
            "IngredientType": "6532",
            "Unit": "ps",
            "IngredientTypeName": "Perunasoseaines, pakaste"
          }],
          [{
            "Name": "maitoa",
            "Amount": "2",
            "PackageRecipe": "false",
            "IngredientType": "9812",
            "Unit": "dl",
            "IngredientTypeName": "Kevytmaito"
          }],
          [{
            "Name": "Pirkka crème fraîchea (ranskankermaa, 18 %)",
            "Ean": "6410405164629",
            "Amount": "1",
            "AmountInfo": "(150 g)",
            "PackageRecipe": "false",
            "IngredientType": "7954",
            "Unit": "prk",
            "IngredientTypeName": "Creme fraiche, 18 %, laktoositon"
          }],
          [{
            "Name": "Pirkka salviapestoa",
            "Ean": "6408641198249",
            "Amount": "2",
            "PackageRecipe": "false",
            "IngredientType": "16444",
            "Unit": "rkl",
            "IngredientTypeName": "Pesto, salvia"
          }]
        ]
      }],
      "Instructions": "# Sekoita porkkanalaatikon rakenne rikki.\r\n# Nosta salaattijuustokuutiot öljymarinadista kulhoon. Riko niiden rakenne haarukalla. Hienonna yrtit ja kuoritut valkosipulinkynnet.\r\n# Lisää juustoseokseen porkkana­laatikko, yrtit, valkosipulit, kananmunat, korppujauhot sekä pippuri ja sekoita tasaiseksi.\r\n# Paista taikina pihveiksi paistinpannulla öljyssä.\r\n# Valmista perunasose pakkauksen ohjeen mukaan. Sekoita ranskankerma ja salviapesto keskenään kastikkeeksi. Tarjoa porkkanalaatikkopihvit perunasoseen ja salviakastikkeen kanssa.",
      "EndNote": "Voit säästää salaattijuuston öljyn ja käyttää sen salaatinkastikkeena. *Annoshinnat on laskettu K-citymarket-ketjun hintatason mukaan 10/2019. Tuoretuotteiden hinnat vaihtelevat sesongeittain.",
      "Description": "Porkkanalaatikkopihvit pelastavat kun joululaatikot jo kyllästyttävät. Salaattijuusto ja yrtit antavat pihveille mukavasti makua. Noin 1,00 €/annos*",
      "DateCreated": "2019-06-28T13:04:05",
      "DateModified": "2019-10-07T10:08:31",
      "TvDate": "2019-12-16T00:00:00",
      "Stamp": {
        "Name": "Testattu K-koekeittiössä",
        "Id": "1"
      },
      "RecipeUse": {
        "Name": "k-ruoka.fi",
        "Id": "1"
      },
      "resourceType": "recipe",
      "Url": "https://www.k-ruoka.fi/reseptit/porkkanalaatikkopihvit",
      "UrlSlug": "porkkanalaatikkopihvit",
      "Sort": [1, 1, 10507]
    }, {
      "Id": "10498",
      "Name": "Kidneypapukastike",
      "PieceSize": {
        "Unit": "g",
        "Amount": "330"
      },
      "Portions": {
        "Unit": "annosta",
        "Amount": "4"
      },
      "PreparationTime": {
        "Description": "alle 15 min",
        "TimeRange": {
          "MinTime": 5,
          "MaxTime": 15
        }
      },
      "UserPortions": {
        "$": {
          "Unit": "henkilölle"
        }
      },
      "Categories": [{
        "MainName": "Pääruoat",
        "MainId": "4",
        "SubName": "Kasvisruoat",
        "SubId": "22"
      }, {
        "MainName": "Pääruoat",
        "MainId": "4",
        "SubName": "Kastikkeet",
        "SubId": "26"
      }, {
        "MainName": "Lisäkkeet",
        "MainId": "6",
        "SubName": "Kasvislisäkkeet",
        "SubId": "42"
      }, {
        "MainName": "Pääraaka-aine",
        "MainId": "14",
        "SubName": "Kasvis",
        "SubId": "115"
      }, {
        "MainName": "Teema",
        "MainId": "15",
        "SubName": "Mitä tänään syötäisiin?",
        "SubId": "144"
      }],
      "Pictures": ["krp13_01_kidneypapu"],
      "PictureUrls": [{
        "Id": "krp13_01_kidneypapu",
        "Normal": "https://k-file-storage-qa.imgix.net/f/k-ruoka/recipe/10498?w=1000&h=1000&fit=clip",
        "Original": "https://k-file-storage-qa.imgix.net/f/k-ruoka/recipe/10498"
      }],
      "VideoUrl": "https://www.youtube.com/watch?v=VyUd5saVTak",
      "VideoEmbedUrl": "<iframe width=\"560\" height=\"315\" src=\"http://www.youtube.com/embed/VyUd5saVTak\" frameborder=\"0\" allowfullscreen></iframe>",
      "SpecialDiets": [{
        "Id": "6",
        "Name": "Kasvis (lakto-ovo)"
      }],
      "EnergyAmounts": {
        "KcalPerPortion": "192.13",
        "KcalPerUnit": "58.22",
        "KJPerPortion": "803.86",
        "KJPerUnit": "243.59",
        "FatPerPortion": "6.10",
        "FatPerUnit": "1.85",
        "ProteinPerPortion": "10.56",
        "ProteinPerUnit": "3.20",
        "CarbohydratePerPortion": "23.79",
        "CarbohydratePerUnit": "7.21"
      },
      "Ingredients": [{
        "SubSectionHeading": "_",
        "SubSectionIngredients": [
          [{
            "Name": "sipuli",
            "Amount": "1",
            "AmountInfo": "(150 g)",
            "PackageRecipe": "false",
            "IngredientType": "6932",
            "Unit": "",
            "IngredientTypeName": "Sipuli"
          }],
          [{
            "Name": "paprikaa (punainen ja keltainen)",
            "Amount": "2",
            "AmountInfo": "(300 g)",
            "PackageRecipe": "false",
            "IngredientType": "12291",
            "Unit": "",
            "IngredientTypeName": "Paprika, värilajitelma"
          }],
          [{
            "Name": "öljyä",
            "Amount": "1",
            "PackageRecipe": "false",
            "IngredientType": "6807",
            "Unit": "rkl",
            "IngredientTypeName": "Rypsiöljy"
          }],
          [{
            "Name": "Pirkka kidneypapuja",
            "Ean": "6410405088420",
            "Amount": "1",
            "AmountInfo": "(380 g)",
            "PackageRecipe": "false",
            "IngredientType": "9825",
            "Unit": "tlk",
            "IngredientTypeName": "Kidneypapu, säilyke"
          }],
          [{
            "Name": "Pirkka tomaattimurskaa",
            "Ean": "6410402011865",
            "Amount": "1",
            "AmountInfo": "(500 g)",
            "PackageRecipe": "false",
            "IngredientType": "7136",
            "Unit": "tlk",
            "IngredientTypeName": "Tomaattimurska, chili"
          }],
          [{
            "Name": "Pirkka salsadippiä (keskivahva tai vahva)",
            "Ean": "6410405048646",
            "Amount": "n. 1",
            "PackageRecipe": "false",
            "IngredientType": "6854",
            "Unit": "dl",
            "IngredientTypeName": "Salsadippi, keskivahva"
          }],
          [{
            "Name": "suolaa",
            "Amount": "n. 1/2",
            "PackageRecipe": "false",
            "IngredientType": "7049",
            "Unit": "tl",
            "IngredientTypeName": "Suola"
          }],
          [{
            "Name": "Pirkka salaattijuustokuutioita",
            "Ean": "6410405167507",
            "Amount": "1/2-1",
            "AmountInfo": "(à 385 g)",
            "PackageRecipe": "false",
            "IngredientType": "6826",
            "Unit": "prk",
            "IngredientTypeName": "Salaattijuustokuutio, laktoositon"
          }]
        ]
      }, {
        "SubSectionHeading": "Lisäksi",
        "SubSectionIngredients": [
          [{
            "Name": "Pirkka tortilloja",
            "PackageRecipe": "false",
            "IngredientType": "7159",
            "IngredientTypeName": "Tortilla, vehnä"
          }],
          [{
            "Name": "Pirkka tummaa pikariisiä",
            "Ean": "6410405230638",
            "PackageRecipe": "false"
          }]
        ]
      }],
      "Instructions": "# Leikkaa sipuli renkaiksi ja paprikat suikaleiksi. Kuullota sipuleita ja paprikoita rasvassa noin 5 minuuttia. Lisää joukkoon valutetut ja huuhdotut pavut sekä tomaattimurska.\r\n# Mausta salsadipillä ja suolalla. Anna hautua kannen alla 5-10 minuuttia. Lisää valutetut salaattijuustokuutiot. Tarjoa tortillojen tai riisin kanssa.",
      "EndNote": "Vinkki! Papupata sopii myös paistetun broilerin tai lihan kanssa.\r\n*Annoshinnat on laskettu K-citymarket-ketjun hintatason mukaan 12/2012. Tuoretuotteiden hinnat vaihtelevat sesongeittain.",
      "Description": "Kidneypapupata on helppo ja nopea valmistaa. Se maistuu sellaisenaan kasvisruokana tortillojen täytteenä tai riisin kanssa. Kokeile myös paistetun broilerin tai possun lisäkkeenä.\r\n\r\nTämäkin resepti vain n. 1,55€/annos*.",
      "DateCreated": "2019-06-27T15:45:48",
      "DateModified": "2019-06-27T16:09:26",
      "Stamp": {
        "Name": "Testattu K-koekeittiössä",
        "Id": "1"
      },
      "RecipeUse": {
        "Name": "k-ruoka.fi",
        "Id": "1"
      },
      "resourceType": "recipe",
      "Url": "https://www.k-ruoka.fi/reseptit/kidneypapukastike",
      "UrlSlug": "kidneypapukastike",
      "Sort": [1, 1, 10498]
    }, {
      "Id": "10494",
      "Name": "Uunisinihomejuustopasta",
      "PieceSize": {
        "Unit": "g",
        "Amount": "300"
      },
      "Portions": {
        "Unit": "annosta",
        "Amount": "4"
      },
      "PreparationTime": {
        "Description": "alle 30 min",
        "TimeRange": {
          "MinTime": 15,
          "MaxTime": 30
        }
      },
      "UserPortions": {
        "$": {
          "Unit": "henkilölle"
        }
      },
      "Categories": [{
        "MainName": "Pääruoat",
        "MainId": "4",
        "SubName": "Pastaruoat",
        "SubId": "31"
      }, {
        "MainName": "Makuja maailmalta",
        "MainId": "2",
        "SubName": "Suomi",
        "SubId": "106"
      }, {
        "MainName": "Pääraaka-aine",
        "MainId": "14",
        "SubName": "Kasvis",
        "SubId": "115"
      }, {
        "MainName": "Ruokalaji",
        "MainId": "16",
        "SubName": "Pastat",
        "SubId": "138"
      }, {
        "MainName": "Asiakassegmentointi (K-asiakkaat)",
        "MainId": "17",
        "SubName": "Nautiskelijat",
        "SubId": "146"
      }, {
        "MainName": "Asiakassegmentointi (K-asiakkaat)",
        "MainId": "17",
        "SubName": "Innostujat",
        "SubId": "147"
      }, {
        "MainName": "Teema",
        "MainId": "15",
        "SubName": "Halpa",
        "SubId": "161"
      }, {
        "MainName": "Sesongin raaka-aineet",
        "MainId": "19",
        "SubName": "Tomaatti",
        "SubId": "179"
      }, {
        "MainName": "Apukategoriat",
        "MainId": "18",
        "SubName": "K-Marketin valikoima",
        "SubId": "181"
      }, {
        "MainName": "Asiakassegmentointi (K-asiakkaat)",
        "MainId": "17",
        "SubName": "Mukavuudenhaluiset ",
        "SubId": "149"
      }, {
        "MainName": "Teema",
        "MainId": "15",
        "SubName": "K-Marketin valikoima",
        "SubId": "151"
      }, {
        "MainName": "Apukategoriat",
        "MainId": "18",
        "SubName": "Helppo",
        "SubId": "153"
      }, {
        "MainName": "Apukategoriat",
        "MainId": "18",
        "SubName": "Trendikäs ",
        "SubId": "156"
      }, {
        "MainName": "Apukategoriat",
        "MainId": "18",
        "SubName": "Lyhyt aktiivinen valmistusaika ",
        "SubId": "157"
      }, {
        "MainName": "Teema",
        "MainId": "15",
        "SubName": "Arkiruoka",
        "SubId": "159"
      }],
      "Pictures": ["uunisinihomepasta-2019"],
      "PictureUrls": [{
        "Id": "uunisinihomepasta-2019",
        "Normal": "https://k-file-storage-qa.imgix.net/f/k-ruoka/recipe/10494?w=1000&h=1000&fit=clip",
        "Original": "https://k-file-storage-qa.imgix.net/f/k-ruoka/recipe/10494"
      }],
      "SpecialDiets": [{
        "Id": "6",
        "Name": "Kasvis (lakto-ovo)"
      }, {
        "Id": "7",
        "Name": "Kananmunaton"
      }],
      "Ingredients": [{
        "SubSectionHeading": "_",
        "SubSectionIngredients": [
          [{
            "Name": "Pirkka Parhaat Red Desire terttuminiluumutomaatteja",
            "Ean": "6410405060457",
            "Amount": "2",
            "AmountInfo": "(à 250 g)",
            "PackageRecipe": "false",
            "IngredientType": "6225",
            "Unit": "rs",
            "IngredientTypeName": "Miniluumutomaatti"
          }],
          [{
            "Name": "Pirkka ekstra-neitsytoliiviöljyä",
            "Ean": "6410402024759",
            "Amount": "1/2",
            "PackageRecipe": "false",
            "IngredientType": "5406",
            "Unit": "dl",
            "IngredientTypeName": "Ekstra neitsytoliiviöljy, Italia"
          }],
          [{
            "Name": "mustapippuria",
            "Amount": "1",
            "PackageRecipe": "false",
            "IngredientType": "6167",
            "Unit": "tl",
            "IngredientTypeName": "Maustemylly, mustapippuri"
          }],
          [{
            "Name": "Pirkka sinihomejuustoa",
            "Ean": "6410400046814",
            "Amount": "1",
            "AmountInfo": "(125 g)",
            "PackageRecipe": "false",
            "IngredientType": "6924",
            "Unit": "pkt",
            "IngredientTypeName": "Sinihomejuusto"
          }],
          [{
            "Name": "hunajaa",
            "Amount": "1-2",
            "PackageRecipe": "false",
            "IngredientType": "5550",
            "Unit": "tl",
            "IngredientTypeName": "Hunaja, juokseva"
          }],
          [{
            "Name": "Pirkka tagliatelle-pastaa",
            "Ean": "6410405189493",
            "Amount": "350",
            "PackageRecipe": "false",
            "IngredientType": "7080",
            "Unit": "g",
            "IngredientTypeName": "Tagliatelle"
          }]
        ]
      }, {
        "SubSectionHeading": "Lisäksi",
        "SubSectionIngredients": [
          [{
            "Name": "tuoretta basilikaa suikaloituna",
            "Ean": "6410405051806",
            "Amount": "1",
            "PackageRecipe": "false",
            "IngredientType": "5271",
            "Unit": "dl",
            "IngredientTypeName": "Basilika, tuore"
          }]
        ]
      }],
      "Instructions": "# Huuhtele tomaatit ja poista ne rangasta. Valuta öljy pieneen uunivuokaan ja lisää mustapippuri. Pyöräytä tomaatit öljyseoksessa ja nosta juusto tomaattien päälle. Valuta pinnalle hunaja.\r\n# Paista 200-asteisessa uunissa 15 minuuttia ja jatka paistamista grillivastusten alla vielä 3-5 minuuttia. Varo etteivät tomaatit pala.\r\n# Keitä sillä välin pasta pakkauksen ohjeen mukaan suolalla maustetussa vedessä.\r\n# Voit halutessasi painella tomaatit varovasti rikki lusikalla. Kumoa seos kattilaan valutetun pastan joukkoon ja nostele ainekset sekaisin. Viimeistele pasta suikaloidulla basilikalla",
      "EndNote": "",
      "Description": "Liemessä-blogin uunifetapastasta inspiraationsa saanut sinihomejuustopasta ihastuttaa helppoudellaan.",
      "DateCreated": "2019-06-18T15:43:55",
      "DateModified": "2019-08-12T12:50:20",
      "Stamp": {
        "Name": "Testattu K-koekeittiössä",
        "Id": "1"
      },
      "RecipeUse": {
        "Name": "k-ruoka.fi",
        "Id": "1"
      },
      "resourceType": "recipe",
      "Url": "https://www.k-ruoka.fi/reseptit/uunisinihomejuustopasta",
      "UrlSlug": "uunisinihomejuustopasta",
      "Sort": [1, 1, 10494]
    }, {
      "Id": "10492",
      "Name": "Maksakastike",
      "PieceSize": {
        "Unit": "g",
        "Amount": "300"
      },
      "Portions": {
        "Unit": "annosta",
        "Amount": "4"
      },
      "PreparationTime": {
        "Description": "alle 30 min",
        "TimeRange": {
          "MinTime": 15,
          "MaxTime": 30
        }
      },
      "UserPortions": {
        "$": {
          "Unit": "henkilölle"
        }
      },
      "Categories": [{
        "MainName": "Asiakassegmentointi (K-asiakkaat)",
        "MainId": "17",
        "SubName": "Mukavuudenhaluiset ",
        "SubId": "149"
      }, {
        "MainName": "Asiakassegmentointi (K-asiakkaat)",
        "MainId": "17",
        "SubName": "Vakiintuneet",
        "SubId": "150"
      }, {
        "MainName": "Apukategoriat",
        "MainId": "18",
        "SubName": "Perinteinen ",
        "SubId": "155"
      }, {
        "MainName": "Pääruoat",
        "MainId": "4",
        "SubName": "Kastikkeet",
        "SubId": "26"
      }, {
        "MainName": "Makuja maailmalta",
        "MainId": "2",
        "SubName": "Suomi",
        "SubId": "106"
      }, {
        "MainName": "Pääraaka-aine",
        "MainId": "14",
        "SubName": "Liha",
        "SubId": "114"
      }],
      "Pictures": ["maksakastike-2019"],
      "PictureUrls": [{
        "Id": "maksakastike-2019",
        "Normal": "https://k-file-storage-qa.imgix.net/f/k-ruoka/recipe/10492?w=1000&h=1000&fit=clip",
        "Original": "https://k-file-storage-qa.imgix.net/f/k-ruoka/recipe/10492"
      }],
      "Ingredients": [{
        "SubSectionHeading": "_",
        "SubSectionIngredients": [
          [{
            "Name": "naudanmaksaa",
            "Amount": "400",
            "PackageRecipe": "false",
            "IngredientType": "6313",
            "Unit": "g",
            "IngredientTypeName": "Naudan maksa"
          }],
          [{
            "Name": "Pirkka sipulia",
            "Ean": "6410402008933",
            "Amount": "4",
            "AmountInfo": "(450 g)",
            "PackageRecipe": "false",
            "IngredientType": "6932",
            "Unit": "",
            "IngredientTypeName": "Sipuli"
          }],
          [{
            "Name": "tuoretta inkivääriä",
            "Amount": "2",
            "PackageRecipe": "false",
            "IngredientType": "5567",
            "Unit": "tl",
            "IngredientTypeName": "Inkivääri, tuore"
          }],
          [{
            "Name": "voita",
            "Amount": "2",
            "PackageRecipe": "false",
            "IngredientType": "15824",
            "Unit": "rkl",
            "IngredientTypeName": "Pirkka voi 400g"
          }],
          [{
            "Name": "suolaa",
            "Amount": "1/2",
            "PackageRecipe": "false",
            "IngredientType": "7049",
            "Unit": "tl",
            "IngredientTypeName": "Suola"
          }],
          [{
            "Name": "mustapippuria",
            "Amount": "1/2",
            "PackageRecipe": "false",
            "IngredientType": "6269",
            "Unit": "tl",
            "IngredientTypeName": "Mustapippuri, kokonainen"
          }],
          [{
            "Name": "vehnäjauhoja",
            "Amount": "1",
            "PackageRecipe": "false",
            "IngredientType": "7293",
            "Unit": "rkl",
            "IngredientTypeName": "Vehnäjauho"
          }],
          [{
            "Name": "vettä",
            "Amount": "2",
            "PackageRecipe": "false",
            "IngredientType": "15792",
            "Unit": "dl",
            "IngredientTypeName": "Vesi"
          }],
          [{
            "Name": "lihaliemikuutio",
            "Amount": "1",
            "PackageRecipe": "false",
            "IngredientType": "5992",
            "Unit": "",
            "IngredientTypeName": "Liemikuutio, liha"
          }],
          [{
            "Name": "Pirkka laktoositonta ruokakermaa",
            "Ean": "6410405093448",
            "Amount": "2",
            "PackageRecipe": "false",
            "IngredientType": "6775",
            "Unit": "dl",
            "IngredientTypeName": "Ruokakerma, 15 %, laktoositon"
          }]
        ]
      }, {
        "SubSectionHeading": "Tarjoiluun",
        "SubSectionIngredients": [
          [{
            "Name": "perunamuusia",
            "Ean": "6430104926502",
            "Amount": "4",
            "PackageRecipe": "false",
            "IngredientType": "6532",
            "Unit": "dl",
            "IngredientTypeName": "Perunasoseaines, pakaste"
          }],
          [{
            "Name": "Pirkka puolukkahilloa",
            "Ean": "6410405054814",
            "Amount": "2",
            "PackageRecipe": "false",
            "IngredientType": "6672",
            "Unit": "dl",
            "IngredientTypeName": "Puolukkahillo"
          }]
        ]
      }],
      "Instructions": "# Poista maksasta kalvot ja taputtele pinta kuivaksi talouspaperilla. Kuutioi maksa. Kuori ja silppua sipulit. Kuori ja raasta inkivääri.\r\n# Kuullota sipulit ja inkivääri pannulla ruokalusikallisessa voita. Kumoa ne sivuun odottamaan ja ruskista maksakuutiot lopussa voissa. Kaada sipulit takaisin pannulle ja mausta seos suolalla ja pippurilla.\r\n# Ripottele joukkoon vehnäjauhot ja kääntele. Kaada pannulle myös vesi, murenneltu lihaliemikuutio ja kerma. Anna maksakastikkeen hautua vielä 5 minuuttia.\r\n# Tarjoa maksakastike perunamuusin ja puolukkahillon kanssa.",
      "EndNote": "",
      "Description": "Maksakastike on helppo ja herkullinen kotiruoka, joka saa makua ripauksesta inkivääriä. Kerma miedontaa maksan makua juuri sopivasti.",
      "DateCreated": "2019-06-13T10:03:02",
      "DateModified": "2019-06-18T13:16:38",
      "Stamp": {
        "Name": "Testattu K-koekeittiössä",
        "Id": "1"
      },
      "RecipeUse": {
        "Name": "k-ruoka.fi",
        "Id": "1"
      },
      "resourceType": "recipe",
      "Url": "https://www.k-ruoka.fi/reseptit/maksakastike",
      "UrlSlug": "maksakastike",
      "Sort": [1, 1, 10492]
    }, {
      "Id": "10491",
      "Name": "Cheese melt sandwich eli lämpimät kinkku-juustoleivät",
      "PieceSize": {
        "Unit": "g"
      },
      "Portions": {
        "Unit": "annosta",
        "Amount": "2"
      },
      "PreparationTime": {
        "Description": "alle 15 min",
        "TimeRange": {
          "MinTime": 5,
          "MaxTime": 15
        }
      },
      "UserPortions": {
        "$": {
          "Unit": "henkilölle"
        }
      },
      "Categories": [{
        "MainName": "Asiakassegmentointi (K-asiakkaat)",
        "MainId": "17",
        "SubName": "Nautiskelijat",
        "SubId": "146"
      }, {
        "MainName": "Asiakassegmentointi (K-asiakkaat)",
        "MainId": "17",
        "SubName": "Mukavuudenhaluiset ",
        "SubId": "149"
      }, {
        "MainName": "Asiakassegmentointi (K-asiakkaat)",
        "MainId": "17",
        "SubName": "Vakiintuneet",
        "SubId": "150"
      }, {
        "MainName": "Aamu-,väli- ja iltapalat",
        "MainId": "11",
        "SubName": "Täytetyt leivät",
        "SubId": "66"
      }, {
        "MainName": "Pääruoat",
        "MainId": "4",
        "SubName": "Hampurilaiset ja täytetyt leivät",
        "SubId": "86"
      }, {
        "MainName": "Pääraaka-aine",
        "MainId": "14",
        "SubName": "Liha",
        "SubId": "114"
      }, {
        "MainName": "Sesonki",
        "MainId": "1",
        "SubName": "Oktoberfest",
        "SubId": "125"
      }, {
        "MainName": "Ruokalaji",
        "MainId": "16",
        "SubName": "Hampurilaiset ja täytetyt leivät",
        "SubId": "140"
      }, {
        "MainName": "Teema",
        "MainId": "15",
        "SubName": "Mitä tänään syötäisiin?",
        "SubId": "144"
      }, {
        "MainName": "Sesonki",
        "MainId": "1",
        "SubName": "Grillaus",
        "SubId": "5"
      }, {
        "MainName": "Makuja maailmalta",
        "MainId": "2",
        "SubName": "Pohjois-Amerikka",
        "SubId": "8"
      }],
      "Pictures": ["P9_19_cheesemelt"],
      "PictureUrls": [{
        "Id": "P9_19_cheesemelt",
        "Normal": "https://k-file-storage-qa.imgix.net/f/k-ruoka/recipe/10491?w=1000&h=1000&fit=clip",
        "Original": "https://k-file-storage-qa.imgix.net/f/k-ruoka/recipe/10491"
      }],
      "VideoUrl": "https://youtu.be/-o_sX0VcqH4",
      "VideoEmbedUrl": "<iframe width=\"560\" height=\"315\" src=\"https://www.youtube.com/embed/-o_sX0VcqH4\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>",
      "Ingredients": [{
        "SubSectionHeading": "_",
        "SubSectionIngredients": [
          [{
            "Name": "Fazer Arjen Ihana Kaura & Hapanjuuri -leipää",
            "Amount": "4",
            "PackageRecipe": "false",
            "IngredientType": "16520",
            "Unit": "viipaletta",
            "IngredientTypeName": "Fazer Arjen Ihana Kaura & Hapanjuuri"
          }],
          [{
            "Name": "voita",
            "Amount": "4",
            "PackageRecipe": "false",
            "IngredientType": "7343",
            "Unit": "rkl",
            "IngredientTypeName": "Voi"
          }],
          [{
            "Name": "Pirkka Dijon-sinappia",
            "Ean": "6410405187369",
            "Amount": "2",
            "PackageRecipe": "false",
            "IngredientType": "5393",
            "Unit": "rkl",
            "IngredientTypeName": "Dijonsinappi"
          }],
          [{
            "Name": "Pirkka Parhaat cheddaria",
            "Ean": "6410405230904",
            "Amount": "1/2",
            "AmountInfo": "(à 200 g)",
            "PackageRecipe": "false",
            "IngredientType": "5349",
            "Unit": "pkt",
            "IngredientTypeName": "Cheddarjuusto"
          }],
          [{
            "Name": "Snellman kotimaista palvikinkkua",
            "Amount": "4",
            "PackageRecipe": "false",
            "IngredientType": "12260",
            "Unit": "viipaletta",
            "IngredientTypeName": "Palvikinkku, viipale"
          }]
        ]
      }],
      "Instructions": "# Voitele leivät voilla. Käännä ja voitele toinen puoli sinapilla. Viipaloi juusto kahdelle leivälle. Levitä kinkku päälle. Nosta toiset leivät kansiksi.\r\n# Paista leivät molemmin puolin paistinpannussa kullanruskeiksi ja niin, että juusto sulaa. Leikkaa leivät kahtia tarjolle.",
      "EndNote": "*Annoshinnat on laskettu K-citymarket-ketjun hintatason mukaan 8/2019. Tuoretuotteiden hinnat vaihtelevat sesongeittain.",
      "Description": "Cheese melt sandwichissa ei täytteitä pihistellä: sinapilla silattujen paahtoleipien välissä on tuplasti kinkkua ja cheddaria. Noin 1,55€/annos",
      "DateCreated": "2019-06-11T14:24:36",
      "DateModified": "2019-09-16T08:51:29",
      "TvDate": "2019-09-09T00:00:00",
      "Stamp": {
        "Name": "Testattu K-koekeittiössä",
        "Id": "1"
      },
      "RecipeUse": {
        "Name": "k-ruoka.fi",
        "Id": "1"
      },
      "resourceType": "recipe",
      "Url": "https://www.k-ruoka.fi/reseptit/cheese-melt-sandwich-eli-lampimat-kinkku-juustoleivat",
      "UrlSlug": "cheese-melt-sandwich-eli-lampimat-kinkku-juustoleivat",
      "Sort": [1, 1, 10491]
    }, {
      "Id": "10488",
      "Name": "Kanaa tikka masala",
      "PieceSize": {
        "Unit": "g",
        "Amount": "310"
      },
      "Portions": {
        "Unit": "annosta",
        "Amount": "4"
      },
      "PreparationTime": {
        "Description": "30 - 60 min",
        "TimeRange": {
          "MinTime": 30,
          "MaxTime": 60
        }
      },
      "UserPortions": {
        "$": {
          "Unit": "henkilölle"
        }
      },
      "Categories": [{
        "MainName": "Makuja maailmalta",
        "MainId": "2",
        "SubName": "Intia",
        "SubId": "9"
      }, {
        "MainName": "Pääruoat",
        "MainId": "4",
        "SubName": "Broileri- ja linturuoat",
        "SubId": "21"
      }, {
        "MainName": "Pääruoat",
        "MainId": "4",
        "SubName": "Kastikkeet",
        "SubId": "26"
      }, {
        "MainName": "Pääraaka-aine",
        "MainId": "14",
        "SubName": "Broileri ja linnut",
        "SubId": "118"
      }, {
        "MainName": "Teema",
        "MainId": "15",
        "SubName": "Mitä tänään syötäisiin?",
        "SubId": "144"
      }, {
        "MainName": "Asiakassegmentointi (K-asiakkaat)",
        "MainId": "17",
        "SubName": "Nautiskelijat",
        "SubId": "146"
      }, {
        "MainName": "Asiakassegmentointi (K-asiakkaat)",
        "MainId": "17",
        "SubName": "Tiedostavat",
        "SubId": "148"
      }, {
        "MainName": "Teema",
        "MainId": "15",
        "SubName": "Arkiruoka",
        "SubId": "159"
      }],
      "Pictures": ["kanaa_tikka_mas_PI11_19"],
      "PictureUrls": [{
        "Id": "kanaa_tikka_mas_PI11_19",
        "Normal": "https://k-file-storage-qa.imgix.net/f/k-ruoka/recipe/10488?w=1000&h=1000&fit=clip",
        "Original": "https://k-file-storage-qa.imgix.net/f/k-ruoka/recipe/10488"
      }],
      "SpecialDiets": [{
        "Id": "4",
        "Name": "Gluteeniton"
      }],
      "Ingredients": [{
        "SubSectionHeading": "_",
        "SubSectionIngredients": [
          [{
            "Name": "Pirkka broilerin ohutleikkeitä, maustamaton",
            "Ean": "6410405127143",
            "Amount": "1",
            "AmountInfo": "(340 g)",
            "PackageRecipe": "false",
            "IngredientType": "5306",
            "Unit": "rs",
            "IngredientTypeName": "Broilerin ohutleike, maustamaton"
          }]
        ]
      }, {
        "SubSectionHeading": "Mausteseos",
        "SubSectionIngredients": [
          [{
            "Name": "inkivääriä raastettuna",
            "Amount": "2",
            "PackageRecipe": "false",
            "IngredientType": "5567",
            "Unit": "tl",
            "IngredientTypeName": "Inkivääri, tuore"
          }],
          [{
            "Name": "valkosipulinkynttä raastettuna",
            "Amount": "3",
            "PackageRecipe": "false",
            "Unit": ""
          }],
          [{
            "Name": "kurkumaa",
            "Amount": "2",
            "PackageRecipe": "false",
            "IngredientType": "10200",
            "Unit": "tl",
            "IngredientTypeName": "Kurkuma, jauhettu"
          }],
          [{
            "Name": "Pirkka Luomu juustokuminaa",
            "Amount": "2",
            "PackageRecipe": "false",
            "IngredientType": "9128",
            "Unit": "tl",
            "IngredientTypeName": "Juustokumina, luomu"
          }],
          [{
            "Name": "paprikajauhetta",
            "Ean": "6410405055781",
            "Amount": "2",
            "PackageRecipe": "false",
            "IngredientType": "6444",
            "Unit": "tl",
            "IngredientTypeName": "Paprika, jauhettu"
          }],
          [{
            "Name": "jauhettua korianteria",
            "Amount": "1",
            "PackageRecipe": "false",
            "IngredientType": "5850",
            "Unit": "tl",
            "IngredientTypeName": "Korianteri, jauhettu"
          }],
          [{
            "Name": "garam masala -maustesekoitusta",
            "Amount": "1",
            "PackageRecipe": "false",
            "IngredientType": "6178",
            "Unit": "tl",
            "IngredientTypeName": "Mausteseos, garam masala"
          }],
          [{
            "Name": "suolaa",
            "Amount": "1",
            "PackageRecipe": "false",
            "IngredientType": "7049",
            "Unit": "tl",
            "IngredientTypeName": "Suola"
          }],
          [{
            "Name": "Pirkka turkkilaista jogurttia",
            "Amount": "1 1/2",
            "PackageRecipe": "false",
            "IngredientType": "7441",
            "Unit": "dl",
            "IngredientTypeName": "Turkkilainen jogurtti, laktoositon"
          }]
        ]
      }, {
        "SubSectionHeading": "Kastike",
        "SubSectionIngredients": [
          [{
            "Name": "sipuli",
            "Amount": "1",
            "AmountInfo": "(100 g)",
            "PackageRecipe": "false",
            "IngredientType": "6932",
            "Unit": "",
            "IngredientTypeName": "Sipuli"
          }],
          [{
            "Name": "voita tai kirkastettua voita",
            "Amount": "2",
            "PackageRecipe": "false",
            "Unit": "rkl"
          }],
          [{
            "Name": "Pirkka tomaattisosetta",
            "Ean": "6410405093202",
            "Amount": "2",
            "PackageRecipe": "false",
            "IngredientType": "7141",
            "Unit": "rkl",
            "IngredientTypeName": "Tomaattisose"
          }],
          [{
            "Name": "K-Menu kuorittuja tomaatteja tomaattimehussa",
            "Ean": "6410405138378",
            "Amount": "1",
            "AmountInfo": "(400 g)",
            "PackageRecipe": "false",
            "IngredientType": "7127",
            "Unit": "tlk",
            "IngredientTypeName": "Tomaatti, kuorittu, tomaattimehussa"
          }],
          [{
            "Name": "Pirkka turkkilaista jogurttia",
            "Amount": "2 1/2",
            "PackageRecipe": "false",
            "IngredientType": "7441",
            "Unit": "dl",
            "IngredientTypeName": "Turkkilainen jogurtti, laktoositon"
          }],
          [{
            "Name": "korianteria hienonnettuna",
            "Amount": "1",
            "PackageRecipe": "false",
            "IngredientType": "5852",
            "Unit": "ruukku",
            "IngredientTypeName": "Korianteri, tuore"
          }]
        ]
      }],
      "Instructions": "# Sekoita mausteseoksen ainekset keskenään ja sekoita puolet seoksesta jogurtin joukkoon. Kääntele broilerit jogurtissa ja anna maustua noin puoli tuntia huoneenlämmössä.\r\n# Suikaloi kuorittu sipuli. Kuullota sipulia voissa yhdessä tomaattisoseen kanssa noin 5 minuuttia. Lisää loput mausteseoksesta ja jatka paistamista vielä muutama minuutti.\r\n# Kumoa kasariin kuoritut tomaatit liemineen, painele ne lastalla rikki ja kuumenna kiehuvaksi. Peitä kannella ja anna hautua.\r\n# Kuumenna uunin grillivastukset 250 asteeseen. Peitä uunipelti foliolla ja nosta siihen broilerit. Paista uunin ylätasolla, kunnes broilerit ovat saaneet väriä, noin 10 minuuttia. Jäähdytä hetki ja paloittele broilerit. Lisää ne yhdessä jogurtin kanssa kastikkeen joukkoon.\r\n# Hienonna päälle lopuksi korianteria ja tarjoa kastike riisin kanssa.",
      "EndNote": "*Annoshinnat on laskettu K-citymarket-ketjun hintatason mukaan 10/2019. Tuoretuotteiden hinnat vaihtelevat sesongeittain.",
      "Description": "Intialaisten ravintoloiden suosikki, tikka masala, onnistuu myös kotona. Lempeän mausteinen kanakastike maistuu koko perheelle. Noin 2,15 €/annos*",
      "DateCreated": "2019-06-10T09:27:36",
      "DateModified": "2019-10-11T14:10:40",
      "TvDate": "2019-11-12T00:00:00",
      "Stamp": {
        "Name": "Testattu K-koekeittiössä",
        "Id": "1"
      },
      "RecipeUse": {
        "Name": "k-ruoka.fi",
        "Id": "1"
      },
      "resourceType": "recipe",
      "Url": "https://www.k-ruoka.fi/reseptit/kanaa-tikka-masala",
      "UrlSlug": "kanaa-tikka-masala",
      "Sort": [1, 1, 10488]
    }, {
      "Id": "10479",
      "Name": "Kylmä kurkkukeitto",
      "PieceSize": {
        "Unit": "g"
      },
      "Portions": {
        "Unit": "annosta"
      },
      "PreparationTime": {
        "Description": "alle 15 min",
        "TimeRange": {
          "MinTime": 5,
          "MaxTime": 15
        }
      },
      "UserPortions": {
        "$": {
          "Unit": "henkilölle"
        }
      },
      "Categories": [{
        "MainName": "Pääruoat",
        "MainId": "4",
        "SubName": "Keitot",
        "SubId": "27"
      }, {
        "MainName": "Pääraaka-aine",
        "MainId": "14",
        "SubName": "Kasvis",
        "SubId": "115"
      }, {
        "MainName": "Sesonki",
        "MainId": "1",
        "SubName": "Kesä",
        "SubId": "121"
      }, {
        "MainName": "Ruokalaji",
        "MainId": "16",
        "SubName": "Keitot",
        "SubId": "136"
      }, {
        "MainName": "Alkuruoat",
        "MainId": "3",
        "SubName": "Keitot",
        "SubId": "17"
      }],
      "Pictures": ["kurkkukeitto_19"],
      "PictureUrls": [{
        "Id": "kurkkukeitto_19",
        "Normal": "https://k-file-storage-qa.imgix.net/f/k-ruoka/recipe/10479?w=1000&h=1000&fit=clip",
        "Original": "https://k-file-storage-qa.imgix.net/f/k-ruoka/recipe/10479"
      }],
      "SpecialDiets": [{
        "Id": "4",
        "Name": "Gluteeniton"
      }, {
        "Id": "6",
        "Name": "Kasvis (lakto-ovo)"
      }],
      "Ingredients": [{
        "SubSectionHeading": "_",
        "SubSectionIngredients": [
          [{
            "Name": "Pirkka Luomu pikkukurkkua",
            "Amount": "300",
            "PackageRecipe": "false",
            "IngredientType": "5896",
            "Unit": "g",
            "IngredientTypeName": "Kurkku, luomu"
          }],
          [{
            "Name": "valkosipulinkynsi",
            "Amount": "1",
            "PackageRecipe": "false",
            "IngredientType": "7262",
            "Unit": "",
            "IngredientTypeName": "Valkosipuli"
          }],
          [{
            "Name": "tuoretta korianteria",
            "Amount": "1",
            "PackageRecipe": "false",
            "IngredientType": "5852",
            "Unit": "ruukku",
            "IngredientTypeName": "Korianteri, tuore"
          }],
          [{
            "Name": "tuoretta basilikaa",
            "Amount": "1/2",
            "PackageRecipe": "false",
            "IngredientType": "5271",
            "Unit": "ruukkua",
            "IngredientTypeName": "Basilika, tuore"
          }],
          [{
            "Name": "kreikkalaista jogurttia",
            "Amount": "500",
            "PackageRecipe": "false",
            "IngredientType": "10111",
            "Unit": "g",
            "IngredientTypeName": "Kreikkalainen jogurtti, maustamaton"
          }],
          [{
            "Name": "oliiviöljyä",
            "Amount": "3",
            "PackageRecipe": "false",
            "IngredientType": "5405",
            "Unit": "rkl",
            "IngredientTypeName": "Ekstra neitsytoliiviöljy"
          }],
          [{
            "Name": "sitruunan mehu",
            "Amount": "1-2",
            "PackageRecipe": "false",
            "IngredientType": "6941",
            "Unit": "",
            "IngredientTypeName": "Sitruuna"
          }],
          [{
            "Name": "tuore vihreä chilipalko",
            "Amount": "1/2",
            "PackageRecipe": "false",
            "IngredientType": "16408",
            "Unit": "",
            "IngredientTypeName": "Chili, vihreä"
          }],
          [{
            "Name": "suolaa",
            "Amount": "1",
            "PackageRecipe": "false",
            "IngredientType": "7049",
            "Unit": "tl",
            "IngredientTypeName": "Suola"
          }]
        ]
      }, {
        "SubSectionHeading": "Pinnalle",
        "SubSectionIngredients": [
          [{
            "Name": "versoja, sipulinvarsia tai ruohosipulia ja mustikoita",
            "PackageRecipe": "false"
          }]
        ]
      }],
      "Instructions": "# Pilko kurkut hiukan pienemmiksi, kuori valkosipulinkynsi, poista chilistä siemenet. Korianterin voit käyttää varsineen, nypi basilikasta lehdet.\r\n# Laita ainekset tehosekoittimeen. Purista mukaan ensin yhden sitruunan mehu, aja tasaiseksi keitoksi ja lisää enemmän sitruunaa jos kaipaat lisää hapokkuutta.\r\n# Viilennä keitto hyvin. Liian paksua keittoa voi ohentaa kylmällä vedellä tai lisätä joukkoon jääpaloja.",
      "EndNote": "",
      "Description": "Kylmä kurkkukeitto viilentää kesähelteellä. Keitto sopii alkuruoaksi tai kevyeksi lounaaksi leivän kanssa. Resepti: Nanna Rintala / Kaikki äitini reseptit.",
      "DateCreated": "2019-06-05T11:52:01",
      "DateModified": "2019-07-12T07:38:05",
      "Stamp": {
        "Name": "Bloggaaja",
        "Id": "2"
      },
      "RecipeUse": {
        "Name": "k-ruoka.fi",
        "Id": "1"
      },
      "resourceType": "recipe",
      "Url": "https://www.k-ruoka.fi/reseptit/kylma-kurkkukeitto",
      "UrlSlug": "kylma-kurkkukeitto",
      "Sort": [1, 1, 10479]
    }, {
      "Id": "10472",
      "Name": "Teriyakikana",
      "PieceSize": {
        "Unit": "g",
        "Amount": "495"
      },
      "Portions": {
        "Unit": "annosta",
        "Amount": "2"
      },
      "PreparationTime": {
        "Description": "30 - 60 min",
        "TimeRange": {
          "MinTime": 30,
          "MaxTime": 60
        }
      },
      "UserPortions": {
        "$": {
          "Unit": "henkilölle"
        }
      },
      "Categories": [{
        "MainName": "Makuja maailmalta",
        "MainId": "2",
        "SubName": "Aasia",
        "SubId": "7"
      }, {
        "MainName": "Pääruoat",
        "MainId": "4",
        "SubName": "Broileri- ja linturuoat",
        "SubId": "21"
      }, {
        "MainName": "Pääruoat",
        "MainId": "4",
        "SubName": "Salaatit",
        "SubId": "28"
      }, {
        "MainName": "Makuja maailmalta",
        "MainId": "2",
        "SubName": "Japani",
        "SubId": "103"
      }, {
        "MainName": "Pääraaka-aine",
        "MainId": "14",
        "SubName": "Broileri ja linnut",
        "SubId": "118"
      }, {
        "MainName": "Asiakassegmentointi (K-asiakkaat)",
        "MainId": "17",
        "SubName": "Nautiskelijat",
        "SubId": "146"
      }, {
        "MainName": "Asiakassegmentointi (K-asiakkaat)",
        "MainId": "17",
        "SubName": "Innostujat",
        "SubId": "147"
      }, {
        "MainName": "Asiakassegmentointi (K-asiakkaat)",
        "MainId": "17",
        "SubName": "Mukavuudenhaluiset ",
        "SubId": "149"
      }, {
        "MainName": "Apukategoriat",
        "MainId": "18",
        "SubName": "Trendikäs ",
        "SubId": "156"
      }],
      "Pictures": ["teriyakikana_6_19"],
      "PictureUrls": [{
        "Id": "teriyakikana_6_19",
        "Normal": "https://k-file-storage-qa.imgix.net/f/k-ruoka/recipe/10472?w=1000&h=1000&fit=clip",
        "Original": "https://k-file-storage-qa.imgix.net/f/k-ruoka/recipe/10472"
      }],
      "Ingredients": [{
        "SubSectionHeading": "Teriyakikastike",
        "SubSectionIngredients": [
          [{
            "Name": "Pirkka japanilaista soijakastiketta",
            "Ean": "6410400048825",
            "Amount": "1/2",
            "PackageRecipe": "false",
            "IngredientType": "6953",
            "Unit": "dl",
            "IngredientTypeName": "Soijakastike"
          }],
          [{
            "Name": "Pirkka riisiviinietikkaa",
            "Ean": "6410405192738",
            "Amount": "1/2",
            "PackageRecipe": "false",
            "IngredientType": "6740",
            "Unit": "dl",
            "IngredientTypeName": "Riisiviinietikka"
          }],
          [{
            "Name": "ruokokidesokeria",
            "Amount": "2",
            "PackageRecipe": "false",
            "IngredientType": "6795",
            "Unit": "rkl",
            "IngredientTypeName": "Ruokokidesokeri"
          }],
          [{
            "Name": "valkosipulinkynsi hienonnettuna",
            "Amount": "1",
            "PackageRecipe": "false",
            "IngredientType": "15809",
            "Unit": "",
            "IngredientTypeName": "Valkosipuli, tuore"
          }],
          [{
            "Name": "Pirkka inkivääriä",
            "Ean": "6410405055606",
            "Amount": "1",
            "PackageRecipe": "false",
            "IngredientType": "5566",
            "Unit": "tl",
            "IngredientTypeName": "Inkivääri, jauhettu"
          }],
          [{
            "Name": "maissitärkkelystä",
            "Amount": "1",
            "PackageRecipe": "false",
            "IngredientType": "6035",
            "Unit": "tl",
            "IngredientTypeName": "Maissitärkkelysjauho"
          }]
        ]
      }, {
        "SubSectionHeading": "Lisäksi",
        "SubSectionIngredients": [
          [{
            "Name": "Kariniemen kananpojan rintafileetä, annospakattu",
            "Amount": "1",
            "AmountInfo": "(2 kpl/300 g)",
            "PackageRecipe": "false",
            "IngredientType": "5286",
            "Unit": "pkt",
            "IngredientTypeName": "Broilerin fileepihvi, maustamaton"
          }],
          [{
            "Name": "öljyä",
            "Amount": "1-2",
            "PackageRecipe": "false",
            "Unit": "rkl"
          }],
          [{
            "Name": "seesaminsiemeniä paahdettuna",
            "Amount": "1-2",
            "PackageRecipe": "false",
            "IngredientType": "16044",
            "Unit": "tl",
            "IngredientTypeName": "Seesaminsiemen, paahdettu"
          }],
          [{
            "Name": "Pirkka jääsalaattia",
            "Ean": "6410405093677",
            "Amount": "1",
            "AmountInfo": "(100 g)",
            "PackageRecipe": "false",
            "IngredientType": "5608",
            "Unit": "ruukku",
            "IngredientTypeName": "Jääsalaatti"
          }],
          [{
            "Name": "korianteria",
            "Amount": "1",
            "PackageRecipe": "false",
            "IngredientType": "5852",
            "Unit": "ruukku",
            "IngredientTypeName": "Korianteri, tuore"
          }],
          [{
            "Name": "kurkku",
            "Amount": "1",
            "AmountInfo": "(250 g)",
            "PackageRecipe": "false",
            "IngredientType": "5895",
            "Unit": "",
            "IngredientTypeName": "Kurkku"
          }],
          [{
            "Name": "riisiviinietikkaa",
            "Amount": "1",
            "PackageRecipe": "false",
            "IngredientType": "6740",
            "Unit": "rkl",
            "IngredientTypeName": "Riisiviinietikka"
          }],
          [{
            "Name": "sokeria",
            "Amount": "1",
            "PackageRecipe": "false",
            "Unit": "tl"
          }],
          [{
            "Name": "suolaa",
            "PackageRecipe": "false",
            "IngredientType": "7049",
            "Unit": "ripaus",
            "IngredientTypeName": "Suola"
          }],
          [{
            "Name": "Pirkka ekstra-neitsytoliiviöljyä",
            "Ean": "6410402024759",
            "Amount": "1",
            "PackageRecipe": "false",
            "IngredientType": "5405",
            "Unit": "rkl",
            "IngredientTypeName": "Ekstra neitsytoliiviöljy"
          }],
          [{
            "Name": "Pirkka syöntikypsä avokado",
            "Ean": "2000388400001",
            "Amount": "1",
            "AmountInfo": "(175 g)",
            "PackageRecipe": "false",
            "IngredientType": "7605",
            "Unit": "",
            "IngredientTypeName": "Avokado, tuore"
          }],
          [{
            "Name": "Pirkka kevätsipulinvartta hienonnettuna",
            "Ean": "6410405093080",
            "Amount": "1-2",
            "PackageRecipe": "false",
            "IngredientType": "5792",
            "Unit": "",
            "IngredientTypeName": "Kevätsipuli"
          }]
        ]
      }],
      "Instructions": "# Mittaa kastikkeen ainekset pieneen kattilaan ja sekoita tasaiseksi kierrevatkaimella. Kuumenna koko ajan sekoittaen, kunnes seos paksuuntuu. Anna kastikkeen jäähtyä.\r\n# Pyyhi broilereiden pinta talouspaperilla ja pane ne marinoitumaan teriyakikastikkeeseen huoneenlämpöön puoleksi tunniksi ennen paistamista.\r\n# Paista broilerit kypsiksi öljyssä pannulla. Sivele fileille marinadia paistamisen aikana. Viipaloi kypsät broilerit.\r\n# Revi jääsalaatti ja korianteri vadille. Halkaise kurkku pitkittäin. Kaavi halutessasi siemenet pois lusikalla ja leikkaa paksuiksi viipaleiksi. Mausta kurkku riisiviinietikalla, sokerilla ja suolalla. Sekoita kurkut salaatin joukkoon ja valuta salaatille oliiviöljyä.\r\n# Halkaise avokado, poista kuori ja kivi. Viipaloi hedelmäliha.\r\n# Nosta salaatin pinnalle viipaloidut broilerit ja avokadot. Ripottele broilereille seesaminsiemeniä ja koristele salaatti kevätsipulilla.",
      "EndNote": "*Annoshinnat on laskettu K-citymarket-ketjun hintatason mukaan 10/2019. Tuoretuotteiden hinnat vaihtelevat sesongeittain.",
      "Description": "Teriyakikastike on helppo valmistaa. Makean tahmaiset teriyakikanat maistuvat raikkaan vihreän salaatin kanssa. Tarjoa halutessasi lisäksi riisiä. Noin 3,70 €/annos*",
      "DateCreated": "2019-06-04T13:51:32",
      "DateModified": "2019-10-07T10:22:35",
      "TvDate": "2019-11-26T00:00:00",
      "Stamp": {
        "Name": "Testattu K-koekeittiössä",
        "Id": "1"
      },
      "RecipeUse": {
        "Name": "k-ruoka.fi",
        "Id": "1"
      },
      "resourceType": "recipe",
      "Url": "https://www.k-ruoka.fi/reseptit/teriyakikana",
      "UrlSlug": "teriyakikana",
      "Sort": [1, 1, 10472]
    }, {
      "Id": "10471",
      "Name": "Paahdettu lanttusalaatti",
      "PieceSize": {
        "Unit": "g"
      },
      "Portions": {
        "Unit": "annosta",
        "Amount": "4"
      },
      "PreparationTime": {
        "Description": "30 - 60 min",
        "TimeRange": {
          "MinTime": 30,
          "MaxTime": 60
        }
      },
      "UserPortions": {
        "$": {
          "Unit": "henkilölle"
        }
      },
      "Categories": [{
        "MainName": "Pääraaka-aine",
        "MainId": "14",
        "SubName": "Kasvis",
        "SubId": "115"
      }, {
        "MainName": "Sesonki",
        "MainId": "1",
        "SubName": "Joulu",
        "SubId": "130"
      }, {
        "MainName": "Asiakassegmentointi (K-asiakkaat)",
        "MainId": "17",
        "SubName": "Tiedostavat",
        "SubId": "148"
      }, {
        "MainName": "Apukategoriat",
        "MainId": "18",
        "SubName": "Helppo",
        "SubId": "153"
      }, {
        "MainName": "Pääruoat",
        "MainId": "4",
        "SubName": "Kasvisruoat",
        "SubId": "22"
      }, {
        "MainName": "Sesonki",
        "MainId": "1",
        "SubName": "Joulun ruoat",
        "SubId": "75"
      }, {
        "MainName": "Makuja maailmalta",
        "MainId": "2",
        "SubName": "Suomi",
        "SubId": "106"
      }],
      "Pictures": ["paahdettu_lanttu_6_19"],
      "PictureUrls": [{
        "Id": "paahdettu_lanttu_6_19",
        "Normal": "https://k-file-storage-qa.imgix.net/f/k-ruoka/recipe/10471?w=1000&h=1000&fit=clip",
        "Original": "https://k-file-storage-qa.imgix.net/f/k-ruoka/recipe/10471"
      }],
      "Ingredients": [{
        "SubSectionHeading": "_",
        "SubSectionIngredients": [
          [{
            "Name": "Pirkka lanttuja",
            "Ean": "6410405097262",
            "Amount": "1",
            "AmountInfo": "(1 kg)",
            "PackageRecipe": "false",
            "IngredientType": "5944",
            "Unit": "ps",
            "IngredientTypeName": "Lanttu"
          }],
          [{
            "Name": "rypsiöljyä",
            "Amount": "1",
            "PackageRecipe": "false",
            "IngredientType": "6807",
            "Unit": "rkl",
            "IngredientTypeName": "Rypsiöljy"
          }],
          [{
            "Name": "Pirkka vaahterasiirappia",
            "Ean": "6410402018604",
            "Amount": "1",
            "PackageRecipe": "false",
            "IngredientType": "7233",
            "Unit": "rkl",
            "IngredientTypeName": "Vaahterasiirappi"
          }],
          [{
            "Name": "kanelia",
            "Amount": "1-2",
            "PackageRecipe": "false",
            "IngredientType": "5689",
            "Unit": "tl",
            "IngredientTypeName": "Kaneli, jauhettu"
          }],
          [{
            "Name": "suolaa",
            "Amount": "1/2",
            "PackageRecipe": "false",
            "Unit": "tl"
          }],
          [{
            "Name": "mustapippuria",
            "Amount": "1/4",
            "PackageRecipe": "false",
            "Unit": "tl"
          }]
        ]
      }, {
        "SubSectionHeading": "Lisäksi",
        "SubSectionIngredients": [
          [{
            "Name": "Pirkka Luomu hasselpähkinöitä",
            "Ean": "6410405229175",
            "Amount": "1",
            "AmountInfo": "(60 g)",
            "PackageRecipe": "false",
            "IngredientType": "8314",
            "Unit": "dl",
            "IngredientTypeName": "Hasselpähkinä, luomu"
          }],
          [{
            "Name": "Pirkka Luomu rucolaa",
            "Ean": "6410405197276",
            "Amount": "1",
            "AmountInfo": "(65 g)",
            "PackageRecipe": "false",
            "IngredientType": "6753",
            "Unit": "ps",
            "IngredientTypeName": "Rucola"
          }],
          [{
            "Name": "Castello Crumbly Blue juustoa",
            "Amount": "1",
            "AmountInfo": "(140 g)",
            "PackageRecipe": "false",
            "Unit": "pkt"
          }],
          [{
            "Name": "tuoretta timjamia",
            "PackageRecipe": "false",
            "IngredientType": "7119",
            "IngredientTypeName": "Timjami, tuore"
          }]
        ]
      }, {
        "SubSectionHeading": "Kastike",
        "SubSectionIngredients": [
          [{
            "Name": "Pirkka ekstra-neitsytoliiviöljyä",
            "Ean": "6410402024759",
            "Amount": "2",
            "PackageRecipe": "false",
            "IngredientType": "5405",
            "Unit": "rkl",
            "IngredientTypeName": "Ekstra neitsytoliiviöljy"
          }],
          [{
            "Name": "Pirkka balsamiviinietikkaa",
            "Ean": "6410402004423",
            "Amount": "2",
            "PackageRecipe": "false",
            "IngredientType": "5257",
            "Unit": "rkl",
            "IngredientTypeName": "Balsamietikka, tumma"
          }],
          [{
            "Name": "suolaa",
            "PackageRecipe": "false",
            "IngredientType": "7049",
            "Unit": "ripaus",
            "IngredientTypeName": "Suola"
          }]
        ]
      }],
      "Instructions": "# Kuori lantut ja leikkaa ne reiluiksi lohkoiksi. Pyörittele lantut öljyssä, vaahterasiirapissa ja mausteissa. Kaada lantut pellille leivinpaperin päälle ja paahda 225 asteessa noin 25 minuuttia. Lantut saavat jäädä hieman napakoiksi. Anna jäähtyä.\r\n# Paahda hasselpähkinät kuivalla pannulla, rouhi veitsellä pienemmäksi.\r\n# Sekoita kastikkeen ainekset kierrevatkaimella keskenään. Levitä rucolat laakealle vadille, valuta pinnalle noin puolet kastikkeesta.\r\n# Nostele lantut vadille ja murenna pinnalle sinihomejuusto. Valuta loppu kastike salaatille ja koristele hasselpähkinöillä sekä tuoreella timjamilla.",
      "EndNote": "*Annoshinnat on laskettu K-citymarket-ketjun hintatason mukaan 10/2019. Tuoretuotteiden hinnat vaihtelevat sesongeittain.",
      "Description": "Tämä salaatti haastaa lanttulaatikon!  Kanelilla ja vaahterasiirapilla maustetut  lantut ovat täydellinen pari sinihomejuuston ja hasselpähkinöiden kanssa. Noin 2,20 €/annos*",
      "DateCreated": "2019-06-04T13:51:16",
      "DateModified": "2019-10-07T09:47:21",
      "Stamp": {
        "Name": "Testattu K-koekeittiössä",
        "Id": "1"
      },
      "RecipeUse": {
        "Name": "k-ruoka.fi",
        "Id": "1"
      },
      "resourceType": "recipe",
      "Url": "https://www.k-ruoka.fi/reseptit/paahdettu-lanttusalaatti",
      "UrlSlug": "paahdettu-lanttusalaatti",
      "Sort": [1, 1, 10471]
    }, {
      "Id": "10465",
      "Name": "Hoisintofu",
      "PieceSize": {
        "Unit": "g"
      },
      "Portions": {
        "Unit": "annosta",
        "Amount": "4"
      },
      "PreparationTime": {
        "Description": "alle 30 min",
        "TimeRange": {
          "MinTime": 15,
          "MaxTime": 30
        }
      },
      "UserPortions": {
        "$": {
          "Unit": "henkilölle"
        }
      },
      "Categories": [{
        "MainName": "Apukategoriat",
        "MainId": "18",
        "SubName": "Trendikäs ",
        "SubId": "156"
      }, {
        "MainName": "Apukategoriat",
        "MainId": "18",
        "SubName": "Lyhyt aktiivinen valmistusaika ",
        "SubId": "157"
      }, {
        "MainName": "Teema",
        "MainId": "15",
        "SubName": "Arkiruoka",
        "SubId": "159"
      }, {
        "MainName": "Pääruoat",
        "MainId": "4",
        "SubName": "Kasvisruoat",
        "SubId": "22"
      }, {
        "MainName": "Pääruoat",
        "MainId": "4",
        "SubName": "Kastikkeet",
        "SubId": "26"
      }, {
        "MainName": "Pääraaka-aine",
        "MainId": "14",
        "SubName": "Kasviproteiini",
        "SubId": "135"
      }, {
        "MainName": "Asiakassegmentointi (K-asiakkaat)",
        "MainId": "17",
        "SubName": "Innostujat",
        "SubId": "147"
      }, {
        "MainName": "Asiakassegmentointi (K-asiakkaat)",
        "MainId": "17",
        "SubName": "Tiedostavat",
        "SubId": "148"
      }, {
        "MainName": "Apukategoriat",
        "MainId": "18",
        "SubName": "Helppo",
        "SubId": "153"
      }, {
        "MainName": "Makuja maailmalta",
        "MainId": "2",
        "SubName": "Aasia",
        "SubId": "7"
      }],
      "Pictures": ["PI10_19_hoisintofu"],
      "PictureUrls": [{
        "Id": "PI10_19_hoisintofu",
        "Normal": "https://k-file-storage-qa.imgix.net/f/k-ruoka/recipe/10465?w=1000&h=1000&fit=clip",
        "Original": "https://k-file-storage-qa.imgix.net/f/k-ruoka/recipe/10465"
      }],
      "SpecialDiets": [{
        "Id": "8",
        "Name": "Vegaaninen"
      }, {
        "Id": "9",
        "Name": "Viljaton"
      }, {
        "Id": "2",
        "Name": "Laktoositon"
      }, {
        "Id": "3",
        "Name": "Maidoton"
      }, {
        "Id": "6",
        "Name": "Kasvis (lakto-ovo)"
      }, {
        "Id": "7",
        "Name": "Kananmunaton"
      }],
      "Ingredients": [{
        "SubSectionHeading": "_",
        "SubSectionIngredients": [
          [{
            "Name": "Pirkka luomu tofua",
            "Amount": "2",
            "AmountInfo": "(à 250 g)",
            "PackageRecipe": "false",
            "Unit": "pkt"
          }],
          [{
            "Name": "seesamiöljyä",
            "Amount": "2",
            "PackageRecipe": "false",
            "IngredientType": "6873",
            "Unit": "rkl",
            "IngredientTypeName": "Seesamiöljy"
          }]
        ]
      }, {
        "SubSectionHeading": "Kastike",
        "SubSectionIngredients": [
          [{
            "Name": "inkivääriä hienonnettuna",
            "Amount": "3",
            "PackageRecipe": "false",
            "IngredientType": "5567",
            "Unit": "rkl",
            "IngredientTypeName": "Inkivääri, tuore"
          }],
          [{
            "Name": "valkosipulinkynttä",
            "Amount": "3",
            "PackageRecipe": "false",
            "IngredientType": "7262",
            "Unit": "",
            "IngredientTypeName": "Valkosipuli"
          }],
          [{
            "Name": "Pirkka mieto punainen chili",
            "Amount": "1",
            "PackageRecipe": "false",
            "IngredientType": "5355",
            "Unit": "",
            "IngredientTypeName": "Chili, punainen"
          }],
          [{
            "Name": "Pirkka hoisinkastiketta",
            "Ean": "6410405192677",
            "Amount": "1 1/2",
            "PackageRecipe": "false",
            "IngredientType": "8414",
            "Unit": "dl",
            "IngredientTypeName": "Hoi Sin -kastike"
          }],
          [{
            "Name": "vettä",
            "Amount": "1",
            "PackageRecipe": "false",
            "IngredientType": "15792",
            "Unit": "dl",
            "IngredientTypeName": "Vesi"
          }],
          [{
            "Name": "soijakastiketta",
            "Amount": "2",
            "PackageRecipe": "false",
            "IngredientType": "6953",
            "Unit": "rkl",
            "IngredientTypeName": "Soijakastike"
          }],
          [{
            "Name": "seesamiöljyä",
            "Amount": "2",
            "PackageRecipe": "false",
            "IngredientType": "6873",
            "Unit": "rkl",
            "IngredientTypeName": "Seesamiöljy"
          }],
          [{
            "Name": "sokeria",
            "Amount": "2",
            "PackageRecipe": "false",
            "IngredientType": "6961",
            "Unit": "rkl",
            "IngredientTypeName": "Sokeri"
          }]
        ]
      }, {
        "SubSectionHeading": "Lisäksi",
        "SubSectionIngredients": [
          [{
            "Name": "Pirkka Parhaat riisinuudelia",
            "Amount": "1",
            "AmountInfo": "(180 g)",
            "PackageRecipe": "false",
            "IngredientType": "6735",
            "Unit": "pkt",
            "IngredientTypeName": "Riisinuudeli"
          }],
          [{
            "Name": "kevätsipulinvartta",
            "Amount": "2",
            "PackageRecipe": "false",
            "IngredientType": "5792",
            "Unit": "",
            "IngredientTypeName": "Kevätsipuli"
          }],
          [{
            "Name": "limetti",
            "Amount": "1",
            "PackageRecipe": "false",
            "IngredientType": "5997",
            "Unit": "",
            "IngredientTypeName": "Limetti"
          }]
        ]
      }],
      "Instructions": "# Tee kastike. Hienonna inkivääri, valkosipuli ja chili kulhoon. Sekoita loput ainekset joukkoon.\r\n# Puristele tofua talouspaperin välissä niin, että kaikki irtoava neste imeytyy paperiin. Kuutioi tofu.\r\n# Kuumenna öljy kasarissa. Paista tofua muutama minuutti. Sekoita kastike joukkoon, kiehauta ja nosta kasari liedeltä.\r\n# Keitä nuudelit pakkauksen ohjeen mukaan ja huuhtaise kylmällä vedellä, niin ne eivät takerru toisiinsa.\r\n# Tarjoa tofu nuudelien, hienonnetun kevätsipulin ja limetinlohkojen kanssa.",
      "EndNote": "*Annoshinnat on laskettu K-citymarket-ketjun hintatason mukaan 10/2019. Tuoretuotteiden hinnat vaihtelevat sesongeittain.",
      "Description": "Tofu on parhaimmillaan upotetuuna tummaan ja täyteläiseen kiinalaiseen hoisinkastikkeeseen. Tämä ruoka on kaikille tofun ystäville ja erityisesti sinulle, joka et erityisemmin ole pitänyt tofusta.\r\nNoin 2,55 €/annos*",
      "DateCreated": "2019-06-03T09:19:01",
      "DateModified": "2019-09-18T09:50:08",
      "Stamp": {
        "Name": "Testattu K-koekeittiössä",
        "Id": "1"
      },
      "RecipeUse": {
        "Name": "k-ruoka.fi",
        "Id": "1"
      },
      "resourceType": "recipe",
      "Url": "https://www.k-ruoka.fi/reseptit/hoisintofu",
      "UrlSlug": "hoisintofu",
      "Sort": [1, 1, 10465]
    }, {
      "Id": "10464",
      "Name": "Paahdettu kurpitsakeitto",
      "PieceSize": {
        "Unit": "g",
        "Amount": "240"
      },
      "Portions": {
        "Unit": "annosta",
        "Amount": "4"
      },
      "PreparationTime": {
        "Description": "30 - 60 min",
        "TimeRange": {
          "MinTime": 30,
          "MaxTime": 60
        }
      },
      "UserPortions": {
        "$": {
          "Unit": "henkilölle"
        }
      },
      "Categories": [{
        "MainName": "Pääruoat",
        "MainId": "4",
        "SubName": "Keitot",
        "SubId": "27"
      }, {
        "MainName": "Pääraaka-aine",
        "MainId": "14",
        "SubName": "Kasvis",
        "SubId": "115"
      }, {
        "MainName": "Sesonki",
        "MainId": "1",
        "SubName": "Sadonkorjuu",
        "SubId": "124"
      }, {
        "MainName": "Sesonki",
        "MainId": "1",
        "SubName": "Halloween",
        "SubId": "126"
      }, {
        "MainName": "Ruokalaji",
        "MainId": "16",
        "SubName": "Keitot",
        "SubId": "136"
      }, {
        "MainName": "Teema",
        "MainId": "15",
        "SubName": "Mitä tänään syötäisiin?",
        "SubId": "144"
      }, {
        "MainName": "Apukategoriat",
        "MainId": "18",
        "SubName": "Lyhyt aktiivinen valmistusaika ",
        "SubId": "157"
      }, {
        "MainName": "Teema",
        "MainId": "15",
        "SubName": "Arkiruoka",
        "SubId": "159"
      }, {
        "MainName": "Asiakassegmentointi (K-asiakkaat)",
        "MainId": "17",
        "SubName": "Nautiskelijat",
        "SubId": "146"
      }, {
        "MainName": "Asiakassegmentointi (K-asiakkaat)",
        "MainId": "17",
        "SubName": "Innostujat",
        "SubId": "147"
      }, {
        "MainName": "Asiakassegmentointi (K-asiakkaat)",
        "MainId": "17",
        "SubName": "Tiedostavat",
        "SubId": "148"
      }, {
        "MainName": "Asiakassegmentointi (K-asiakkaat)",
        "MainId": "17",
        "SubName": "Mukavuudenhaluiset ",
        "SubId": "149"
      }, {
        "MainName": "Asiakassegmentointi (K-asiakkaat)",
        "MainId": "17",
        "SubName": "Vakiintuneet",
        "SubId": "150"
      }, {
        "MainName": "Apukategoriat",
        "MainId": "18",
        "SubName": "Helppo",
        "SubId": "153"
      }],
      "Pictures": ["PI10_19_paahdettu_kurp"],
      "PictureUrls": [{
        "Id": "PI10_19_paahdettu_kurp",
        "Normal": "https://k-file-storage-qa.imgix.net/f/k-ruoka/recipe/10464?w=1000&h=1000&fit=clip",
        "Original": "https://k-file-storage-qa.imgix.net/f/k-ruoka/recipe/10464"
      }],
      "VideoUrl": "https://youtu.be/Mtv790iyMgA",
      "VideoEmbedUrl": "<iframe width=\"1136\" height=\"639\" src=\"https://www.youtube.com/embed/Mtv790iyMgA\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>\r\n•",
      "SpecialDiets": [{
        "Id": "6",
        "Name": "Kasvis (lakto-ovo)"
      }, {
        "Id": "7",
        "Name": "Kananmunaton"
      }],
      "Ingredients": [{
        "SubSectionHeading": "_",
        "SubSectionIngredients": [
          [{
            "Name": "kuorittua, paloiteltua kurpitsaa",
            "Amount": "700",
            "PackageRecipe": "false",
            "IngredientType": "16083",
            "Unit": "g",
            "IngredientTypeName": "kurpitsa"
          }],
          [{
            "Name": "oliiviöljyä",
            "Amount": "2",
            "PackageRecipe": "false",
            "IngredientType": "5405",
            "Unit": "rkl",
            "IngredientTypeName": "Ekstra neitsytoliiviöljy"
          }],
          [{
            "Name": "suolaa",
            "Amount": "1/2",
            "PackageRecipe": "false",
            "IngredientType": "7049",
            "Unit": "tl",
            "IngredientTypeName": "Suola"
          }],
          [{
            "Name": "Arla Lempi Fraîche -kermaa",
            "Amount": "1",
            "AmountInfo": "(2 dl)",
            "PackageRecipe": "false",
            "IngredientType": "16082",
            "Unit": "prk",
            "IngredientTypeName": "Arla fraiche kerma, 30 %"
          }],
          [{
            "Name": "kasvisfondia",
            "Amount": "2",
            "PackageRecipe": "false",
            "IngredientType": "5440",
            "Unit": "rkl",
            "IngredientTypeName": "Fondi, kasvis"
          }],
          [{
            "Name": "sahramia",
            "AmountInfo": "ripaus",
            "PackageRecipe": "false",
            "IngredientType": "6817",
            "IngredientTypeName": "Sahrami"
          }],
          [{
            "Name": "kiehuvan kuumaa vettä",
            "Amount": "3-4",
            "PackageRecipe": "false",
            "IngredientType": "15792",
            "Unit": "dl",
            "IngredientTypeName": "Vesi"
          }],
          [{
            "Name": "Pirkka juureslastuja",
            "Ean": "6410405094209",
            "Amount": "1",
            "AmountInfo": "(75 g)",
            "PackageRecipe": "false",
            "IngredientType": "5587",
            "Unit": "ps",
            "IngredientTypeName": "Juureslastu"
          }]
        ]
      }],
      "Instructions": "# Kuori kurpitsa ja paloittele se pataan tai uunin kestävään kattilaan. Pirskota öljy ja suola päälle. Kypsennä  225-asteisessa uunissa, ilman kantta noin 30-40 minuuttia, kunnes kurpitsa on aivan pehmeää.\r\n# Ota pata uunista. Lisää kerma, voit jättää sitä tilkan annosten päälle. Lisää fondi ja sahrami. Soseuta keitto tasaiseksi ja lisää kuumaa vettä sen verran, että koostumuksesta tulee sopivan pehmeä.\r\n# Koristele keitto juureslastuilla.",
      "EndNote": "*Annoshinnat on laskettu K-citymarket-ketjun hintatason mukaan 10/2019. Tuoretuotteiden hinnat vaihtelevat sesongeittain.",
      "Description": "Helppo kurpitsakeitto kypsennetäänkin uunissa. Näin kurpitsan maku säilyy täyteläisimmillään ja hieman makeutuu. Kiinnostavan vivahteen tuo ripaus sahramia. Noin 1,75€/annos*",
      "DateCreated": "2019-06-03T09:08:44",
      "DateModified": "2019-10-15T20:19:10",
      "TvDate": "2019-10-09T00:00:00",
      "Stamp": {
        "Name": "Testattu K-koekeittiössä",
        "Id": "1"
      },
      "RecipeUse": {
        "Name": "k-ruoka.fi",
        "Id": "1"
      },
      "resourceType": "recipe",
      "Url": "https://www.k-ruoka.fi/reseptit/paahdettu-kurpitsakeitto",
      "UrlSlug": "paahdettu-kurpitsakeitto",
      "Sort": [1, 1, 10464]
    }, {
      "Id": "10463",
      "Name": "Halloweenbagelit",
      "PieceSize": {
        "Unit": "g"
      },
      "Portions": {
        "Unit": "annosta",
        "Amount": "10"
      },
      "PreparationTime": {
        "Description": "yli 60 min",
        "TimeRange": {
          "MinTime": 60
        }
      },
      "UserPortions": {
        "$": {
          "Unit": "henkilölle"
        }
      },
      "Categories": [{
        "MainName": "Pääruoat",
        "MainId": "4",
        "SubName": "Hampurilaiset ja täytetyt leivät",
        "SubId": "86"
      }, {
        "MainName": "Sesonki",
        "MainId": "1",
        "SubName": "Halloween",
        "SubId": "126"
      }, {
        "MainName": "Teema",
        "MainId": "15",
        "SubName": "Brunssi",
        "SubId": "131"
      }, {
        "MainName": "Ruokalaji",
        "MainId": "16",
        "SubName": "Hampurilaiset ja täytetyt leivät",
        "SubId": "140"
      }, {
        "MainName": "Asiakassegmentointi (K-asiakkaat)",
        "MainId": "17",
        "SubName": "Nautiskelijat",
        "SubId": "146"
      }, {
        "MainName": "Asiakassegmentointi (K-asiakkaat)",
        "MainId": "17",
        "SubName": "Innostujat",
        "SubId": "147"
      }],
      "Pictures": ["PI10_19_halloweenbagel"],
      "PictureUrls": [{
        "Id": "PI10_19_halloweenbagel",
        "Normal": "https://k-file-storage-qa.imgix.net/f/k-ruoka/recipe/10463?w=1000&h=1000&fit=clip",
        "Original": "https://k-file-storage-qa.imgix.net/f/k-ruoka/recipe/10463"
      }],
      "Ingredients": [{
        "SubSectionHeading": "_",
        "SubSectionIngredients": [
          [{
            "Name": "vettä",
            "Amount": "2 1/2",
            "PackageRecipe": "false",
            "Unit": "dl"
          }],
          [{
            "Name": "suolaa",
            "Amount": "1",
            "PackageRecipe": "false",
            "IngredientType": "7049",
            "Unit": "tl",
            "IngredientTypeName": "Suola"
          }],
          [{
            "Name": "sokeria",
            "Amount": "1",
            "PackageRecipe": "false",
            "IngredientType": "6961",
            "Unit": "tl",
            "IngredientTypeName": "Sokeri"
          }],
          [{
            "Name": "Pirkka kuivahiivaa",
            "Ean": "6410400033036",
            "Amount": "1",
            "AmountInfo": "(11 g)",
            "PackageRecipe": "false",
            "IngredientType": "5883",
            "Unit": "ps",
            "IngredientTypeName": "Kuivahiiva"
          }],
          [{
            "Name": "vehnäjauhoja",
            "Amount": "6 1/2",
            "PackageRecipe": "false",
            "IngredientType": "7293",
            "Unit": "dl",
            "IngredientTypeName": "Vehnäjauho"
          }],
          [{
            "Name": "voita tai leivontamargariinia sulatettuna",
            "Amount": "50",
            "PackageRecipe": "false",
            "IngredientType": "7343",
            "Unit": "g",
            "IngredientTypeName": "Voi"
          }],
          [{
            "Name": "Dr. Oetker mustaa geeliväriä",
            "Amount": "1",
            "AmountInfo": "tuubi (15 g)",
            "PackageRecipe": "false",
            "Unit": ""
          }]
        ]
      }, {
        "SubSectionHeading": "Pinnalle",
        "SubSectionIngredients": [
          [{
            "Name": "unikonsiemeniä",
            "PackageRecipe": "false",
            "IngredientType": "7227",
            "IngredientTypeName": "Unikonsiemen"
          }],
          [{
            "Name": "mustia seesaminsiemeniä",
            "PackageRecipe": "false",
            "IngredientType": "6871",
            "IngredientTypeName": "Seesaminsiemen"
          }]
        ]
      }, {
        "SubSectionHeading": "Lisäksi",
        "SubSectionIngredients": [
          [{
            "Name": "Pirkka maustamatonta tuorejuustoa",
            "Ean": "6410405156051",
            "Amount": "1",
            "AmountInfo": "(180 g)",
            "PackageRecipe": "false",
            "IngredientType": "7173",
            "Unit": "rs",
            "IngredientTypeName": "Tuorejuusto, maustamaton"
          }],
          [{
            "Name": "Pirkka kylmäsavukirjolohiviipaleita",
            "Ean": "6410405225986",
            "Amount": "2",
            "AmountInfo": "(à 150 g)",
            "PackageRecipe": "false",
            "IngredientType": "5907",
            "Unit": "pkt",
            "IngredientTypeName": "Kylmäsavukirjolohi, viipale"
          }]
        ]
      }],
      "Instructions": "# Lämmitä vesi +42-asteiseksi. Lisää joukkoon suola ja sokeri. Sekoita kuivahiiva jauhojen joukkoon ja vatkaa joukkoon osa jauhoista. Alusta taikinaan vähitellen loput vehnäjauhot, lisää loppuvaiheessa myös jäähtynyt voisula. Vaivaa taikinaa, kunnes se irtoaa käsistä ja kulhon reunoista.\r\n# Jaa taikina kahteen osaan ja sekoita toiseen puoleen taikinaa geeliväri.  Kohota taikinoita kulhoissa liinan alla lämpimässä paikassa 20-30 minuuttia.\r\n# Leivo taikinat jauhotetulla pöydällä pitkiksi tangoiksi (n. 50 cm). Yhdistä tangot vierekkäin ja jaa 10 palaan. Pyöritä palat pulliksi ja kohota liinan alla noin 15 minuuttia.\r\n# Kuumenna vesi kiehuvaksi isossa kattilassa. Paina pullan keskelle reikä peukalolla ja venytä rinkeliksi. Pudota enintään kolme rinkeliä kerrallaan veteen ja keitä 30-40 sekuntia. Nosta rinkelit reikäkauhalla pellille leivinpaperin päälle ja ripottele päälle siemeniä.\r\n# Paista 225-asteisen uunin keskitasolla noin 15 minuuttia eli kunnes vaalea osa on ruskistunut.\r\n# Halkaise rinkelit jäähtyneinä. Paahda bagelit halutessasi. Levitä rinkelinpuolikkaille tuorejuustoa, asettele leiville kylmäsavulohiviipaleet ja nosta kannet päälle.",
      "EndNote": "*Annoshinnat on laskettu K-citymarket-ketjun hintatason mukaan 10/2019. Tuoretuotteiden hinnat vaihtelevat sesongeittain.",
      "Description": "Puoliksi mustat halloweenbagelit ovat näyttävä näky kylmäsavustetun lohen kanssa. Mausta tuorejuusto halutessasi piparjuuritahnalla. Noin 1,30 €/annos*",
      "DateCreated": "2019-05-31T13:58:17",
      "DateModified": "2019-09-18T09:58:00",
      "Stamp": {
        "Name": "Testattu K-koekeittiössä",
        "Id": "1"
      },
      "RecipeUse": {
        "Name": "k-ruoka.fi",
        "Id": "1"
      },
      "resourceType": "recipe",
      "Url": "https://www.k-ruoka.fi/reseptit/halloweenbagelit",
      "UrlSlug": "halloweenbagelit",
      "Sort": [1, 1, 10463]
    }, {
      "Id": "10461",
      "Name": "Caldo verde eli portugalilainen makkarakeitto",
      "PieceSize": {
        "Unit": "g",
        "Amount": "490"
      },
      "Portions": {
        "Unit": "annosta",
        "Amount": "4"
      },
      "PreparationTime": {
        "Description": "30 - 60 min",
        "TimeRange": {
          "MinTime": 30,
          "MaxTime": 60
        }
      },
      "UserPortions": {
        "$": {
          "Unit": "henkilölle"
        }
      },
      "Categories": [{
        "MainName": "Makuja maailmalta",
        "MainId": "2",
        "SubName": "Muut maat",
        "SubId": "15"
      }, {
        "MainName": "Pääruoat",
        "MainId": "4",
        "SubName": "Keitot",
        "SubId": "27"
      }, {
        "MainName": "Pääraaka-aine",
        "MainId": "14",
        "SubName": "Makkara",
        "SubId": "113"
      }, {
        "MainName": "Ruokalaji",
        "MainId": "16",
        "SubName": "Keitot",
        "SubId": "136"
      }, {
        "MainName": "Teema",
        "MainId": "15",
        "SubName": "Mitä tänään syötäisiin?",
        "SubId": "144"
      }, {
        "MainName": "Asiakassegmentointi (K-asiakkaat)",
        "MainId": "17",
        "SubName": "Innostujat",
        "SubId": "147"
      }, {
        "MainName": "Asiakassegmentointi (K-asiakkaat)",
        "MainId": "17",
        "SubName": "Mukavuudenhaluiset ",
        "SubId": "149"
      }, {
        "MainName": "Teema",
        "MainId": "15",
        "SubName": "K-Marketin valikoima",
        "SubId": "151"
      }, {
        "MainName": "Apukategoriat",
        "MainId": "18",
        "SubName": "K-Marketin valikoima",
        "SubId": "181"
      }],
      "Pictures": ["caldo_verde_PI11_19"],
      "PictureUrls": [{
        "Id": "caldo_verde_PI11_19",
        "Normal": "https://k-file-storage-qa.imgix.net/f/k-ruoka/recipe/10461?w=1000&h=1000&fit=clip",
        "Original": "https://k-file-storage-qa.imgix.net/f/k-ruoka/recipe/10461"
      }],
      "SpecialDiets": [{
        "Id": "3",
        "Name": "Maidoton"
      }, {
        "Id": "4",
        "Name": "Gluteeniton"
      }, {
        "Id": "9",
        "Name": "Viljaton"
      }],
      "Ingredients": [{
        "SubSectionHeading": "_",
        "SubSectionIngredients": [
          [{
            "Name": "Pirkka siskonmakkaraa",
            "Amount": "2",
            "AmountInfo": "(à 250 g)",
            "PackageRecipe": "false",
            "IngredientType": "6940",
            "Unit": "pkt",
            "IngredientTypeName": "Siskonmakkara"
          }],
          [{
            "Name": "Pirkka rypsiöljyä",
            "Amount": "2",
            "PackageRecipe": "false",
            "IngredientType": "6807",
            "Unit": "rkl",
            "IngredientTypeName": "Rypsiöljy"
          }],
          [{
            "Name": "jauhoista perunaa (punainen pussi)",
            "Amount": "3",
            "AmountInfo": "(300 g)",
            "PackageRecipe": "false",
            "IngredientType": "6520",
            "Unit": "",
            "IngredientTypeName": "Peruna, jauhoinen"
          }],
          [{
            "Name": "sipuli",
            "Amount": "1",
            "AmountInfo": "(100 g)",
            "PackageRecipe": "false",
            "IngredientType": "6932",
            "Unit": "",
            "IngredientTypeName": "Sipuli"
          }],
          [{
            "Name": "valkosipulinkynttä",
            "Amount": "3",
            "PackageRecipe": "false",
            "IngredientType": "7262",
            "Unit": "",
            "IngredientTypeName": "Valkosipuli"
          }],
          [{
            "Name": "paprikajauhetta",
            "Amount": "2",
            "PackageRecipe": "false",
            "IngredientType": "6444",
            "Unit": "tl",
            "IngredientTypeName": "Paprika, jauhettu"
          }],
          [{
            "Name": "chilijauhetta)",
            "AmountInfo": "(1 tl",
            "PackageRecipe": "false",
            "IngredientType": "5352",
            "IngredientTypeName": "Chili, jauhettu"
          }],
          [{
            "Name": "vettä",
            "Amount": "1,2",
            "PackageRecipe": "false",
            "IngredientType": "15792",
            "Unit": "l",
            "IngredientTypeName": "Vesi"
          }],
          [{
            "Name": "kanafondia",
            "Amount": "2",
            "PackageRecipe": "false",
            "IngredientType": "5439",
            "Unit": "rkl",
            "IngredientTypeName": "Fondi, kana"
          }],
          [{
            "Name": "suolaa",
            "Amount": "1/2",
            "PackageRecipe": "false",
            "IngredientType": "7049",
            "Unit": "tl",
            "IngredientTypeName": "Suola"
          }],
          [{
            "Name": "Pirkka lehtikaalisuikaleita",
            "Ean": "6410405146786",
            "Amount": "2",
            "AmountInfo": "(à 125 g)",
            "PackageRecipe": "false",
            "IngredientType": "10442",
            "Unit": "ps",
            "IngredientTypeName": "Lehtikaali, suikaloitu"
          }],
          [{
            "Name": "Pirkka isoja valkoisia papuja",
            "Ean": "6410405102614",
            "Amount": "1",
            "AmountInfo": "(380 g/230 g)",
            "PackageRecipe": "false",
            "IngredientType": "6459",
            "Unit": "tlk",
            "IngredientTypeName": "Papu, valkoinen, säilyke"
          }],
          [{
            "Name": "Pirkka (Luomu)sitruunan kuori raastettuna",
            "Amount": "1",
            "PackageRecipe": "false",
            "IngredientType": "6942",
            "Unit": "",
            "IngredientTypeName": "Sitruuna, luomu"
          }]
        ]
      }],
      "Instructions": "# Purista siskonmakkarasta nokareita ja paista ne kattilassa öljyssä (1 rkl), kunnes ne ovat kauniin ruskeita. Siirrä sivuun odottamaan.\r\n# Pese, kuori ja kuutioi perunat. Hienonna sipuli ja valkosipulinkynnet.\r\n# Kuumenna loppu öljy kattilassa. Lisää paprikajauhe ja halutessasi chilijauhe. Lisää perunat ja sipulit ja kuullota niitä sekoittaen noin 5 minuuttia. Kaada kattilaan 1/2 l vettä ja lisää fondi. Keitä, kunnes perunat ovat pehmenneet.\r\n# Soseuta perunat survimella tai sauvasekoittimella. Lisää kattilaan loput vedestä, suola, lehtikaalisuikaleet sekä pavut. Kuumenna kiehuvaksi ja lisää siskonmakkarat sekä sitruunankuoriraaste. Peitä kannella ja anna hautua muutama minuutti.",
      "EndNote": "*Annoshinnat on laskettu K-citymarket-ketjun hintatason mukaan 10/2019. Tuoretuotteiden hinnat vaihtelevat sesongeittain.",
      "Description": "Caldo verde tarkoittaa portugaliksi vihreää lientä. Keitto saa värinsä lehtikaalista ja ruokaisuutta tuovat siskonmakkara sekä valkoiset pavut. Noin 3,10 €/annos*",
      "DateCreated": "2019-05-31T10:25:09",
      "DateModified": "2019-10-11T14:10:40",
      "TvDate": "2019-11-19T00:00:00",
      "Stamp": {
        "Name": "Testattu K-koekeittiössä",
        "Id": "1"
      },
      "RecipeUse": {
        "Name": "k-ruoka.fi",
        "Id": "1"
      },
      "resourceType": "recipe",
      "Url": "https://www.k-ruoka.fi/reseptit/caldo-verde-eli-portugalilainen-makkarakeitto",
      "UrlSlug": "caldo-verde-eli-portugalilainen-makkarakeitto",
      "Sort": [1, 1, 10461]
    }, {
      "Id": "10460",
      "Name": "Karitsa-kikhernecurry",
      "PieceSize": {
        "Unit": "g",
        "Amount": "300"
      },
      "Portions": {
        "Unit": "annosta",
        "Amount": "4"
      },
      "PreparationTime": {
        "Description": "30 - 60 min",
        "TimeRange": {
          "MinTime": 30,
          "MaxTime": 60
        }
      },
      "UserPortions": {
        "$": {
          "Unit": "henkilölle"
        }
      },
      "Categories": [{
        "MainName": "Pääraaka-aine",
        "MainId": "14",
        "SubName": "Liha",
        "SubId": "114"
      }, {
        "MainName": "Asiakassegmentointi (K-asiakkaat)",
        "MainId": "17",
        "SubName": "Nautiskelijat",
        "SubId": "146"
      }, {
        "MainName": "Asiakassegmentointi (K-asiakkaat)",
        "MainId": "17",
        "SubName": "Tiedostavat",
        "SubId": "148"
      }, {
        "MainName": "Apukategoriat",
        "MainId": "18",
        "SubName": "Helppo",
        "SubId": "153"
      }, {
        "MainName": "Makuja maailmalta",
        "MainId": "2",
        "SubName": "Intia",
        "SubId": "9"
      }, {
        "MainName": "Pääruoat",
        "MainId": "4",
        "SubName": "Jauheliharuoat",
        "SubId": "23"
      }],
      "Pictures": ["karitsa_kikher_PI11_19"],
      "PictureUrls": [{
        "Id": "karitsa_kikher_PI11_19",
        "Normal": "https://k-file-storage-qa.imgix.net/f/k-ruoka/recipe/10460?w=1000&h=1000&fit=clip",
        "Original": "https://k-file-storage-qa.imgix.net/f/k-ruoka/recipe/10460"
      }],
      "SpecialDiets": [{
        "Id": "2",
        "Name": "Laktoositon"
      }, {
        "Id": "3",
        "Name": "Maidoton"
      }, {
        "Id": "4",
        "Name": "Gluteeniton"
      }, {
        "Id": "7",
        "Name": "Kananmunaton"
      }, {
        "Id": "9",
        "Name": "Viljaton"
      }],
      "Ingredients": [{
        "SubSectionHeading": "Mausteseos",
        "SubSectionIngredients": [
          [{
            "Name": "korianterinsiemeniä",
            "Amount": "2",
            "PackageRecipe": "false",
            "IngredientType": "10050",
            "Unit": "tl",
            "IngredientTypeName": "Korianterinsiemen"
          }],
          [{
            "Name": "kokonaisia juustokuminansiemeniä",
            "Amount": "2",
            "PackageRecipe": "false",
            "IngredientType": "5593",
            "Unit": "tl",
            "IngredientTypeName": "Juustokumina"
          }],
          [{
            "Name": "kokonaisia kardemummansiemeniä",
            "Amount": "2",
            "PackageRecipe": "false",
            "IngredientType": "5701",
            "Unit": "tl",
            "IngredientTypeName": "Kardemumma"
          }],
          [{
            "Name": "kokonaisia neilikoita",
            "Amount": "1/2",
            "PackageRecipe": "false",
            "IngredientType": "6341",
            "Unit": "tl",
            "IngredientTypeName": "Neilikka, kokonainen"
          }],
          [{
            "Name": "jauhettua muskottipähkinää",
            "Amount": "1/4",
            "PackageRecipe": "false",
            "IngredientType": "11630",
            "Unit": "tl",
            "IngredientTypeName": "Muskottipähkinä, jauhettu"
          }],
          [{
            "Name": "tähtianis",
            "Amount": "1",
            "PackageRecipe": "false",
            "IngredientType": "7213",
            "Unit": "",
            "IngredientTypeName": "Tähtianis"
          }],
          [{
            "Name": "laakerinlehteä",
            "Amount": "2",
            "PackageRecipe": "false",
            "IngredientType": "5918",
            "Unit": "",
            "IngredientTypeName": "Laakerinlehti"
          }]
        ]
      }, {
        "SubSectionHeading": "Curry",
        "SubSectionIngredients": [
          [{
            "Name": "sipulia",
            "Amount": "2",
            "AmountInfo": "(200 g)",
            "PackageRecipe": "false",
            "IngredientType": "6932",
            "Unit": "",
            "IngredientTypeName": "Sipuli"
          }],
          [{
            "Name": "valkosipulinkynttä",
            "Amount": "2",
            "PackageRecipe": "false",
            "IngredientType": "7262",
            "Unit": "",
            "IngredientTypeName": "Valkosipuli"
          }],
          [{
            "Name": "pieni pala (n. 4 cm) inkivääriä",
            "PackageRecipe": "false",
            "IngredientType": "5567",
            "IngredientTypeName": "Inkivääri, tuore"
          }],
          [{
            "Name": "kypsää tomaattia",
            "Amount": "2",
            "AmountInfo": "(200 g)",
            "PackageRecipe": "false",
            "IngredientType": "7124",
            "Unit": "",
            "IngredientTypeName": "Tomaatti"
          }],
          [{
            "Name": "rypsiöljyä",
            "Amount": "1",
            "PackageRecipe": "false",
            "IngredientType": "6807",
            "Unit": "rkl",
            "IngredientTypeName": "Rypsiöljy"
          }],
          [{
            "Name": "(luomu) karitsan jauhelihaa",
            "Amount": "400",
            "PackageRecipe": "false",
            "IngredientType": "9402",
            "Unit": "g",
            "IngredientTypeName": "Karitsan jauheliha"
          }],
          [{
            "Name": "chilijauhetta",
            "Amount": "1",
            "PackageRecipe": "false",
            "IngredientType": "5352",
            "Unit": "tl",
            "IngredientTypeName": "Chili, jauhettu"
          }],
          [{
            "Name": "suolaa",
            "Amount": "1 1/4",
            "PackageRecipe": "false",
            "IngredientType": "7049",
            "Unit": "tl",
            "IngredientTypeName": "Suola"
          }],
          [{
            "Name": "vettä",
            "Amount": "4",
            "PackageRecipe": "false",
            "IngredientType": "15792",
            "Unit": "dl",
            "IngredientTypeName": "Vesi"
          }],
          [{
            "Name": "Pirkka Luomu kikherneitä",
            "Ean": "6410405088383",
            "Amount": "1",
            "AmountInfo": "(380/230 g)",
            "PackageRecipe": "false",
            "IngredientType": "5797",
            "Unit": "tlk",
            "IngredientTypeName": "Kikherne, säilyke"
          }],
          [{
            "Name": "Pirkka Luomu babypinaattia",
            "Ean": "6410405170491",
            "Amount": "2",
            "AmountInfo": "(à 65 g)",
            "PackageRecipe": "false",
            "IngredientType": "12738",
            "Unit": "ps",
            "IngredientTypeName": "Pinaatti, baby, luomu"
          }]
        ]
      }],
      "Instructions": "# Paahda mausteseoksen mausteita kuivalla pannulla, kunnes niistä irtoaa tuoksua. Siirrä sivuun jäähtymään hetkeksi ja aja mausteet jauheeksi sauvasekoittimen myllyssä tai jauha ne isossa morttelissa. Siivilöi seos.\r\n# Kuori ja hienonna keltasipulit. Kuutioi tomaatit. Raasta pieneen kulhoon valkosipulit ja inkivääri, sekoita raasteet keskenään.\r\n# Kuumenna öljy paistokasarissa tai padassa. Ruskista siinä karitsan jauheliha kahdessa erässä ja siirrä jauheliha sivuun. Jätä karitsasta irronnutta rasvaa pataan, jos mahdollista.\r\n# Kuullota padassa sipulit. Lisää valkosipuli-inkivääritahna, chilijauhe, mausteseos ja suola. Paista sekoitellen muutama minuutti ja lisää sitten tomaatit, jauheliha ja valutetut kikherneet.\r\n# Lisää vesi ja kuumenna kiehuvaksi. Anna hautua kannen alla 15 minuuttia. Lisää lopuksi pinaatti. Tarjoa curry keitetyn riisin kanssa.",
      "EndNote": "Voit myös käyttää jauhettua juustokuminaa. *Annoshinnat on laskettu K-citymarket-ketjun hintatason mukaan 10/2019. Tuoretuotteiden hinnat vaihtelevat sesongeittain.",
      "Description": "Lempeän mausteinen karitsa-kikhernecurry syntyy kätevästi karitsan jauhelihasta. Tarjoa lisäksi keitettyä riisiä. Noin 3,65 €/annos*",
      "DateCreated": "2019-05-31T10:21:58",
      "DateModified": "2019-10-02T10:00:46",
      "Stamp": {
        "Name": "Testattu K-koekeittiössä",
        "Id": "1"
      },
      "RecipeUse": {
        "Name": "k-ruoka.fi",
        "Id": "1"
      },
      "resourceType": "recipe",
      "Url": "https://www.k-ruoka.fi/reseptit/karitsa-kikhernecurry",
      "UrlSlug": "karitsa-kikhernecurry",
      "Sort": [1, 1, 10460]
    }, {
      "Id": "10458",
      "Name": "Pad thai eli paistetut nuudelit",
      "PieceSize": {
        "Unit": "g",
        "Amount": "300"
      },
      "Portions": {
        "Unit": "annosta",
        "Amount": "2"
      },
      "PreparationTime": {
        "Description": "alle 30 min",
        "TimeRange": {
          "MinTime": 15,
          "MaxTime": 30
        }
      },
      "UserPortions": {
        "$": {
          "Unit": "henkilölle"
        }
      },
      "Categories": [{
        "MainName": "Pääruoat",
        "MainId": "4",
        "SubName": "Wokit",
        "SubId": "88"
      }, {
        "MainName": "Apukategoriat",
        "MainId": "18",
        "SubName": "Lyhyt aktiivinen valmistusaika ",
        "SubId": "157"
      }, {
        "MainName": "Makuja maailmalta",
        "MainId": "2",
        "SubName": "Thaimaa",
        "SubId": "107"
      }, {
        "MainName": "Teema",
        "MainId": "15",
        "SubName": "Mitä tänään syötäisiin?",
        "SubId": "144"
      }, {
        "MainName": "Asiakassegmentointi (K-asiakkaat)",
        "MainId": "17",
        "SubName": "Nautiskelijat",
        "SubId": "146"
      }, {
        "MainName": "Asiakassegmentointi (K-asiakkaat)",
        "MainId": "17",
        "SubName": "Innostujat",
        "SubId": "147"
      }, {
        "MainName": "Apukategoriat",
        "MainId": "18",
        "SubName": "Helppo",
        "SubId": "153"
      }, {
        "MainName": "Apukategoriat",
        "MainId": "18",
        "SubName": "Trendikäs ",
        "SubId": "156"
      }],
      "Pictures": ["pad_thai_nuud_PI11_19"],
      "PictureUrls": [{
        "Id": "pad_thai_nuud_PI11_19",
        "Normal": "https://k-file-storage-qa.imgix.net/f/k-ruoka/recipe/10458?w=1000&h=1000&fit=clip",
        "Original": "https://k-file-storage-qa.imgix.net/f/k-ruoka/recipe/10458"
      }],
      "VideoUrl": "https://youtu.be/ALG8qqTmXmE",
      "VideoEmbedUrl": "<iframe width=\"1136\" height=\"639\" src=\"https://www.youtube.com/embed/ALG8qqTmXmE\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>",
      "Ingredients": [{
        "SubSectionHeading": "_",
        "SubSectionIngredients": [
          [{
            "Name": "Pirkka luomu tofua",
            "Amount": "1/2",
            "AmountInfo": "(à 250 g)",
            "PackageRecipe": "false",
            "Unit": "pkt"
          }],
          [{
            "Name": "Pirkka broilerin fileesuikaleita, maustamaton",
            "Ean": "6410405086686",
            "Amount": "1/2",
            "AmountInfo": "(à 250 g)",
            "PackageRecipe": "false",
            "IngredientType": "5327",
            "Unit": "rs",
            "IngredientTypeName": "Broilerinfileesuikale, maustamaton"
          }],
          [{
            "Name": "rypsiöljyä",
            "Amount": "2",
            "PackageRecipe": "false",
            "IngredientType": "6807",
            "Unit": "rkl",
            "IngredientTypeName": "Rypsiöljy"
          }],
          [{
            "Name": "valkosipulinkynttä hienonnettuna",
            "Amount": "2-3",
            "PackageRecipe": "false",
            "IngredientType": "7262",
            "Unit": "",
            "IngredientTypeName": "Valkosipuli"
          }],
          [{
            "Name": "kananmuna",
            "Amount": "1",
            "PackageRecipe": "false",
            "IngredientType": "5683",
            "Unit": "",
            "IngredientTypeName": "Kananmuna"
          }],
          [{
            "Name": "mungpavun ituja",
            "Amount": "2",
            "AmountInfo": "(n. 80 g)",
            "PackageRecipe": "false",
            "IngredientType": "8496",
            "Unit": "dl",
            "IngredientTypeName": "Ituja, mungpapu"
          }],
          [{
            "Name": "porkkana suikaleina",
            "Amount": "1",
            "AmountInfo": "(n. 100 g)",
            "PackageRecipe": "false",
            "IngredientType": "6582",
            "Unit": "",
            "IngredientTypeName": "Porkkana"
          }],
          [{
            "Name": "kevätsipulin varsi",
            "Amount": "1",
            "PackageRecipe": "false",
            "Unit": ""
          }],
          [{
            "Name": "vettä",
            "Amount": "3 1/2",
            "PackageRecipe": "false",
            "IngredientType": "15792",
            "Unit": "dl",
            "IngredientTypeName": "Vesi"
          }],
          [{
            "Name": "Pirkka riisinuudeleita",
            "Amount": "1/2",
            "AmountInfo": "(à 180g)",
            "PackageRecipe": "false",
            "IngredientType": "6735",
            "Unit": "pkt",
            "IngredientTypeName": "Riisinuudeli"
          }]
        ]
      }, {
        "SubSectionHeading": "Kastike",
        "SubSectionIngredients": [
          [{
            "Name": "osterikastiketta",
            "Amount": "2",
            "PackageRecipe": "false",
            "IngredientType": "6411",
            "Unit": "rkl",
            "IngredientTypeName": "Osterikastike"
          }],
          [{
            "Name": "sokeria",
            "Amount": "1 1/2",
            "PackageRecipe": "false",
            "IngredientType": "6961",
            "Unit": "rkl",
            "IngredientTypeName": "Sokeri"
          }],
          [{
            "Name": "viinietikkaa",
            "Amount": "1 1/2",
            "PackageRecipe": "false",
            "IngredientType": "7274",
            "Unit": "rkl",
            "IngredientTypeName": "Valkoviinietikka"
          }],
          [{
            "Name": "kalakastiketta",
            "Amount": "1 1/2",
            "PackageRecipe": "false",
            "IngredientType": "5656",
            "Unit": "rkl",
            "IngredientTypeName": "Kalakastike"
          }]
        ]
      }, {
        "SubSectionHeading": "Lisäksi",
        "SubSectionIngredients": [
          [{
            "Name": "Pirkka kuorittuja maapähkinöitä",
            "Ean": "6410405118141",
            "Amount": "2",
            "PackageRecipe": "false",
            "IngredientType": "6023",
            "Unit": "rkl",
            "IngredientTypeName": "Maapähkinä"
          }],
          [{
            "Name": "limetinlohkoja",
            "PackageRecipe": "false",
            "IngredientType": "5997",
            "IngredientTypeName": "Limetti"
          }],
          [{
            "Name": "Pirkka mieto punainen chili",
            "Ean": "6410405091857",
            "Amount": "1",
            "PackageRecipe": "false",
            "IngredientType": "5355",
            "Unit": "",
            "IngredientTypeName": "Chili, punainen"
          }]
        ]
      }],
      "Instructions": "# Viipaloi tofu, kuivaa palat talouspaperilla ja leikkaa pieniksi kuutioiksi. Ruskista tofukuutiot, broileri ja valkosipulit öljyssä. Lisää kananmuna ja kypsennä sekoitellen. Siirrä sivuun itujen, porkkanasuikaleiden ja paloitellun (noin 3 cm) kevätsipulin kanssa.\r\n# Kaada vesi ja keskenään sekoitetut kastikkeen ainekset kuumalle pannulle. Kiehauta ja lisää joukkoon nuudelit. Keitä sekoitellen, kunnes nuudelit ovat kypsiä ja vesi on lähes täysin haihtunut.\r\n# Kaada joukkoon kaikki muut ainekset ja kuumenna vielä hetki. Maista ja lisää tarvittaessa suolaisuutta (kalakastiketta). Lisää annoksiin lopuksi rouhittuja maapähkinöitä, limetinlohkoja ja hienonnettua chiliä.",
      "EndNote": "Huom! Anna nuudelien kiehua niin kauan, että neste on haihtunut. Lisää kaikki ainekset pannulle ja pyöräytä ainekset kuumalla pannulla sekaisin. *Annoshinnat on laskettu K-citymarket-ketjun hintatason mukaan 10/2019. Tuoretuotteiden hinnat vaihtelevat sesongeittain.",
      "Description": "Thairuokien klassikko, pad thai, onnistuu helposti myös kotikeittiössä. Pad thai toimii myös katkaravuilla tai kasvisversiona tofun kanssa. Kokeile ja ihastu! Noin 4,55 €/annos*",
      "DateCreated": "2019-05-28T12:55:26",
      "DateModified": "2019-11-08T14:39:25",
      "TvDate": "2019-11-07T00:00:00",
      "Stamp": {
        "Name": "Testattu K-koekeittiössä",
        "Id": "1"
      },
      "RecipeUse": {
        "Name": "k-ruoka.fi",
        "Id": "1"
      },
      "resourceType": "recipe",
      "Url": "https://www.k-ruoka.fi/reseptit/pad-thai-eli-paistetut-nuudelit",
      "UrlSlug": "pad-thai-eli-paistetut-nuudelit",
      "Sort": [1, 1, 10458]
    }, {
      "Id": "10457",
      "Name": "Banaanileipä eli banana bread",
      "PieceSize": {
        "Unit": "g"
      },
      "Portions": {
        "Unit": "annosta",
        "Amount": "10"
      },
      "PreparationTime": {
        "Description": "yli 60 min",
        "TimeRange": {
          "MinTime": 60
        }
      },
      "UserPortions": {
        "$": {
          "Unit": "henkilölle"
        }
      },
      "Categories": [{
        "MainName": "Makuja maailmalta",
        "MainId": "2",
        "SubName": "Pohjois-Amerikka",
        "SubId": "8"
      }, {
        "MainName": "Pääruoat",
        "MainId": "4",
        "SubName": "Kastikkeet",
        "SubId": "26"
      }, {
        "MainName": "Makeat leivonnaiset",
        "MainId": "7",
        "SubName": "Kahvikakut",
        "SubId": "50"
      }, {
        "MainName": "Aamu-,väli- ja iltapalat",
        "MainId": "11",
        "SubName": "Muut aamu-, väli- ja iltapalat",
        "SubId": "72"
      }, {
        "MainName": "Makeat leivonnaiset",
        "MainId": "7",
        "SubName": "Muut kakut",
        "SubId": "98"
      }, {
        "MainName": "Iltapalat",
        "MainId": "21",
        "SubName": "Muut",
        "SubId": "197"
      }, {
        "MainName": "Teema",
        "MainId": "15",
        "SubName": "Brunssi",
        "SubId": "131"
      }, {
        "MainName": "Asiakassegmentointi (K-asiakkaat)",
        "MainId": "17",
        "SubName": "Nautiskelijat",
        "SubId": "146"
      }, {
        "MainName": "Asiakassegmentointi (K-asiakkaat)",
        "MainId": "17",
        "SubName": "Innostujat",
        "SubId": "147"
      }, {
        "MainName": "Apukategoriat",
        "MainId": "18",
        "SubName": "Helppo",
        "SubId": "153"
      }, {
        "MainName": "Apukategoriat",
        "MainId": "18",
        "SubName": "Trendikäs ",
        "SubId": "156"
      }, {
        "MainName": "Aamupalat",
        "MainId": "20",
        "SubName": "Muut",
        "SubId": "190"
      }],
      "Pictures": ["PI9_19_banaanileipa"],
      "PictureUrls": [{
        "Id": "PI9_19_banaanileipa",
        "Normal": "https://k-file-storage-qa.imgix.net/f/k-ruoka/recipe/10457?w=1000&h=1000&fit=clip",
        "Original": "https://k-file-storage-qa.imgix.net/f/k-ruoka/recipe/10457"
      }],
      "Ingredients": [{
        "SubSectionHeading": "_",
        "SubSectionIngredients": [
          [{
            "Name": "kypsää banaania",
            "Amount": "3",
            "AmountInfo": "(450 g)",
            "PackageRecipe": "false",
            "IngredientType": "5262",
            "Unit": "",
            "IngredientTypeName": "Banaani"
          }],
          [{
            "Name": "voisulaa",
            "Amount": "100",
            "PackageRecipe": "false",
            "IngredientType": "7343",
            "Unit": "g",
            "IngredientTypeName": "Voi"
          }],
          [{
            "Name": "fariinisokeria",
            "Amount": "1",
            "PackageRecipe": "false",
            "IngredientType": "6963",
            "Unit": "dl",
            "IngredientTypeName": "Sokeri, fariini"
          }],
          [{
            "Name": "sokeria",
            "Amount": "1/2",
            "PackageRecipe": "false",
            "IngredientType": "6961",
            "Unit": "dl",
            "IngredientTypeName": "Sokeri"
          }],
          [{
            "Name": "kananmuna",
            "Amount": "1",
            "PackageRecipe": "false",
            "IngredientType": "5683",
            "Unit": "",
            "IngredientTypeName": "Kananmuna"
          }],
          [{
            "Name": "vehnäjauhoja",
            "Ean": "6410405193414",
            "Amount": "3",
            "PackageRecipe": "false",
            "IngredientType": "7293",
            "Unit": "dl",
            "IngredientTypeName": "Vehnäjauho"
          }],
          [{
            "Name": "kaurahiutaleita",
            "Ean": "6410400041178",
            "Amount": "2",
            "PackageRecipe": "false",
            "IngredientType": "5741",
            "Unit": "dl",
            "IngredientTypeName": "Kaurahiutale"
          }],
          [{
            "Name": "Pirkka pekaanipähkinää",
            "Ean": "6410405126641",
            "Amount": "1",
            "PackageRecipe": "false",
            "IngredientType": "6497",
            "Unit": "dl",
            "IngredientTypeName": "Pekaanipähkinä"
          }],
          [{
            "Name": "rusinoita",
            "Amount": "1",
            "PackageRecipe": "false",
            "IngredientType": "6796",
            "Unit": "dl",
            "IngredientTypeName": "Rusina"
          }],
          [{
            "Name": "vaniljasokeria",
            "Ean": "6410402023769",
            "Amount": "2",
            "PackageRecipe": "false",
            "IngredientType": "7280",
            "Unit": "tl",
            "IngredientTypeName": "Vaniljasokeri"
          }],
          [{
            "Name": "kanelia",
            "Ean": "6410405055644",
            "Amount": "1",
            "PackageRecipe": "false",
            "IngredientType": "5689",
            "Unit": "tl",
            "IngredientTypeName": "Kaneli, jauhettu"
          }],
          [{
            "Name": "kardemummaa",
            "Ean": "6410402024612",
            "Amount": "1",
            "PackageRecipe": "false",
            "IngredientType": "5701",
            "Unit": "tl",
            "IngredientTypeName": "Kardemumma"
          }],
          [{
            "Name": "Pirkka ruokasoodaa",
            "Ean": "6410402023745",
            "Amount": "1",
            "PackageRecipe": "false",
            "IngredientType": "6794",
            "Unit": "tl",
            "IngredientTypeName": "Ruokasooda"
          }],
          [{
            "Name": "suolaa",
            "Amount": "1",
            "PackageRecipe": "false",
            "IngredientType": "7049",
            "Unit": "tl",
            "IngredientTypeName": "Suola"
          }]
        ]
      }, {
        "SubSectionHeading": "Pinnalle",
        "SubSectionIngredients": [
          [{
            "Name": "Pirkka vaahterasiirappia",
            "Ean": "6410402018604",
            "Amount": "2",
            "PackageRecipe": "false",
            "IngredientType": "7233",
            "Unit": "rkl",
            "IngredientTypeName": "Vaahterasiirappi"
          }, {
            "Name": "siirappia",
            "PackageRecipe": "false",
            "IngredientType": "6898",
            "IngredientTypeName": "Siirappi, tumma"
          }],
          [{
            "Name": "banaani",
            "Amount": "1",
            "PackageRecipe": "false",
            "IngredientType": "5262",
            "Unit": "",
            "IngredientTypeName": "Banaani"
          }],
          [{
            "Name": "Pirkka pekaanipähkinöitä",
            "Ean": "6410405126641",
            "Amount": "1/2",
            "PackageRecipe": "false",
            "IngredientType": "6497",
            "Unit": "dl",
            "IngredientTypeName": "Pekaanipähkinä"
          }]
        ]
      }],
      "Instructions": "# Voitele suorakaiteen mallinen, litran vetoinen leipävuoka.\r\n# Muussaa banaanit haarukalla.\r\n# Sekoita kulhossa hieman jäähtynyt voisula, sokerit, muna ja banaanisose.\r\n# Sekoita kuivat aineet keskenään ja kääntele taikinaan.\r\n# Kaada taikina vuokaan. Valuta leivän päälle siirappia ja koristele leipä halkaistulla banaanilla ja pähkinöillä.\r\n# Paista banaanileipää 175-asteisen uunin keskitasolla 50 minuuttia. Peitä leipä tarvittaessa leivinpaperilla, jos se tummuu liikaa.\r\n# Anna banaanileivän jäähtyä ennen kuin irrotat sen vuoasta.\r\n# Tarjoa banaanileipä esimerkiksi pähkinävoin kanssa.",
      "EndNote": "*Annoshinnat on laskettu K-citymarket-ketjun hintatason mukaan 10/2019. Tuoretuotteiden hinnat vaihtelevat sesongeittain.",
      "Description": "Banaanileipä eli banana bread on mehevä jenkkiherkku, joka nimestään huolimatta on enemmän kakku kuin leipä. Ihanan helppo taikina valmistuu nopeasti sekoittamalla. Banaanileipä on täydellinen loppusijoituspaikka kypsille pantteribanaaneille! Noin 0,65 €/annos*",
      "DateCreated": "2019-05-27T15:22:58",
      "DateModified": "2019-11-01T18:37:58",
      "Stamp": {
        "Name": "Testattu K-koekeittiössä",
        "Id": "1"
      },
      "RecipeUse": {
        "Name": "k-ruoka.fi",
        "Id": "1"
      },
      "resourceType": "recipe",
      "Url": "https://www.k-ruoka.fi/reseptit/banaanileipa-eli-banana-bread",
      "UrlSlug": "banaanileipa-eli-banana-bread",
      "Sort": [1, 1, 10457]
    }, {
      "Id": "10455",
      "Name": "Soseutettu sipulikeitto",
      "PieceSize": {
        "Unit": "g",
        "Amount": "350"
      },
      "Portions": {
        "Unit": "annosta",
        "Amount": "4"
      },
      "PreparationTime": {
        "Description": "30 - 60 min",
        "TimeRange": {
          "MinTime": 30,
          "MaxTime": 60
        }
      },
      "UserPortions": {
        "$": {
          "Unit": "henkilölle"
        }
      },
      "Categories": [{
        "MainName": "Alkuruoat",
        "MainId": "3",
        "SubName": "Keitot",
        "SubId": "17"
      }, {
        "MainName": "Pääruoat",
        "MainId": "4",
        "SubName": "Kasvisruoat",
        "SubId": "22"
      }, {
        "MainName": "Asiakassegmentointi (K-asiakkaat)",
        "MainId": "17",
        "SubName": "Tiedostavat",
        "SubId": "148"
      }, {
        "MainName": "Teema",
        "MainId": "15",
        "SubName": "K-Marketin valikoima",
        "SubId": "151"
      }, {
        "MainName": "Apukategoriat",
        "MainId": "18",
        "SubName": "K-Marketin valikoima",
        "SubId": "181"
      }, {
        "MainName": "Pääruoat",
        "MainId": "4",
        "SubName": "Keitot",
        "SubId": "27"
      }, {
        "MainName": "Pääraaka-aine",
        "MainId": "14",
        "SubName": "Kasvis",
        "SubId": "115"
      }, {
        "MainName": "Sesonki",
        "MainId": "1",
        "SubName": "Sadonkorjuu",
        "SubId": "124"
      }, {
        "MainName": "Ruokalaji",
        "MainId": "16",
        "SubName": "Keitot",
        "SubId": "136"
      }, {
        "MainName": "Teema",
        "MainId": "15",
        "SubName": "Mitä tänään syötäisiin?",
        "SubId": "144"
      }, {
        "MainName": "Asiakassegmentointi (K-asiakkaat)",
        "MainId": "17",
        "SubName": "Nautiskelijat",
        "SubId": "146"
      }],
      "Pictures": ["PI10_19_soseutettu_sipu"],
      "PictureUrls": [{
        "Id": "PI10_19_soseutettu_sipu",
        "Normal": "https://k-file-storage-qa.imgix.net/f/k-ruoka/recipe/10455?w=1000&h=1000&fit=clip",
        "Original": "https://k-file-storage-qa.imgix.net/f/k-ruoka/recipe/10455"
      }],
      "VideoUrl": "https://youtu.be/-T6FemKggd8",
      "VideoEmbedUrl": "<iframe width=\"1136\" height=\"639\" src=\"https://www.youtube.com/embed/-T6FemKggd8\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>",
      "SpecialDiets": [{
        "Id": "3",
        "Name": "Maidoton"
      }, {
        "Id": "7",
        "Name": "Kananmunaton"
      }],
      "Ingredients": [{
        "SubSectionHeading": "_",
        "SubSectionIngredients": [
          [{
            "Name": "Pirkka sipulia",
            "Ean": "6410402008933",
            "Amount": "1",
            "PackageRecipe": "false",
            "IngredientType": "6932",
            "Unit": "kg",
            "IngredientTypeName": "Sipuli"
          }],
          [{
            "Name": "valkosipulinkynttä",
            "Amount": "5",
            "PackageRecipe": "false",
            "IngredientType": "7262",
            "Unit": "",
            "IngredientTypeName": "Valkosipuli"
          }],
          [{
            "Name": "Pirkka ekstra-neitsytoliiviöljyä",
            "Ean": "6410402024759",
            "Amount": "5",
            "PackageRecipe": "false",
            "IngredientType": "5405",
            "Unit": "rkl",
            "IngredientTypeName": "Ekstra neitsytoliiviöljy"
          }],
          [{
            "Name": "hunajaa",
            "Amount": "2",
            "PackageRecipe": "false",
            "IngredientType": "5550",
            "Unit": "rkl",
            "IngredientTypeName": "Hunaja, juokseva"
          }],
          [{
            "Name": "tuoretta timjamia",
            "Amount": "2",
            "PackageRecipe": "false",
            "IngredientType": "7119",
            "Unit": "rkl",
            "IngredientTypeName": "Timjami, tuore"
          }],
          [{
            "Name": "mustapippuria",
            "PackageRecipe": "false",
            "IngredientType": "6269",
            "Unit": "ripaus",
            "IngredientTypeName": "Mustapippuri, kokonainen"
          }],
          [{
            "Name": "Pirkka I-olutta (2,7%)",
            "Ean": "6410405069559",
            "Amount": "2",
            "AmountInfo": "(à 0,33 l)",
            "PackageRecipe": "false",
            "IngredientType": "12023",
            "Unit": "tlk",
            "IngredientTypeName": "Olut, lager, vaalea"
          }],
          [{
            "Name": "vettä",
            "Amount": "5",
            "PackageRecipe": "false",
            "IngredientType": "15792",
            "Unit": "dl",
            "IngredientTypeName": "Vesi"
          }],
          [{
            "Name": "kasvisliemikuutiota",
            "Amount": "2",
            "PackageRecipe": "false",
            "IngredientType": "5991",
            "Unit": "",
            "IngredientTypeName": "Liemikuutio, kasvis"
          }]
        ]
      }, {
        "SubSectionHeading": "Päälle",
        "SubSectionIngredients": [
          [{
            "Name": "valkosipulinkynsi",
            "Amount": "1",
            "PackageRecipe": "false",
            "IngredientType": "7262",
            "Unit": "",
            "IngredientTypeName": "Valkosipuli"
          }],
          [{
            "Name": "Pirkka ranskalaista jättipatonkia",
            "Amount": "1/2",
            "AmountInfo": "(à 350 g)",
            "PackageRecipe": "false",
            "IngredientType": "6019",
            "Unit": "",
            "IngredientTypeName": "Maalaisleipä"
          }],
          [{
            "Name": "Pirkka ekstra-neitsytoliiviöljyä",
            "Ean": "6410402024759",
            "Amount": "3",
            "PackageRecipe": "false",
            "IngredientType": "5405",
            "Unit": "rkl",
            "IngredientTypeName": "Ekstra neitsytoliiviöljy"
          }]
        ]
      }],
      "Instructions": "# Kuori ja leikkaa sipulit ohuiksi renkaiksi. Kuori ja viipaloi valkosipulinkynnet.\r\n# Kuumenna öljy isossa kattilassa. Kuullota sipuleita noin 10 minuuttia. Lisää hunaja, timjami sekä pippuri ja kääntele sekaisin.\r\n# Kaada kattilaan olut, vesi ja murennetut kasvisliemikuutiot ja anna kiehua hiljalleen puoli tuntia ilman kantta.\r\n# Tee krutongit. Leikkaa valkosipulinkynsi viipaleiksi ja patonki noin sentin paksuisiksi paloiksi. Kuumenna öljy ja valkosipuli pannulla. Paista leipäkuutiot rapeiksi öljyssä.\r\n# Soseuta sipulikeitto sauvasekoittimella. Tarkista maku ja lisää tarvittaessa suolaa.",
      "EndNote": "*Annoshinnat on laskettu K-citymarket-ketjun hintatason mukaan 10/2019. Tuoretuotteiden hinnat vaihtelevat sesongeittain.",
      "Description": "Soseutettu sipulikeitto on samettisen pehmoista lusikoitavaa! Soppa vain paranee, jos jaksat haudutella vähän kauemmin. Noin 1,45 €/annos*",
      "DateCreated": "2019-05-27T13:44:49",
      "DateModified": "2019-10-15T20:09:13",
      "TvDate": "2019-10-14T00:00:00",
      "Stamp": {
        "Name": "Testattu K-koekeittiössä",
        "Id": "1"
      },
      "RecipeUse": {
        "Name": "k-ruoka.fi",
        "Id": "1"
      },
      "resourceType": "recipe",
      "Url": "https://www.k-ruoka.fi/reseptit/soseutettu-sipulikeitto",
      "UrlSlug": "soseutettu-sipulikeitto",
      "Sort": [1, 1, 10455]
    }, {
      "Id": "10454",
      "Name": "Punajuuri-vuohenjuustokeitto",
      "PieceSize": {
        "Unit": "g",
        "Amount": "425"
      },
      "Portions": {
        "Unit": "annosta",
        "Amount": "4"
      },
      "PreparationTime": {
        "Description": "30 - 60 min",
        "TimeRange": {
          "MinTime": 30,
          "MaxTime": 60
        }
      },
      "UserPortions": {
        "$": {
          "Unit": "henkilölle"
        }
      },
      "Categories": [{
        "MainName": "Asiakassegmentointi (K-asiakkaat)",
        "MainId": "17",
        "SubName": "Mukavuudenhaluiset ",
        "SubId": "149"
      }, {
        "MainName": "Teema",
        "MainId": "15",
        "SubName": "K-Marketin valikoima",
        "SubId": "151"
      }, {
        "MainName": "Apukategoriat",
        "MainId": "18",
        "SubName": "K-Marketin valikoima",
        "SubId": "181"
      }, {
        "MainName": "Makuja maailmalta",
        "MainId": "2",
        "SubName": "Skandinavia",
        "SubId": "105"
      }, {
        "MainName": "Pääraaka-aine",
        "MainId": "14",
        "SubName": "Kasvis",
        "SubId": "115"
      }, {
        "MainName": "Sesonki",
        "MainId": "1",
        "SubName": "Sadonkorjuu",
        "SubId": "124"
      }, {
        "MainName": "Ruokalaji",
        "MainId": "16",
        "SubName": "Keitot",
        "SubId": "136"
      }, {
        "MainName": "Teema",
        "MainId": "15",
        "SubName": "Mitä tänään syötäisiin?",
        "SubId": "144"
      }, {
        "MainName": "Asiakassegmentointi (K-asiakkaat)",
        "MainId": "17",
        "SubName": "Tiedostavat",
        "SubId": "148"
      }, {
        "MainName": "Alkuruoat",
        "MainId": "3",
        "SubName": "Keitot",
        "SubId": "17"
      }, {
        "MainName": "Pääruoat",
        "MainId": "4",
        "SubName": "Kasvisruoat",
        "SubId": "22"
      }, {
        "MainName": "Pääruoat",
        "MainId": "4",
        "SubName": "Keitot",
        "SubId": "27"
      }],
      "Pictures": ["PI10_19_punajuuri_vuoh"],
      "PictureUrls": [{
        "Id": "PI10_19_punajuuri_vuoh",
        "Normal": "https://k-file-storage-qa.imgix.net/f/k-ruoka/recipe/10454?w=1000&h=1000&fit=clip",
        "Original": "https://k-file-storage-qa.imgix.net/f/k-ruoka/recipe/10454"
      }],
      "Ingredients": [{
        "SubSectionHeading": "_",
        "SubSectionIngredients": [
          [{
            "Name": "Pirkka punajuuria",
            "Ean": "6410405097248",
            "Amount": "1",
            "AmountInfo": "(1 kg)",
            "PackageRecipe": "false",
            "IngredientType": "6655",
            "Unit": "ps",
            "IngredientTypeName": "Punajuuri"
          }],
          [{
            "Name": "punasipulia",
            "Amount": "2",
            "AmountInfo": "(150 g)",
            "PackageRecipe": "false",
            "IngredientType": "6667",
            "Unit": "",
            "IngredientTypeName": "Punasipuli"
          }],
          [{
            "Name": "valkosipulinkynttä",
            "Amount": "2",
            "PackageRecipe": "false",
            "IngredientType": "7262",
            "Unit": "",
            "IngredientTypeName": "Valkosipuli"
          }],
          [{
            "Name": "Pirkka ekstra-neitsytoliiviöljyä",
            "Ean": "6410402024759",
            "Amount": "2",
            "PackageRecipe": "false",
            "IngredientType": "5405",
            "Unit": "rkl",
            "IngredientTypeName": "Ekstra neitsytoliiviöljy"
          }],
          [{
            "Name": "vettä",
            "Amount": "1",
            "PackageRecipe": "false",
            "IngredientType": "15792",
            "Unit": "l",
            "IngredientTypeName": "Vesi"
          }],
          [{
            "Name": "Knorr Luomu kasvisliemikuutiota",
            "Ean": "8714100258767",
            "Amount": "2",
            "PackageRecipe": "false",
            "IngredientType": "5991",
            "Unit": "",
            "IngredientTypeName": "Liemikuutio, kasvis"
          }],
          [{
            "Name": "vuohenmaitotuorejuustoa (Chavroux)",
            "Amount": "1",
            "AmountInfo": "(150 g)",
            "PackageRecipe": "false",
            "IngredientType": "15842",
            "Unit": "prk",
            "IngredientTypeName": "vuohenmaitotuorejuustoa (Chavroux)"
          }],
          [{
            "Name": "Pirkka laktoositonta ruokakermaa (15 %)",
            "Ean": "6410405093448",
            "Amount": "1",
            "PackageRecipe": "false",
            "IngredientType": "6775",
            "Unit": "dl",
            "IngredientTypeName": "Ruokakerma, 15 %, laktoositon"
          }],
          [{
            "Name": "hunajaa",
            "Amount": "2",
            "PackageRecipe": "false",
            "IngredientType": "5550",
            "Unit": "rkl",
            "IngredientTypeName": "Hunaja, juokseva"
          }],
          [{
            "Name": "Pirkka tummaa balsamietikkakastiketta",
            "Ean": "6410405114860",
            "Amount": "2",
            "PackageRecipe": "false",
            "IngredientType": "5259",
            "Unit": "rkl",
            "IngredientTypeName": "Balsamietikkakastike, tumma"
          }]
        ]
      }, {
        "SubSectionHeading": "Päälle",
        "SubSectionIngredients": [
          [{
            "Name": "herneenversoja",
            "Amount": "1",
            "PackageRecipe": "false",
            "IngredientType": "5527",
            "Unit": "dl",
            "IngredientTypeName": "Herneenverso"
          }],
          [{
            "Name": "Pirkka salaattisiemensekoitusta",
            "Ean": "6410405127341",
            "Amount": "1/2",
            "PackageRecipe": "false",
            "IngredientType": "6847",
            "Unit": "dl",
            "IngredientTypeName": "Siemensekoitus"
          }]
        ]
      }],
      "Instructions": "# Kuori ja paloittele punajuuret, punasipulit ja valkosipulinkynnet.\r\n# Kuullota sipulit oliiviöljyssä isossa kattilassa. Lisää punajuuret, vesi ja murennetut kasvisliemikuutiot.\r\n# Keitä punajuuria ja sipuleita kasvisliemessä noin 40 minuuttia, kunnes punajuuret ovat kypsiä.\r\n# Lisää vuohenjuusto, ruokakerma, hunaja ja balsamietikkakastike. Keitä hiljalleen sekoitellen, kunnes juusto sulaa.\r\n# Soseuta keitto sauvasekoittimella.\r\n# Koristele punajuuri-vuohenjuustokeitto herneenversoilla ja siemenillä.",
      "EndNote": "*Annoshinnat on laskettu K-citymarket-ketjun hintatason mukaan 10/2019. Tuoretuotteiden hinnat vaihtelevat sesongeittain.",
      "Description": "Punajuuri-vuohenjuustokeitto on hurmaavan pinkki soppa, johon juusto antaa upeaa täyteläisyyttä. Tätä punajuuri-vuohenjuustokeittoa teet varmasti toistekin! Noin 2,10 €/annos*",
      "DateCreated": "2019-05-27T13:19:32",
      "DateModified": "2019-10-07T12:27:16",
      "TvDate": "2019-10-11T00:00:00",
      "Stamp": {
        "Name": "Testattu K-koekeittiössä",
        "Id": "1"
      },
      "RecipeUse": {
        "Name": "k-ruoka.fi",
        "Id": "1"
      },
      "resourceType": "recipe",
      "Url": "https://www.k-ruoka.fi/reseptit/punajuuri-vuohenjuustokeitto",
      "UrlSlug": "punajuuri-vuohenjuustokeitto",
      "Sort": [1, 1, 10454]
    }, {
      "Id": "10453",
      "Name": "Uuniperunat ja lämmin kinkku-pinaattikastike",
      "PieceSize": {
        "Unit": "g",
        "Amount": "303"
      },
      "Portions": {
        "Unit": "annosta",
        "Amount": "4"
      },
      "PreparationTime": {
        "Description": "yli 60 min",
        "TimeRange": {
          "MinTime": 60
        }
      },
      "UserPortions": {
        "$": {
          "Unit": "henkilölle"
        }
      },
      "Categories": [{
        "MainName": "Pääruoat",
        "MainId": "4",
        "SubName": "Uuniruoat",
        "SubId": "30"
      }, {
        "MainName": "Makuja maailmalta",
        "MainId": "2",
        "SubName": "Suomi",
        "SubId": "106"
      }, {
        "MainName": "Ruokalaji",
        "MainId": "16",
        "SubName": "Uuniruoat",
        "SubId": "141"
      }, {
        "MainName": "Asiakassegmentointi (K-asiakkaat)",
        "MainId": "17",
        "SubName": "Mukavuudenhaluiset ",
        "SubId": "149"
      }, {
        "MainName": "Asiakassegmentointi (K-asiakkaat)",
        "MainId": "17",
        "SubName": "Vakiintuneet",
        "SubId": "150"
      }, {
        "MainName": "Teema",
        "MainId": "15",
        "SubName": "Arkiruoka",
        "SubId": "159"
      }, {
        "MainName": "Teema",
        "MainId": "15",
        "SubName": "Halpa",
        "SubId": "161"
      }, {
        "MainName": "Teema",
        "MainId": "15",
        "SubName": "Lasten makuun",
        "SubId": "165"
      }, {
        "MainName": "Pääraaka-aine",
        "MainId": "14",
        "SubName": "Peruna",
        "SubId": "207"
      }],
      "Pictures": ["uuniperunat_PI12_19"],
      "PictureUrls": [{
        "Id": "uuniperunat_PI12_19",
        "Normal": "https://k-file-storage-qa.imgix.net/f/k-ruoka/recipe/10453?w=1000&h=1000&fit=clip",
        "Original": "https://k-file-storage-qa.imgix.net/f/k-ruoka/recipe/10453"
      }],
      "SpecialDiets": [{
        "Id": "4",
        "Name": "Gluteeniton"
      }, {
        "Id": "7",
        "Name": "Kananmunaton"
      }, {
        "Id": "9",
        "Name": "Viljaton"
      }],
      "Ingredients": [{
        "SubSectionHeading": "_",
        "SubSectionIngredients": [
          [{
            "Name": "Pirkka Rosamunda uuniperunaa (violetti pussi)",
            "Ean": "6410405174253",
            "Amount": "4",
            "PackageRecipe": "false",
            "IngredientType": "7232",
            "Unit": "",
            "IngredientTypeName": "Uuniperuna"
          }]
        ]
      }, {
        "SubSectionHeading": "Kinkku-pinaattimuhennos",
        "SubSectionIngredients": [
          [{
            "Name": "valkosipulinkynttä",
            "Amount": "2",
            "PackageRecipe": "false",
            "IngredientType": "7262",
            "Unit": "",
            "IngredientTypeName": "Valkosipuli"
          }],
          [{
            "Name": "rypsiöljyä",
            "Amount": "2",
            "PackageRecipe": "false",
            "IngredientType": "6807",
            "Unit": "tl",
            "IngredientTypeName": "Rypsiöljy"
          }],
          [{
            "Name": "Pirkka lehtipinaattia (pakaste)",
            "Ean": "6410405145895",
            "Amount": "1",
            "AmountInfo": "(200 g)",
            "PackageRecipe": "false",
            "IngredientType": "12738",
            "Unit": "ps",
            "IngredientTypeName": "Pinaatti, baby, luomu"
          }],
          [{
            "Name": "Arla Lempi Fraîche -kermaa",
            "Amount": "1",
            "AmountInfo": "(2 dl)",
            "PackageRecipe": "false",
            "IngredientType": "16082",
            "Unit": "tlk",
            "IngredientTypeName": "Arla fraiche kerma, 30 %"
          }],
          [{
            "Name": "Pirkka Parhaat ylikypsää uunikinkkua",
            "Ean": "6410405155733",
            "Amount": "1",
            "AmountInfo": "(250 g)",
            "PackageRecipe": "false",
            "IngredientType": "15038",
            "Unit": "pkt",
            "IngredientTypeName": "Uunikinkku"
          }],
          [{
            "Name": "Arla Passeli Port Salut -juustoa",
            "Amount": "150",
            "PackageRecipe": "false",
            "IngredientType": "6642",
            "Unit": "g",
            "IngredientTypeName": "Port Salut -juusto"
          }],
          [{
            "Name": "mustapippuria",
            "Amount": "1/4",
            "PackageRecipe": "false",
            "IngredientType": "6167",
            "Unit": "tl",
            "IngredientTypeName": "Maustemylly, mustapippuri"
          }]
        ]
      }],
      "Instructions": "# Harjaa perunat puhtaiksi ja pistele perunat haarukalla. Pyöräytä perunat öljyssä ja paista 200 asteessa noin tunti.\r\n# Viipaloi valkosipulinkynnet ja kuullota ne öljyssä. Lisää pannulle kohmeinen lehtipinaatti ja paista sekoitellen muutama minuutti. Kaada pannulle fraiche-kerma ja kuumenna kiehuvaksi.\r\n# Viipaloi kinkku ja raasta juusto. Lisää kinkku ja puolet juustosta pannulle. Mausta mustapippurilla.\r\n# Tee kypsiin perunoihin ristiviilto ja purista perunaa, niin että peruna aukeaa. Jaa täyte perunoille ja ripottele pinnalle loput juustosta.",
      "EndNote": "Voit myös valmistaa perunat etukäteen ja lämmittää ne ennen tarjoamista. *Annoshinnat on laskettu K-citymarket-ketjun hintatason mukaan 11/2019. Tuoretuotteiden hinnat vaihtelevat sesongeittain.",
      "Description": "Valmista uuniperunoille vaihteeksi lämmin täyte. Herkullinen kinkku-pinaattimuhennos syntyy perunoiden kypsyessä. N. 1,95€/annos*",
      "DateCreated": "2019-05-24T10:00:59",
      "DateModified": "2019-11-06T16:02:14",
      "Stamp": {
        "Name": "Testattu K-koekeittiössä",
        "Id": "1"
      },
      "RecipeUse": {
        "Name": "k-ruoka.fi",
        "Id": "1"
      },
      "resourceType": "recipe",
      "Url": "https://www.k-ruoka.fi/reseptit/uuniperunat-ja-lammin-kinkku-pinaattikastike",
      "UrlSlug": "uuniperunat-ja-lammin-kinkku-pinaattikastike",
      "Sort": [1, 1, 10453]
    }, {
      "Id": "10452",
      "Name": "Tex mex -bataattivuoka",
      "PieceSize": {
        "Unit": "g",
        "Amount": "440"
      },
      "Portions": {
        "Unit": "annosta",
        "Amount": "4"
      },
      "PreparationTime": {
        "Description": "30 - 60 min",
        "TimeRange": {
          "MinTime": 30,
          "MaxTime": 60
        }
      },
      "UserPortions": {
        "$": {
          "Unit": "henkilölle"
        }
      },
      "Categories": [{
        "MainName": "Pääruoat",
        "MainId": "4",
        "SubName": "Kasvisruoat",
        "SubId": "22"
      }, {
        "MainName": "Pääruoat",
        "MainId": "4",
        "SubName": "Uuniruoat",
        "SubId": "30"
      }, {
        "MainName": "Makuja maailmalta",
        "MainId": "2",
        "SubName": "Suomi",
        "SubId": "106"
      }, {
        "MainName": "Pääraaka-aine",
        "MainId": "14",
        "SubName": "Kasvis",
        "SubId": "115"
      }, {
        "MainName": "Pääraaka-aine",
        "MainId": "14",
        "SubName": "Kasviproteiini",
        "SubId": "135"
      }, {
        "MainName": "Ruokalaji",
        "MainId": "16",
        "SubName": "Uuniruoat",
        "SubId": "141"
      }, {
        "MainName": "Apukategoriat",
        "MainId": "18",
        "SubName": "K-Marketin valikoima",
        "SubId": "181"
      }, {
        "MainName": "Asiakassegmentointi (K-asiakkaat)",
        "MainId": "17",
        "SubName": "Innostujat",
        "SubId": "147"
      }, {
        "MainName": "Asiakassegmentointi (K-asiakkaat)",
        "MainId": "17",
        "SubName": "Tiedostavat",
        "SubId": "148"
      }, {
        "MainName": "Asiakassegmentointi (K-asiakkaat)",
        "MainId": "17",
        "SubName": "Mukavuudenhaluiset ",
        "SubId": "149"
      }, {
        "MainName": "Teema",
        "MainId": "15",
        "SubName": "K-Marketin valikoima",
        "SubId": "151"
      }, {
        "MainName": "Apukategoriat",
        "MainId": "18",
        "SubName": "Puolivalmisteita sisältävä",
        "SubId": "158"
      }, {
        "MainName": "Teema",
        "MainId": "15",
        "SubName": "Arkiruoka",
        "SubId": "159"
      }],
      "Pictures": ["texmex_bataatti_PI12_19"],
      "PictureUrls": [{
        "Id": "texmex_bataatti_PI12_19",
        "Normal": "https://k-file-storage-qa.imgix.net/f/k-ruoka/recipe/10452?w=1000&h=1000&fit=clip",
        "Original": "https://k-file-storage-qa.imgix.net/f/k-ruoka/recipe/10452"
      }],
      "SpecialDiets": [{
        "Id": "1",
        "Name": "Vähälaktoosinen"
      }, {
        "Id": "4",
        "Name": "Gluteeniton"
      }, {
        "Id": "6",
        "Name": "Kasvis (lakto-ovo)"
      }, {
        "Id": "9",
        "Name": "Viljaton"
      }],
      "Ingredients": [{
        "SubSectionHeading": "_",
        "SubSectionIngredients": [
          [{
            "Name": "bataatti",
            "Amount": "1",
            "AmountInfo": "(n. 800 g)",
            "PackageRecipe": "false",
            "IngredientType": "5273",
            "Unit": "",
            "IngredientTypeName": "Bataatti"
          }],
          [{
            "Name": "punasipulia",
            "Amount": "1-2",
            "AmountInfo": "(150 g)",
            "PackageRecipe": "false",
            "IngredientType": "6667",
            "Unit": "",
            "IngredientTypeName": "Punasipuli"
          }],
          [{
            "Name": "suolaa",
            "Amount": "1/2",
            "PackageRecipe": "false",
            "IngredientType": "7049",
            "Unit": "tl",
            "IngredientTypeName": "Suola"
          }],
          [{
            "Name": "mustapippuria",
            "Amount": "1/4",
            "PackageRecipe": "false",
            "IngredientType": "11644",
            "Unit": "tl",
            "IngredientTypeName": "Mustapippuri, jauhettu"
          }],
          [{
            "Name": "Verso Härkistä (Original)",
            "Amount": "1",
            "AmountInfo": "(250 g)",
            "PackageRecipe": "false",
            "IngredientType": "8308",
            "Unit": "pkt",
            "IngredientTypeName": "Härkäpapuvalmiste"
          }],
          [{
            "Name": "Pirkka mietoa tacokastiketta",
            "Ean": "6410405117915",
            "Amount": "2",
            "AmountInfo": "(à 230 g)",
            "PackageRecipe": "false",
            "IngredientType": "7077",
            "Unit": "tlk",
            "IngredientTypeName": "Tacokastike, mieto"
          }],
          [{
            "Name": "cayennepippuria tai chilijauhetta",
            "Amount": "1/2",
            "PackageRecipe": "false",
            "IngredientType": "5348",
            "Unit": "tl",
            "IngredientTypeName": "Cayennepippuri"
          }],
          [{
            "Name": "Pirkka mustapapuja",
            "Ean": "6410405144454",
            "Amount": "1",
            "AmountInfo": "(380/230 g)",
            "PackageRecipe": "false",
            "IngredientType": "6454",
            "Unit": "tlk",
            "IngredientTypeName": "Papu, musta"
          }],
          [{
            "Name": "Pirkka emmental-mozzarellajuustoraastetta",
            "Ean": "6410405048561",
            "Amount": "1",
            "AmountInfo": "(150 g)",
            "PackageRecipe": "false",
            "IngredientType": "5600",
            "Unit": "ps",
            "IngredientTypeName": "Juustoraaste, emmental-mozzarella"
          }],
          [{
            "Name": "Pirkka mietoa punaista chiliä",
            "Ean": "6410405091857",
            "Amount": "1-2",
            "PackageRecipe": "false",
            "IngredientType": "5355",
            "Unit": "",
            "IngredientTypeName": "Chili, punainen"
          }]
        ]
      }, {
        "SubSectionHeading": "Lisäksi",
        "SubSectionIngredients": [
          [{
            "Name": "voita paistoastian voiteluun",
            "PackageRecipe": "false",
            "IngredientType": "7345",
            "IngredientTypeName": "Voi, luomu"
          }]
        ]
      }],
      "Instructions": "# Pese, kuori ja leikkaa bataatti ohuiksi puolikuiksi. Kuori ja suikaloi punasipulit.\r\n# Voitele iso uunivuoka ja levitä siihen bataatit sekä sipulit.  Mausta suolalla ja pippurilla. Paista 225-asteisessa uunissa 15-20 minuuttia, kunnes bataatit ovat pehmenneet.\r\n# Sekoita Härkiksen joukkoon tacokastike ja cayennepippuri. Huuhdo ja valuta pavut.\r\n# Nosta vuoka uunista ja säädä lämpö grillivastukselle 225 asteeseen. Lisää vuokaan ensin pavut ja 1/3 juustosta. Lisää sitten Härkis-seos. Ripottele pinnalle loput juustoraasteesta.\r\n# Paista vielä noin 8 minuuttia. Suikaloi chilit ja ripottele ne valmiin ruoan pinnalle.",
      "EndNote": "Voit myös valmistaa ruoan uunipellillä leivinpaperin päällä. *Annoshinnat on laskettu K-citymarket-ketjun hintatason mukaan 11/2019. Tuoretuotteiden hinnat vaihtelevat sesongeittain.",
      "Description": "Helppo arkiruoka syntyy bataatista ja tacokastikkeella mehevöitetystä Härkiksestä. Lisää chiliä maun mukaan. Noin 3,35 €/annos*",
      "DateCreated": "2019-05-24T09:54:21",
      "DateModified": "2019-11-06T16:07:22",
      "Stamp": {
        "Name": "Testattu K-koekeittiössä",
        "Id": "1"
      },
      "RecipeUse": {
        "Name": "k-ruoka.fi",
        "Id": "1"
      },
      "resourceType": "recipe",
      "Url": "https://www.k-ruoka.fi/reseptit/tex-mex--bataattivuoka",
      "UrlSlug": "tex-mex--bataattivuoka",
      "Sort": [1, 1, 10452]
    }, {
      "Id": "10451",
      "Name": "Paahdettu kalkkuna",
      "PieceSize": {
        "Unit": "g"
      },
      "Portions": {
        "Unit": "annosta",
        "Amount": "6"
      },
      "PreparationTime": {
        "Description": "yli 60 min",
        "TimeRange": {
          "MinTime": 60
        }
      },
      "UserPortions": {
        "$": {
          "Unit": "henkilölle"
        }
      },
      "Categories": [{
        "MainName": "Makuja maailmalta",
        "MainId": "2",
        "SubName": "Pohjois-Amerikka",
        "SubId": "8"
      }, {
        "MainName": "Pääruoat",
        "MainId": "4",
        "SubName": "Broileri- ja linturuoat",
        "SubId": "21"
      }, {
        "MainName": "Pääruoat",
        "MainId": "4",
        "SubName": "Uuniruoat",
        "SubId": "30"
      }, {
        "MainName": "Sesonki",
        "MainId": "1",
        "SubName": "Joulun ruoat",
        "SubId": "75"
      }, {
        "MainName": "Pääraaka-aine",
        "MainId": "14",
        "SubName": "Broileri ja linnut",
        "SubId": "118"
      }, {
        "MainName": "Sesonki",
        "MainId": "1",
        "SubName": "Joulu",
        "SubId": "130"
      }, {
        "MainName": "Ruokalaji",
        "MainId": "16",
        "SubName": "Uuniruoat",
        "SubId": "141"
      }, {
        "MainName": "Asiakassegmentointi (K-asiakkaat)",
        "MainId": "17",
        "SubName": "Nautiskelijat",
        "SubId": "146"
      }, {
        "MainName": "Apukategoriat",
        "MainId": "18",
        "SubName": "Haastavampi",
        "SubId": "154"
      }, {
        "MainName": "Apukategoriat",
        "MainId": "18",
        "SubName": "Trendikäs ",
        "SubId": "156"
      }, {
        "MainName": "Teema",
        "MainId": "15",
        "SubName": "Fiininpi",
        "SubId": "162"
      }, {
        "MainName": "Teema",
        "MainId": "15",
        "SubName": "Juhlat",
        "SubId": "164"
      }],
      "Pictures": ["paahdettu_kalkk_PI12_19"],
      "PictureUrls": [{
        "Id": "paahdettu_kalkk_PI12_19",
        "Normal": "https://k-file-storage-qa.imgix.net/f/k-ruoka/recipe/10451?w=1000&h=1000&fit=clip",
        "Original": "https://k-file-storage-qa.imgix.net/f/k-ruoka/recipe/10451"
      }],
      "SpecialDiets": [{
        "Id": "4",
        "Name": "Gluteeniton"
      }],
      "Ingredients": [{
        "SubSectionHeading": "_",
        "SubSectionIngredients": [
          [{
            "Name": "Pirkka suomalainen pakastettu kalkkuna",
            "Ean": "2386438000002",
            "Amount": "1",
            "AmountInfo": "(n. 5,5 kg)",
            "PackageRecipe": "false",
            "IngredientType": "9310",
            "Unit": "",
            "IngredientTypeName": "Kalkkuna, kokonainen, pakaste"
          }],
          [{
            "Name": "suolaa",
            "Amount": "2",
            "PackageRecipe": "false",
            "IngredientType": "7049",
            "Unit": "tl",
            "IngredientTypeName": "Suola"
          }],
          [{
            "Name": "mustapippuria",
            "Amount": "1",
            "PackageRecipe": "false",
            "IngredientType": "6167",
            "Unit": "tl",
            "IngredientTypeName": "Maustemylly, mustapippuri"
          }],
          [{
            "Name": "kokonaista valkosipulia",
            "Ean": "6410405060471",
            "Amount": "3",
            "PackageRecipe": "false",
            "IngredientType": "7262",
            "Unit": "",
            "IngredientTypeName": "Valkosipuli"
          }],
          [{
            "Name": "vettä",
            "Amount": "1/2",
            "PackageRecipe": "false",
            "IngredientType": "15792",
            "Unit": "l",
            "IngredientTypeName": "Vesi"
          }],
          [{
            "Name": "Pirkka kanafondia",
            "Amount": "2",
            "PackageRecipe": "false",
            "Unit": "rkl"
          }],
          [{
            "Name": "Pirkka Parhaat mustaherukkahyytelöä",
            "Ean": "6410405174086",
            "Amount": "1",
            "AmountInfo": "(250 g)",
            "PackageRecipe": "false",
            "IngredientType": "6261",
            "Unit": "prk",
            "IngredientTypeName": "Mustaherukkahyytelö"
          }],
          [{
            "Name": "Pirkka vaahterasiirappia",
            "Ean": "6410402018604",
            "Amount": "3/4",
            "PackageRecipe": "false",
            "IngredientType": "7233",
            "Unit": "dl",
            "IngredientTypeName": "Vaahterasiirappi"
          }],
          [{
            "Name": "Pirkka Dijon-sinappia",
            "Ean": "6410405187369",
            "Amount": "2",
            "PackageRecipe": "false",
            "IngredientType": "5393",
            "Unit": "tl",
            "IngredientTypeName": "Dijonsinappi"
          }],
          [{
            "Name": "oksaa tuoretta rosmariinia",
            "Amount": "4",
            "PackageRecipe": "false",
            "IngredientType": "6751",
            "Unit": "",
            "IngredientTypeName": "Rosmariini, tuore"
          }],
          [{
            "Name": "laakerinlehteä",
            "Amount": "5",
            "PackageRecipe": "false",
            "IngredientType": "5918",
            "Unit": "",
            "IngredientTypeName": "Laakerinlehti"
          }]
        ]
      }],
      "Instructions": "# Nosta kalkkuna sulamaan jääkaappiin vähintään kaksi päivää aiemmin.\r\n# Pyyhi kalkkunan pinta kuivaksi. Käännä kalkkuna leikkuulaudalle rintapuoli alaspäin. Leikkaa tukevilla keittiö- tai lintusaksilla molemmin puolin selkärankaa niin, että selkäranka irtoaa. Pyyhi sisus kuivaksi.\r\n# Käännä lintu rintapuoli ylöspäin ja paina napakasti rintalastan kohdalta, jotta ranka litistyy. Leikkaa tukevalla ja terävällä keittiöveitsellä kalkkuna puoliksi rintalastan suuntaisesti. Mausta suolalla ja pippurilla. Nosta kalkkunanpuolikkaat korkeareunaiselle uunipannulle.\r\n# Leikkaa valkosipulit leveyssuunnassa puoliksi. Mittaa vesi, kanafondi, mustaherukkahyytelö, vaahterasiirappi, sinappi, rosmariini ja laakerinlehdet kattilaan. Kuumenna kiehuvaksi, sekoita välillä. Kaada liemi uunipannulle ja lisää puolitetut valkosipulit.\r\n# Työnnä paistolämpömittari kalkkunan pulleimpaan kohtaan. Peitä kalkkuna foliolla. Paista 175-asteisessa uunissa noin 1 tunti. Poista folio ja valele kalkkunaa vuokaan kertyneellä nesteellä 15 minuutin välein, kunnes kalkkuna on kypsä. Paistoaika on yhteensä noin 2 t 30 min. Ota kalkkuna uunista, kun lihasneste on kirkasta ja sisälämpötila 70-72 astetta. Lämpö nousee vielä muutaman asteen uunista ottamisen jälkeen.\r\n# Nosta kalkkunapuolikkaat vuoasta tarjoiluvadille. Peitä löyhästi foliolla ja anna vetäytyä noin 15 minuuttia.\r\n# Lisää valkosipulit tarjoiluvadille ja kaada liemi kannuun. Tarjoa valkosipulit ja kastike kalkkunan kanssa.",
      "EndNote": "*Annoshinnat on laskettu K-citymarket-ketjun hintatason mukaan 11/2019. Tuoretuotteiden hinnat vaihtelevat sesongeittain.",
      "Description": "Puolikkaiksi halkaistu kalkkuna kypsyy nopeammin ja on näyttävä ruoka talven juhlapöytään. Mustaherukkaglaze antaa kalkkunalle kauniin pinnan. Noin 8,70 €/annos*",
      "DateCreated": "2019-05-24T08:36:17",
      "DateModified": "2019-11-06T15:10:58",
      "Stamp": {
        "Name": "Testattu K-koekeittiössä",
        "Id": "1"
      },
      "RecipeUse": {
        "Name": "k-ruoka.fi",
        "Id": "1"
      },
      "resourceType": "recipe",
      "Url": "https://www.k-ruoka.fi/reseptit/paahdettu-kalkkuna",
      "UrlSlug": "paahdettu-kalkkuna",
      "Sort": [1, 1, 10451]
    }, {
      "Id": "10449",
      "Name": "Lohiramen",
      "PieceSize": {
        "Unit": "g",
        "Amount": "600"
      },
      "Portions": {
        "Unit": "annosta",
        "Amount": "4"
      },
      "PreparationTime": {
        "Description": "alle 30 min",
        "TimeRange": {
          "MinTime": 15,
          "MaxTime": 30
        }
      },
      "UserPortions": {
        "$": {
          "Unit": "henkilölle"
        }
      },
      "Categories": [{
        "MainName": "Pääruoat",
        "MainId": "4",
        "SubName": "Kalaruoat",
        "SubId": "20"
      }, {
        "MainName": "Makuja maailmalta",
        "MainId": "2",
        "SubName": "Japani",
        "SubId": "103"
      }, {
        "MainName": "Asiakassegmentointi (K-asiakkaat)",
        "MainId": "17",
        "SubName": "Nautiskelijat",
        "SubId": "146"
      }, {
        "MainName": "Asiakassegmentointi (K-asiakkaat)",
        "MainId": "17",
        "SubName": "Innostujat",
        "SubId": "147"
      }, {
        "MainName": "Apukategoriat",
        "MainId": "18",
        "SubName": "Trendikäs ",
        "SubId": "156"
      }, {
        "MainName": "Pääraaka-aine",
        "MainId": "14",
        "SubName": "Lohi",
        "SubId": "180"
      }],
      "Pictures": ["lohiramen_6_19"],
      "PictureUrls": [{
        "Id": "lohiramen_6_19",
        "Normal": "https://k-file-storage-qa.imgix.net/f/k-ruoka/recipe/10449?w=1000&h=1000&fit=clip",
        "Original": "https://k-file-storage-qa.imgix.net/f/k-ruoka/recipe/10449"
      }],
      "SpecialDiets": [{
        "Id": "3",
        "Name": "Maidoton"
      }],
      "Ingredients": [{
        "SubSectionHeading": "_",
        "SubSectionIngredients": [
          [{
            "Name": "Pirkka Luomu kananmunaa",
            "Ean": "6408641114904",
            "Amount": "2",
            "PackageRecipe": "false",
            "IngredientType": "5685",
            "Unit": "",
            "IngredientTypeName": "Kananmuna, luomu"
          }],
          [{
            "Name": "vettä",
            "Amount": "2",
            "PackageRecipe": "false",
            "IngredientType": "15792",
            "Unit": "l",
            "IngredientTypeName": "Vesi"
          }],
          [{
            "Name": "Pirkka kasvisfondia",
            "Amount": "4",
            "PackageRecipe": "false",
            "IngredientType": "5440",
            "Unit": "rkl",
            "IngredientTypeName": "Fondi, kasvis"
          }],
          [{
            "Name": "tuoretta inkivääriä",
            "Amount": "5",
            "AmountInfo": "cm:n pala",
            "PackageRecipe": "false",
            "IngredientType": "5567",
            "Unit": "",
            "IngredientTypeName": "Inkivääri, tuore"
          }],
          [{
            "Name": "valkosipulinkynttä",
            "Amount": "2",
            "PackageRecipe": "false",
            "IngredientType": "7262",
            "Unit": "",
            "IngredientTypeName": "Valkosipuli"
          }],
          [{
            "Name": "Pirkka Parhaat lasinuudelia",
            "Amount": "1",
            "AmountInfo": "(100 g)",
            "PackageRecipe": "false",
            "Unit": "pkt"
          }],
          [{
            "Name": "rypsiöljyä",
            "Amount": "1",
            "PackageRecipe": "false",
            "IngredientType": "6807",
            "Unit": "rkl",
            "IngredientTypeName": "Rypsiöljy"
          }],
          [{
            "Name": "lohifileetä",
            "Amount": "500",
            "PackageRecipe": "false",
            "IngredientType": "6002",
            "Unit": "g",
            "IngredientTypeName": "Lohifilee"
          }],
          [{
            "Name": "Pirkka japanilaista soijakastiketta",
            "Ean": "6410400048825",
            "Amount": "4",
            "PackageRecipe": "false",
            "IngredientType": "6953",
            "Unit": "rkl",
            "IngredientTypeName": "Soijakastike"
          }],
          [{
            "Name": "Pirkka suippopaprikaa",
            "Ean": "6410405051844",
            "Amount": "1",
            "AmountInfo": "(200 g)",
            "PackageRecipe": "false",
            "IngredientType": "6994",
            "Unit": "ps",
            "IngredientTypeName": "Suippopaprika"
          }],
          [{
            "Name": "Pirkka kevätsipulia",
            "Ean": "6410405093080",
            "Amount": "1/2",
            "AmountInfo": "(à 100 g)",
            "PackageRecipe": "false",
            "IngredientType": "5792",
            "Unit": "ps",
            "IngredientTypeName": "Kevätsipuli"
          }],
          [{
            "Name": "Pirkka riisiviinietikkaa",
            "Ean": "6410405192738",
            "Amount": "2",
            "PackageRecipe": "false",
            "IngredientType": "6740",
            "Unit": "rkl",
            "IngredientTypeName": "Riisiviinietikka"
          }]
        ]
      }, {
        "SubSectionHeading": "Lisäksi",
        "SubSectionIngredients": [
          [{
            "Name": "Alfalfa silmusalaattia tai ituja",
            "PackageRecipe": "false",
            "IngredientType": "8495",
            "IngredientTypeName": "Ituja, alfalfa"
          }]
        ]
      }],
      "Instructions": "# Keitä munia noin 7 minuuttia ja jäähdytä ne. Kuori ja halkaise munat.\r\n# Mittaa kattilaan vesi ja fondi. Kuumenna kiehuvaksi.\r\n# Kuori ja suikaloi inkivääri sekä viipaloi valkosipulinkynnet. Lisää ne kattilaan. Anna liemen kiehua noin 10 minuuttia.\r\n# Valmista sillä aikaa nuudelit pakkauksen ohjeen mukaan. Valuta ja sekoita joukkoon rypsiöljy.\r\n# Paloittele lohifilee neljään palaan. Aloita paistaminen nahkapuolelta. Paahda kalapaloja pannulla molemmin puolin yhteensä 4-5 minuuttia. Valuta päälle 1 rkl soijakastiketta.\r\n# Rullaa suippopaprikaa kevyesti leikkuulautaa vasten, leikkaa kanta ja kopista siemenet pois. Viipaloi paprikat. Suikaloi kevätsipulit\r\n# Mausta ramenliemi lopulla soijakastikkeella (3 rkl) ja riisiviinietikalla.\r\n# Jaa nuudelit isoihin kulhoihin. Kaada päälle kuumaa lientä ja lisää paprikaa, keväsipulia, kananmunat ja lohipala. Viimeistele annokset kevätsipulilla ja iduilla. Tarjoa heti.",
      "EndNote": "Kokeile myös soijamarinoituja kananmunia! Ohjeen löydät K-Ruoka -reseptihausta. *Annoshinnat on laskettu K-citymarket-ketjun hintatason mukaan 10/2019. Tuoretuotteiden hinnat vaihtelevat sesongeittain.",
      "Description": "Lohramen eli japanilainen lohi-nuudelikeitto onnistuu muutamalla niksillä myös arki-iltana. Vaihtele kasviksia makusi mukaan. Noin 4,75 €/annos*",
      "DateCreated": "2019-05-24T08:31:59",
      "DateModified": "2019-10-07T10:17:58",
      "TvDate": "2019-11-11T00:00:00",
      "Stamp": {
        "Name": "Testattu K-koekeittiössä",
        "Id": "1"
      },
      "RecipeUse": {
        "Name": "k-ruoka.fi",
        "Id": "1"
      },
      "resourceType": "recipe",
      "Url": "https://www.k-ruoka.fi/reseptit/lohiramen",
      "UrlSlug": "lohiramen",
      "Sort": [1, 1, 10449]
    }, {
      "Id": "10448",
      "Name": "Joulumakkara lanttupedillä",
      "PieceSize": {
        "Unit": "g",
        "Amount": "327"
      },
      "Portions": {
        "Unit": "annosta",
        "Amount": "3"
      },
      "PreparationTime": {
        "Description": "30 - 60 min",
        "TimeRange": {
          "MinTime": 30,
          "MaxTime": 60
        }
      },
      "UserPortions": {
        "$": {
          "Unit": "henkilölle"
        }
      },
      "Categories": [{
        "MainName": "Asiakassegmentointi (K-asiakkaat)",
        "MainId": "17",
        "SubName": "Mukavuudenhaluiset ",
        "SubId": "149"
      }, {
        "MainName": "Asiakassegmentointi (K-asiakkaat)",
        "MainId": "17",
        "SubName": "Vakiintuneet",
        "SubId": "150"
      }, {
        "MainName": "Apukategoriat",
        "MainId": "18",
        "SubName": "Helppo",
        "SubId": "153"
      }, {
        "MainName": "Apukategoriat",
        "MainId": "18",
        "SubName": "Puolivalmisteita sisältävä",
        "SubId": "158"
      }, {
        "MainName": "Teema",
        "MainId": "15",
        "SubName": "Arkiruoka",
        "SubId": "159"
      }, {
        "MainName": "Pääruoat",
        "MainId": "4",
        "SubName": "Makkararuoat",
        "SubId": "25"
      }, {
        "MainName": "Sesonki",
        "MainId": "1",
        "SubName": "Joulun ruoat",
        "SubId": "75"
      }, {
        "MainName": "Makuja maailmalta",
        "MainId": "2",
        "SubName": "Skandinavia",
        "SubId": "105"
      }, {
        "MainName": "Pääraaka-aine",
        "MainId": "14",
        "SubName": "Makkara",
        "SubId": "113"
      }, {
        "MainName": "Pääraaka-aine",
        "MainId": "14",
        "SubName": "Liha",
        "SubId": "114"
      }, {
        "MainName": "Sesonki",
        "MainId": "1",
        "SubName": "Joulu",
        "SubId": "130"
      }],
      "Pictures": ["joulumakkara_lant_6_19"],
      "PictureUrls": [{
        "Id": "joulumakkara_lant_6_19",
        "Normal": "https://k-file-storage-qa.imgix.net/f/k-ruoka/recipe/10448?w=1000&h=1000&fit=clip",
        "Original": "https://k-file-storage-qa.imgix.net/f/k-ruoka/recipe/10448"
      }],
      "SpecialDiets": [{
        "Id": "4",
        "Name": "Gluteeniton"
      }, {
        "Id": "7",
        "Name": "Kananmunaton"
      }],
      "Ingredients": [{
        "SubSectionHeading": "_",
        "SubSectionIngredients": [
          [{
            "Name": "HK maakarit Juhla raakamakkaraa",
            "Amount": "1",
            "AmountInfo": "(280 g)",
            "PackageRecipe": "false",
            "IngredientType": "16170",
            "Unit": "pkt",
            "IngredientTypeName": "HK juhlaraakamakkara"
          }],
          [{
            "Name": "rypsiöljyä",
            "Amount": "1",
            "PackageRecipe": "false",
            "IngredientType": "6807",
            "Unit": "rkl",
            "IngredientTypeName": "Rypsiöljy"
          }]
        ]
      }, {
        "SubSectionHeading": "Lanttumuussi",
        "SubSectionIngredients": [
          [{
            "Name": "Pirkka pestyä lanttua",
            "Ean": "6410405097262",
            "Amount": "1",
            "AmountInfo": "(1 kg)",
            "PackageRecipe": "false",
            "IngredientType": "5944",
            "Unit": "ps",
            "IngredientTypeName": "Lanttu"
          }],
          [{
            "Name": "Pirkka kuohukermaa tai ruokakermaa",
            "Ean": "6410405142597",
            "Amount": "1",
            "PackageRecipe": "false",
            "IngredientType": "5891",
            "Unit": "dl",
            "IngredientTypeName": "Kuohukerma"
          }],
          [{
            "Name": "voita",
            "Amount": "25",
            "PackageRecipe": "false",
            "IngredientType": "7344",
            "Unit": "g",
            "IngredientTypeName": "Voi, laktoositon"
          }],
          [{
            "Name": "Pirkka Parhaat parmesaania raastettuna",
            "Ean": "6410405102959",
            "Amount": "2",
            "PackageRecipe": "false",
            "IngredientType": "6468",
            "Unit": "dl",
            "IngredientTypeName": "Parmesaani"
          }],
          [{
            "Name": "suolaa",
            "Amount": "1/4",
            "PackageRecipe": "false",
            "IngredientType": "7049",
            "Unit": "tl",
            "IngredientTypeName": "Suola"
          }],
          [{
            "Name": "mustapippuria",
            "Amount": "1/4",
            "PackageRecipe": "false",
            "IngredientType": "11644",
            "Unit": "tl",
            "IngredientTypeName": "Mustapippuri, jauhettu"
          }],
          [{
            "Name": "Pirkka kapriksia",
            "Ean": "6410405070654",
            "Amount": "1",
            "AmountInfo": "(50 g/35 g)",
            "PackageRecipe": "false",
            "IngredientType": "5695",
            "Unit": "prk",
            "IngredientTypeName": "Kapris"
          }]
        ]
      }],
      "Instructions": "# Laita vesi kiehumaan lanttuja varten. Kuori ja paloittele lantut noin sentin kuutioiksi. Keitä suolalla maustetussa vedessä kypsiksi, 20-25 minuuttia.\r\n# Kuumenna sillä aikaa öljy pannulla ja paista siinä matalalla lämmöllä makkarakieppiä molemmin puolin käännellen 8-10 minuuttia.\r\n# Valuta lantut huolellisesti ja anna kuivahtaa siivilässä. Kuumenna kerma ja voi pienessä kattilassa. Siirrä lantut takaisin keittokattilaan, kaada päälle kermaseos ja survo survimella rouheaksi. Sekoita joukkoon oarmesaaniraaste ja mausteet.\r\n# Levitä lanttusose tarjoiluastiaan ja nosta päälle makkara. Ripottele päälle kaprikset. Tarjoa halutessasi lisäksi sinappia.",
      "EndNote": "*Annoshinnat on laskettu K-citymarket-ketjun hintatason mukaan 10/2019. Tuoretuotteiden hinnat vaihtelevat sesongeittain.",
      "Description": "Joulumausteilla ryyditetty makkara tarjotaan mehevän lanttumuussin kanssa. Noin 3,20 €/annos*",
      "DateCreated": "2019-05-24T08:30:36",
      "DateModified": "2019-10-28T09:16:03",
      "TvDate": "2019-12-17T00:00:00",
      "Stamp": {
        "Name": "Testattu K-koekeittiössä",
        "Id": "1"
      },
      "RecipeUse": {
        "Name": "k-ruoka.fi",
        "Id": "1"
      },
      "resourceType": "recipe",
      "Url": "https://www.k-ruoka.fi/reseptit/joulumakkara-lanttupedilla",
      "UrlSlug": "joulumakkara-lanttupedilla",
      "Sort": [1, 1, 10448]
    }, {
      "Id": "10446",
      "Name": "Joulukana ja punakaalipihvit",
      "PieceSize": {
        "Unit": "g",
        "Amount": "510"
      },
      "Portions": {
        "Unit": "annosta",
        "Amount": "4"
      },
      "PreparationTime": {
        "Description": "yli 60 min",
        "TimeRange": {
          "MinTime": 60
        }
      },
      "UserPortions": {
        "$": {
          "Unit": "henkilölle"
        }
      },
      "Categories": [{
        "MainName": "Asiakassegmentointi (K-asiakkaat)",
        "MainId": "17",
        "SubName": "Tiedostavat",
        "SubId": "148"
      }, {
        "MainName": "Apukategoriat",
        "MainId": "18",
        "SubName": "Trendikäs ",
        "SubId": "156"
      }, {
        "MainName": "Pääruoat",
        "MainId": "4",
        "SubName": "Uuniruoat",
        "SubId": "30"
      }, {
        "MainName": "Lisäkkeet",
        "MainId": "6",
        "SubName": "Kasvislisäkkeet",
        "SubId": "42"
      }, {
        "MainName": "Sesonki",
        "MainId": "1",
        "SubName": "Joulun ruoat",
        "SubId": "75"
      }, {
        "MainName": "Makuja maailmalta",
        "MainId": "2",
        "SubName": "Skandinavia",
        "SubId": "105"
      }, {
        "MainName": "Sesonki",
        "MainId": "1",
        "SubName": "Joulu",
        "SubId": "130"
      }, {
        "MainName": "Asiakassegmentointi (K-asiakkaat)",
        "MainId": "17",
        "SubName": "Nautiskelijat",
        "SubId": "146"
      }, {
        "MainName": "Pääruoat",
        "MainId": "4",
        "SubName": "Broileri- ja linturuoat",
        "SubId": "21"
      }],
      "Pictures": ["joulukana_ja_pun_KR6_19"],
      "PictureUrls": [{
        "Id": "joulukana_ja_pun_KR6_19",
        "Normal": "https://k-file-storage-qa.imgix.net/f/k-ruoka/recipe/10446?w=1000&h=1000&fit=clip",
        "Original": "https://k-file-storage-qa.imgix.net/f/k-ruoka/recipe/10446"
      }],
      "Ingredients": [{
        "SubSectionHeading": "_",
        "SubSectionIngredients": [
          [{
            "Name": "Kariniemen Juhla Kananpoika",
            "Amount": "1",
            "AmountInfo": "(n. 1,3 kg)",
            "PackageRecipe": "false",
            "IngredientType": "16219",
            "Unit": "",
            "IngredientTypeName": "Kariniemen Juhla Kananpoika"
          }],
          [{
            "Name": "klementiiniä",
            "Ean": "6410405045010",
            "Amount": "2",
            "AmountInfo": "(280 g)",
            "PackageRecipe": "false",
            "IngredientType": "5837",
            "Unit": "",
            "IngredientTypeName": "Klementiini"
          }]
        ]
      }, {
        "SubSectionHeading": "Punakaalipihvit",
        "SubSectionIngredients": [
          [{
            "Name": "punakaali",
            "Amount": "1",
            "AmountInfo": "(n. 1,1 kg)",
            "PackageRecipe": "false",
            "IngredientType": "6663",
            "Unit": "",
            "IngredientTypeName": "Punakaali"
          }],
          [{
            "Name": "Pirkka rypsiöljyä",
            "Ean": "6410405191434",
            "Amount": "1",
            "PackageRecipe": "false",
            "IngredientType": "6807",
            "Unit": "rkl",
            "IngredientTypeName": "Rypsiöljy"
          }],
          [{
            "Name": "suolaa",
            "Amount": "1/4",
            "PackageRecipe": "false",
            "IngredientType": "7049",
            "Unit": "tl",
            "IngredientTypeName": "Suola"
          }],
          [{
            "Name": "Pirkka pekaanipähkinöitä",
            "Ean": "6410405126641",
            "Amount": "1/2",
            "AmountInfo": "(à 125 g)",
            "PackageRecipe": "false",
            "IngredientType": "6497",
            "Unit": "ps",
            "IngredientTypeName": "Pekaanipähkinä"
          }],
          [{
            "Name": "klementiinin raastettu kuori",
            "Amount": "1",
            "PackageRecipe": "false",
            "IngredientType": "9969",
            "Unit": "",
            "IngredientTypeName": "Klementiini, luomu"
          }],
          [{
            "Name": "klementiininmehua",
            "Amount": "1/2",
            "PackageRecipe": "false",
            "IngredientType": "5837",
            "Unit": "dl",
            "IngredientTypeName": "Klementiini"
          }],
          [{
            "Name": "Pirkka ekstra-neitsytoliiviöljyä",
            "Ean": "6410402024759",
            "Amount": "3",
            "PackageRecipe": "false",
            "IngredientType": "5405",
            "Unit": "rkl",
            "IngredientTypeName": "Ekstra neitsytoliiviöljy"
          }],
          [{
            "Name": "Pirkka vaahterasiirappia",
            "Ean": "6410402018604",
            "Amount": "1-2",
            "PackageRecipe": "false",
            "IngredientType": "7233",
            "Unit": "rkl",
            "IngredientTypeName": "Vaahterasiirappi"
          }],
          [{
            "Name": "suolaa",
            "Amount": "1/4",
            "PackageRecipe": "false",
            "IngredientType": "7049",
            "Unit": "tl",
            "IngredientTypeName": "Suola"
          }],
          [{
            "Name": "mustapippuria",
            "Amount": "1/4",
            "PackageRecipe": "false",
            "IngredientType": "6167",
            "Unit": "tl",
            "IngredientTypeName": "Maustemylly, mustapippuri"
          }],
          [{
            "Name": "lehtipersiljaa hienonnettuna",
            "Amount": "1",
            "PackageRecipe": "false",
            "IngredientType": "5954",
            "Unit": "dl",
            "IngredientTypeName": "Lehtipersilja"
          }],
          [{
            "Name": "Pirkka granaattiomenan siemeniä (pakaste)",
            "Ean": "6410405156457",
            "Amount": "1",
            "PackageRecipe": "false",
            "IngredientType": "8150",
            "Unit": "dl",
            "IngredientTypeName": "Granaattiomenan siemen, pakaste"
          }]
        ]
      }],
      "Instructions": "# Ota broileri huoneenlämpöön tunti ennen valmistusta.\r\n# Pese yksi klementiini huolellisesti ja leikkaa se puoliksi. Täytä broileri klementiinin puolikkailla ja sido broilerin jalat narulla. Typistä siiven kärjet saksilla, jotta ne eivät pala.\r\n# Nosta broileri uunivuokaan rintapuoli ylöspäin. Paista ensin 200 asteessa 15 minuuttia. Nosta vuoka uunista, käännä broileri selkä ylöspäin ja jatka paistamista 175 asteessa 25 minuuttia.\r\n# Pese ja viipaloi toinen klementiini. Nosta vuoka uunista, paina klementiiniviipaleet kanan päälle ja jatka vielä kypsentämistä 30 minuuttia, kunnes broilerin sisälämpötila on 75 astetta. Voit valella vuokaan kertynyttä lientä broilerin päälle. Peitä kypsä broileri löyhästi foliolla ja anna vetäytyä.\r\n# Poista kaalista tarvittaessa rikkoutuneet ulkolehdet. Leikkaa kaalin keskeltä kannan kohdalta ja vähän molemmin puolin 4 kiekkoa (paksuus noin 2 cm). Nosta leivinpaperin päälle uunipellille ja voitele öljyllä. Mausta suolalla ja paista 200 asteessa 30-35 minuuttia.\r\n# Paahda pekaanipähkinät pannulla, jäähdytä ja rouhi veitsellä. Sekoita kulhossa keskenään pähkinärouhe, klementiinin mehu- ja kuoriraaste, öljy ja vaahterasiirappi sekä mausteet. Lisää lopuksi hienonnettu lehtipersilja ja sulatetut granaattiomenan siemenet.\r\n# Siirrä kaalipihvit tarjoilulautaselle, levitä päälle kastike ja tarjoa broilerin kanssa.",
      "EndNote": "Vinkki! Valmista jäljelle jääneestä punakaalista helppo pikakimchi. *Annoshinnat on laskettu K-citymarket-ketjun hintatason mukaan 10/2019. Tuoretuotteiden hinnat vaihtelevat sesongeittain.",
      "Description": "Tarjoa joulupöydässä vaihteeksi broileria! Jouluinen broileri saa makua klementiinistä. Tarjoa lisäksi meheviä punakaalipihvejä ja helppo jouluateria on valmis. Noin 3,75 €/annos*",
      "DateCreated": "2019-05-24T08:27:05",
      "DateModified": "2019-10-28T09:15:47",
      "TvDate": "2019-12-19T00:00:00",
      "Stamp": {
        "Name": "Testattu K-koekeittiössä",
        "Id": "1"
      },
      "RecipeUse": {
        "Name": "k-ruoka.fi",
        "Id": "1"
      },
      "resourceType": "recipe",
      "Url": "https://www.k-ruoka.fi/reseptit/joulukana-ja-punakaalipihvit",
      "UrlSlug": "joulukana-ja-punakaalipihvit",
      "Sort": [1, 1, 10446]
    }, {
      "Id": "10428",
      "Name": "Italialainen pasta carbonara",
      "PieceSize": {
        "Unit": "g",
        "Amount": "263"
      },
      "Portions": {
        "Unit": "annosta",
        "Amount": "4"
      },
      "PreparationTime": {
        "Description": "alle 30 min",
        "TimeRange": {
          "MinTime": 15,
          "MaxTime": 30
        }
      },
      "UserPortions": {
        "$": {
          "Unit": "henkilölle"
        }
      },
      "Categories": [{
        "MainName": "Asiakassegmentointi (K-asiakkaat)",
        "MainId": "17",
        "SubName": "Vakiintuneet",
        "SubId": "150"
      }, {
        "MainName": "Teema",
        "MainId": "15",
        "SubName": "K-Marketin valikoima",
        "SubId": "151"
      }, {
        "MainName": "Apukategoriat",
        "MainId": "18",
        "SubName": "Helppo",
        "SubId": "153"
      }, {
        "MainName": "Teema",
        "MainId": "15",
        "SubName": "Arkiruoka",
        "SubId": "159"
      }, {
        "MainName": "Teema",
        "MainId": "15",
        "SubName": "Halpa",
        "SubId": "161"
      }, {
        "MainName": "Apukategoriat",
        "MainId": "18",
        "SubName": "K-Marketin valikoima",
        "SubId": "181"
      }, {
        "MainName": "Makuja maailmalta",
        "MainId": "2",
        "SubName": "Italia",
        "SubId": "10"
      }, {
        "MainName": "Pääruoat",
        "MainId": "4",
        "SubName": "Pastaruoat",
        "SubId": "31"
      }, {
        "MainName": "Pääraaka-aine",
        "MainId": "14",
        "SubName": "Liha",
        "SubId": "114"
      }, {
        "MainName": "Ruokalaji",
        "MainId": "16",
        "SubName": "Pastat",
        "SubId": "138"
      }, {
        "MainName": "Asiakassegmentointi (K-asiakkaat)",
        "MainId": "17",
        "SubName": "Nautiskelijat",
        "SubId": "146"
      }],
      "Pictures": ["pekonipasta_19"],
      "PictureUrls": [{
        "Id": "pekonipasta_19",
        "Normal": "https://k-file-storage-qa.imgix.net/f/k-ruoka/recipe/10428?w=1000&h=1000&fit=clip",
        "Original": "https://k-file-storage-qa.imgix.net/f/k-ruoka/recipe/10428"
      }],
      "SpecialDiets": [{
        "Id": "5",
        "Name": "Vähemmän sokeria"
      }],
      "Ingredients": [{
        "SubSectionHeading": "_",
        "SubSectionIngredients": [
          [{
            "Name": "Pirkka spagettia",
            "Amount": "400",
            "PackageRecipe": "false",
            "IngredientType": "6989",
            "Unit": "g",
            "IngredientTypeName": "Spagetti"
          }],
          [{
            "Name": "Pirkka pekonia",
            "Ean": "6410405220288",
            "Amount": "1",
            "AmountInfo": "(150 g)",
            "PackageRecipe": "false",
            "Unit": "pkt"
          }],
          [{
            "Name": "Pirkka Luomu kananmunaa",
            "Ean": "6408641114904",
            "Amount": "2",
            "PackageRecipe": "false",
            "IngredientType": "5685",
            "Unit": "",
            "IngredientTypeName": "Kananmuna, luomu"
          }],
          [{
            "Name": "mustapippuria",
            "Amount": "1",
            "PackageRecipe": "false",
            "IngredientType": "6269",
            "Unit": "tl",
            "IngredientTypeName": "Mustapippuri, kokonainen"
          }],
          [{
            "Name": "Pirkka Parhaat parmesaania",
            "Ean": "6410405102959",
            "Amount": "100",
            "PackageRecipe": "false",
            "IngredientType": "12455",
            "Unit": "g",
            "IngredientTypeName": "Pecorino Romano -juusto"
          }],
          [{
            "Name": "(suolaa)",
            "PackageRecipe": "false",
            "IngredientType": "7049",
            "IngredientTypeName": "Suola"
          }]
        ]
      }],
      "Instructions": "# Suikaloi pekoni. Lisää pekoni kylmälle pannulla ja paista miedohkolla lämmöllä, kunnes pekonista alkaa rasva irrota. Jatka kuumentamista, kunnes pekoni on kypsää. Siirrä pannu liedeltä.\r\n# Keitä sillä aikaa spagetti suolalla maustetussa vedessä pakkauksen ohjeen mukaan.\r\n# Vatkaa kananmunien rakenne rikki ja mausta mustapippurilla. Raasta juusto raastimen pienellä terällä.\r\n# Kun pasta on kypsää, ota keitinvettä talteen 1-2 dl. Valuta pasta ja siirrä se valmiiksi lämmitettyyn kulhoon.\r\n# Lisää pastan joukkoon pekoni ja sekoita. Kaada kananmunat pastan joukkoon koko ajan vatkaten ja ripottele heti perään puolet juustoraasteesta. Tässä vaiheessa voit lisätä keitinvettä.\r\n# Lisää loput juustosta huolellisesti sekoittaen. Pastan tulisi olla kiiltävän kosteaa, mutta kulhonpohjalla ei saisi olla runsaasti lientä. Lisää keitinvettä, jos koostumus on liian kuiva. Rouhi lopuksi joukkoon runsaasti mustapippuria ja lisää suolaa tarvittaessa. Tarjoa heti.",
      "EndNote": "*Annoshinnat on laskettu K-citymarket-ketjun hintatason mukaan 1/2017. Tuoretuotteiden hinnat vaihtelevat sesongeittain.",
      "Description": "Pasta carbonara on italialainen klassikko, joka vie kielen mennessään. Aito carbonara valmistetaan munista ja juustosta.",
      "DateCreated": "2019-05-09T14:51:51",
      "DateModified": "2019-09-17T13:05:09",
      "Stamp": {
        "Name": "Testattu K-koekeittiössä",
        "Id": "1"
      },
      "RecipeUse": {
        "Name": "k-ruoka.fi",
        "Id": "1"
      },
      "resourceType": "recipe",
      "Url": "https://www.k-ruoka.fi/reseptit/italialainen-pasta-carbonara",
      "UrlSlug": "italialainen-pasta-carbonara",
      "Sort": [1, 1, 10428]
    }, {
      "Id": "10427",
      "Name": "Ahvenfileet",
      "PieceSize": {
        "Unit": "g",
        "Amount": "340"
      },
      "Portions": {
        "Unit": "annosta",
        "Amount": "4"
      },
      "PreparationTime": {
        "Description": "30 - 60 min",
        "TimeRange": {
          "MinTime": 30,
          "MaxTime": 60
        }
      },
      "UserPortions": {
        "$": {
          "Unit": "henkilölle"
        }
      },
      "Categories": [{
        "MainName": "Pääruoat",
        "MainId": "4",
        "SubName": "Kalaruoat",
        "SubId": "20"
      }, {
        "MainName": "Makuja maailmalta",
        "MainId": "2",
        "SubName": "Suomi",
        "SubId": "106"
      }, {
        "MainName": "Pääraaka-aine",
        "MainId": "14",
        "SubName": "Kala",
        "SubId": "116"
      }, {
        "MainName": "Sesonki",
        "MainId": "1",
        "SubName": "Kesä",
        "SubId": "121"
      }, {
        "MainName": "Asiakassegmentointi (K-asiakkaat)",
        "MainId": "17",
        "SubName": "Innostujat",
        "SubId": "147"
      }, {
        "MainName": "Apukategoriat",
        "MainId": "18",
        "SubName": "Helppo",
        "SubId": "153"
      }, {
        "MainName": "Apukategoriat",
        "MainId": "18",
        "SubName": "Trendikäs ",
        "SubId": "156"
      }, {
        "MainName": "Teema",
        "MainId": "15",
        "SubName": "Arkiruoka",
        "SubId": "159"
      }, {
        "MainName": "Teema",
        "MainId": "15",
        "SubName": "Fiininpi",
        "SubId": "162"
      }],
      "Pictures": ["KR_5_19_ahvenfileet"],
      "PictureUrls": [{
        "Id": "KR_5_19_ahvenfileet",
        "Normal": "https://k-file-storage-qa.imgix.net/f/k-ruoka/recipe/10427?w=1000&h=1000&fit=clip",
        "Original": "https://k-file-storage-qa.imgix.net/f/k-ruoka/recipe/10427"
      }],
      "VideoUrl": "https://youtu.be/zY7p23AvcYY",
      "VideoEmbedUrl": "<iframe width=\"1136\" height=\"639\" src=\"https://www.youtube.com/embed/zY7p23AvcYY\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>",
      "SpecialDiets": [{
        "Id": "4",
        "Name": "Gluteeniton"
      }, {
        "Id": "7",
        "Name": "Kananmunaton"
      }],
      "Ingredients": [{
        "SubSectionHeading": "_",
        "SubSectionIngredients": [
          [{
            "Name": "ahvenfileitä",
            "Amount": "600",
            "PackageRecipe": "false",
            "IngredientType": "5206",
            "Unit": "g",
            "IngredientTypeName": "Ahvenfilee, tuore"
          }],
          [{
            "Name": "voita",
            "Amount": "2",
            "PackageRecipe": "false",
            "IngredientType": "7343",
            "Unit": "rkl",
            "IngredientTypeName": "Voi"
          }],
          [{
            "Name": "suolaa",
            "Amount": "1",
            "PackageRecipe": "false",
            "IngredientType": "7049",
            "Unit": "tl",
            "IngredientTypeName": "Suola"
          }]
        ]
      }, {
        "SubSectionHeading": "Ruskistettu voi",
        "SubSectionIngredients": [
          [{
            "Name": "voita",
            "Amount": "100",
            "PackageRecipe": "false",
            "IngredientType": "7343",
            "Unit": "g",
            "IngredientTypeName": "Voi"
          }],
          [{
            "Name": "fenkolinsiemeniä huhmaressa hienonnettuna",
            "Amount": "1",
            "PackageRecipe": "false",
            "IngredientType": "5431",
            "Unit": "rkl",
            "IngredientTypeName": "Fenkolinsiemen"
          }],
          [{
            "Name": "sitruunankuorta raastettuna",
            "Amount": "1",
            "PackageRecipe": "false",
            "IngredientType": "6941",
            "Unit": "tl",
            "IngredientTypeName": "Sitruuna"
          }],
          [{
            "Name": "(kehäkukan terälehtiä)",
            "PackageRecipe": "false",
            "IngredientType": "5884",
            "IngredientTypeName": "Kukka, syötävä"
          }]
        ]
      }, {
        "SubSectionHeading": "Juuresmuusi",
        "SubSectionIngredients": [
          [{
            "Name": "porkkanoita",
            "Amount": "300",
            "PackageRecipe": "false",
            "IngredientType": "6582",
            "Unit": "g",
            "IngredientTypeName": "Porkkana"
          }],
          [{
            "Name": "perunoita",
            "Amount": "200",
            "PackageRecipe": "false",
            "IngredientType": "7368",
            "Unit": "g",
            "IngredientTypeName": "Yleisperuna"
          }],
          [{
            "Name": "palsternakkaa",
            "Amount": "100",
            "PackageRecipe": "false",
            "IngredientType": "6432",
            "Unit": "g",
            "IngredientTypeName": "Palsternakka"
          }],
          [{
            "Name": "selleriä",
            "Amount": "100",
            "PackageRecipe": "false",
            "IngredientType": "5958",
            "Unit": "g",
            "IngredientTypeName": "Lehtiselleri"
          }],
          [{
            "Name": "suolaa",
            "Amount": "1",
            "PackageRecipe": "false",
            "IngredientType": "7049",
            "Unit": "tl",
            "IngredientTypeName": "Suola"
          }],
          [{
            "Name": "voita",
            "Amount": "1",
            "PackageRecipe": "false",
            "IngredientType": "7343",
            "Unit": "rkl",
            "IngredientTypeName": "Voi"
          }]
        ]
      }],
      "Instructions": "# Kuori ja leikkaa juurekset pieniksi paloiksi. Pane juurekset ja suola kattilaan ja kaada vettä sen verran, että juurekset juuri ja juuri peittyvät. Keitä juurekset kypsiksi, 20-25 minuuttia. Valuta enin keitinliemi juureksista kulhoon ja ota se talteen. Lisää voi juuresten joukkoon ja survo juurekset muusiksi. Ohenna muusi keitinliemellä sopivan paksuiseksi.\r\n# Valmista ruskistettu voi. Paloittele voi paksupohjaiseen kattilaan. Sulata ja kuumenna voi. Ensin se vaahtoaa jonkin verran. Kuumentuessa voista haihtuu vesi ja se alkaa ruskistua. Sekoita voita välillä, jotta huomaat voin ruskistumisasteen. Voin paahtuminen kestää 7-10 minuuttia. Voi on valmista, kun se on paahtuneen ruskeaa. Ota kattila liedeltä ja kaada voi kulhoon. Lisää joukkoon hienonnetut fenkolinsiemenet ja sitruunan kuoriraaste.\r\n# Kuumenna voi pannulla, ja kun se hieman vaahtoaa, asettele ahvenfileet pannulle. Käännä fileet, kun reunat hieman ruskistuvat. Ahvenfileet kypsyvät muutamassa minuutissa. Ripottele fileiden pinnalle suolaa. Ahvenfileet kannattaa paistaa juuri ennen tarjoamista, periaatteena pannulta lautaselle.\r\n# Valuta ruskistettua voita ahvenfileille ennen tarjoamista. Koristele ahvenfileet halutessasi syötävillä kukilla. Tarjoa ahvenfileiden lisänä juuresmuusia ja syksyn värikkäitä kasviksia.",
      "EndNote": "Vinkki! Jos ruskistettua voita jää jäljelle, laita se jäähtyneenä jääkaappiin ja käytä myöhemmin esimerkiksi leivän päällä. *Annoshinnat on laskettu K-citymarket-ketjun hintatason mukaan 8/2019. Tuoretuotteiden hinnat vaihtelevat sesongeittain.",
      "Description": "Paistetut ahvenfileet tarjotaan ruskistetun voin, juuresmuusin ja värikkäiden kasvisten kanssa. Voiko herkullisempaa olla?  Resepti: Kalaasi -blogi. Noin 5,15€/annos",
      "DateCreated": "2019-05-09T11:38:00",
      "DateModified": "2019-11-03T20:10:43",
      "TvDate": "2019-10-31T00:00:00",
      "Stamp": {
        "Name": "Bloggaaja",
        "Id": "2"
      },
      "RecipeUse": {
        "Name": "k-ruoka.fi",
        "Id": "1"
      },
      "resourceType": "recipe",
      "Url": "https://www.k-ruoka.fi/reseptit/ahvenfileet",
      "UrlSlug": "ahvenfileet",
      "Sort": [1, 1, 10427]
    }, {
      "Id": "10418",
      "Name": "Juurespizza",
      "PieceSize": {
        "Unit": "g"
      },
      "Portions": {
        "Unit": "annosta",
        "Amount": "6"
      },
      "PreparationTime": {
        "Description": "alle 30 min",
        "TimeRange": {
          "MinTime": 15,
          "MaxTime": 30
        }
      },
      "UserPortions": {
        "$": {
          "Unit": "henkilölle"
        }
      },
      "Categories": [{
        "MainName": "Pääruoat",
        "MainId": "4",
        "SubName": "Kasvisruoat",
        "SubId": "22"
      }, {
        "MainName": "Asiakassegmentointi (K-asiakkaat)",
        "MainId": "17",
        "SubName": "Tiedostavat",
        "SubId": "148"
      }, {
        "MainName": "Suolaiset leivonnaiset",
        "MainId": "8",
        "SubName": "Pizzat",
        "SubId": "56"
      }, {
        "MainName": "Pääruoat",
        "MainId": "4",
        "SubName": "Pizzat",
        "SubId": "87"
      }, {
        "MainName": "Sesonki",
        "MainId": "1",
        "SubName": "Sadonkorjuu",
        "SubId": "124"
      }, {
        "MainName": "Teema",
        "MainId": "15",
        "SubName": "Mitä tänään syötäisiin?",
        "SubId": "144"
      }, {
        "MainName": "Asiakassegmentointi (K-asiakkaat)",
        "MainId": "17",
        "SubName": "Nautiskelijat",
        "SubId": "146"
      }, {
        "MainName": "Asiakassegmentointi (K-asiakkaat)",
        "MainId": "17",
        "SubName": "Innostujat",
        "SubId": "147"
      }],
      "Pictures": ["PI9_19_juurespizza"],
      "PictureUrls": [{
        "Id": "PI9_19_juurespizza",
        "Normal": "https://k-file-storage-qa.imgix.net/f/k-ruoka/recipe/10418?w=1000&h=1000&fit=clip",
        "Original": "https://k-file-storage-qa.imgix.net/f/k-ruoka/recipe/10418"
      }],
      "Ingredients": [{
        "SubSectionHeading": "Pizzataikina",
        "SubSectionIngredients": [
          [{
            "Name": "vettä",
            "Amount": "1",
            "PackageRecipe": "false",
            "IngredientType": "15792",
            "Unit": "dl",
            "IngredientTypeName": "Vesi"
          }],
          [{
            "Name": "tuorehiivaa",
            "Amount": "1/3",
            "AmountInfo": "(à 50 g)",
            "PackageRecipe": "false",
            "IngredientType": "5534",
            "Unit": "pala",
            "IngredientTypeName": "Hiiva, tuore"
          }],
          [{
            "Name": "vehnäjauhoja",
            "Amount": "2 1/2",
            "PackageRecipe": "false",
            "IngredientType": "7293",
            "Unit": "dl",
            "IngredientTypeName": "Vehnäjauho"
          }],
          [{
            "Name": "oliiviöljyä",
            "Amount": "2",
            "PackageRecipe": "false",
            "IngredientType": "5405",
            "Unit": "rkl",
            "IngredientTypeName": "Ekstra neitsytoliiviöljy"
          }],
          [{
            "Name": "tuoretta rosmariinia hienonnettuna",
            "Amount": "2",
            "PackageRecipe": "false",
            "IngredientType": "6751",
            "Unit": "tl",
            "IngredientTypeName": "Rosmariini, tuore"
          }],
          [{
            "Name": "suolaa",
            "Amount": "1/2",
            "PackageRecipe": "false",
            "IngredientType": "7049",
            "Unit": "tl",
            "IngredientTypeName": "Suola"
          }]
        ]
      }, {
        "SubSectionHeading": "Täyte",
        "SubSectionIngredients": [
          [{
            "Name": "pieni keltajuuri",
            "Amount": "1",
            "AmountInfo": "(80 g)",
            "PackageRecipe": "false",
            "IngredientType": "7473",
            "Unit": "",
            "IngredientTypeName": "Keltajuuri"
          }],
          [{
            "Name": "pieni punajuuri",
            "Amount": "1",
            "AmountInfo": "(80 g)",
            "PackageRecipe": "false",
            "IngredientType": "6655",
            "Unit": "",
            "IngredientTypeName": "Punajuuri"
          }],
          [{
            "Name": "pieni raitajuuri",
            "Amount": "1",
            "AmountInfo": "(60 g)",
            "PackageRecipe": "false",
            "IngredientType": "13287",
            "Unit": "",
            "IngredientTypeName": "Raitajuuri"
          }],
          [{
            "Name": "Arla Lempi crème fraîchea",
            "Amount": "1/2",
            "AmountInfo": "(à 200 g)",
            "PackageRecipe": "false",
            "IngredientType": "7954",
            "Unit": "prk",
            "IngredientTypeName": "Creme fraiche, 18 %, laktoositon"
          }],
          [{
            "Name": "oliiviöljyä",
            "Amount": "1",
            "PackageRecipe": "false",
            "IngredientType": "5405",
            "Unit": "rkl",
            "IngredientTypeName": "Ekstra neitsytoliiviöljy"
          }],
          [{
            "Name": "hunajaa",
            "Amount": "1",
            "PackageRecipe": "false",
            "IngredientType": "5550",
            "Unit": "rkl",
            "IngredientTypeName": "Hunaja, juokseva"
          }],
          [{
            "Name": "Pirkka Parhaat sormisuolaa",
            "Ean": "6410405076915",
            "Amount": "1/2",
            "PackageRecipe": "false",
            "IngredientType": "7049",
            "Unit": "tl",
            "IngredientTypeName": "Suola"
          }],
          [{
            "Name": "Castello Crumbly Blue juustoa",
            "Amount": "1/2",
            "AmountInfo": "(à 140 g)",
            "PackageRecipe": "false",
            "Unit": "pkt"
          }]
        ]
      }, {
        "SubSectionHeading": "Päälle",
        "SubSectionIngredients": [
          [{
            "Name": "Pirkka tummaa balsamietikkakastiketta",
            "Ean": "6410405114860",
            "PackageRecipe": "false",
            "IngredientType": "7628",
            "IngredientTypeName": "Balsamietikkakastike, tumma, kirsikka"
          }],
          [{
            "Name": "tuoretta timjamia",
            "PackageRecipe": "false",
            "IngredientType": "7119",
            "IngredientTypeName": "Timjami, tuore"
          }]
        ]
      }],
      "Instructions": "# Liuota hiiva kädenlämpöiseen veteen. Lisää jauhot, öljy, rosmariini ja suola ja vaivaa tasaiseksi. Anna taikinan kohota peitettynä vedottomassa paikassa vähintään puoli tuntia.\r\n# Kuori keltajuuri, punajuuri ja raitajuuri. Leikkaa ne mandoliinilla tai juustohöylällä ohuiksi viipaleiksi.\r\n# Kauli pizzataikina jauhotetun leivinpaperin päällä uunipellin kokoiseksi, soikeaksi ja ohueksi pohjaksi.\r\n# Levitä taikinalle ranskankerma ja juuresviipaleet. Valuta viipaleille oliiviöljyä ja hunajaa ja ripottele päälle sormisuolaa.\r\n# Murustele juusto pizzan päälle.\r\n# Paista pizzaa 225-asteisessa uunissa noin 15 minuuttia.\r\n# Valuta pizzan päälle balsamiviinietikkakastiketta ja viimeistele tuoreella timjamilla.",
      "EndNote": "Voit halutessasi tehdä pizzan myös pelkistä punajuurista. *Annoshinnat on laskettu K-citymarket-ketjun hintatason mukaan 8/2019. Tuoretuotteiden hinnat vaihtelevat sesongeittain.",
      "Description": "Sinihomejuusto sopii mahtavasti juurespizzaan Hyvin ohuiksi siivuiksi leikatut puna-, kelta- ja raitajuuret kypsyvät yhtä aikaa pizzapohjan kanssa eikä niitä tarvitse etukäteen keittää. Noin 0,60€/annos",
      "DateCreated": "2019-05-02T15:39:07",
      "DateModified": "2019-09-16T08:51:28",
      "TvDate": "2019-09-25T00:00:00",
      "Stamp": {
        "Name": "Testattu K-koekeittiössä",
        "Id": "1"
      },
      "RecipeUse": {
        "Name": "k-ruoka.fi",
        "Id": "1"
      },
      "resourceType": "recipe",
      "Url": "https://www.k-ruoka.fi/reseptit/juurespizza",
      "UrlSlug": "juurespizza",
      "Sort": [1, 1, 10418]
    }, {
      "Id": "10416",
      "Name": "Kookos-kanakeitto",
      "PieceSize": {
        "Unit": "g"
      },
      "Portions": {
        "Unit": "annosta",
        "Amount": "4"
      },
      "PreparationTime": {
        "Description": "alle 30 min",
        "TimeRange": {
          "MinTime": 15,
          "MaxTime": 30
        }
      },
      "UserPortions": {
        "$": {
          "Unit": "henkilölle"
        }
      },
      "Categories": [{
        "MainName": "Asiakassegmentointi (K-asiakkaat)",
        "MainId": "17",
        "SubName": "Mukavuudenhaluiset ",
        "SubId": "149"
      }, {
        "MainName": "Asiakassegmentointi (K-asiakkaat)",
        "MainId": "17",
        "SubName": "Vakiintuneet",
        "SubId": "150"
      }, {
        "MainName": "Teema",
        "MainId": "15",
        "SubName": "K-Marketin valikoima",
        "SubId": "151"
      }, {
        "MainName": "Apukategoriat",
        "MainId": "18",
        "SubName": "K-Marketin valikoima",
        "SubId": "181"
      }, {
        "MainName": "Makuja maailmalta",
        "MainId": "2",
        "SubName": "Aasia",
        "SubId": "7"
      }, {
        "MainName": "Pääruoat",
        "MainId": "4",
        "SubName": "Broileri- ja linturuoat",
        "SubId": "21"
      }, {
        "MainName": "Pääruoat",
        "MainId": "4",
        "SubName": "Keitot",
        "SubId": "27"
      }, {
        "MainName": "Pääraaka-aine",
        "MainId": "14",
        "SubName": "Broileri ja linnut",
        "SubId": "118"
      }, {
        "MainName": "Ruokalaji",
        "MainId": "16",
        "SubName": "Keitot",
        "SubId": "136"
      }],
      "Pictures": ["PI9_19_kookos_kanakeit"],
      "PictureUrls": [{
        "Id": "PI9_19_kookos_kanakeit",
        "Normal": "https://k-file-storage-qa.imgix.net/f/k-ruoka/recipe/10416?w=1000&h=1000&fit=clip",
        "Original": "https://k-file-storage-qa.imgix.net/f/k-ruoka/recipe/10416"
      }],
      "SpecialDiets": [{
        "Id": "3",
        "Name": "Maidoton"
      }],
      "Ingredients": [{
        "SubSectionHeading": "_",
        "SubSectionIngredients": [
          [{
            "Name": "Pirkka hunajamarinoituja broilerin fileesuikaleita",
            "Ean": "6410405125378",
            "Amount": "1",
            "AmountInfo": "(450 g)",
            "PackageRecipe": "false",
            "IngredientType": "5322",
            "Unit": "rs",
            "IngredientTypeName": "Broilerinfileesuikale, hunajamarinoitu"
          }],
          [{
            "Name": "purjoa",
            "Amount": "100",
            "PackageRecipe": "false",
            "IngredientType": "6678",
            "Unit": "g",
            "IngredientTypeName": "Purjo, tuore"
          }],
          [{
            "Name": "perunaa",
            "Amount": "4",
            "AmountInfo": "(420 g)",
            "PackageRecipe": "false",
            "IngredientType": "7368",
            "Unit": "",
            "IngredientTypeName": "Yleisperuna"
          }],
          [{
            "Name": "porkkanaa",
            "Ean": "6410402027521",
            "Amount": "2",
            "AmountInfo": "(270 g)",
            "PackageRecipe": "false",
            "IngredientType": "6582",
            "Unit": "",
            "IngredientTypeName": "Porkkana"
          }],
          [{
            "Name": "Pirkka mieto punainen chili",
            "Ean": "6410405091857",
            "Amount": "1",
            "PackageRecipe": "false",
            "IngredientType": "5355",
            "Unit": "",
            "IngredientTypeName": "Chili, punainen"
          }],
          [{
            "Name": "punaista paprikaa",
            "Amount": "1",
            "AmountInfo": "(200 g)",
            "PackageRecipe": "false",
            "IngredientType": "6448",
            "Unit": "",
            "IngredientTypeName": "Paprika, punainen"
          }],
          [{
            "Name": "öljyä",
            "Amount": "2",
            "PackageRecipe": "false",
            "IngredientType": "6807",
            "Unit": "rkl",
            "IngredientTypeName": "Rypsiöljy"
          }],
          [{
            "Name": "currya",
            "Amount": "2",
            "PackageRecipe": "false",
            "IngredientType": "5382",
            "Unit": "tl",
            "IngredientTypeName": "Curry"
          }],
          [{
            "Name": "kurkumaa",
            "Amount": "1",
            "PackageRecipe": "false",
            "IngredientType": "10200",
            "Unit": "tl",
            "IngredientTypeName": "Kurkuma, jauhettu"
          }],
          [{
            "Name": "vettä",
            "Amount": "5",
            "PackageRecipe": "false",
            "IngredientType": "15792",
            "Unit": "dl",
            "IngredientTypeName": "Vesi"
          }],
          [{
            "Name": "kanaliemikuutiota",
            "Amount": "2",
            "PackageRecipe": "false",
            "IngredientType": "5990",
            "Unit": "",
            "IngredientTypeName": "Liemikuutio, kana"
          }],
          [{
            "Name": "Pirkka kookosmaitoa",
            "Ean": "6410402020669",
            "Amount": "1",
            "AmountInfo": "(400 ml)",
            "PackageRecipe": "false",
            "IngredientType": "5847",
            "Unit": "tlk",
            "IngredientTypeName": "Kookosmaito"
          }]
        ]
      }, {
        "SubSectionHeading": "Koristelu",
        "SubSectionIngredients": [
          [{
            "Name": "tuoretta persiljaa",
            "Amount": "1",
            "PackageRecipe": "false",
            "IngredientType": "6517",
            "Unit": "dl",
            "IngredientTypeName": "Persilja, tuore"
          }]
        ]
      }],
      "Instructions": "# Ota broilerin fileesuikaleet huoneenlämpöön. Halkaise ja huuhdo purjo ja leikkaa renkaiksi. Kuori ja leikkaa perunat parin sentin kuutioiksi. Kuori ja leikkaa porkkanat sentin kiekoiksi.\r\n# Viipaloi chili renkaiksi, voit halutessasi käyttää myös siemenet. Poista paprikasta siemenet ja leikkaa muutaman sentin paloiksi.\r\n# Kuumenna öljy kattilassa ja lisää mausteet, broilerit, purjo ja chili. Paista noin 5 minuuttia, kunnes broileri on kypsää.\r\n# Lisää kattilaan vesi, kanaliemikuutiot, perunat ja porkkanat ja keitä noin 10 minuuttia, kunnes perunat ovat lähes kypsiä. Lisää paprikat ja kookosmaito ja keitä vielä noin 5 minuuttia.\r\n# Koristele kookos-kanakeitto tuoreella persiljalla.",
      "EndNote": "*Annoshinnat on laskettu K-citymarket-ketjun hintatason mukaan 8/2019. Tuoretuotteiden hinnat vaihtelevat sesongeittain.",
      "Description": "Kookos-kanakeitto on helppo arkisoppa, joka valmistuu nopeasti. Kookosmaidon ansiosta keitto maistuu ihanan täyteläiseltä. Noin 2,25€/annos",
      "DateCreated": "2019-05-02T13:47:12",
      "DateModified": "2019-08-09T09:48:29",
      "Stamp": {
        "Name": "Testattu K-koekeittiössä",
        "Id": "1"
      },
      "RecipeUse": {
        "Name": "k-ruoka.fi",
        "Id": "1"
      },
      "resourceType": "recipe",
      "Url": "https://www.k-ruoka.fi/reseptit/kookos-kanakeitto",
      "UrlSlug": "kookos-kanakeitto",
      "Sort": [1, 1, 10416]
    }, {
      "Id": "10405",
      "Name": "Korealainen possubulgogi ja seesami-kaalisalaatti",
      "PieceSize": {
        "Unit": "g"
      },
      "Portions": {
        "Unit": "annosta",
        "Amount": "4"
      },
      "PreparationTime": {
        "Description": "30 - 60 min",
        "TimeRange": {
          "MinTime": 30,
          "MaxTime": 60
        }
      },
      "UserPortions": {
        "$": {
          "Unit": "henkilölle"
        }
      },
      "Categories": [{
        "MainName": "Makuja maailmalta",
        "MainId": "2",
        "SubName": "Aasia",
        "SubId": "7"
      }, {
        "MainName": "Pääruoat",
        "MainId": "4",
        "SubName": "Liharuoat",
        "SubId": "24"
      }, {
        "MainName": "Pääraaka-aine",
        "MainId": "14",
        "SubName": "Liha",
        "SubId": "114"
      }],
      "Pictures": ["korealainen_possup_HK19"],
      "PictureUrls": [{
        "Id": "korealainen_possup_HK19",
        "Normal": "https://k-file-storage-qa.imgix.net/f/k-ruoka/recipe/10405?w=1000&h=1000&fit=clip",
        "Original": "https://k-file-storage-qa.imgix.net/f/k-ruoka/recipe/10405"
      }],
      "Ingredients": [{
        "SubSectionHeading": "_",
        "SubSectionIngredients": [
          [{
            "Name": "HK Rypsiporsas Korean bulgogi nopeita fileesuikaleita",
            "Amount": "2",
            "AmountInfo": "(à 335 g)",
            "PackageRecipe": "false",
            "IngredientType": "16188",
            "Unit": "pkt",
            "IngredientTypeName": "HK rypsiporsas Korean Bulgogi nopeita fileesuikaleita"
          }],
          [{
            "Name": "suippo- tai valkokaalia",
            "Amount": "300",
            "PackageRecipe": "false",
            "IngredientType": "7256",
            "Unit": "g",
            "IngredientTypeName": "Valkokaali"
          }],
          [{
            "Name": "paksoita",
            "Amount": "2",
            "PackageRecipe": "false",
            "IngredientType": "7484",
            "Unit": "",
            "IngredientTypeName": "Paksoi"
          }],
          [{
            "Name": "porkkanaa",
            "Amount": "150",
            "PackageRecipe": "false",
            "IngredientType": "6582",
            "Unit": "g",
            "IngredientTypeName": "Porkkana"
          }],
          [{
            "Name": "punainen chili",
            "Amount": "1",
            "PackageRecipe": "false",
            "IngredientType": "5355",
            "Unit": "",
            "IngredientTypeName": "Chili, punainen"
          }],
          [{
            "Name": "suolaa",
            "PackageRecipe": "false",
            "IngredientType": "7049",
            "Unit": "ripaus",
            "IngredientTypeName": "Suola"
          }]
        ]
      }, {
        "SubSectionHeading": "Kastike",
        "SubSectionIngredients": [
          [{
            "Name": "majoneesia",
            "Amount": "180",
            "PackageRecipe": "false",
            "IngredientType": "6058",
            "Unit": "g",
            "IngredientTypeName": "Majoneesi"
          }],
          [{
            "Name": "tahinia",
            "Amount": "40",
            "PackageRecipe": "false",
            "IngredientType": "6872",
            "Unit": "g",
            "IngredientTypeName": "Tahini, seesamitahna"
          }],
          [{
            "Name": "seesaminsiemeniä paahdettuna",
            "Amount": "15",
            "PackageRecipe": "false",
            "IngredientType": "6871",
            "Unit": "g",
            "IngredientTypeName": "Seesaminsiemen"
          }],
          [{
            "Name": "sitruunanmehua",
            "Amount": "2",
            "PackageRecipe": "false",
            "IngredientType": "6941",
            "Unit": "rkl",
            "IngredientTypeName": "Sitruuna"
          }],
          [{
            "Name": "miriniä eli makeaa riisiviiniä",
            "Amount": "2",
            "PackageRecipe": "false",
            "IngredientType": "16053",
            "Unit": "rkl",
            "IngredientTypeName": "Mirini, makea riisiviini"
          }]
        ]
      }],
      "Instructions": "# Leikkaa suippokaali, paksoi sekä kuoritut porkkanat ohuiksi suikaleiksi. Viipaloi chili ja sekoita kaikki vihannekset keskenään ja mausta suolalla.\r\n# Sekoita kastikkeen ainekset huolellisesti ja kaada vihannesten joukkoon. Sekoita ja anna maustua jääkaapissa puolisen tuntia.\r\n# Paista fileesuikaleet nopeasti kuumalla pannulla pienessä määrässä rypsiöljyä. Lisää joukkoon maustepussin sisältö ja sekoita hyvin. Lisää tilkka vettä, jotta mausteet leviävät suikaleisiin tasaisesti. Viimeistele bulgogi toisessa pussissa mukana tulleilla seesaminsiemenillä. Tarjoa possubulgogi korealaisen kaalisalaatin kanssa.",
      "EndNote": "",
      "Description": "Korealainen bulgogi on ohuina siivuina nopeasti kypsennettävää ja herkullisessa marinadissa mehevöitynyttä naudanlihaa, mutta se onnistuu myös possusta. Resepti: HK",
      "DateCreated": "2019-04-29T12:00:00",
      "DateModified": "2019-06-28T09:32:58",
      "Stamp": {
        "Name": "Kumppani",
        "Id": "6"
      },
      "RecipeUse": {
        "Name": "k-ruoka.fi",
        "Id": "1"
      },
      "resourceType": "recipe",
      "Url": "https://www.k-ruoka.fi/reseptit/korealainen-possubulgogi-ja-seesami-kaalisalaatti",
      "UrlSlug": "korealainen-possubulgogi-ja-seesami-kaalisalaatti",
      "Sort": [1, 1, 10405]
    }, {
      "Id": "10401",
      "Name": "Wallenberginpihvi naudan sisäpaistista",
      "PieceSize": {
        "Unit": "g"
      },
      "Portions": {
        "Unit": "annosta",
        "Amount": "4"
      },
      "PreparationTime": {
        "Description": "alle 30 min",
        "TimeRange": {
          "MinTime": 15,
          "MaxTime": 30
        }
      },
      "UserPortions": {
        "$": {
          "Unit": "henkilölle"
        }
      },
      "Categories": [{
        "MainName": "Pääruoat",
        "MainId": "4",
        "SubName": "Jauheliharuoat",
        "SubId": "23"
      }, {
        "MainName": "Pääraaka-aine",
        "MainId": "14",
        "SubName": "Jauheliha",
        "SubId": "117"
      }],
      "Pictures": ["Wallenberginpihvi_HK19"],
      "PictureUrls": [{
        "Id": "Wallenberginpihvi_HK19",
        "Normal": "https://k-file-storage-qa.imgix.net/f/k-ruoka/recipe/10401?w=1000&h=1000&fit=clip",
        "Original": "https://k-file-storage-qa.imgix.net/f/k-ruoka/recipe/10401"
      }],
      "Ingredients": [{
        "SubSectionHeading": "_",
        "SubSectionIngredients": [
          [{
            "Name": "HK hienoksi jauhettua naudan sisäpaistia",
            "Amount": "1",
            "AmountInfo": "(300 g)",
            "PackageRecipe": "false",
            "IngredientType": "16169",
            "Unit": "pkt",
            "IngredientTypeName": "HK hienoksi jauhettu naudan sisäpaisti"
          }],
          [{
            "Name": "kuohukermaa",
            "Amount": "2 1/2",
            "PackageRecipe": "false",
            "IngredientType": "5891",
            "Unit": "dl",
            "IngredientTypeName": "Kuohukerma"
          }],
          [{
            "Name": "kananmunan keltuaista",
            "Amount": "2",
            "PackageRecipe": "false",
            "IngredientType": "5683",
            "Unit": "",
            "IngredientTypeName": "Kananmuna"
          }],
          [{
            "Name": "nelimaustetta",
            "Amount": "1",
            "PackageRecipe": "false",
            "Unit": "tl"
          }],
          [{
            "Name": "suolaa",
            "PackageRecipe": "false",
            "IngredientType": "7049",
            "Unit": "ripaus",
            "IngredientTypeName": "Suola"
          }],
          [{
            "Name": "korppujauhoja",
            "Amount": "n. 3",
            "PackageRecipe": "false",
            "IngredientType": "5872",
            "Unit": "dl",
            "IngredientTypeName": "Korppujauho"
          }],
          [{
            "Name": "öljyä ja voita paistamiseen",
            "PackageRecipe": "false",
            "IngredientType": "6807",
            "IngredientTypeName": "Rypsiöljy"
          }]
        ]
      }, {
        "SubSectionHeading": "Ruskistettu voi",
        "SubSectionIngredients": [
          [{
            "Name": "voita",
            "Amount": "100",
            "PackageRecipe": "false",
            "IngredientType": "7343",
            "Unit": "g",
            "IngredientTypeName": "Voi"
          }]
        ]
      }],
      "Instructions": "# Huolehdi, että raaka-aineet ovat kylmiä, kun aloitat valmistamaan jauhelihamassaa. Sekoita keltuaiset ja mausteet jauhelihan joukkoon kulhossa. Lisää kermaa joukkoon vähän kerrallaan koko ajan sekoittaen. Sekoita kunnes lopputuloksena on sileä jauhelihamassa.\r\n# Pane korppujauhot laakeaan astiaan. Jaa jauhelihamassa neljään osaan kostutetulla lusikalla suoraan korppujauhoille. Ripottele korppujauhoja massapallojen päälle, jotta voit muotoilla niistä siistejä paksuja pihvejä. Jauhota pihvit kauttaaltaan korppujauhoilla. Pidä pihvit kylmässä siihen asti, kunnes aloitat paistamaan niitä.\r\n# Valmista ruskistettu voi. Pane voi kattilaan ja ruskista hiljalleen keskilämmöllä, kunnes voi alkaa vaahdota voimakkaasti ja ruskistuu kauniin väriseksi. Valmiin ruskistetun voin tuoksu on voimakkaan pähkinäinen.\r\n# Paista pihvit keskilämmöllä voi-rypsiöljyseoksessa noin 4 minuutta per puoli. Valele pihvejä paiston aikana paistorasvalla. Tarjoa wallenbergin pihvit perunamuusin, herneiden ja ruskistetun voin kera.",
      "EndNote": "",
      "Description": "Wallenberginpihvi on klassikko, joka perinteisesti valmistetaan vasikanlihasta, mutta se onnistuu myös vähärasvaisesta naudan jauhelihasta. Resepti: HK",
      "DateCreated": "2019-04-29T10:59:18",
      "DateModified": "2019-06-28T09:34:31",
      "Stamp": {
        "Name": "Kumppani",
        "Id": "6"
      },
      "RecipeUse": {
        "Name": "k-ruoka.fi",
        "Id": "1"
      },
      "resourceType": "recipe",
      "Url": "https://www.k-ruoka.fi/reseptit/wallenberginpihvi-naudan-sisapaistista",
      "UrlSlug": "wallenberginpihvi-naudan-sisapaistista",
      "Sort": [1, 1, 10401]
    }, {
      "Id": "10397",
      "Name": "Karitsaburgeri",
      "PieceSize": {
        "Unit": "g"
      },
      "Portions": {
        "Unit": "annosta",
        "Amount": "4"
      },
      "PreparationTime": {
        "Description": "yli 60 min",
        "TimeRange": {
          "MinTime": 60
        }
      },
      "UserPortions": {
        "$": {
          "Unit": "henkilölle"
        }
      },
      "Categories": [{
        "MainName": "Sesonki",
        "MainId": "1",
        "SubName": "Grillaus",
        "SubId": "5"
      }, {
        "MainName": "Pääruoat",
        "MainId": "4",
        "SubName": "Hampurilaiset ja täytetyt leivät",
        "SubId": "86"
      }, {
        "MainName": "Sesonki",
        "MainId": "1",
        "SubName": "Kesä",
        "SubId": "121"
      }, {
        "MainName": "Sesonki",
        "MainId": "1",
        "SubName": "Juhannus",
        "SubId": "122"
      }, {
        "MainName": "Ruokalaji",
        "MainId": "16",
        "SubName": "Hampurilaiset ja täytetyt leivät",
        "SubId": "140"
      }, {
        "MainName": "Asiakassegmentointi (K-asiakkaat)",
        "MainId": "17",
        "SubName": "Nautiskelijat",
        "SubId": "146"
      }, {
        "MainName": "Asiakassegmentointi (K-asiakkaat)",
        "MainId": "17",
        "SubName": "Innostujat",
        "SubId": "147"
      }, {
        "MainName": "Asiakassegmentointi (K-asiakkaat)",
        "MainId": "17",
        "SubName": "Tiedostavat",
        "SubId": "148"
      }],
      "Pictures": ["karitsaburgeri_19"],
      "PictureUrls": [{
        "Id": "karitsaburgeri_19",
        "Normal": "https://k-file-storage-qa.imgix.net/f/k-ruoka/recipe/10397?w=1000&h=1000&fit=clip",
        "Original": "https://k-file-storage-qa.imgix.net/f/k-ruoka/recipe/10397"
      }],
      "Ingredients": [{
        "SubSectionHeading": "_",
        "SubSectionIngredients": [
          [{
            "Name": "munakoiso",
            "Amount": "1",
            "PackageRecipe": "false",
            "IngredientType": "6248",
            "Unit": "",
            "IngredientTypeName": "Munakoiso"
          }],
          [{
            "Name": "salottisipulia",
            "Amount": "2",
            "PackageRecipe": "false",
            "IngredientType": "6851",
            "Unit": "",
            "IngredientTypeName": "Salottisipuli"
          }],
          [{
            "Name": "timjamia hienonnettuna",
            "Amount": "1",
            "PackageRecipe": "false",
            "IngredientType": "7119",
            "Unit": "rkl",
            "IngredientTypeName": "Timjami, tuore"
          }],
          [{
            "Name": "oliiviöljyä",
            "Amount": "4",
            "PackageRecipe": "false",
            "IngredientType": "6386",
            "Unit": "rkl",
            "IngredientTypeName": "Oliiviöljy"
          }],
          [{
            "Name": "suolaa",
            "PackageRecipe": "false",
            "IngredientType": "7049",
            "Unit": "ripaus",
            "IngredientTypeName": "Suola"
          }],
          [{
            "Name": "mustapippuria",
            "PackageRecipe": "false",
            "IngredientType": "6269",
            "Unit": "ripaus",
            "IngredientTypeName": "Mustapippuri, kokonainen"
          }],
          [{
            "Name": "Naughty BRGR Base aiolia",
            "Amount": "2",
            "PackageRecipe": "false",
            "IngredientType": "16074",
            "Unit": "dl",
            "IngredientTypeName": "Naughty BRGR Base aioli"
          }],
          [{
            "Name": "karitsan jauhelihaa",
            "Amount": "600",
            "PackageRecipe": "false",
            "IngredientType": "9402",
            "Unit": "g",
            "IngredientTypeName": "Karitsan jauheliha"
          }],
          [{
            "Name": "suolaa",
            "PackageRecipe": "false",
            "IngredientType": "7049",
            "IngredientTypeName": "Suola"
          }],
          [{
            "Name": "mustapippuria",
            "PackageRecipe": "false",
            "IngredientType": "6269",
            "IngredientTypeName": "Mustapippuri, kokonainen"
          }],
          [{
            "Name": "punasipulia",
            "Amount": "2",
            "PackageRecipe": "false",
            "IngredientType": "6667",
            "Unit": "",
            "IngredientTypeName": "Punasipuli"
          }],
          [{
            "Name": "punaviinietikkaa",
            "Amount": "1",
            "PackageRecipe": "false",
            "IngredientType": "6668",
            "Unit": "dl",
            "IngredientTypeName": "Punaviinietikka"
          }],
          [{
            "Name": "sokeria",
            "Amount": "1",
            "PackageRecipe": "false",
            "IngredientType": "6961",
            "Unit": "dl",
            "IngredientTypeName": "Sokeri"
          }],
          [{
            "Name": "lehtipersiljaa",
            "Amount": "1/2",
            "PackageRecipe": "false",
            "IngredientType": "5954",
            "Unit": "ruukkua",
            "IngredientTypeName": "Lehtipersilja"
          }],
          [{
            "Name": "minttua",
            "Amount": "1/2",
            "PackageRecipe": "false",
            "IngredientType": "15789",
            "Unit": "ruukkua",
            "IngredientTypeName": "Minttu"
          }],
          [{
            "Name": "Vaasan The BRGR Naughty Burgers -sämpylää",
            "Amount": "4",
            "PackageRecipe": "false",
            "IngredientType": "16426",
            "Unit": "",
            "IngredientTypeName": "Vaasan Naughty BRGR sämpylä"
          }],
          [{
            "Name": "fetajuustoa",
            "Amount": "100",
            "PackageRecipe": "false",
            "IngredientType": "5432",
            "Unit": "g",
            "IngredientTypeName": "Fetajuusto"
          }],
          [{
            "Name": "punasipuli",
            "Amount": "1",
            "PackageRecipe": "false",
            "IngredientType": "6667",
            "Unit": "",
            "IngredientTypeName": "Punasipuli"
          }],
          [{
            "Name": "salaatinlehteä",
            "Amount": "4",
            "PackageRecipe": "false",
            "IngredientType": "5608",
            "Unit": "",
            "IngredientTypeName": "Jääsalaatti"
          }]
        ]
      }],
      "Instructions": "# Pistele munakoisoon haarukalla muutamia reikiä sinne tänne. Paahda kypsäksi uunissa 180-asteessa uuninpellillä leivinpaperin päällä 45 minuuttia. Nosta pelliltä leikkuulaudalle jäähtymään.\r\n# Kuori ja kuutioi salottisipulit pieniksi kuutioiksi. Kuullota sipulit ja timjaminlehdet oliiviöljyssä pannulla, kunnes ne ovat pehmenneet.\r\n# Halkaise paahdettu munakoiso veitsellä kahtia pituussuunnassa. Kaavi munakoison sisus kuorista irti, silppua malto veitsellä karkeasti, lisää pannulle sipulien joukkoon ja keitä keskilämmöllä kunnes nesteet ovat haihtuneet ja seos on sakeaa. Mausta suolalla, pippurilla ja laita jääkaappiin jäähtymään.\r\n# Kuori punasipuli ja leikkaa ohuiksi renkaiksi kulhoon. Lisää kulhoon etikka, sokeri ja mausta suolalla. Anna marinoitua 15 minuuttia. Valuta marinoitu punasipuli ja laita kulhoon. Riivi joukkoon lehtipersiljan ja mintun lehdet. Sekoita.\r\n# Sekoita jäähtynyt munakoisomassa ja Aioli keskenään. Mausta tarvittaessa suolalla ja pippurilla.\r\n# Muotoile jauhelihasta 4 tasakokoista lihapullaa ja taputtele niistä varovasti sämpylän halkaisijan kokoisia pihvejä. Mausta pihvit suolalla ja pippurilla.\r\n# Grillaa pihvejä kuumassa grillissä 3 minuuttia per puoli tai kunnes kypsyys on hieman rose.\r\n# Paahda sämpylöitä pannulla tai uunissa 3 minuuttia tai kunnes ne ovat paahtuneet kullankeltaisiksi.\r\n# Sivele tuunattua Aiolia paahdettujen sämpylöiden leikkauspinnan puolelle tasaisesti.\r\n# Kokoa burgeri siten että pohjalle tulee ensin pihvi, sitten fetajuusto murustellen, marinoitu punasipuli-yrttisalaatti ja salaatinlehti. Laita lopuksi kansi päälle ja tarjoa.",
      "EndNote": "",
      "Description": "Karitsaburgerin täytteenä on karitsan jauhelihapihvi, paahdettua munakoisoa, marinoitua punasipuli-yrttisalaattia ja fetaa. Resepti: Akseli Herlevi",
      "DateCreated": "2019-04-29T10:19:35",
      "DateModified": "2019-04-29T14:48:06",
      "Stamp": {
        "Name": "Kumppani",
        "Id": "6"
      },
      "RecipeUse": {
        "Name": "k-ruoka.fi",
        "Id": "1"
      },
      "resourceType": "recipe",
      "Url": "https://www.k-ruoka.fi/reseptit/karitsaburgeri",
      "UrlSlug": "karitsaburgeri",
      "Sort": [1, 1, 10397]
    }, {
      "Id": "10396",
      "Name": "Nacho Burger",
      "PieceSize": {
        "Unit": "g"
      },
      "Portions": {
        "Unit": "annosta",
        "Amount": "4"
      },
      "PreparationTime": {
        "Description": "alle 30 min",
        "TimeRange": {
          "MinTime": 15,
          "MaxTime": 30
        }
      },
      "UserPortions": {
        "$": {
          "Unit": "henkilölle"
        }
      },
      "Categories": [{
        "MainName": "Sesonki",
        "MainId": "1",
        "SubName": "Grillaus",
        "SubId": "5"
      }, {
        "MainName": "Sesonki",
        "MainId": "1",
        "SubName": "Uusivuosi",
        "SubId": "81"
      }, {
        "MainName": "Pääruoat",
        "MainId": "4",
        "SubName": "Hampurilaiset ja täytetyt leivät",
        "SubId": "86"
      }, {
        "MainName": "Pääraaka-aine",
        "MainId": "14",
        "SubName": "Jauheliha",
        "SubId": "117"
      }, {
        "MainName": "Sesonki",
        "MainId": "1",
        "SubName": "Kesä",
        "SubId": "121"
      }, {
        "MainName": "Sesonki",
        "MainId": "1",
        "SubName": "Juhannus",
        "SubId": "122"
      }, {
        "MainName": "Ruokalaji",
        "MainId": "16",
        "SubName": "Hampurilaiset ja täytetyt leivät",
        "SubId": "140"
      }, {
        "MainName": "Asiakassegmentointi (K-asiakkaat)",
        "MainId": "17",
        "SubName": "Nautiskelijat",
        "SubId": "146"
      }, {
        "MainName": "Asiakassegmentointi (K-asiakkaat)",
        "MainId": "17",
        "SubName": "Mukavuudenhaluiset ",
        "SubId": "149"
      }],
      "Pictures": ["grand_theft_nacho_b_19"],
      "PictureUrls": [{
        "Id": "grand_theft_nacho_b_19",
        "Normal": "https://k-file-storage-qa.imgix.net/f/k-ruoka/recipe/10396?w=1000&h=1000&fit=clip",
        "Original": "https://k-file-storage-qa.imgix.net/f/k-ruoka/recipe/10396"
      }],
      "Ingredients": [{
        "SubSectionHeading": "_",
        "SubSectionIngredients": [
          [{
            "Name": "Pirkka syöntikypsää avokadoa",
            "Ean": "2000388400001",
            "Amount": "2",
            "AmountInfo": "(350 g)",
            "PackageRecipe": "false",
            "IngredientType": "7605",
            "Unit": "",
            "IngredientTypeName": "Avokado, tuore"
          }],
          [{
            "Name": "suolaa",
            "PackageRecipe": "false",
            "IngredientType": "7049",
            "Unit": "ripaus",
            "IngredientTypeName": "Suola"
          }],
          [{
            "Name": "mustapippuria",
            "PackageRecipe": "false",
            "IngredientType": "6269",
            "Unit": "ripaus",
            "IngredientTypeName": "Mustapippuri, kokonainen"
          }],
          [{
            "Name": "limetin mehu",
            "Amount": "1",
            "PackageRecipe": "false",
            "IngredientType": "5997",
            "Unit": "",
            "IngredientTypeName": "Limetti"
          }],
          [{
            "Name": "pieni punasipuli",
            "Amount": "1",
            "AmountInfo": "(60 g)",
            "PackageRecipe": "false",
            "IngredientType": "6667",
            "Unit": "",
            "IngredientTypeName": "Punasipuli"
          }],
          [{
            "Name": "Tamminen Naughty BRGR naudan burgerpihviä",
            "Amount": "2",
            "AmountInfo": "(à 280 g)",
            "PackageRecipe": "false",
            "IngredientType": "16305",
            "Unit": "pkt",
            "IngredientTypeName": "Naughty BRGR burgeripihvi"
          }],
          [{
            "Name": "Naughty BRGR burger makusuolaa",
            "Amount": "4",
            "PackageRecipe": "false",
            "IngredientType": "16010",
            "Unit": "tl",
            "IngredientTypeName": "Naughty BRGR Cuba -suola"
          }],
          [{
            "Name": "Valio Naughty BRGR cheddarjuustoa",
            "Amount": "8",
            "PackageRecipe": "false",
            "IngredientType": "16427",
            "Unit": "viipaletta",
            "IngredientTypeName": "Valio Naughty BRGR cheddarjuusto"
          }],
          [{
            "Name": "Vaasan The BRGR Naughty Burgers -sämpylää",
            "Amount": "4",
            "PackageRecipe": "false",
            "IngredientType": "16426",
            "Unit": "",
            "IngredientTypeName": "Vaasan Naughty BRGR sämpylä"
          }],
          [{
            "Name": "tomaattiviipaletta",
            "Amount": "4",
            "PackageRecipe": "false",
            "IngredientType": "7124",
            "Unit": "",
            "IngredientTypeName": "Tomaatti"
          }],
          [{
            "Name": "Pirkka tortillachipsejä",
            "Ean": "6410405084415",
            "Amount": "2",
            "PackageRecipe": "false",
            "IngredientType": "6031",
            "Unit": "dl",
            "IngredientTypeName": "Maissilastu"
          }],
          [{
            "Name": "Pirkka meksikolaista salsa chilpotle -kastiketta",
            "Ean": "6410405150257",
            "Amount": "n. 4",
            "PackageRecipe": "false",
            "IngredientType": "6857",
            "Unit": "rkl",
            "IngredientTypeName": "Salsakastike, chipotle"
          }]
        ]
      }, {
        "SubSectionHeading": "Jalapenomajoneesi",
        "SubSectionIngredients": [
          [{
            "Name": "Naughty BRGR Base aiolia",
            "Amount": "2",
            "PackageRecipe": "false",
            "IngredientType": "16074",
            "Unit": "dl",
            "IngredientTypeName": "Naughty BRGR Base aioli"
          }],
          [{
            "Name": "jalapenoviipaleita",
            "Amount": "1",
            "PackageRecipe": "false",
            "IngredientType": "5573",
            "Unit": "dl",
            "IngredientTypeName": "Jalapeno, viipale"
          }],
          [{
            "Name": "korianteria hienonnettuna",
            "Amount": "4",
            "PackageRecipe": "false",
            "IngredientType": "5852",
            "Unit": "rkl",
            "IngredientTypeName": "Korianteri, tuore"
          }],
          [{
            "Name": "suolaa",
            "PackageRecipe": "false",
            "IngredientType": "7049",
            "Unit": "ripaus",
            "IngredientTypeName": "Suola"
          }],
          [{
            "Name": "mustapippuria",
            "PackageRecipe": "false",
            "IngredientType": "6269",
            "Unit": "ripaus",
            "IngredientTypeName": "Mustapippuri, kokonainen"
          }]
        ]
      }],
      "Instructions": "# Hienonna jalapenot. Jätä osa jalapenoista koristeiksi. Sekoita jalapenomajoneesin ainekset keskenään.\r\n# Halkaise avokadot ja irrota hedelmäliha varovasti lusikalla. Leikkaa avokado ohuiksi viipaleiksi ja paina veitsen kyljellä viipaleet viuhkaksi. Mausta suolalla, pippurilla ja limetinmehulla.\r\n# Kuori ja hienonna punasipuli.\r\n# Mausta burgerpihvit kauttaaltaan makusuolalla. Grillaa kuumassa grillissä noin 3 minuuttia per puoli. Kun olet kääntänyt pihvit ensimmäisen kerran, laita grillattujen pihvien päälle cheddarviipaleet sulamaan.\r\n# Paahda sämpylöitä pannulla tai uunissa noin 3 minuuttia, kunnes ne ovat paahtuneet kauniisti.\r\n# Sivele jalapenomajoneesia paahdettujen sämpylöiden leikkauspinnoille.\r\n# Kokoa burgeri siten, että pohjalle tulee ensin tomaattiviipale, sitten pihvi juustoineen, sitten nachoja, avokadoviipaleita, punasipulia. Viimeistele chilpotle-kastikkeella ja jalapenoviipaleilla. Nosta sämpylät kanneksi ja tarjoa heti.",
      "EndNote": "*Annoshinnat on laskettu K-citymarket-ketjun hintatason mukaan 11/2019. Tuoretuotteiden hinnat vaihtelevat sesongeittain.",
      "Description": "Nachot sopivat loistavasti burgerin väliin. Tunnet ihanan rusahduksen purtaessa ja sitten iskee jalapenojen etikkainen leimahdus! Noin 5,10 €/annos*\r\nResepti: Akseli Herlevi",
      "DateCreated": "2019-04-29T10:19:23",
      "DateModified": "2019-11-06T16:18:50",
      "Stamp": {
        "Name": "Kumppani",
        "Id": "6"
      },
      "RecipeUse": {
        "Name": "k-ruoka.fi",
        "Id": "1"
      },
      "resourceType": "recipe",
      "Url": "https://www.k-ruoka.fi/reseptit/nacho-burger",
      "UrlSlug": "nacho-burger",
      "Sort": [1, 1, 10396]
    }, {
      "Id": "10395",
      "Name": "Tuplapekoniburgeri",
      "PieceSize": {
        "Unit": "g"
      },
      "Portions": {
        "Unit": "annosta",
        "Amount": "4"
      },
      "PreparationTime": {
        "Description": "alle 30 min",
        "TimeRange": {
          "MinTime": 15,
          "MaxTime": 30
        }
      },
      "UserPortions": {
        "$": {
          "Unit": "henkilölle"
        }
      },
      "Categories": [{
        "MainName": "Sesonki",
        "MainId": "1",
        "SubName": "Grillaus",
        "SubId": "5"
      }, {
        "MainName": "Pääruoat",
        "MainId": "4",
        "SubName": "Grilliruoat",
        "SubId": "73"
      }, {
        "MainName": "Pääruoat",
        "MainId": "4",
        "SubName": "Hampurilaiset ja täytetyt leivät",
        "SubId": "86"
      }, {
        "MainName": "Pääraaka-aine",
        "MainId": "14",
        "SubName": "Liha",
        "SubId": "114"
      }, {
        "MainName": "Sesonki",
        "MainId": "1",
        "SubName": "Kesä",
        "SubId": "121"
      }, {
        "MainName": "Sesonki",
        "MainId": "1",
        "SubName": "Juhannus",
        "SubId": "122"
      }, {
        "MainName": "Ruokalaji",
        "MainId": "16",
        "SubName": "Hampurilaiset ja täytetyt leivät",
        "SubId": "140"
      }, {
        "MainName": "Asiakassegmentointi (K-asiakkaat)",
        "MainId": "17",
        "SubName": "Nautiskelijat",
        "SubId": "146"
      }, {
        "MainName": "Asiakassegmentointi (K-asiakkaat)",
        "MainId": "17",
        "SubName": "Mukavuudenhaluiset ",
        "SubId": "149"
      }, {
        "MainName": "Asiakassegmentointi (K-asiakkaat)",
        "MainId": "17",
        "SubName": "Vakiintuneet",
        "SubId": "150"
      }],
      "Pictures": ["tuplapekoniburgeri_19"],
      "PictureUrls": [{
        "Id": "tuplapekoniburgeri_19",
        "Normal": "https://k-file-storage-qa.imgix.net/f/k-ruoka/recipe/10395?w=1000&h=1000&fit=clip",
        "Original": "https://k-file-storage-qa.imgix.net/f/k-ruoka/recipe/10395"
      }],
      "Ingredients": [{
        "SubSectionHeading": "_",
        "SubSectionIngredients": [
          [{
            "Name": "HK Amerikanpekonia",
            "Amount": "1",
            "AmountInfo": "(170 g)",
            "PackageRecipe": "false",
            "IngredientType": "6498",
            "Unit": "pkt",
            "IngredientTypeName": "Pekoni"
          }],
          [{
            "Name": "Naughty BRGR punasipulihilloketta",
            "Amount": "1",
            "PackageRecipe": "false",
            "IngredientType": "16066",
            "Unit": "dl",
            "IngredientTypeName": "Naughty BRGR punasipulihilloke"
          }],
          [{
            "Name": "Vaasan The BRGR Naughty Burgers -sämpylää",
            "Amount": "4",
            "PackageRecipe": "false",
            "IngredientType": "16426",
            "Unit": "",
            "IngredientTypeName": "Vaasan Naughty BRGR sämpylä"
          }],
          [{
            "Name": "Naughty BRGR burger makusuolaa",
            "Amount": "4",
            "PackageRecipe": "false",
            "IngredientType": "16010",
            "Unit": "tl",
            "IngredientTypeName": "Naughty BRGR Cuba -suola"
          }],
          [{
            "Name": "Naughty BRGR burgeripihviä",
            "Amount": "4",
            "PackageRecipe": "false",
            "IngredientType": "16305",
            "Unit": "",
            "IngredientTypeName": "Naughty BRGR burgeripihvi"
          }],
          [{
            "Name": "Valio Naughty BRGR cheddarjuustoa",
            "Amount": "8",
            "PackageRecipe": "false",
            "IngredientType": "16427",
            "Unit": "viipaletta",
            "IngredientTypeName": "Valio Naughty BRGR cheddarjuusto"
          }],
          [{
            "Name": "suolakurkkuviipaleita",
            "Amount": "1",
            "PackageRecipe": "false",
            "IngredientType": "14464",
            "Unit": "dl",
            "IngredientTypeName": "Suolakurkku, viipale"
          }],
          [{
            "Name": "French's amerikkalaista sinappia",
            "Amount": "4",
            "PackageRecipe": "false",
            "IngredientType": "16157",
            "Unit": "rkl",
            "IngredientTypeName": "French's amerikkalainen sinappi"
          }]
        ]
      }],
      "Instructions": "# Paista pekoniviipaleet rapeiksi pannulla ja nosta talouspaperin päälle valumaan.\r\n# Hienonna puolet pekoniviipaleista ja sekoita punasipulihillokkeen kanssa kulhossa.\r\n# Mausta pihvit makusuolalla tasaisesti kauttaaltaan ja grillaa kuumassa grillissä 3 minuuttia per puoli.\r\n# Kun olet kääntänyt pihvit, niin laita sen jälkeen pihvin grillatun puolen päälle cheddarjuustot valmiiksi sulamaan.\r\n# Paahda sämpylöitä pannulla tai uunissa 3 minuuttia tai kunnes ne ovat paahtuneet kullankeltaisiksi. Kokoa burgeri reseptin aineksista.",
      "EndNote": "",
      "Description": "Tässä burgerissa ei turhia kursailla. Simply delicious! Resepti: Akseli Herlevi",
      "DateCreated": "2019-04-29T10:17:44",
      "DateModified": "2019-04-29T14:49:11",
      "Stamp": {
        "Name": "Kumppani",
        "Id": "6"
      },
      "RecipeUse": {
        "Name": "k-ruoka.fi",
        "Id": "1"
      },
      "resourceType": "recipe",
      "Url": "https://www.k-ruoka.fi/reseptit/tuplapekoniburgeri",
      "UrlSlug": "tuplapekoniburgeri",
      "Sort": [1, 1, 10395]
    }, {
      "Id": "10394",
      "Name": "Lohiburgeri",
      "PieceSize": {
        "Unit": "g"
      },
      "Portions": {
        "Unit": "annosta",
        "Amount": "4"
      },
      "PreparationTime": {
        "Description": "alle 30 min",
        "TimeRange": {
          "MinTime": 15,
          "MaxTime": 30
        }
      },
      "UserPortions": {
        "$": {
          "Unit": "henkilölle"
        }
      },
      "Categories": [{
        "MainName": "Sesonki",
        "MainId": "1",
        "SubName": "Grillaus",
        "SubId": "5"
      }, {
        "MainName": "Pääruoat",
        "MainId": "4",
        "SubName": "Kalaruoat",
        "SubId": "20"
      }, {
        "MainName": "Pääruoat",
        "MainId": "4",
        "SubName": "Hampurilaiset ja täytetyt leivät",
        "SubId": "86"
      }, {
        "MainName": "Pääraaka-aine",
        "MainId": "14",
        "SubName": "Kala",
        "SubId": "116"
      }, {
        "MainName": "Sesonki",
        "MainId": "1",
        "SubName": "Kesä",
        "SubId": "121"
      }, {
        "MainName": "Sesonki",
        "MainId": "1",
        "SubName": "Juhannus",
        "SubId": "122"
      }, {
        "MainName": "Ruokalaji",
        "MainId": "16",
        "SubName": "Hampurilaiset ja täytetyt leivät",
        "SubId": "140"
      }, {
        "MainName": "Asiakassegmentointi (K-asiakkaat)",
        "MainId": "17",
        "SubName": "Nautiskelijat",
        "SubId": "146"
      }, {
        "MainName": "Asiakassegmentointi (K-asiakkaat)",
        "MainId": "17",
        "SubName": "Innostujat",
        "SubId": "147"
      }, {
        "MainName": "Asiakassegmentointi (K-asiakkaat)",
        "MainId": "17",
        "SubName": "Tiedostavat",
        "SubId": "148"
      }],
      "Pictures": ["lohiburgeri_19"],
      "PictureUrls": [{
        "Id": "lohiburgeri_19",
        "Normal": "https://k-file-storage-qa.imgix.net/f/k-ruoka/recipe/10394?w=1000&h=1000&fit=clip",
        "Original": "https://k-file-storage-qa.imgix.net/f/k-ruoka/recipe/10394"
      }],
      "Ingredients": [{
        "SubSectionHeading": "_",
        "SubSectionIngredients": [
          [{
            "Name": "fenkoli",
            "Amount": "1",
            "PackageRecipe": "false",
            "IngredientType": "5430",
            "Unit": "",
            "IngredientTypeName": "Fenkoli"
          }],
          [{
            "Name": "punasipuli",
            "Amount": "1",
            "PackageRecipe": "false",
            "IngredientType": "6667",
            "Unit": "",
            "IngredientTypeName": "Punasipuli"
          }],
          [{
            "Name": "Naughty BRGR Base aiolia",
            "Amount": "2",
            "PackageRecipe": "false",
            "IngredientType": "16074",
            "Unit": "dl",
            "IngredientTypeName": "Naughty BRGR Base aioli"
          }],
          [{
            "Name": "suolaa",
            "PackageRecipe": "false",
            "IngredientType": "7049",
            "Unit": "ripaus",
            "IngredientTypeName": "Suola"
          }],
          [{
            "Name": "pippuria",
            "PackageRecipe": "false",
            "IngredientType": "6269",
            "Unit": "ripaus",
            "IngredientTypeName": "Mustapippuri, kokonainen"
          }],
          [{
            "Name": "sitruuna",
            "Amount": "1",
            "PackageRecipe": "false",
            "IngredientType": "6941",
            "Unit": "",
            "IngredientTypeName": "Sitruuna"
          }],
          [{
            "Name": "salsa verdeä (ks. erillinen resepti)",
            "Amount": "2",
            "PackageRecipe": "false",
            "Unit": "dl"
          }],
          [{
            "Name": "Hätälä lohiburgerpihvejä",
            "Amount": "2",
            "AmountInfo": "(à 2 x 80 g)",
            "PackageRecipe": "false",
            "IngredientType": "16207",
            "Unit": "pkt",
            "IngredientTypeName": "Hätälä lohiburgerpihvi"
          }],
          [{
            "Name": "voita",
            "Amount": "2",
            "PackageRecipe": "false",
            "IngredientType": "7343",
            "Unit": "rkl",
            "IngredientTypeName": "Voi"
          }],
          [{
            "Name": "kurkkuviipaletta",
            "Amount": "4",
            "PackageRecipe": "false",
            "IngredientType": "5895",
            "Unit": "",
            "IngredientTypeName": "Kurkku"
          }],
          [{
            "Name": "tomaattiviipaletta",
            "Amount": "4",
            "PackageRecipe": "false",
            "IngredientType": "7124",
            "Unit": "",
            "IngredientTypeName": "Tomaatti"
          }],
          [{
            "Name": "Vaasan The BRGR Naughty Burgers -sämpylää",
            "Amount": "4",
            "PackageRecipe": "false",
            "IngredientType": "16426",
            "Unit": "",
            "IngredientTypeName": "Vaasan Naughty BRGR sämpylä"
          }]
        ]
      }],
      "Instructions": "# Suikaloi pesty fenkoli ja kuorittu punasipuli ohuiksi suikaleiksi kulhoon. Sekoita joukkoon aioli, mausta suolalla, pippurilla ja sitruunanmehulla.\r\n# Valmista salsa verde reseptin mukaan.\r\n# Paista lohiburgerpihvit pannulla voissa ja mausta suolalla ja pippurilla.\r\n# Leikkaa kurkku valmiiksi ohuiksi viipaleiksi.\r\n# Paahda sämpylöitä pannulla tai uunissa 3 minuuttia tai kunnes ne ovat paahtuneet kullankeltaisiksi.\r\n# Sivele Salsa Verdeä paahdettujen sämpylöiden leikkauspinnan puolelle tasaisesti.\r\n# Kokoa burgeri reseptin aineksista niin että pohjan päälle tulee ensin pihvi, sitten kurkku ja tomaattiviipaleet ja fenkolislaw.",
      "EndNote": "",
      "Description": "Lohiburgereiden täytteenä on raikasta fenkolislawta ja salsa verdeä. Voit sivellä salsa verden sijaan sämpylöille myös Pirkka sitruunapestoa. Resepti: Akseli Herlevi",
      "DateCreated": "2019-04-29T10:16:33",
      "DateModified": "2019-04-29T14:42:54",
      "Stamp": {
        "Name": "Kumppani",
        "Id": "6"
      },
      "RecipeUse": {
        "Name": "k-ruoka.fi",
        "Id": "1"
      },
      "resourceType": "recipe",
      "Url": "https://www.k-ruoka.fi/reseptit/lohiburgeri",
      "UrlSlug": "lohiburgeri",
      "Sort": [1, 1, 10394]
    }, {
      "Id": "10393",
      "Name": "Herkkusieni-halloumiburgeri",
      "PieceSize": {
        "Unit": "g"
      },
      "Portions": {
        "Unit": "annosta",
        "Amount": "4"
      },
      "PreparationTime": {
        "Description": "alle 30 min",
        "TimeRange": {
          "MinTime": 15,
          "MaxTime": 30
        }
      },
      "UserPortions": {
        "$": {
          "Unit": "henkilölle"
        }
      },
      "Categories": [{
        "MainName": "Pääruoat",
        "MainId": "4",
        "SubName": "Hampurilaiset ja täytetyt leivät",
        "SubId": "86"
      }, {
        "MainName": "Sesonki",
        "MainId": "1",
        "SubName": "Kesä",
        "SubId": "121"
      }, {
        "MainName": "Sesonki",
        "MainId": "1",
        "SubName": "Juhannus",
        "SubId": "122"
      }, {
        "MainName": "Ruokalaji",
        "MainId": "16",
        "SubName": "Hampurilaiset ja täytetyt leivät",
        "SubId": "140"
      }, {
        "MainName": "Sesonki",
        "MainId": "1",
        "SubName": "Grillaus",
        "SubId": "5"
      }],
      "Pictures": ["herkkuisienihalloum_19"],
      "PictureUrls": [{
        "Id": "herkkuisienihalloum_19",
        "Normal": "https://k-file-storage-qa.imgix.net/f/k-ruoka/recipe/10393?w=1000&h=1000&fit=clip",
        "Original": "https://k-file-storage-qa.imgix.net/f/k-ruoka/recipe/10393"
      }],
      "SpecialDiets": [{
        "Id": "6",
        "Name": "Kasvis (lakto-ovo)"
      }],
      "Ingredients": [{
        "SubSectionHeading": "_",
        "SubSectionIngredients": [
          [{
            "Name": "hapan omena (esim. Granny Smith)",
            "Amount": "1",
            "AmountInfo": "(200 g)",
            "PackageRecipe": "false",
            "IngredientType": "12050",
            "Unit": "",
            "IngredientTypeName": "Omena, vihreä, Granny Smith"
          }],
          [{
            "Name": "lehtiselleriä",
            "Amount": "100",
            "PackageRecipe": "false",
            "IngredientType": "5958",
            "Unit": "g",
            "IngredientTypeName": "Lehtiselleri"
          }],
          [{
            "Name": "sitruuna",
            "Amount": "1",
            "PackageRecipe": "false",
            "IngredientType": "6941",
            "Unit": "",
            "IngredientTypeName": "Sitruuna"
          }],
          [{
            "Name": "suolaa",
            "PackageRecipe": "false",
            "IngredientType": "7049",
            "Unit": "ripaus",
            "IngredientTypeName": "Suola"
          }],
          [{
            "Name": "mustapippuria",
            "PackageRecipe": "false",
            "IngredientType": "6269",
            "Unit": "ripaus",
            "IngredientTypeName": "Mustapippuri, kokonainen"
          }],
          [{
            "Name": "Vaasan The BRGR Naughty Burgers -sämpylää",
            "Amount": "4",
            "PackageRecipe": "false",
            "IngredientType": "16426",
            "Unit": "",
            "IngredientTypeName": "Vaasan Naughty BRGR sämpylä"
          }],
          [{
            "Name": "tuoreita herkkusieniä",
            "Amount": "150",
            "PackageRecipe": "false",
            "IngredientType": "5516",
            "Unit": "g",
            "IngredientTypeName": "Herkkusieni, tuore"
          }],
          [{
            "Name": "oliiviöljyä",
            "Amount": "1/2",
            "PackageRecipe": "false",
            "IngredientType": "6386",
            "Unit": "dl",
            "IngredientTypeName": "Oliiviöljy"
          }],
          [{
            "Name": "valkosipulinkynttä viipaloituna",
            "Amount": "2",
            "PackageRecipe": "false",
            "IngredientType": "15809",
            "Unit": "",
            "IngredientTypeName": "Valkosipuli, tuore"
          }],
          [{
            "Name": "tuoretta timjamia hienonnettuna",
            "Amount": "1/4",
            "PackageRecipe": "false",
            "IngredientType": "7119",
            "Unit": "ruukkua",
            "IngredientTypeName": "Timjami, tuore"
          }],
          [{
            "Name": "balsamiviinietikkaa",
            "Amount": "1/2",
            "PackageRecipe": "false",
            "IngredientType": "5257",
            "Unit": "dl",
            "IngredientTypeName": "Balsamietikka, tumma"
          }],
          [{
            "Name": "Pirkka Parhaat halloumia",
            "Ean": "6410405196828",
            "Amount": "1",
            "AmountInfo": "(250 g)",
            "PackageRecipe": "false",
            "IngredientType": "5490",
            "Unit": "pkt",
            "IngredientTypeName": "Halloumi"
          }],
          [{
            "Name": "punasipuli",
            "Amount": "1",
            "PackageRecipe": "false",
            "IngredientType": "6667",
            "Unit": "",
            "IngredientTypeName": "Punasipuli"
          }],
          [{
            "Name": "Naughty BRGR Base aiolia",
            "Amount": "1",
            "PackageRecipe": "false",
            "IngredientType": "16074",
            "Unit": "dl",
            "IngredientTypeName": "Naughty BRGR Base aioli"
          }],
          [{
            "Name": "suolakurkkuviipaleita",
            "Amount": "1",
            "PackageRecipe": "false",
            "IngredientType": "14464",
            "Unit": "dl",
            "IngredientTypeName": "Suolakurkku, viipale"
          }],
          [{
            "Name": "salaatinlehteä",
            "Amount": "4",
            "PackageRecipe": "false",
            "IngredientType": "5608",
            "Unit": "",
            "IngredientTypeName": "Jääsalaatti"
          }]
        ]
      }],
      "Instructions": "# Kuutioi omena ja lehtiselleri kulhoon pieniksi kuutioiksi ja lisää joukkoon puolet hienonnetusta timjamista sekä aioli. Mausta sitruunanmehulla sekä reilusti suolalla ja pippurilla\r\n# Ruskista herkkusieniä pannulla öljyssä 2 minuuttia ja lisää joukkoon valkosipuli ja timjami. Paista vielä minuutti ja lisää joukkoon balsamico. Anna kiehua hieman kasaan ja mausta suolalla ja pippurilla.\r\n# Leikkaa halloumijuusto neljäksi tasakokoiseksi viipaleeksi ja paista pannulla oliiviöljyssä kullanruskeiksi. Leikkaa kuorittu punasipuli ohuiksi viipaleiksi.\r\n# Paahda sämpylöitä pannulla tai uunissa 3 minuuttia tai kunnes ne ovat paahtuneet kullankeltaisiksi. Kokoa burgeri reseptin aineksista.",
      "EndNote": "",
      "Description": "",
      "DateCreated": "2019-04-29T10:16:18",
      "DateModified": "2019-05-15T13:59:49",
      "Stamp": {
        "Name": "Kumppani",
        "Id": "6"
      },
      "RecipeUse": {
        "Name": "k-ruoka.fi",
        "Id": "1"
      },
      "resourceType": "recipe",
      "Url": "https://www.k-ruoka.fi/reseptit/herkkusieni-halloumiburgeri",
      "UrlSlug": "herkkusieni-halloumiburgeri",
      "Sort": [1, 1, 10393]
    }, {
      "Id": "10392",
      "Name": "French Onion Burgeri",
      "PieceSize": {
        "Unit": "g"
      },
      "Portions": {
        "Unit": "annosta",
        "Amount": "4"
      },
      "PreparationTime": {
        "Description": "alle 30 min",
        "TimeRange": {
          "MinTime": 15,
          "MaxTime": 30
        }
      },
      "UserPortions": {
        "$": {
          "Unit": "henkilölle"
        }
      },
      "Categories": [{
        "MainName": "Sesonki",
        "MainId": "1",
        "SubName": "Grillaus",
        "SubId": "5"
      }, {
        "MainName": "Pääruoat",
        "MainId": "4",
        "SubName": "Hampurilaiset ja täytetyt leivät",
        "SubId": "86"
      }, {
        "MainName": "Pääraaka-aine",
        "MainId": "14",
        "SubName": "Liha",
        "SubId": "114"
      }, {
        "MainName": "Sesonki",
        "MainId": "1",
        "SubName": "Kesä",
        "SubId": "121"
      }, {
        "MainName": "Ruokalaji",
        "MainId": "16",
        "SubName": "Hampurilaiset ja täytetyt leivät",
        "SubId": "140"
      }, {
        "MainName": "Teema",
        "MainId": "15",
        "SubName": "Mitä tänään syötäisiin?",
        "SubId": "144"
      }],
      "Pictures": ["frenchonionburger_19"],
      "PictureUrls": [{
        "Id": "frenchonionburger_19",
        "Normal": "https://k-file-storage-qa.imgix.net/f/k-ruoka/recipe/10392?w=1000&h=1000&fit=clip",
        "Original": "https://k-file-storage-qa.imgix.net/f/k-ruoka/recipe/10392"
      }],
      "VideoUrl": "https://youtu.be/RzReI-_r_U0",
      "VideoEmbedUrl": "<iframe width=\"427\" height=\"240\" src=\"https://www.youtube.com/embed/RzReI-_r_U0\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>",
      "Ingredients": [{
        "SubSectionHeading": "_",
        "SubSectionIngredients": [
          [{
            "Name": "sipulia",
            "Amount": "3",
            "PackageRecipe": "false",
            "IngredientType": "6932",
            "Unit": "",
            "IngredientTypeName": "Sipuli"
          }],
          [{
            "Name": "oliiviöljyä",
            "Amount": "1",
            "PackageRecipe": "false",
            "IngredientType": "6386",
            "Unit": "rkl",
            "IngredientTypeName": "Oliiviöljy"
          }],
          [{
            "Name": "tähtianista",
            "Amount": "2",
            "PackageRecipe": "false",
            "IngredientType": "7213",
            "Unit": "",
            "IngredientTypeName": "Tähtianis"
          }],
          [{
            "Name": "sokeria",
            "Amount": "1",
            "PackageRecipe": "false",
            "IngredientType": "6961",
            "Unit": "rkl",
            "IngredientTypeName": "Sokeri"
          }],
          [{
            "Name": "suolaa",
            "PackageRecipe": "false",
            "IngredientType": "7049",
            "Unit": "ripaus",
            "IngredientTypeName": "Suola"
          }],
          [{
            "Name": "mustapippuria",
            "PackageRecipe": "false",
            "IngredientType": "6269",
            "Unit": "ripaus",
            "IngredientTypeName": "Mustapippuri, kokonainen"
          }],
          [{
            "Name": "Tamminen Naughty BRGR naudan burgerpihviä",
            "Amount": "4",
            "PackageRecipe": "false",
            "IngredientType": "16305",
            "Unit": "",
            "IngredientTypeName": "Naughty BRGR burgeripihvi"
          }],
          [{
            "Name": "makusuolaa",
            "Amount": "4",
            "PackageRecipe": "false",
            "IngredientType": "16010",
            "Unit": "tl",
            "IngredientTypeName": "Naughty BRGR Cuba -suola"
          }],
          [{
            "Name": "Naughty Burger Swiss juustoa",
            "Amount": "4",
            "PackageRecipe": "false",
            "IngredientType": "16308",
            "Unit": "viipaletta",
            "IngredientTypeName": "Naughty Burger Swiss juusto"
          }],
          [{
            "Name": "Vaasan The BRGR Naughty Burgers -sämpylää",
            "Amount": "4",
            "PackageRecipe": "false",
            "IngredientType": "16426",
            "Unit": "",
            "IngredientTypeName": "Vaasan Naughty BRGR sämpylä"
          }],
          [{
            "Name": "Naughty BRGR Base aiolia",
            "Amount": "2",
            "PackageRecipe": "false",
            "IngredientType": "16074",
            "Unit": "dl",
            "IngredientTypeName": "Naughty BRGR Base aioli"
          }],
          [{
            "Name": "soijakastiketta",
            "Amount": "4",
            "PackageRecipe": "false",
            "IngredientType": "6953",
            "Unit": "rkl",
            "IngredientTypeName": "Soijakastike"
          }],
          [{
            "Name": "salaatinlehteä",
            "Amount": "4",
            "PackageRecipe": "false",
            "IngredientType": "5608",
            "Unit": "",
            "IngredientTypeName": "Jääsalaatti"
          }],
          [{
            "Name": "perunalastuja",
            "Amount": "2",
            "PackageRecipe": "false",
            "IngredientType": "12545",
            "Unit": "dl",
            "IngredientTypeName": "Perunalastu, maustamaton"
          }]
        ]
      }],
      "Instructions": "# Viipaloi kuoritut sipulit ohuiksi viipaleiksi ja siirrä 1/3 sipuleista syrjään.\r\n# Paista 2/3 sipuleista pannulla oliiviöljyssä tähtianisten kanssa kypsiksi, lisää joukkoon sokeri ja mausta suolalla ja pippurilla. Poista lopuksi tähtianikset.\r\n# Mausta pihvit makusuolalla tasaisesti kauttaaltaan ja grillaa kuumassa grillissä 3 minuuttia per puoli.\r\n# Kun olet kääntänyt pihvit, niin laita sen jälkeen pihvin grillatun puolen päälle juustoviipaleet valmiiksi sulamaan.\r\n# Paahda sämpylöitä pannulla tai uunissa 3 minuuttia tai kunnes ne ovat paahtuneet kullankeltaisiksi.\r\n# Sekoita kulhossa Aioli ja soijakastike sekaisin. Kokoa burgeri reseptin aineksista.",
      "EndNote": "",
      "Description": "Tässä reseptissä seurataan ranskalaisen sipulikeiton makumaailmaa burgeriksi! Resepti: Akseli Herlevi",
      "DateCreated": "2019-04-29T10:15:55",
      "DateModified": "2019-08-30T09:44:53",
      "Stamp": {
        "Name": "Kumppani",
        "Id": "6"
      },
      "RecipeUse": {
        "Name": "k-ruoka.fi",
        "Id": "1"
      },
      "resourceType": "recipe",
      "Url": "https://www.k-ruoka.fi/reseptit/french-onion-burgeri",
      "UrlSlug": "french-onion-burgeri",
      "Sort": [1, 1, 10392]
    }, {
      "Id": "10391",
      "Name": "Korealainen BBQ-bulgogiburgeri",
      "PieceSize": {
        "Unit": "g"
      },
      "Portions": {
        "Unit": "annosta",
        "Amount": "4"
      },
      "PreparationTime": {
        "Description": "alle 30 min",
        "TimeRange": {
          "MinTime": 15,
          "MaxTime": 30
        }
      },
      "UserPortions": {
        "$": {
          "Unit": "henkilölle"
        }
      },
      "Categories": [{
        "MainName": "Sesonki",
        "MainId": "1",
        "SubName": "Grillaus",
        "SubId": "5"
      }, {
        "MainName": "Pääruoat",
        "MainId": "4",
        "SubName": "Grilliruoat",
        "SubId": "73"
      }, {
        "MainName": "Pääruoat",
        "MainId": "4",
        "SubName": "Hampurilaiset ja täytetyt leivät",
        "SubId": "86"
      }, {
        "MainName": "Pääraaka-aine",
        "MainId": "14",
        "SubName": "Liha",
        "SubId": "114"
      }, {
        "MainName": "Sesonki",
        "MainId": "1",
        "SubName": "Kesä",
        "SubId": "121"
      }, {
        "MainName": "Ruokalaji",
        "MainId": "16",
        "SubName": "Hampurilaiset ja täytetyt leivät",
        "SubId": "140"
      }],
      "Pictures": ["KorealainenBBu_19"],
      "PictureUrls": [{
        "Id": "KorealainenBBu_19",
        "Normal": "https://k-file-storage-qa.imgix.net/f/k-ruoka/recipe/10391?w=1000&h=1000&fit=clip",
        "Original": "https://k-file-storage-qa.imgix.net/f/k-ruoka/recipe/10391"
      }],
      "Ingredients": [{
        "SubSectionHeading": "_",
        "SubSectionIngredients": [
          [{
            "Name": "HK Amerikan pekonia",
            "Amount": "1",
            "AmountInfo": "(170 g)",
            "PackageRecipe": "false",
            "IngredientType": "6498",
            "Unit": "pkt",
            "IngredientTypeName": "Pekoni"
          }],
          [{
            "Name": "Korealaista bulgogi-marinadia (Sempio)",
            "Ean": "8801005222167",
            "Amount": "1",
            "PackageRecipe": "false",
            "IngredientType": "16265",
            "Unit": "dl",
            "IngredientTypeName": "Marinadi, bulgogi (Sempio)"
          }],
          [{
            "Name": "Tamminen Naughty BRGR naudan burgerpihviä",
            "Amount": "4",
            "PackageRecipe": "false",
            "IngredientType": "16305",
            "Unit": "",
            "IngredientTypeName": "Naughty BRGR burgeripihvi"
          }],
          [{
            "Name": "Naughty BRGR burger makusuolaa",
            "Amount": "4",
            "PackageRecipe": "false",
            "IngredientType": "16010",
            "Unit": "tl",
            "IngredientTypeName": "Naughty BRGR Cuba -suola"
          }],
          [{
            "Name": "Valio Naughty BRGR cheddarjuustoa",
            "Amount": "4",
            "PackageRecipe": "false",
            "IngredientType": "16427",
            "Unit": "viipaletta",
            "IngredientTypeName": "Valio Naughty BRGR cheddarjuusto"
          }],
          [{
            "Name": "Naughty BRGR Cuban Frita Sauce -kastiketta",
            "Amount": "1 1/4",
            "PackageRecipe": "false",
            "IngredientType": "16307",
            "Unit": "dl",
            "IngredientTypeName": "Naughty BRGR Cuban Frita Sauce"
          }],
          [{
            "Name": "mustia seesaminsiemeniä",
            "Amount": "4",
            "PackageRecipe": "false",
            "IngredientType": "6871",
            "Unit": "tl",
            "IngredientTypeName": "Seesaminsiemen"
          }],
          [{
            "Name": "Vaasan The BRGR Naughty Burgers -sämpylää",
            "Amount": "4",
            "PackageRecipe": "false",
            "IngredientType": "16426",
            "Unit": "",
            "IngredientTypeName": "Vaasan Naughty BRGR sämpylä"
          }],
          [{
            "Name": "Naughty Kimchi Mayoa",
            "Amount": "2",
            "PackageRecipe": "false",
            "IngredientType": "16026",
            "Unit": "dl",
            "IngredientTypeName": "Naughty BRGR Kimchi Mayo"
          }],
          [{
            "Name": "salaatinlehteä",
            "Amount": "4",
            "PackageRecipe": "false",
            "IngredientType": "5608",
            "Unit": "",
            "IngredientTypeName": "Jääsalaatti"
          }]
        ]
      }],
      "Instructions": "# Paista pekoniviipaleet pannulla rapeiksi ja nosta talouspaperin päälle valumaan.\r\n# Leikkaa rapeat pekoniviipaleet noin 2 cm:n kokoisiksi paloiksi ja sekoita kulhossa bulgogi-kastikkeen kanssa.\r\n# Mausta pihvit makusuolalla tasaisesti kauttaaltaan ja grillaa kuumassa grillissä 3 minuuttia per puoli.\r\n# Kun olet kääntänyt pihvit, niin laita sen jälkeen pihvin grillatun puolen päälle juustoviipaleet valmiiksi sulamaan.\r\n# Sekoita kulhossa Naughty Frita kastike ja seesaminsiemenet.\r\n# Paahda sämpylöitä pannulla tai uunissa 3 minuuttia tai kunnes ne ovat paahtuneet kullankeltaisiksi.\r\n# Kokoa burgeri reseptin aineksista.",
      "EndNote": "",
      "Description": "Tästä hieman mausteisempi burgeri! BBQ:ta tulee ovista ja ikkunoista. Resepti: Akseli Herlevi",
      "DateCreated": "2019-04-29T10:06:49",
      "DateModified": "2019-04-30T07:51:25",
      "Stamp": {
        "Name": "Kumppani",
        "Id": "6"
      },
      "RecipeUse": {
        "Name": "k-ruoka.fi",
        "Id": "1"
      },
      "resourceType": "recipe",
      "Url": "https://www.k-ruoka.fi/reseptit/korealainen-bbq-bulgogiburgeri",
      "UrlSlug": "korealainen-bbq-bulgogiburgeri",
      "Sort": [1, 1, 10391]
    }, {
      "Id": "10390",
      "Name": "BLT-burgeri",
      "PieceSize": {
        "Unit": "g"
      },
      "Portions": {
        "Unit": "annosta",
        "Amount": "4"
      },
      "PreparationTime": {
        "Description": "alle 30 min",
        "TimeRange": {
          "MinTime": 15,
          "MaxTime": 30
        }
      },
      "UserPortions": {
        "$": {
          "Unit": "henkilölle"
        }
      },
      "Categories": [{
        "MainName": "Sesonki",
        "MainId": "1",
        "SubName": "Grillaus",
        "SubId": "5"
      }, {
        "MainName": "Makuja maailmalta",
        "MainId": "2",
        "SubName": "Pohjois-Amerikka",
        "SubId": "8"
      }, {
        "MainName": "Pääruoat",
        "MainId": "4",
        "SubName": "Grilliruoat",
        "SubId": "73"
      }, {
        "MainName": "Pääruoat",
        "MainId": "4",
        "SubName": "Hampurilaiset ja täytetyt leivät",
        "SubId": "86"
      }, {
        "MainName": "Pääraaka-aine",
        "MainId": "14",
        "SubName": "Liha",
        "SubId": "114"
      }, {
        "MainName": "Ruokalaji",
        "MainId": "16",
        "SubName": "Hampurilaiset ja täytetyt leivät",
        "SubId": "140"
      }, {
        "MainName": "Iltapalat",
        "MainId": "21",
        "SubName": "Täytetyt leivät",
        "SubId": "195"
      }, {
        "MainName": "Asiakassegmentointi (K-asiakkaat)",
        "MainId": "17",
        "SubName": "Nautiskelijat",
        "SubId": "146"
      }, {
        "MainName": "Asiakassegmentointi (K-asiakkaat)",
        "MainId": "17",
        "SubName": "Innostujat",
        "SubId": "147"
      }, {
        "MainName": "Asiakassegmentointi (K-asiakkaat)",
        "MainId": "17",
        "SubName": "Mukavuudenhaluiset ",
        "SubId": "149"
      }, {
        "MainName": "Apukategoriat",
        "MainId": "18",
        "SubName": "Helppo",
        "SubId": "153"
      }, {
        "MainName": "Apukategoriat",
        "MainId": "18",
        "SubName": "Trendikäs ",
        "SubId": "156"
      }, {
        "MainName": "Teema",
        "MainId": "15",
        "SubName": "Viikonloppu",
        "SubId": "160"
      }],
      "Pictures": ["BLTburgeri_19"],
      "PictureUrls": [{
        "Id": "BLTburgeri_19",
        "Normal": "https://k-file-storage-qa.imgix.net/f/k-ruoka/recipe/10390?w=1000&h=1000&fit=clip",
        "Original": "https://k-file-storage-qa.imgix.net/f/k-ruoka/recipe/10390"
      }],
      "Ingredients": [{
        "SubSectionHeading": "_",
        "SubSectionIngredients": [
          [{
            "Name": "HK Amerikan pekonia",
            "Amount": "1",
            "AmountInfo": "(170 g)",
            "PackageRecipe": "false",
            "IngredientType": "6498",
            "Unit": "pkt",
            "IngredientTypeName": "Pekoni"
          }],
          [{
            "Name": "Vaasan The BRGR Naughty Burgers -sämpylää",
            "Amount": "4",
            "PackageRecipe": "false",
            "IngredientType": "16426",
            "Unit": "",
            "IngredientTypeName": "Vaasan Naughty BRGR sämpylä"
          }],
          [{
            "Name": "Tamminen Naughty BRGR naudan burgerpihviä",
            "Amount": "4",
            "PackageRecipe": "false",
            "IngredientType": "16305",
            "Unit": "",
            "IngredientTypeName": "Naughty BRGR burgeripihvi"
          }],
          [{
            "Name": "Naughty BRGR burger makusuolaa",
            "Amount": "4",
            "PackageRecipe": "false",
            "IngredientType": "16010",
            "Unit": "tl",
            "IngredientTypeName": "Naughty BRGR Cuba -suola"
          }],
          [{
            "Name": "Naughty Burger Swiss juustoa",
            "Amount": "4",
            "PackageRecipe": "false",
            "IngredientType": "16308",
            "Unit": "viipaletta",
            "IngredientTypeName": "Naughty Burger Swiss juusto"
          }],
          [{
            "Name": "Naughty Base Aiolia",
            "Amount": "2",
            "PackageRecipe": "false",
            "IngredientType": "16074",
            "Unit": "dl",
            "IngredientTypeName": "Naughty BRGR Base aioli"
          }],
          [{
            "Name": "pihvitomaattia",
            "Amount": "2",
            "PackageRecipe": "false",
            "IngredientType": "6542",
            "Unit": "",
            "IngredientTypeName": "Pihvitomaatti"
          }],
          [{
            "Name": "jäävuorisalaatin lehteä",
            "Amount": "4",
            "PackageRecipe": "false",
            "IngredientType": "5635",
            "Unit": "",
            "IngredientTypeName": "Jäävuorisalaatti"
          }]
        ]
      }],
      "Instructions": "# Paista pekoniviipaleet pannulla rapeiksi ja nosta talouspaperin päälle valumaan.\r\n# Mausta pihvit makusuolalla tasaisesti kauttaaltaan ja grillaa kuumassa grillissä 3 minuuttia per puoli.\r\n# Kun olet kääntänyt pihvit, niin laita sen jälkeen pihvin grillatun puolen päälle juustoviipaleet valmiiksi sulamaan.\r\n# Paahda sämpylöitä pannulla tai uunissa 3 minuuttia tai kunnes ne ovat paahtuneet kullankeltaisiksi.\r\n# Viipaloi pihvitomaatit tasapaksuisiksi viipaleiksi. Kokoa burgeri reseptin aineksista.",
      "EndNote": "",
      "Description": "Klassinen BLT-sandwich taipuu myös burgeriksi. Tämä on yksi Naughty Burger ravintoloiden suosituimmista burgereista. Resepti: Akseli Herlevi",
      "DateCreated": "2019-04-29T10:06:28",
      "DateModified": "2019-11-02T15:18:16",
      "Stamp": {
        "Name": "Kumppani",
        "Id": "6"
      },
      "RecipeUse": {
        "Name": "k-ruoka.fi",
        "Id": "1"
      },
      "resourceType": "recipe",
      "Url": "https://www.k-ruoka.fi/reseptit/blt-burgeri",
      "UrlSlug": "blt-burgeri",
      "Sort": [1, 1, 10390]
    }, {
      "Id": "10389",
      "Name": "BBQ-broileriburgeri",
      "PieceSize": {
        "Unit": "g"
      },
      "Portions": {
        "Unit": "annosta",
        "Amount": "4"
      },
      "PreparationTime": {
        "Description": "alle 30 min",
        "TimeRange": {
          "MinTime": 15,
          "MaxTime": 30
        }
      },
      "UserPortions": {
        "$": {
          "Unit": "henkilölle"
        }
      },
      "Categories": [{
        "MainName": "Sesonki",
        "MainId": "1",
        "SubName": "Grillaus",
        "SubId": "5"
      }, {
        "MainName": "Pääruoat",
        "MainId": "4",
        "SubName": "Hampurilaiset ja täytetyt leivät",
        "SubId": "86"
      }, {
        "MainName": "Pääraaka-aine",
        "MainId": "14",
        "SubName": "Liha",
        "SubId": "114"
      }, {
        "MainName": "Sesonki",
        "MainId": "1",
        "SubName": "Kesä",
        "SubId": "121"
      }, {
        "MainName": "Sesonki",
        "MainId": "1",
        "SubName": "Sadonkorjuu",
        "SubId": "124"
      }, {
        "MainName": "Teema",
        "MainId": "15",
        "SubName": "Viikonloppu",
        "SubId": "160"
      }, {
        "MainName": "Teema",
        "MainId": "15",
        "SubName": "Lasten juhlat",
        "SubId": "163"
      }, {
        "MainName": "Teema",
        "MainId": "15",
        "SubName": "Lasten makuun",
        "SubId": "165"
      }, {
        "MainName": "Iltapalat",
        "MainId": "21",
        "SubName": "Täytetyt leivät",
        "SubId": "195"
      }, {
        "MainName": "Ruokalaji",
        "MainId": "16",
        "SubName": "Hampurilaiset ja täytetyt leivät",
        "SubId": "140"
      }, {
        "MainName": "Asiakassegmentointi (K-asiakkaat)",
        "MainId": "17",
        "SubName": "Nautiskelijat",
        "SubId": "146"
      }, {
        "MainName": "Asiakassegmentointi (K-asiakkaat)",
        "MainId": "17",
        "SubName": "Innostujat",
        "SubId": "147"
      }, {
        "MainName": "Asiakassegmentointi (K-asiakkaat)",
        "MainId": "17",
        "SubName": "Mukavuudenhaluiset ",
        "SubId": "149"
      }, {
        "MainName": "Apukategoriat",
        "MainId": "18",
        "SubName": "Helppo",
        "SubId": "153"
      }, {
        "MainName": "Apukategoriat",
        "MainId": "18",
        "SubName": "Trendikäs ",
        "SubId": "156"
      }],
      "Pictures": ["bbqbroileriburgeri_19"],
      "PictureUrls": [{
        "Id": "bbqbroileriburgeri_19",
        "Normal": "https://k-file-storage-qa.imgix.net/f/k-ruoka/recipe/10389?w=1000&h=1000&fit=clip",
        "Original": "https://k-file-storage-qa.imgix.net/f/k-ruoka/recipe/10389"
      }],
      "Ingredients": [{
        "SubSectionHeading": "_",
        "SubSectionIngredients": [
          [{
            "Name": "HK Amerikan pekonia",
            "Amount": "1",
            "AmountInfo": "(170 g)",
            "PackageRecipe": "false",
            "IngredientType": "6498",
            "Unit": "pkt",
            "IngredientTypeName": "Pekoni"
          }],
          [{
            "Name": "Kariniemen kananpojan fileepihvejä, maustamaton",
            "Amount": "4",
            "PackageRecipe": "false",
            "IngredientType": "5286",
            "Unit": "kpl",
            "IngredientTypeName": "Broilerin fileepihvi, maustamaton"
          }],
          [{
            "Name": "Naughty BRGR Cuba -suolaa",
            "Amount": "4",
            "PackageRecipe": "false",
            "IngredientType": "16010",
            "Unit": "tl",
            "IngredientTypeName": "Naughty BRGR Cuba -suola"
          }],
          [{
            "Name": "BBQ-kastiketta",
            "Amount": "1",
            "PackageRecipe": "false",
            "IngredientType": "5466",
            "Unit": "dl",
            "IngredientTypeName": "Grillauskastike, BBQ"
          }],
          [{
            "Name": "Naughty BRGR American BBQ Mayo -majoneesia",
            "Amount": "2",
            "PackageRecipe": "false",
            "IngredientType": "16304",
            "Unit": "dl",
            "IngredientTypeName": "Naughty BRGR American BBQ Mayo -majoneesi"
          }],
          [{
            "Name": "Vaasan The BRGR Naughty Burgers -sämpylää",
            "Amount": "4",
            "AmountInfo": "(à 6 kpl/540 g)",
            "PackageRecipe": "false",
            "IngredientType": "16426",
            "Unit": "",
            "IngredientTypeName": "Vaasan Naughty BRGR sämpylä"
          }],
          [{
            "Name": "Naughty BRGR chutneya",
            "Amount": "1",
            "PackageRecipe": "false",
            "IngredientType": "16306",
            "Unit": "dl",
            "IngredientTypeName": "Naughty BRGR chutney"
          }],
          [{
            "Name": "punasipulia",
            "Amount": "1",
            "AmountInfo": "(100 g)",
            "PackageRecipe": "false",
            "IngredientType": "6667",
            "Unit": "",
            "IngredientTypeName": "Punasipuli"
          }],
          [{
            "Name": "suolakurkkuviipaleita",
            "Amount": "1",
            "PackageRecipe": "false",
            "IngredientType": "14464",
            "Unit": "dl",
            "IngredientTypeName": "Suolakurkku, viipale"
          }],
          [{
            "Name": "jäävuorisalaatinlehteä",
            "Amount": "4",
            "PackageRecipe": "false",
            "IngredientType": "5635",
            "Unit": "",
            "IngredientTypeName": "Jäävuorisalaatti"
          }],
          [{
            "Name": "Valio Koskenlaskija Ruoka cheddar -juustoa",
            "Amount": "2",
            "PackageRecipe": "false",
            "IngredientType": "14410",
            "Unit": "dl",
            "IngredientTypeName": "Sulatejuusto, cheddar, laktoositon"
          }]
        ]
      }],
      "Instructions": "# Paista pekoniviipaleet rapeiksi pannulla ja nosta talouspaperin päälle valumaan.\r\n# Mausta broilerin rintafileet Cubasuolalla ja grillaa kuumassa grillissä kypsiksi.\r\n# Leikkaa pekonit karkeiksi noin 2 cm:n kokoisiksi paloiksi ja sekoita BBQ-kastikkeen joukkoon.\r\n# Leikkaa kuorittu punasipuli ohuiksi viipaleiksi.\r\n# Paahda sämpylöitä pannulla tai uunissa 3 minuuttia tai kunnes ne ovat paahtuneet kullankeltaisiksi ja levitä Naughty American BBQ majoneesia sämpylöiden leikkauspinnoille.\r\n# Kokoa burgeri reseptin aineksista.\r\n# Lämmitä cheddarjuustokastike varovasti kattilassa ja nosta erilliseen astiaan burgereiden vierelle.",
      "EndNote": "",
      "Description": "Tämä burgeri on mässyjen mässy. Erilaisilla kastikkeilla kruunattu burgeri dipataan lopuksi vielä cheddarkastikkeeseen. Resepti: Akseli Herlevi",
      "DateCreated": "2019-04-29T10:05:48",
      "DateModified": "2019-11-02T13:23:41",
      "Stamp": {
        "Name": "Kumppani",
        "Id": "6"
      },
      "RecipeUse": {
        "Name": "k-ruoka.fi",
        "Id": "1"
      },
      "resourceType": "recipe",
      "Url": "https://www.k-ruoka.fi/reseptit/bbq-broileriburgeri",
      "UrlSlug": "bbq-broileriburgeri",
      "Sort": [1, 1, 10389]
    }, {
      "Id": "10388",
      "Name": "Korean burger",
      "PieceSize": {
        "Unit": "g"
      },
      "Portions": {
        "Unit": "annosta",
        "Amount": "4"
      },
      "PreparationTime": {
        "Description": "alle 30 min",
        "TimeRange": {
          "MinTime": 15,
          "MaxTime": 30
        }
      },
      "UserPortions": {
        "$": {
          "Unit": "henkilölle"
        }
      },
      "Categories": [{
        "MainName": "Sesonki",
        "MainId": "1",
        "SubName": "Grillaus",
        "SubId": "5"
      }, {
        "MainName": "Makuja maailmalta",
        "MainId": "2",
        "SubName": "Aasia",
        "SubId": "7"
      }, {
        "MainName": "Pääruoat",
        "MainId": "4",
        "SubName": "Hampurilaiset ja täytetyt leivät",
        "SubId": "86"
      }, {
        "MainName": "Ruokalaji",
        "MainId": "16",
        "SubName": "Hampurilaiset ja täytetyt leivät",
        "SubId": "140"
      }],
      "Pictures": ["korean_burger_HK19"],
      "PictureUrls": [{
        "Id": "korean_burger_HK19",
        "Normal": "https://k-file-storage-qa.imgix.net/f/k-ruoka/recipe/10388?w=1000&h=1000&fit=clip",
        "Original": "https://k-file-storage-qa.imgix.net/f/k-ruoka/recipe/10388"
      }],
      "Ingredients": [{
        "SubSectionHeading": "_",
        "SubSectionIngredients": [
          [{
            "Name": "HK Karkeaksi jauhettua naudan rintaa",
            "Amount": "2",
            "AmountInfo": "(à 400 g)",
            "PackageRecipe": "false",
            "IngredientType": "15865",
            "Unit": "pkt",
            "IngredientTypeName": "HK Karkeaksi jauhettu naudan rinta"
          }],
          [{
            "Name": "rypsiöljyä paistamiseen",
            "PackageRecipe": "false",
            "IngredientType": "6807",
            "IngredientTypeName": "Rypsiöljy"
          }],
          [{
            "Name": "suolaa",
            "PackageRecipe": "false",
            "IngredientType": "7049",
            "Unit": "ripaus",
            "IngredientTypeName": "Suola"
          }],
          [{
            "Name": "mustapippuria",
            "PackageRecipe": "false",
            "IngredientType": "6269",
            "Unit": "ripaus",
            "IngredientTypeName": "Mustapippuri, kokonainen"
          }],
          [{
            "Name": "cheddarjuustoa",
            "Amount": "8",
            "PackageRecipe": "false",
            "IngredientType": "5349",
            "Unit": "viipaletta",
            "IngredientTypeName": "Cheddarjuusto"
          }],
          [{
            "Name": "kimchiä",
            "Amount": "1 1/4",
            "PackageRecipe": "false",
            "IngredientType": "15866",
            "Unit": "dl",
            "IngredientTypeName": "Kimchi"
          }],
          [{
            "Name": "sriracha-chilikastiketta",
            "Amount": "4",
            "PackageRecipe": "false",
            "IngredientType": "6992",
            "Unit": "rkl",
            "IngredientTypeName": "Sriracha"
          }],
          [{
            "Name": "ruohosipulia hienonnettuna",
            "Amount": "4",
            "PackageRecipe": "false",
            "IngredientType": "6772",
            "Unit": "rkl",
            "IngredientTypeName": "Ruohosipuli, tuore"
          }]
        ]
      }, {
        "SubSectionHeading": "Misomajoneesi",
        "SubSectionIngredients": [
          [{
            "Name": "majoneesia",
            "Amount": "1",
            "PackageRecipe": "false",
            "IngredientType": "6058",
            "Unit": "dl",
            "IngredientTypeName": "Majoneesi"
          }],
          [{
            "Name": "misotahnaa",
            "Amount": "2",
            "PackageRecipe": "false",
            "IngredientType": "15801",
            "Unit": "tl",
            "IngredientTypeName": "Misotahna"
          }],
          [{
            "Name": "sitruunanmehua",
            "Amount": "2",
            "PackageRecipe": "false",
            "IngredientType": "6941",
            "Unit": "tl",
            "IngredientTypeName": "Sitruuna"
          }]
        ]
      }, {
        "SubSectionHeading": "Pikkelöity punasipuli",
        "SubSectionIngredients": [
          [{
            "Name": "punasipulia",
            "Amount": "2",
            "PackageRecipe": "false",
            "IngredientType": "6667",
            "Unit": "",
            "IngredientTypeName": "Punasipuli"
          }],
          [{
            "Name": "väkiviinaetikkaa",
            "Amount": "1",
            "PackageRecipe": "false",
            "IngredientType": "7367",
            "Unit": "dl",
            "IngredientTypeName": "Väkiviinaetikka"
          }],
          [{
            "Name": "sokeria",
            "Amount": "1",
            "PackageRecipe": "false",
            "IngredientType": "6961",
            "Unit": "dl",
            "IngredientTypeName": "Sokeri"
          }],
          [{
            "Name": "vettä",
            "Amount": "1",
            "PackageRecipe": "false",
            "IngredientType": "15792",
            "Unit": "dl",
            "IngredientTypeName": "Vesi"
          }]
        ]
      }],
      "Instructions": "# Valmista ensin misomajoneesi. Sekoita misotahna ja sitruunanmehu majoneesin kanssa huolellisesti ja anna maustua ainakin tunti jääkaapissa.\r\n# Mittaa vesi, sokeri ja etika kattilaan ja kiehauta. Kaada liemi ohueksi viipaloitujen punasipulien päälle. Jäähdytä.\r\n# Jaa jauheliha neljään osaan ja muotoile niistä 2-3 cm:n paksuisia burgeripihvejä. Paista rypsiöljyssä noin puolitoista minuuttia per puoli. Mausta suolalla ja rouhitulla mustapippurilla. Laita juustoviipaleet pihvien päälle ja paista vielä 200 asteisessa uunissa noin 4 minuuttia, kunnes juusto on sulanut ja pihvit ovat kypsiä.\r\n# Lämmitä sämpylät uunissa tai pannulla. Voitele pohjaan ja kanteen misomajoneesia. Kokoa pohjan päälle pikkelöity punasipuli, pihvi ja juusto, kimchi, srirachaa ja lopuksi hieman ruohosipulia.",
      "EndNote": "",
      "Description": "Korealaiset burgerit täytetään kimchillä, misomajoneesilla ja srirachalla. Resepti: HK",
      "DateCreated": "2019-04-29T09:21:14",
      "DateModified": "2019-06-28T09:34:50",
      "Stamp": {
        "Name": "Kumppani",
        "Id": "6"
      },
      "RecipeUse": {
        "Name": "k-ruoka.fi",
        "Id": "1"
      },
      "resourceType": "recipe",
      "Url": "https://www.k-ruoka.fi/reseptit/korean-burger",
      "UrlSlug": "korean-burger",
      "Sort": [1, 1, 10388]
    }, {
      "Id": "10385",
      "Name": "Täytetty kokonaisena savustettu kirjolohi",
      "PieceSize": {
        "Unit": "g",
        "Amount": "258"
      },
      "Portions": {
        "Unit": "annosta",
        "Amount": "4-6"
      },
      "PreparationTime": {
        "Description": "yli 60 min",
        "TimeRange": {
          "MinTime": 60
        }
      },
      "UserPortions": {
        "$": {
          "Unit": "henkilölle"
        }
      },
      "Categories": [{
        "MainName": "Pääruoat",
        "MainId": "4",
        "SubName": "Kalaruoat",
        "SubId": "20"
      }, {
        "MainName": "Pääraaka-aine",
        "MainId": "14",
        "SubName": "Kala",
        "SubId": "116"
      }, {
        "MainName": "Sesonki",
        "MainId": "1",
        "SubName": "Kesä",
        "SubId": "121"
      }, {
        "MainName": "Sesonki",
        "MainId": "1",
        "SubName": "Juhannus",
        "SubId": "122"
      }, {
        "MainName": "Asiakassegmentointi (K-asiakkaat)",
        "MainId": "17",
        "SubName": "Innostujat",
        "SubId": "147"
      }],
      "Pictures": ["KCM_Taytetty_kokonaisen"],
      "PictureUrls": [{
        "Id": "KCM_Taytetty_kokonaisen",
        "Normal": "https://k-file-storage-qa.imgix.net/f/k-ruoka/recipe/10385?w=1000&h=1000&fit=clip",
        "Original": "https://k-file-storage-qa.imgix.net/f/k-ruoka/recipe/10385"
      }],
      "SpecialDiets": [{
        "Id": "2",
        "Name": "Laktoositon"
      }, {
        "Id": "3",
        "Name": "Maidoton"
      }],
      "Ingredients": [{
        "SubSectionHeading": "_",
        "SubSectionIngredients": [
          [{
            "Name": "kokonainen kirjolohi perattuna",
            "Amount": "1",
            "AmountInfo": "(n. 1,3 kg)",
            "PackageRecipe": "false",
            "IngredientType": "5812",
            "Unit": "",
            "IngredientTypeName": "Kirjolohi, kokonainen"
          }],
          [{
            "Name": "kylmää vettä",
            "Amount": "1",
            "PackageRecipe": "false",
            "IngredientType": "15792",
            "Unit": "l",
            "IngredientTypeName": "Vesi"
          }],
          [{
            "Name": "karkeaa merisuolaa",
            "Amount": "1",
            "PackageRecipe": "false",
            "IngredientType": "6222",
            "Unit": "dl",
            "IngredientTypeName": "Merisuola, karkea"
          }]
        ]
      }, {
        "SubSectionHeading": "Täyte",
        "SubSectionIngredients": [
          [{
            "Name": "Pirkka Parhaat kuorittuja sinisimpukoita (pakaste)",
            "Ean": "6410405221797",
            "Amount": "1",
            "AmountInfo": "(200 g)",
            "PackageRecipe": "false",
            "Unit": "pkt"
          }],
          [{
            "Name": "Pirkka isoja katkarapuja (pakaste)",
            "Ean": "6410405045416",
            "Amount": "1",
            "AmountInfo": "(180 g)",
            "PackageRecipe": "false",
            "IngredientType": "5735",
            "Unit": "ps",
            "IngredientTypeName": "Katkarapu, pakaste"
          }],
          [{
            "Name": "Pirkka Parhaat kampasimpukoita (pakaste)",
            "Amount": "1/2",
            "AmountInfo": "(à 150 g)",
            "PackageRecipe": "false",
            "IngredientType": "5681",
            "Unit": "pkt",
            "IngredientTypeName": "Kampasimpukka, pakaste"
          }],
          [{
            "Name": "dijonsinappia",
            "Amount": "1",
            "PackageRecipe": "false",
            "IngredientType": "6917",
            "Unit": "rkl",
            "IngredientTypeName": "Sinappi"
          }],
          [{
            "Name": "Pirkka appelsiinitäysmehua",
            "Amount": "1",
            "PackageRecipe": "false",
            "IngredientType": "5235",
            "Unit": "rkl",
            "IngredientTypeName": "Täysmehu, appelsiini"
          }],
          [{
            "Name": "suolaa",
            "Amount": "1/2",
            "PackageRecipe": "false",
            "IngredientType": "7049",
            "Unit": "tl",
            "IngredientTypeName": "Suola"
          }],
          [{
            "Name": "mustapippuria",
            "Amount": "1/2",
            "PackageRecipe": "false",
            "IngredientType": "6271",
            "Unit": "tl",
            "IngredientTypeName": "Mustapippuri, rouhittu"
          }],
          [{
            "Name": "rypsiöljyä",
            "Amount": "1/2",
            "PackageRecipe": "false",
            "IngredientType": "6807",
            "Unit": "dl",
            "IngredientTypeName": "Rypsiöljy"
          }],
          [{
            "Name": "tuoretta ruohosipulia hienonnettuna",
            "Amount": "1/2",
            "PackageRecipe": "false",
            "IngredientType": "6772",
            "Unit": "dl",
            "IngredientTypeName": "Ruohosipuli, tuore"
          }]
        ]
      }, {
        "SubSectionHeading": "Lisäksi",
        "SubSectionIngredients": [
          [{
            "Name": "sidontanarua",
            "PackageRecipe": "false",
            "IngredientType": "15924",
            "IngredientTypeName": "Paistinaru"
          }],
          [{
            "Name": "savustuspurua (esim. leppä)",
            "Amount": "3",
            "PackageRecipe": "false",
            "IngredientType": "16076",
            "Unit": "dl",
            "IngredientTypeName": "Savustuspuru"
          }],
          [{
            "Name": "sokeripalaa",
            "Amount": "2",
            "PackageRecipe": "false",
            "IngredientType": "6431",
            "Unit": "",
            "IngredientTypeName": "Palasokeri"
          }]
        ]
      }, {
        "SubSectionHeading": "Appelsiinimajoneesi",
        "SubSectionIngredients": [
          [{
            "Name": "Hellmann's majoneesia",
            "Amount": "2",
            "PackageRecipe": "false",
            "IngredientType": "6058",
            "Unit": "dl",
            "IngredientTypeName": "Majoneesi"
          }],
          [{
            "Name": "Pirkka appelsiinitäysmehua",
            "Ean": "6410402023059",
            "Amount": "1",
            "PackageRecipe": "false",
            "IngredientType": "5235",
            "Unit": "dl",
            "IngredientTypeName": "Täysmehu, appelsiini"
          }]
        ]
      }],
      "Instructions": "# Huuhdo perattu kirjolohi nopeasti ja pyyhi sen pinta ja sisus huolellisesti talouspaperilla.\r\n# Valmista suolaliemi kalan suolaamista varten. Yhdistä laakeaan reunalliseen astiaan kylmä vesi ja merisuola. Sekoita kunnes suola on liuennut veteen. Upota kirjolohi suolaveteen ja anna suolautua tunnin ajan.\r\n# Nosta kala vedestä ja kuivaa se huolellisesti.\r\n# Levitä savustuspuruja tasaisesti savustuslaatikon pohjalle ja lisää pinnalle palasokerit.\r\n# Sekoita keskenään kaikki äyriäisseoksen ainekset, täytä kala ja sido kala sidontanarulla niin, että täyte pysyy kalan sisässä.\r\n# Nosta kala savustimen ritilän päälle. Sulje kansi. Esilämmitä kaasugrilli 200 asteeseen ja nosta savustuslaatikko grilliin. Odota 5-10 minuuttia savun muodostumista. Savusta 40-50 minuuttia, kunnes kala on mehevän kypsää. Kala on kypsää, kun nahka, rintaevä ja ruodot irtoavat helposti.\r\n# Valmista majoneesi. Keitä appelsiinimehua, kunnes siitä on jäljellä enää noin 2 ½ rkl. Anna mehutiivisteen jäähtyä ja sekoita se sitten majoneesiin. Tarjoa lohi raikkaan salaatin ja appelsiinimajoneesin kanssa.",
      "EndNote": "",
      "Description": "Kokonaisena savustettu kirjolohi täytetään äyriäisillä. Tarjoa lohi appelsiinisalaatin kanssa.",
      "DateCreated": "2019-04-25T11:55:13",
      "DateModified": "2019-08-07T15:34:55",
      "Stamp": {
        "Name": "Testattu K-koekeittiössä",
        "Id": "1"
      },
      "RecipeUse": {
        "Name": "k-ruoka.fi",
        "Id": "1"
      },
      "resourceType": "recipe",
      "Url": "https://www.k-ruoka.fi/reseptit/taytetty-kokonaisena-savustettu-kirjolohi",
      "UrlSlug": "taytetty-kokonaisena-savustettu-kirjolohi",
      "Sort": [1, 1, 10385]
    }, {
      "Id": "10379",
      "Name": "Hook-siivet",
      "PieceSize": {
        "Unit": "g"
      },
      "Portions": {
        "Unit": "annosta",
        "Amount": "4"
      },
      "PreparationTime": {
        "Description": "alle 30 min",
        "TimeRange": {
          "MinTime": 15,
          "MaxTime": 30
        }
      },
      "UserPortions": {
        "$": {
          "Unit": "henkilölle"
        }
      },
      "Categories": [{
        "MainName": "Sesonki",
        "MainId": "1",
        "SubName": "Vappu",
        "SubId": "3"
      }, {
        "MainName": "Sesonki",
        "MainId": "1",
        "SubName": "Grillaus",
        "SubId": "5"
      }, {
        "MainName": "Makuja maailmalta",
        "MainId": "2",
        "SubName": "Pohjois-Amerikka",
        "SubId": "8"
      }, {
        "MainName": "Pääruoat",
        "MainId": "4",
        "SubName": "Broileri- ja linturuoat",
        "SubId": "21"
      }, {
        "MainName": "Pääruoat",
        "MainId": "4",
        "SubName": "Grilliruoat",
        "SubId": "73"
      }, {
        "MainName": "Pääraaka-aine",
        "MainId": "14",
        "SubName": "Broileri ja linnut",
        "SubId": "118"
      }, {
        "MainName": "Asiakassegmentointi (K-asiakkaat)",
        "MainId": "17",
        "SubName": "Vakiintuneet",
        "SubId": "150"
      }, {
        "MainName": "Sesonki",
        "MainId": "1",
        "SubName": "Oktoberfest",
        "SubId": "125"
      }, {
        "MainName": "Sesonki",
        "MainId": "1",
        "SubName": "Halloween",
        "SubId": "126"
      }, {
        "MainName": "Teema",
        "MainId": "15",
        "SubName": "Mitä tänään syötäisiin?",
        "SubId": "144"
      }, {
        "MainName": "Asiakassegmentointi (K-asiakkaat)",
        "MainId": "17",
        "SubName": "Nautiskelijat",
        "SubId": "146"
      }, {
        "MainName": "Asiakassegmentointi (K-asiakkaat)",
        "MainId": "17",
        "SubName": "Innostujat",
        "SubId": "147"
      }, {
        "MainName": "Asiakassegmentointi (K-asiakkaat)",
        "MainId": "17",
        "SubName": "Mukavuudenhaluiset ",
        "SubId": "149"
      }],
      "Pictures": ["PI6_19_hook_siivet"],
      "PictureUrls": [{
        "Id": "PI6_19_hook_siivet",
        "Normal": "https://k-file-storage-qa.imgix.net/f/k-ruoka/recipe/10379?w=1000&h=1000&fit=clip",
        "Original": "https://k-file-storage-qa.imgix.net/f/k-ruoka/recipe/10379"
      }],
      "VideoUrl": "https://youtu.be/E1VK0Ig-d7E",
      "VideoEmbedUrl": "<iframe width=\"560\" height=\"315\" src=\"https://www.youtube.com/embed/E1VK0Ig-d7E\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>",
      "Ingredients": [{
        "SubSectionHeading": "_",
        "SubSectionIngredients": [
          [{
            "Name": "Hook broilerin paahdettuja siipiä",
            "Amount": "1",
            "AmountInfo": "(700 g)",
            "PackageRecipe": "false",
            "IngredientType": "16203",
            "Unit": "pkt",
            "IngredientTypeName": "Hook broilerin paahdettuja siipiä"
          }],
          [{
            "Name": "Hook siipikastiketta",
            "Amount": "1-1 1/2",
            "PackageRecipe": "false",
            "IngredientType": "16204",
            "Unit": "dl",
            "IngredientTypeName": "Hook siipikastike"
          }],
          [{
            "Name": "Hook-valkosipuli-, sinihomejuusto- tai ranch-dippiä",
            "Amount": "1",
            "PackageRecipe": "false",
            "IngredientType": "16205",
            "Unit": "dl",
            "IngredientTypeName": "Hook dippi, valkosipuli"
          }]
        ]
      }],
      "Instructions": "# Siivet ovat kypsiä, joten niille riittää kuumennus, jolloin myös pinta rapeutuu. Voit kuumentaa siivet uunissa, grillissä tai uppopaistaa öljyssä.\r\n# Uunissa: Laita siivet uunipellille leivinpaperin päälle. Kypsennä 200-asteisessa uunissa 10-15 minuuttia. Grillissä: Grillaa siipiä kuumassa grillissä noin 5-10 minuuttia siipiä tiuhaan käännellen, kunnes pinta rapeutuu mutta ei pala. Upporasvassa: Kuumenna öljy padassa liedellä 180-asteiseksi. Uppopaista öljyssä 2-3 minuuttia.\r\n# Siirrä kuumat siivet kulhoon ja anna levätä minuutti. Lisää noin 1 dl siipikastiketta tai makusi mukaan ja sekoita. Tarjoa loppu kastike siipien kanssa.\r\n# Tarjoa siivet valitsemasi dipin kanssa.",
      "EndNote": "",
      "Description": "Wingsit eli kanansiivet on nopea, helppo ja herkullinen ruoka illanistujaisiin sekä grillibileisiin. Makua voit vaihdella Hookin herkullisilla kastikkeilla.",
      "DateCreated": "2019-04-24T10:40:58",
      "DateModified": "2019-09-23T12:34:42",
      "TvDate": "2019-09-06T00:00:00",
      "Stamp": {
        "Name": "Testattu K-koekeittiössä",
        "Id": "1"
      },
      "RecipeUse": {
        "Name": "k-ruoka.fi",
        "Id": "1"
      },
      "resourceType": "recipe",
      "Url": "https://www.k-ruoka.fi/reseptit/hook-siivet",
      "UrlSlug": "hook-siivet",
      "Sort": [1, 1, 10379]
    }, {
      "Id": "10378",
      "Name": "Kinkkukiusaus pellillä",
      "PieceSize": {
        "Unit": "g"
      },
      "Portions": {
        "Unit": "annosta",
        "Amount": "6"
      },
      "PreparationTime": {
        "Description": "30 - 60 min",
        "TimeRange": {
          "MinTime": 30,
          "MaxTime": 60
        }
      },
      "UserPortions": {
        "$": {
          "Unit": "henkilölle"
        }
      },
      "Categories": [{
        "MainName": "Pääruoat",
        "MainId": "4",
        "SubName": "Liharuoat",
        "SubId": "24"
      }, {
        "MainName": "Pääruoat",
        "MainId": "4",
        "SubName": "Uuniruoat",
        "SubId": "30"
      }, {
        "MainName": "Makuja maailmalta",
        "MainId": "2",
        "SubName": "Suomi",
        "SubId": "106"
      }, {
        "MainName": "Pääraaka-aine",
        "MainId": "14",
        "SubName": "Liha",
        "SubId": "114"
      }, {
        "MainName": "Ruokalaji",
        "MainId": "16",
        "SubName": "Uuniruoat",
        "SubId": "141"
      }, {
        "MainName": "Asiakassegmentointi (K-asiakkaat)",
        "MainId": "17",
        "SubName": "Mukavuudenhaluiset ",
        "SubId": "149"
      }, {
        "MainName": "Asiakassegmentointi (K-asiakkaat)",
        "MainId": "17",
        "SubName": "Vakiintuneet",
        "SubId": "150"
      }],
      "Pictures": ["PI10_19_peltikinkkukiusa"],
      "PictureUrls": [{
        "Id": "PI10_19_peltikinkkukiusa",
        "Normal": "https://k-file-storage-qa.imgix.net/f/k-ruoka/recipe/10378?w=1000&h=1000&fit=clip",
        "Original": "https://k-file-storage-qa.imgix.net/f/k-ruoka/recipe/10378"
      }],
      "Ingredients": [{
        "SubSectionHeading": "_",
        "SubSectionIngredients": [
          [{
            "Name": "Pirkka ylikypsää saunapalvikinkkua (pala)",
            "Ean": "6410405225597",
            "Amount": "1/2",
            "AmountInfo": "(à 400 g)",
            "PackageRecipe": "false",
            "IngredientType": "7370",
            "Unit": "pkt",
            "IngredientTypeName": "Ylikypsä saunapalvikinkku, pala"
          }],
          [{
            "Name": "Pirkka sipulikuutioita (pakaste)",
            "Ean": "6410405156280",
            "Amount": "1/2",
            "AmountInfo": "(à 200 g)",
            "PackageRecipe": "false",
            "IngredientType": "6938",
            "Unit": "ps",
            "IngredientTypeName": "Sipulikuutio, pakaste"
          }],
          [{
            "Name": "suolaa",
            "Amount": "n. 1/4",
            "PackageRecipe": "false",
            "IngredientType": "7049",
            "Unit": "tl",
            "IngredientTypeName": "Suola"
          }],
          [{
            "Name": "mustapippuria",
            "Amount": "n. 1/2",
            "PackageRecipe": "false",
            "IngredientType": "6269",
            "Unit": "tl",
            "IngredientTypeName": "Mustapippuri, kokonainen"
          }],
          [{
            "Name": "Pirkka perunasuikaleita (pakaste)",
            "Ean": "6410405068699",
            "Amount": "2",
            "AmountInfo": "(à 500 g)",
            "PackageRecipe": "false",
            "IngredientType": "6534",
            "Unit": "ps",
            "IngredientTypeName": "Perunasuikale, pakaste"
          }],
          [{
            "Name": "Pirkka laktoositonta juustokermaa (15 %)",
            "Ean": "6410405121103",
            "Amount": "2",
            "AmountInfo": "(à 2 dl)",
            "PackageRecipe": "false",
            "IngredientType": "13605",
            "Unit": "tlk",
            "IngredientTypeName": "Ruokakerma, juusto, laktoositon"
          }],
          [{
            "Name": "Pirkka emmental-mozzarellajuustoraastetta",
            "Ean": "6410405048585",
            "Amount": "1",
            "AmountInfo": "(150 g)",
            "PackageRecipe": "false",
            "IngredientType": "5600",
            "Unit": "ps",
            "IngredientTypeName": "Juustoraaste, emmental-mozzarella"
          }]
        ]
      }],
      "Instructions": "# Leikkaa kinkku suikaleiksi.\r\n# Kaada pussillinen perunasuikaleita leivinpaperilla vuoratulle syvälle uunipannulle. Mausta suolalla ja pippurilla  Levitä päälle sipulit ja kinkut. Lisää toinen pussillinen perunasuikaleita ja kaada päälle ruokakermat. Lisää halutessasi pinnalle juustoraastetta.\r\n# Kypsennä 200-asteisen uunin alatasolla noin 50 minuuttia tai kunnes perunat ovat kypsiä ja kerma lähes imeytynyt. Anna vetäytyä hetki uunista ottamisen jälkeen.",
      "EndNote": "*Annoshinnat on laskettu K-citymarket-ketjun hintatason mukaan 10/2019. Tuoretuotteiden hinnat vaihtelevat sesongeittain.",
      "Description": "Helppo kinkkukiusaus syntyy pakasteperunoista ja -sipuleista. Kun teet kiusauksen pellille, saat enemmän rapeaa pintaa ja kiusaus kypsyy nopeammin. Noin 1,60 €/annos*",
      "DateCreated": "2019-04-23T18:50:55",
      "DateModified": "2019-09-18T09:44:50",
      "Stamp": {
        "Name": "Testattu K-koekeittiössä",
        "Id": "1"
      },
      "RecipeUse": {
        "Name": "k-ruoka.fi",
        "Id": "1"
      },
      "resourceType": "recipe",
      "Url": "https://www.k-ruoka.fi/reseptit/kinkkukiusaus-pellilla",
      "UrlSlug": "kinkkukiusaus-pellilla",
      "Sort": [1, 1, 10378]
    }, {
      "Id": "10376",
      "Name": "Karamellipossupizza",
      "PieceSize": {
        "Unit": "g"
      },
      "Portions": {
        "Unit": "kpl",
        "Amount": "2"
      },
      "PreparationTime": {
        "Description": "30 - 60 min",
        "TimeRange": {
          "MinTime": 30,
          "MaxTime": 60
        }
      },
      "UserPortions": {
        "$": {
          "Unit": "henkilölle"
        }
      },
      "Categories": [{
        "MainName": "Makuja maailmalta",
        "MainId": "2",
        "SubName": "Aasia",
        "SubId": "7"
      }, {
        "MainName": "Pääruoat",
        "MainId": "4",
        "SubName": "Liharuoat",
        "SubId": "24"
      }, {
        "MainName": "Pääruoat",
        "MainId": "4",
        "SubName": "Pizzat",
        "SubId": "87"
      }, {
        "MainName": "Pääraaka-aine",
        "MainId": "14",
        "SubName": "Liha",
        "SubId": "114"
      }, {
        "MainName": "Ruokalaji",
        "MainId": "16",
        "SubName": "Pizzat",
        "SubId": "139"
      }, {
        "MainName": "Teema",
        "MainId": "15",
        "SubName": "Mitä tänään syötäisiin?",
        "SubId": "144"
      }, {
        "MainName": "Asiakassegmentointi (K-asiakkaat)",
        "MainId": "17",
        "SubName": "Nautiskelijat",
        "SubId": "146"
      }, {
        "MainName": "Asiakassegmentointi (K-asiakkaat)",
        "MainId": "17",
        "SubName": "Innostujat",
        "SubId": "147"
      }, {
        "MainName": "Asiakassegmentointi (K-asiakkaat)",
        "MainId": "17",
        "SubName": "Tiedostavat",
        "SubId": "148"
      }, {
        "MainName": "Asiakassegmentointi (K-asiakkaat)",
        "MainId": "17",
        "SubName": "Mukavuudenhaluiset ",
        "SubId": "149"
      }],
      "Pictures": ["PI9_19_karamellipossup"],
      "PictureUrls": [{
        "Id": "PI9_19_karamellipossup",
        "Normal": "https://k-file-storage-qa.imgix.net/f/k-ruoka/recipe/10376?w=1000&h=1000&fit=clip",
        "Original": "https://k-file-storage-qa.imgix.net/f/k-ruoka/recipe/10376"
      }],
      "VideoUrl": "https://youtu.be/rs4Eqt6FJ0M",
      "VideoEmbedUrl": "<iframe width=\"560\" height=\"315\" src=\"https://www.youtube.com/embed/rs4Eqt6FJ0M\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>",
      "Ingredients": [{
        "SubSectionHeading": "Pohja",
        "SubSectionIngredients": [
          [{
            "Name": "vettä",
            "Amount": "2",
            "PackageRecipe": "false",
            "IngredientType": "15792",
            "Unit": "dl",
            "IngredientTypeName": "Vesi"
          }],
          [{
            "Name": "sokeria",
            "Amount": "1",
            "PackageRecipe": "false",
            "IngredientType": "6961",
            "Unit": "tl",
            "IngredientTypeName": "Sokeri"
          }],
          [{
            "Name": "suolaa",
            "Amount": "3/4",
            "PackageRecipe": "false",
            "IngredientType": "7049",
            "Unit": "tl",
            "IngredientTypeName": "Suola"
          }],
          [{
            "Name": "Pirkka vehnäjauhoja",
            "Amount": "n. 4",
            "PackageRecipe": "false",
            "IngredientType": "7293",
            "Unit": "dl",
            "IngredientTypeName": "Vehnäjauho"
          }],
          [{
            "Name": "Pirkka kuivahiivaa",
            "Ean": "6410400033036",
            "Amount": "1",
            "AmountInfo": "(11 g)",
            "PackageRecipe": "false",
            "IngredientType": "5883",
            "Unit": "ps",
            "IngredientTypeName": "Kuivahiiva"
          }],
          [{
            "Name": "voita pannun voiteluun",
            "Amount": "2",
            "PackageRecipe": "false",
            "Unit": "rkl"
          }]
        ]
      }, {
        "SubSectionHeading": "Pikkelöidyt kasvikset",
        "SubSectionIngredients": [
          [{
            "Name": "retiisiä",
            "Amount": "5",
            "AmountInfo": "(50 g)",
            "PackageRecipe": "false",
            "IngredientType": "6725",
            "Unit": "",
            "IngredientTypeName": "Retiisi"
          }],
          [{
            "Name": "porkkana",
            "Amount": "1",
            "AmountInfo": "(60 g)",
            "PackageRecipe": "false",
            "IngredientType": "6582",
            "Unit": "",
            "IngredientTypeName": "Porkkana"
          }],
          [{
            "Name": "kurkkua",
            "Amount": "100",
            "PackageRecipe": "false",
            "IngredientType": "5895",
            "Unit": "g",
            "IngredientTypeName": "Kurkku"
          }],
          [{
            "Name": "pieni valkosipulinkynsi",
            "Amount": "1",
            "AmountInfo": "pieni",
            "PackageRecipe": "false",
            "IngredientType": "7262",
            "Unit": "",
            "IngredientTypeName": "Valkosipuli"
          }],
          [{
            "Name": "Pirkka riisiviinietikkaa",
            "Amount": "2",
            "PackageRecipe": "false",
            "IngredientType": "6740",
            "Unit": "rkl",
            "IngredientTypeName": "Riisiviinietikka"
          }],
          [{
            "Name": "paahdettua seesamiöljyä",
            "Amount": "1",
            "PackageRecipe": "false",
            "IngredientType": "6873",
            "Unit": "tl",
            "IngredientTypeName": "Seesamiöljy"
          }],
          [{
            "Name": "sokeria",
            "Amount": "1/2",
            "PackageRecipe": "false",
            "IngredientType": "6961",
            "Unit": "tl",
            "IngredientTypeName": "Sokeri"
          }],
          [{
            "Name": "suolaa",
            "Amount": "1/4",
            "PackageRecipe": "false",
            "IngredientType": "7049",
            "Unit": "tl",
            "IngredientTypeName": "Suola"
          }]
        ]
      }, {
        "SubSectionHeading": "Lisäksi",
        "SubSectionIngredients": [
          [{
            "Name": "Hk Rypsiporsas nopeita fileesuikaleita, karamellipossu",
            "Amount": "1",
            "AmountInfo": "(360 g)",
            "PackageRecipe": "false",
            "Unit": "rs"
          }],
          [{
            "Name": "öljyä",
            "Amount": "1",
            "PackageRecipe": "false",
            "IngredientType": "6807",
            "Unit": "rkl",
            "IngredientTypeName": "Rypsiöljy"
          }],
          [{
            "Name": "K-Menu mozzarellajuustoraastetta",
            "Ean": "6410405136602",
            "Amount": "1",
            "AmountInfo": "(250 g)",
            "PackageRecipe": "false",
            "IngredientType": "5603",
            "Unit": "ps",
            "IngredientTypeName": "Juustoraaste, mozzarella"
          }],
          [{
            "Name": "Pirkka srirachakastiketta",
            "Ean": "6410405192653",
            "Amount": "1/2-1",
            "PackageRecipe": "false",
            "IngredientType": "6992",
            "Unit": "rkl",
            "IngredientTypeName": "Sriracha"
          }],
          [{
            "Name": "Hellmann's majoneesia",
            "Amount": "1/2",
            "PackageRecipe": "false",
            "IngredientType": "6058",
            "Unit": "dl",
            "IngredientTypeName": "Majoneesi"
          }],
          [{
            "Name": "kevätsipulinvartta hienonnettuna",
            "Amount": "1-2",
            "PackageRecipe": "false",
            "IngredientType": "5792",
            "Unit": "",
            "IngredientTypeName": "Kevätsipuli"
          }],
          [{
            "Name": "korianteria hienonnettuna",
            "Amount": "1/2",
            "PackageRecipe": "false",
            "IngredientType": "5852",
            "Unit": "ruukkua",
            "IngredientTypeName": "Korianteri, tuore"
          }],
          [{
            "Name": "Pirkka mieto punainen chili viipaloituna",
            "Ean": "6410405091857",
            "Amount": "1",
            "PackageRecipe": "false",
            "IngredientType": "5355",
            "Unit": "",
            "IngredientTypeName": "Chili, punainen"
          }],
          [{
            "Name": "(mustia) seesaminsiemeniä",
            "Amount": "1/2",
            "PackageRecipe": "false",
            "IngredientType": "6871",
            "Unit": "rkl",
            "IngredientTypeName": "Seesaminsiemen"
          }]
        ]
      }],
      "Instructions": "# Lämmitä vesi noin 42-asteiseksi. Lisää joukkoon sokeri, suola ja pieneen jauhomäärään sekoitettu kuivahiiva. Alusta loput jauhot vähitellen. Taikina saa jäädä melko löysäksi. Kohota taikina lämpimässä paikassa kaksinkertaiseksi.\r\n# Leikkaa pestyt retiisit ohuiksi viipaleiksi. Kuori porkkana ja leikkaa siitä kuorimaveitsellä pitkiä suikaleita. Halkaise kurkku, kaavi siemenet lusikalla pois kurkun keskeltä. Leikkaa kurkku viipaleiksi. Kuori ja hienonna valkosipulinkynsi.\r\n# Sekoita kasvikset riisiviinietikan, seesamiöljyn, sokerin ja suolan kanssa ja anna tekeytyä jääkaapissa sen aikaa, kun paistat pizzat.\r\n# Ruskista porsaan fileesuikaleet öljyssä pannulla kahdessa erässä. Sekoita lopuksi lihojen joukkoon pakkauksessa oleva kastike.\r\n# Jaa taikina kahteen osaan. Voitele valurautapannu (⌀ noin 20 cm).  Painele taikina jauhotetuilla käsillä vuoan pohjalle, älä nosta taikinaa reunoille. Levitä pizzapohjalle puolet juustoraasteesta ja possun suikaleista.\r\n# Paista pizzaa 250-asteisen uunin alimmalla tasolla noin 15 minuuttia. Valmistele ja paista samoin toinen pizza. Levitä taikina varovasti kuumalle pannulle.\r\n# Nosta paistettujen pizzojen pinnalle valutettuja pikkelöityjä kasviksia. Valuta pinnalle majoneesia ja srirachakastiketta. Koristele pizzat kevätsipulilla, korianterilla, chiliviipaleilla ja seesaminsiemenillä.",
      "EndNote": "*Annoshinnat on laskettu K-citymarket-ketjun hintatason mukaan 8/2019. Tuoretuotteiden hinnat vaihtelevat sesongeittain.",
      "Description": "Aasialaisvaikutteinen karamellipossupizza saa pinnalleen vietnamilaisesta Bánh mì -patongista tuttuja makuja. Pannupizzapohja paistuu kätevästi valurautapannulla. Noin 6,15€/kpl",
      "DateCreated": "2019-04-23T18:31:15",
      "DateModified": "2019-09-16T08:51:28",
      "TvDate": "2019-09-17T00:00:00",
      "Stamp": {
        "Name": "Testattu K-koekeittiössä",
        "Id": "1"
      },
      "RecipeUse": {
        "Name": "k-ruoka.fi",
        "Id": "1"
      },
      "resourceType": "recipe",
      "Url": "https://www.k-ruoka.fi/reseptit/karamellipossupizza",
      "UrlSlug": "karamellipossupizza",
      "Sort": [1, 1, 10376]
    }, {
      "Id": "10374",
      "Name": "Mustajuurisalaatti",
      "PieceSize": {
        "Unit": "g"
      },
      "Portions": {
        "Unit": "annosta",
        "Amount": "4"
      },
      "PreparationTime": {
        "Description": "alle 30 min",
        "TimeRange": {
          "MinTime": 15,
          "MaxTime": 30
        }
      },
      "UserPortions": {
        "$": {
          "Unit": "henkilölle"
        }
      },
      "Categories": [{
        "MainName": "Alkuruoat",
        "MainId": "3",
        "SubName": "Salaatit",
        "SubId": "18"
      }, {
        "MainName": "Pääruoat",
        "MainId": "4",
        "SubName": "Kasvisruoat",
        "SubId": "22"
      }, {
        "MainName": "Lisäkkeet",
        "MainId": "6",
        "SubName": "Kasvislisäkkeet",
        "SubId": "42"
      }, {
        "MainName": "Lisäkkeet",
        "MainId": "6",
        "SubName": "Salaatit",
        "SubId": "44"
      }, {
        "MainName": "Pääraaka-aine",
        "MainId": "14",
        "SubName": "Kasvis",
        "SubId": "115"
      }, {
        "MainName": "Sesonki",
        "MainId": "1",
        "SubName": "Sadonkorjuu",
        "SubId": "124"
      }, {
        "MainName": "Asiakassegmentointi (K-asiakkaat)",
        "MainId": "17",
        "SubName": "Nautiskelijat",
        "SubId": "146"
      }, {
        "MainName": "Asiakassegmentointi (K-asiakkaat)",
        "MainId": "17",
        "SubName": "Innostujat",
        "SubId": "147"
      }, {
        "MainName": "Asiakassegmentointi (K-asiakkaat)",
        "MainId": "17",
        "SubName": "Tiedostavat",
        "SubId": "148"
      }],
      "Pictures": ["KR5_19_mustajuurisal"],
      "PictureUrls": [{
        "Id": "KR5_19_mustajuurisal",
        "Normal": "https://k-file-storage-qa.imgix.net/f/k-ruoka/recipe/10374?w=1000&h=1000&fit=clip",
        "Original": "https://k-file-storage-qa.imgix.net/f/k-ruoka/recipe/10374"
      }],
      "Ingredients": [{
        "SubSectionHeading": "_",
        "SubSectionIngredients": [
          [{
            "Name": "mustajuuria",
            "Amount": "1",
            "PackageRecipe": "false",
            "IngredientType": "6265",
            "Unit": "kg",
            "IngredientTypeName": "Mustajuuri"
          }],
          [{
            "Name": "vettä",
            "Amount": "1",
            "PackageRecipe": "false",
            "IngredientType": "15792",
            "Unit": "dl",
            "IngredientTypeName": "Vesi"
          }],
          [{
            "Name": "voita",
            "Amount": "50",
            "PackageRecipe": "false",
            "IngredientType": "15824",
            "Unit": "g",
            "IngredientTypeName": "Pirkka voi 400g"
          }],
          [{
            "Name": "korianterinsiemeniä rouhittuna",
            "Amount": "n. 1",
            "PackageRecipe": "false",
            "IngredientType": "10050",
            "Unit": "tl",
            "IngredientTypeName": "Korianterinsiemen"
          }],
          [{
            "Name": "suolaa",
            "PackageRecipe": "false",
            "IngredientType": "7049",
            "Unit": "ripaus",
            "IngredientTypeName": "Suola"
          }]
        ]
      }, {
        "SubSectionHeading": "Lisäksi",
        "SubSectionIngredients": [
          [{
            "Name": "erilaisia pieniä versoja",
            "PackageRecipe": "false",
            "IngredientType": "15430",
            "IngredientTypeName": "Versoseos"
          }],
          [{
            "Name": "Pirkka Parhaat sinihomejuustoa",
            "Ean": "6410405205186",
            "Amount": "1",
            "AmountInfo": "(175 g)",
            "PackageRecipe": "false",
            "IngredientType": "6924",
            "Unit": "pkt",
            "IngredientTypeName": "Sinihomejuusto"
          }],
          [{
            "Name": "Pirkka pekaanipähkinöitä",
            "Ean": "6410405126641",
            "Amount": "1/2",
            "PackageRecipe": "false",
            "IngredientType": "6497",
            "Unit": "dl",
            "IngredientTypeName": "Pekaanipähkinä"
          }]
        ]
      }],
      "Instructions": "# Pese ja harjaa mustajuuret puhtaiksi. Kuori juuret veden alla. Siisti kanta ja paloittele noin 5 cm:n paloiksi. Paksummat mustajuuret voit leikata pituussuunnassa halki.\r\n# Kuumenna kattilassa vesi ja voi. Lisää mustajuuret ja kypsennä kannen alla pienellä lämmöllä 10-15 minuuttia riippuen mustajuurien paksuudesta. Lisää korianteri ja suola, kypsennä vielä muutama minuutti. Anna jäähtyä hetki.\r\n# Asettele versot vadille, säästä hieman koristeeksi. Leikkaa sinihomejuusto reiluiksi paloiksi. Paahda pekaanipähkinöitä kevyesti pannulla ja rouhi niitä veitsellä hieman pienemmäksi.\r\n# Nostele mustajuuret versojen päälle. Asettele vadille sinihomejuusto ja ripottele pinnalle pekaanipähkinät. Koristele versoilla.",
      "EndNote": "*Annoshinnat on laskettu K-citymarket-ketjun hintatason mukaan 8/2019. Tuoretuotteiden hinnat vaihtelevat sesongeittain.",
      "Description": "Voissa karamellisoituneet mustajuuret ovat hienostunut alkuruoka tai lisäke. Tarjoa ne versojen, pehmeän sinihomejuuston ja paahdettujen pähkinöiden kanssa. Noin 2,05€/annos",
      "DateCreated": "2019-04-23T18:30:07",
      "DateModified": "2019-07-16T12:52:53",
      "Stamp": {
        "Name": "Testattu K-koekeittiössä",
        "Id": "1"
      },
      "RecipeUse": {
        "Name": "k-ruoka.fi",
        "Id": "1"
      },
      "resourceType": "recipe",
      "Url": "https://www.k-ruoka.fi/reseptit/mustajuurisalaatti",
      "UrlSlug": "mustajuurisalaatti",
      "Sort": [1, 1, 10374]
    }, {
      "Id": "10372",
      "Name": "Hapankaali-nakkikeitto",
      "PieceSize": {
        "Unit": "g"
      },
      "Portions": {
        "Unit": "annosta",
        "Amount": "4"
      },
      "PreparationTime": {
        "Description": "alle 30 min",
        "TimeRange": {
          "MinTime": 15,
          "MaxTime": 30
        }
      },
      "UserPortions": {
        "$": {
          "Unit": "henkilölle"
        }
      },
      "Categories": [{
        "MainName": "Pääruoat",
        "MainId": "4",
        "SubName": "Makkararuoat",
        "SubId": "25"
      }, {
        "MainName": "Pääruoat",
        "MainId": "4",
        "SubName": "Keitot",
        "SubId": "27"
      }, {
        "MainName": "Pääraaka-aine",
        "MainId": "14",
        "SubName": "Makkara",
        "SubId": "113"
      }, {
        "MainName": "Ruokalaji",
        "MainId": "16",
        "SubName": "Keitot",
        "SubId": "136"
      }, {
        "MainName": "Teema",
        "MainId": "15",
        "SubName": "Mitä tänään syötäisiin?",
        "SubId": "144"
      }, {
        "MainName": "Asiakassegmentointi (K-asiakkaat)",
        "MainId": "17",
        "SubName": "Mukavuudenhaluiset ",
        "SubId": "149"
      }, {
        "MainName": "Asiakassegmentointi (K-asiakkaat)",
        "MainId": "17",
        "SubName": "Vakiintuneet",
        "SubId": "150"
      }, {
        "MainName": "Teema",
        "MainId": "15",
        "SubName": "K-Marketin valikoima",
        "SubId": "151"
      }, {
        "MainName": "Apukategoriat",
        "MainId": "18",
        "SubName": "K-Marketin valikoima",
        "SubId": "181"
      }],
      "Pictures": ["PI9_19_hapank_nakkikei"],
      "PictureUrls": [{
        "Id": "PI9_19_hapank_nakkikei",
        "Normal": "https://k-file-storage-qa.imgix.net/f/k-ruoka/recipe/10372?w=1000&h=1000&fit=clip",
        "Original": "https://k-file-storage-qa.imgix.net/f/k-ruoka/recipe/10372"
      }],
      "VideoUrl": "https://youtu.be/uQ1GSxMjp3Q",
      "VideoEmbedUrl": "<iframe width=\"560\" height=\"315\" src=\"https://www.youtube.com/embed/uQ1GSxMjp3Q\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>",
      "Ingredients": [{
        "SubSectionHeading": "_",
        "SubSectionIngredients": [
          [{
            "Name": "porkkanaa",
            "Amount": "3",
            "AmountInfo": "(200 g)",
            "PackageRecipe": "false",
            "IngredientType": "6582",
            "Unit": "",
            "IngredientTypeName": "Porkkana"
          }],
          [{
            "Name": "sipuli",
            "Amount": "1",
            "AmountInfo": "(100 g)",
            "PackageRecipe": "false",
            "IngredientType": "6932",
            "Unit": "",
            "IngredientTypeName": "Sipuli"
          }],
          [{
            "Name": "valkosipulinkynttä",
            "Amount": "2-3",
            "PackageRecipe": "false",
            "IngredientType": "7262",
            "Unit": "",
            "IngredientTypeName": "Valkosipuli"
          }],
          [{
            "Name": "öljyä",
            "Amount": "1",
            "PackageRecipe": "false",
            "IngredientType": "6807",
            "Unit": "rkl",
            "IngredientTypeName": "Rypsiöljy"
          }],
          [{
            "Name": "Pirkka tomaattisosetta",
            "Ean": "6410405093202",
            "Amount": "1/2",
            "PackageRecipe": "false",
            "IngredientType": "7141",
            "Unit": "dl",
            "IngredientTypeName": "Tomaattisose"
          }],
          [{
            "Name": "Pirkka hapankaalia",
            "Ean": "6410405038555",
            "Amount": "1",
            "AmountInfo": "(400 g)",
            "PackageRecipe": "false",
            "IngredientType": "5497",
            "Unit": "rs",
            "IngredientTypeName": "Hapankaali"
          }],
          [{
            "Name": "vettä",
            "Amount": "1",
            "PackageRecipe": "false",
            "IngredientType": "15792",
            "Unit": "l",
            "IngredientTypeName": "Vesi"
          }],
          [{
            "Name": "suolaa",
            "Amount": "3/4",
            "PackageRecipe": "false",
            "IngredientType": "7049",
            "Unit": "tl",
            "IngredientTypeName": "Suola"
          }],
          [{
            "Name": "mustapippuria",
            "Amount": "1/2",
            "PackageRecipe": "false",
            "IngredientType": "6269",
            "Unit": "tl",
            "IngredientTypeName": "Mustapippuri, kokonainen"
          }],
          [{
            "Name": "HK Aito nakkeja",
            "Amount": "1",
            "AmountInfo": "(420 g)",
            "PackageRecipe": "false",
            "Unit": "pkt"
          }],
          [{
            "Name": "persiljaa hienonnettuna",
            "Amount": "1/2",
            "PackageRecipe": "false",
            "IngredientType": "6517",
            "Unit": "ruukkua",
            "IngredientTypeName": "Persilja, tuore"
          }],
          [{
            "Name": "(Pirkka smetanaa)",
            "Ean": "6410405168108",
            "PackageRecipe": "false"
          }]
        ]
      }],
      "Instructions": "# Kuori porkkanat ja raasta ne karkeaksi raasteeksi. Kuori ja hienonna sipuli ja valkosipulinkynnet.\r\n# Kuullota sipulia ja valkosipulia öljyssä kattilassa. Lisää porkkanaraaste sekä tomaattisose ja kuullota vielä hetki.\r\n# Lisää kattilaan hapankaali ja vesi. Kuumenna kiehuvaksi ja keitä 10-15 minuuttia. Mausta suolalla ja mustapippurilla.\r\n# Viipaloi nakit ja ruskista ne pannulla. Lisää nakit ja persilja keiton joukkoon juuri ennen tarjoamista. Tarjoa halutessasi smetanan kanssa.",
      "EndNote": "*Annoshinnat on laskettu K-citymarket-ketjun hintatason mukaan 8/2019. Tuoretuotteiden hinnat vaihtelevat sesongeittain.",
      "Description": "Tomaattinen hapankaali-nakkikeitto on helppo keitto, jonka maku vain paranee seuraavaksi päiväksi. Tarjoa halutessasi smetanan kanssa  borssikeiton tapaan. Noin 2,25€/annos",
      "DateCreated": "2019-04-23T18:27:33",
      "DateModified": "2019-09-16T08:51:29",
      "TvDate": "2019-09-16T00:00:00",
      "Stamp": {
        "Name": "Testattu K-koekeittiössä",
        "Id": "1"
      },
      "RecipeUse": {
        "Name": "k-ruoka.fi",
        "Id": "1"
      },
      "resourceType": "recipe",
      "Url": "https://www.k-ruoka.fi/reseptit/hapankaali-nakkikeitto",
      "UrlSlug": "hapankaali-nakkikeitto",
      "Sort": [1, 1, 10372]
    }, {
      "Id": "10371",
      "Name": "Ruusukaalisalaatti ja paistettu kirjolohi",
      "PieceSize": {
        "Unit": "g"
      },
      "Portions": {
        "Unit": "annosta",
        "Amount": "4"
      },
      "PreparationTime": {
        "Description": "alle 30 min",
        "TimeRange": {
          "MinTime": 15,
          "MaxTime": 30
        }
      },
      "UserPortions": {
        "$": {
          "Unit": "henkilölle"
        }
      },
      "Categories": [{
        "MainName": "Pääruoat",
        "MainId": "4",
        "SubName": "Kalaruoat",
        "SubId": "20"
      }, {
        "MainName": "Pääraaka-aine",
        "MainId": "14",
        "SubName": "Kala",
        "SubId": "116"
      }, {
        "MainName": "Teema",
        "MainId": "15",
        "SubName": "Mitä tänään syötäisiin?",
        "SubId": "144"
      }, {
        "MainName": "Asiakassegmentointi (K-asiakkaat)",
        "MainId": "17",
        "SubName": "Tiedostavat",
        "SubId": "148"
      }, {
        "MainName": "Asiakassegmentointi (K-asiakkaat)",
        "MainId": "17",
        "SubName": "Vakiintuneet",
        "SubId": "150"
      }],
      "Pictures": ["KR5_19_ruusukaalisalaa"],
      "PictureUrls": [{
        "Id": "KR5_19_ruusukaalisalaa",
        "Normal": "https://k-file-storage-qa.imgix.net/f/k-ruoka/recipe/10371?w=1000&h=1000&fit=clip",
        "Original": "https://k-file-storage-qa.imgix.net/f/k-ruoka/recipe/10371"
      }],
      "Ingredients": [{
        "SubSectionHeading": "_",
        "SubSectionIngredients": [
          [{
            "Name": "ruusukaaleja",
            "Amount": "400",
            "PackageRecipe": "false",
            "IngredientType": "6801",
            "Unit": "g",
            "IngredientTypeName": "Ruusukaali"
          }],
          [{
            "Name": "Pirkka Luomu hasselpähkinöitä",
            "Amount": "1",
            "AmountInfo": "(n. 60 g)",
            "PackageRecipe": "false",
            "Unit": "dl"
          }],
          [{
            "Name": "öljyä",
            "Amount": "2",
            "PackageRecipe": "false",
            "Unit": "rkl"
          }],
          [{
            "Name": "sitruunanmehua",
            "Amount": "1",
            "PackageRecipe": "false",
            "IngredientType": "6941",
            "Unit": "rkl",
            "IngredientTypeName": "Sitruuna"
          }],
          [{
            "Name": "sitruunankuorta raastettuna",
            "Amount": "1",
            "PackageRecipe": "false",
            "IngredientType": "15804",
            "Unit": "tl",
            "IngredientTypeName": "Sitruuna, tuore"
          }],
          [{
            "Name": "Pirkka juoksevaa hunajaa",
            "Amount": "1",
            "PackageRecipe": "false",
            "Unit": "tl"
          }],
          [{
            "Name": "suolaa",
            "Amount": "3/4",
            "PackageRecipe": "false",
            "Unit": "tl"
          }],
          [{
            "Name": "mustapippuria",
            "PackageRecipe": "false",
            "Unit": "ripaus"
          }],
          [{
            "Name": "tuoretta minttua hienonnettuna",
            "Amount": "1",
            "PackageRecipe": "false",
            "IngredientType": "15789",
            "Unit": "ruukku",
            "IngredientTypeName": "Minttu"
          }]
        ]
      }, {
        "SubSectionHeading": "Paistetut linssit",
        "SubSectionIngredients": [
          [{
            "Name": "Urtekram belugalinssejä",
            "Amount": "1",
            "PackageRecipe": "false",
            "Unit": "dl"
          }],
          [{
            "Name": "valkosipulinkynsi",
            "Amount": "1",
            "PackageRecipe": "false",
            "IngredientType": "7262",
            "Unit": "",
            "IngredientTypeName": "Valkosipuli"
          }],
          [{
            "Name": "voita",
            "Amount": "1",
            "PackageRecipe": "false",
            "Unit": "rkl"
          }],
          [{
            "Name": "suolaa",
            "PackageRecipe": "false",
            "Unit": "ripaus"
          }]
        ]
      }, {
        "SubSectionHeading": "Paistettu kirjolohi",
        "SubSectionIngredients": [
          [{
            "Name": "Pirkka Parhaat Benella kirjolohifileetä",
            "Ean": "2386448100006",
            "Amount": "1",
            "AmountInfo": "(n. 600 g)",
            "PackageRecipe": "false",
            "IngredientType": "5811",
            "Unit": "pkt",
            "IngredientTypeName": "Kirjolohifilee"
          }],
          [{
            "Name": "suolaa",
            "Amount": "n. 3/4",
            "PackageRecipe": "false",
            "Unit": "tl"
          }],
          [{
            "Name": "Meira kalamaustetta, sitrus-inkivääri",
            "Amount": "2-3",
            "PackageRecipe": "false",
            "Unit": "tl"
          }],
          [{
            "Name": "voita ja öljyä",
            "PackageRecipe": "false",
            "IngredientType": "7345",
            "IngredientTypeName": "Voi, luomu"
          }]
        ]
      }],
      "Instructions": "# Keitä linssit pakkauksen ohjeen mukaan ja valuta ne huolellisesti. Paista kypsiä linssejä hetki pannulla rasvassa valkosipulin kanssa. Mausta suolalla.\r\n# Poista tarvittaessa ruusukaalien uloimmat lehdet. Leikkaa ruusukaalit ohuen ohuiksi viipaleiksi, jätä kannat käyttämättä.\r\n# Paahda hasselpähkinöitä kevyesti kuivalla pannulla. Rouhi pähkinöitä veitsellä pienemmäksi.\r\n# Sekoita öljy, sitruunanmehu- ja kuori, hunaja ja mausteet keskenään ja kääntele ruusukaalien joukkoon. Sekoita salaattiin lopuksi pähkinät ja minttu. Anna salaatin tekeytyä hetki jääkaapissa.\r\n# Leikkaa kirjolohi annospaloiksi ja mausta suolalla ja kalamausteella. Paista kalapalat rasvassa pannulla kypsiksi. Tarjoa lohi paistettujen linssien ja ruusukaalisalaatin kanssa.",
      "EndNote": "*Annoshinnat on laskettu K-citymarket-ketjun hintatason mukaan 8/2019. Tuoretuotteiden hinnat vaihtelevat sesongeittain.",
      "Description": "Ruusukaali maistuu ihan uudenlaiselta, kun sen tarjoaa raakana salaatissa ohueksi leikattuna. Ruusukaalisalaatti tarjoataan paistetun kalan ja linssien kanssa. Noin 5,45€/annos",
      "DateCreated": "2019-04-23T18:26:42",
      "DateModified": "2019-08-30T09:44:04",
      "TvDate": "2019-10-18T00:00:00",
      "Stamp": {
        "Name": "Testattu K-koekeittiössä",
        "Id": "1"
      },
      "RecipeUse": {
        "Name": "k-ruoka.fi",
        "Id": "1"
      },
      "resourceType": "recipe",
      "Url": "https://www.k-ruoka.fi/reseptit/ruusukaalisalaatti-ja-paistettu-kirjolohi",
      "UrlSlug": "ruusukaalisalaatti-ja-paistettu-kirjolohi",
      "Sort": [1, 1, 10371]
    }, {
      "Id": "10370",
      "Name": "Maa-artisokkaohratto",
      "PieceSize": {
        "Unit": "g",
        "Amount": "280"
      },
      "Portions": {
        "Unit": "annosta",
        "Amount": "4"
      },
      "PreparationTime": {
        "Description": "yli 60 min",
        "TimeRange": {
          "MinTime": 60
        }
      },
      "UserPortions": {
        "$": {
          "Unit": "henkilölle"
        }
      },
      "Categories": [{
        "MainName": "Pääruoat",
        "MainId": "4",
        "SubName": "Kasvisruoat",
        "SubId": "22"
      }, {
        "MainName": "Pääraaka-aine",
        "MainId": "14",
        "SubName": "Kasvis",
        "SubId": "115"
      }, {
        "MainName": "Sesonki",
        "MainId": "1",
        "SubName": "Sadonkorjuu",
        "SubId": "124"
      }, {
        "MainName": "Teema",
        "MainId": "15",
        "SubName": "Mitä tänään syötäisiin?",
        "SubId": "144"
      }, {
        "MainName": "Asiakassegmentointi (K-asiakkaat)",
        "MainId": "17",
        "SubName": "Nautiskelijat",
        "SubId": "146"
      }, {
        "MainName": "Asiakassegmentointi (K-asiakkaat)",
        "MainId": "17",
        "SubName": "Innostujat",
        "SubId": "147"
      }, {
        "MainName": "Asiakassegmentointi (K-asiakkaat)",
        "MainId": "17",
        "SubName": "Tiedostavat",
        "SubId": "148"
      }],
      "Pictures": ["KR5_19_maa_artisok_ohr"],
      "PictureUrls": [{
        "Id": "KR5_19_maa_artisok_ohr",
        "Normal": "https://k-file-storage-qa.imgix.net/f/k-ruoka/recipe/10370?w=1000&h=1000&fit=clip",
        "Original": "https://k-file-storage-qa.imgix.net/f/k-ruoka/recipe/10370"
      }],
      "VideoUrl": "https://youtu.be/NbsDszIrvmw",
      "VideoEmbedUrl": "<iframe width=\"1136\" height=\"639\" src=\"https://www.youtube.com/embed/NbsDszIrvmw\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>",
      "Ingredients": [{
        "SubSectionHeading": "_",
        "SubSectionIngredients": [
          [{
            "Name": "maa-artisokkia",
            "Amount": "500",
            "PackageRecipe": "false",
            "IngredientType": "6017",
            "Unit": "g",
            "IngredientTypeName": "Maa-artisokka"
          }],
          [{
            "Name": "öljyä",
            "Amount": "1",
            "PackageRecipe": "false",
            "Unit": "rkl"
          }],
          [{
            "Name": "vettä",
            "Amount": "1",
            "PackageRecipe": "false",
            "IngredientType": "15792",
            "Unit": "l",
            "IngredientTypeName": "Vesi"
          }],
          [{
            "Name": "Knorr luomu kasvisliemikuutio",
            "Amount": "1",
            "PackageRecipe": "false",
            "IngredientType": "7417",
            "Unit": "",
            "IngredientTypeName": "Liemikuutio, kasvis, luomu"
          }],
          [{
            "Name": "Pirkka salottisipulia",
            "Amount": "3",
            "AmountInfo": "(120 g)",
            "PackageRecipe": "false",
            "IngredientType": "6851",
            "Unit": "",
            "IngredientTypeName": "Salottisipuli"
          }],
          [{
            "Name": "valkosipulinkynttä hienonnettuna",
            "Amount": "2",
            "PackageRecipe": "false",
            "IngredientType": "7262",
            "Unit": "",
            "IngredientTypeName": "Valkosipuli"
          }],
          [{
            "Name": "öljyä",
            "Amount": "2",
            "PackageRecipe": "false",
            "IngredientType": "6807",
            "Unit": "rkl",
            "IngredientTypeName": "Rypsiöljy"
          }],
          [{
            "Name": "Pirkka esikypsytettyjä rikottuja ohrasuurimoita",
            "Ean": "6410405112811",
            "Amount": "2 1/4",
            "PackageRecipe": "false",
            "IngredientType": "6365",
            "Unit": "dl",
            "IngredientTypeName": "Ohrasuurimo, rikottu"
          }],
          [{
            "Name": "Pirkka Parhaat parmesaania raastettuna",
            "Ean": "6410405102959",
            "Amount": "1 1/2",
            "AmountInfo": "(50 g)",
            "PackageRecipe": "false",
            "IngredientType": "6468",
            "Unit": "dl",
            "IngredientTypeName": "Parmesaani"
          }],
          [{
            "Name": "sitruunanmehua",
            "Amount": "2",
            "PackageRecipe": "false",
            "Unit": "rkl"
          }],
          [{
            "Name": "mustapippuria",
            "PackageRecipe": "false",
            "IngredientType": "6269",
            "Unit": "ripaus",
            "IngredientTypeName": "Mustapippuri, kokonainen"
          }],
          [{
            "Name": "suolaa",
            "Amount": "1/2",
            "PackageRecipe": "false",
            "IngredientType": "7049",
            "Unit": "tl",
            "IngredientTypeName": "Suola"
          }],
          [{
            "Name": "voita",
            "Amount": "2",
            "PackageRecipe": "false",
            "IngredientType": "7343",
            "Unit": "rkl",
            "IngredientTypeName": "Voi"
          }],
          [{
            "Name": "tuoretta timjamia",
            "PackageRecipe": "false"
          }],
          [{
            "Name": "suolaa",
            "PackageRecipe": "false",
            "IngredientType": "7049",
            "Unit": "ripaus",
            "IngredientTypeName": "Suola"
          }]
        ]
      }],
      "Instructions": "# Pese maa-artisokat ja paloittele ne. Jätä yksi maa-artisokka koristeeksi. Paahda maa-artisokkapaloja öljyssä kattilassa, kunnes ne saavat kunnolla väriä.\r\n# Lisää kattilaan 1/2 litraa vettä ja kasvisliemikuutio. Kuumenna kiehuvaksi ja keitä maa-artisokkia kannen alla 15 minuuttia tai kunnes ne ovat kypsiä.\r\n# Ota 3 dl keitinlientä talteen ja soseuta maa-artisokat lopun keitinliemen kanssa sauvasekoittimella tasaiseksi soseeksi.\r\n# Kuori ja hienonna salottisipulit ja valkosipulinkynnet. Kuullota sipuleita öljyssä pannulla. Kuullota myös ohraa hetki.\r\n# Kiehauta loput 1/2 litraa vettä.  Lisää vettä ohran joukkoon noin 1 dl kerrallaan. Anna edellisen nesteen imeytyä ennen kuin lisäät seuraavan erän. Jatka kunnes kaikki neste on käytetty, myös maa-artisokkien keitinliemi, ja ohra on kypsää. Aikaa kuluu 30-35 minuuttia.\r\n# Lisää lopuksi maa-artisokkasose, juustoraaste sekä sitruunanmehu. Mausta pippurilla ja tarvittaessa suolalla. Ohratto saa jäädä löysäksi.\r\n# Leikkaa maa-artisokka ohuiksi viipaleiksi ja paista voissa timjaminoksien kanssa rapeiksi. Mausta suolalla. Koristele annokset maa-artisokkaviipaleilla ja tuoreella timjamilla.",
      "EndNote": "*Annoshinnat on laskettu K-citymarket-ketjun hintatason mukaan 8/2019. Tuoretuotteiden hinnat vaihtelevat sesongeittain.\r\nParmesaanin tilalla voit käyttää emmentalia.",
      "Description": "Maa-artisokkaohratto hurmaa juurevan täyteläisellä maullaan. Mikä parasta maa-artisokkia ei tarvitse kuoria ennen soseuttamista. Noin 2,00€/annos",
      "DateCreated": "2019-04-23T18:26:12",
      "DateModified": "2019-09-29T14:50:05",
      "TvDate": "2019-10-01T00:00:00",
      "Stamp": {
        "Name": "Testattu K-koekeittiössä",
        "Id": "1"
      },
      "RecipeUse": {
        "Name": "k-ruoka.fi",
        "Id": "1"
      },
      "resourceType": "recipe",
      "Url": "https://www.k-ruoka.fi/reseptit/maa-artisokkaohratto",
      "UrlSlug": "maa-artisokkaohratto",
      "Sort": [1, 1, 10370]
    }, {
      "Id": "10369",
      "Name": "Kurpitsafondue",
      "PieceSize": {
        "Unit": "g"
      },
      "Portions": {
        "Unit": "annosta",
        "Amount": "4"
      },
      "PreparationTime": {
        "Description": "yli 60 min",
        "TimeRange": {
          "MinTime": 60
        }
      },
      "UserPortions": {
        "$": {
          "Unit": "henkilölle"
        }
      },
      "Categories": [{
        "MainName": "Sesonki",
        "MainId": "1",
        "SubName": "Halloween",
        "SubId": "126"
      }, {
        "MainName": "Ruokalaji",
        "MainId": "16",
        "SubName": "Uuniruoat",
        "SubId": "141"
      }, {
        "MainName": "Asiakassegmentointi (K-asiakkaat)",
        "MainId": "17",
        "SubName": "Nautiskelijat",
        "SubId": "146"
      }, {
        "MainName": "Asiakassegmentointi (K-asiakkaat)",
        "MainId": "17",
        "SubName": "Innostujat",
        "SubId": "147"
      }, {
        "MainName": "Asiakassegmentointi (K-asiakkaat)",
        "MainId": "17",
        "SubName": "Mukavuudenhaluiset ",
        "SubId": "149"
      }, {
        "MainName": "Makuja maailmalta",
        "MainId": "2",
        "SubName": "Ranska",
        "SubId": "13"
      }, {
        "MainName": "Pääruoat",
        "MainId": "4",
        "SubName": "Uuniruoat",
        "SubId": "30"
      }],
      "Pictures": ["PI10_19_kurpitsafondue"],
      "PictureUrls": [{
        "Id": "PI10_19_kurpitsafondue",
        "Normal": "https://k-file-storage-qa.imgix.net/f/k-ruoka/recipe/10369?w=1000&h=1000&fit=clip",
        "Original": "https://k-file-storage-qa.imgix.net/f/k-ruoka/recipe/10369"
      }],
      "SpecialDiets": [{
        "Id": "4",
        "Name": "Gluteeniton"
      }, {
        "Id": "6",
        "Name": "Kasvis (lakto-ovo)"
      }],
      "Ingredients": [{
        "SubSectionHeading": "_",
        "SubSectionIngredients": [
          [{
            "Name": "pientä hokkaidokurpitsaa",
            "Amount": "4",
            "AmountInfo": "(à n. 400 g)",
            "PackageRecipe": "false",
            "IngredientType": "16477",
            "Unit": "",
            "IngredientTypeName": "Hokkaidokurpitsa"
          }, {
            "Name": "talvikurpitsa",
            "Amount": "1",
            "AmountInfo": "(n. 2 kg)",
            "PackageRecipe": "false",
            "Unit": ""
          }],
          [{
            "Name": "fondueaineksia (esim. President tai Emmi)",
            "Amount": "1",
            "AmountInfo": "(400 g)",
            "PackageRecipe": "false",
            "Unit": "pkt"
          }],
          [{
            "Name": "comtéjuustoa",
            "Amount": "350",
            "PackageRecipe": "false",
            "Unit": "g"
          }, {
            "Name": "gruyèrea",
            "PackageRecipe": "false",
            "IngredientType": "5487",
            "IngredientTypeName": "Gruyere"
          }],
          [{
            "Name": "(alkoholitonta) kuivaa valkoviiniä",
            "Amount": "2",
            "PackageRecipe": "false",
            "IngredientType": "15272",
            "Unit": "dl",
            "IngredientTypeName": "Valkoviini, alkoholiton"
          }],
          [{
            "Name": "salviaa",
            "Amount": "1/2",
            "PackageRecipe": "false",
            "IngredientType": "6859",
            "Unit": "ruukku",
            "IngredientTypeName": "Salvia, tuore"
          }]
        ]
      }, {
        "SubSectionHeading": "Lisäksi",
        "SubSectionIngredients": [
          [{
            "Name": "Pirkka Parhaat maalaispatonkia",
            "Ean": "6410405171436",
            "Amount": "1",
            "PackageRecipe": "false",
            "IngredientType": "6493",
            "Unit": "",
            "IngredientTypeName": "Patonki, maalais"
          }]
        ]
      }],
      "Instructions": "# Huuhtele kurpitsat ja leikkaa niistä hattu pois. Koverra lusikalla siemenet pois. Tasoita tarvittaessa veitsellä kurpitsan kanta, jotta se pysyy pystyasennossa. Nosta kurpitsat hattuineen uunipellille leivinpaperin päälle.\r\n# Paista hokkaidokurpitsoja 175-asteisessa uunissa 30-35 minuuttia. Kurpitsan liha saa hieman jo pehmentyä. Isommalle kurpitsalle paistoaika on noin 50 minuuttia.\r\n# Paloittele fondueainekset ja raasta juusto. Annostele kurpitsoille kerroksittain fondueaineksia, juustoa, viiniä ja salvian lehtiä.\r\n# Nosta kurpitsat takaisin uuniin ilman hattuja ja paista 200 asteessa noin 20 minuuttia, kunnes juusto on sulanut ja kuumaa. (Paista isoa kurpitsaa noin 35 minuuttia).\r\n# Tarjoa kurpitsafondue patongin tai rapeakuorisen leivän kanssa ja kaavi haarukalla mukaan kypsynyttä kurpitsaa.",
      "EndNote": "Voit myös maustaa fonduen muskottipähkinällä tai ripauksella jauhettua maustepippuria. *Annoshinnat on laskettu K-citymarket-ketjun hintatason mukaan 10/2019. Tuoretuotteiden hinnat vaihtelevat sesongeittain.",
      "Description": "Syksyn paras lohturuoka syntyy kokonaisista kurpitsoista ja valmiista fondueaineksista. Helppo, mutta juhlava ruoka sopii vaikka illanistujaisiin tarjottavaksi. Noin 6,30 €/annos*",
      "DateCreated": "2019-04-23T14:49:06",
      "DateModified": "2019-09-23T12:28:06",
      "Stamp": {
        "Name": "Testattu K-koekeittiössä",
        "Id": "1"
      },
      "RecipeUse": {
        "Name": "k-ruoka.fi",
        "Id": "1"
      },
      "resourceType": "recipe",
      "Url": "https://www.k-ruoka.fi/reseptit/kurpitsafondue",
      "UrlSlug": "kurpitsafondue",
      "Sort": [1, 1, 10369]
    }, {
      "Id": "10368",
      "Name": "Carne con chili eli meksikolainen lihapata",
      "PieceSize": {
        "Unit": "g",
        "Amount": "395"
      },
      "Portions": {
        "Unit": "annosta",
        "Amount": "5"
      },
      "PreparationTime": {
        "Description": "yli 60 min",
        "TimeRange": {
          "MinTime": 60
        }
      },
      "UserPortions": {
        "$": {
          "Unit": "henkilölle"
        }
      },
      "Categories": [{
        "MainName": "Makuja maailmalta",
        "MainId": "2",
        "SubName": "Meksiko ja texmex",
        "SubId": "12"
      }, {
        "MainName": "Pääruoat",
        "MainId": "4",
        "SubName": "Pataruoat",
        "SubId": "29"
      }, {
        "MainName": "Pääraaka-aine",
        "MainId": "14",
        "SubName": "Liha",
        "SubId": "114"
      }, {
        "MainName": "Ruokalaji",
        "MainId": "16",
        "SubName": "Pataruoat",
        "SubId": "142"
      }, {
        "MainName": "Teema",
        "MainId": "15",
        "SubName": "Mitä tänään syötäisiin?",
        "SubId": "144"
      }, {
        "MainName": "Asiakassegmentointi (K-asiakkaat)",
        "MainId": "17",
        "SubName": "Nautiskelijat",
        "SubId": "146"
      }, {
        "MainName": "Asiakassegmentointi (K-asiakkaat)",
        "MainId": "17",
        "SubName": "Tiedostavat",
        "SubId": "148"
      }],
      "Pictures": ["PI10_19_carne_con_chili"],
      "PictureUrls": [{
        "Id": "PI10_19_carne_con_chili",
        "Normal": "https://k-file-storage-qa.imgix.net/f/k-ruoka/recipe/10368?w=1000&h=1000&fit=clip",
        "Original": "https://k-file-storage-qa.imgix.net/f/k-ruoka/recipe/10368"
      }],
      "SpecialDiets": [{
        "Id": "2",
        "Name": "Laktoositon"
      }, {
        "Id": "3",
        "Name": "Maidoton"
      }, {
        "Id": "4",
        "Name": "Gluteeniton"
      }, {
        "Id": "7",
        "Name": "Kananmunaton"
      }],
      "Ingredients": [{
        "SubSectionHeading": "_",
        "SubSectionIngredients": [
          [{
            "Name": "porsaan lapaa",
            "Amount": "1,4",
            "PackageRecipe": "false",
            "IngredientType": "6610",
            "Unit": "kg",
            "IngredientTypeName": "Porsaan lapa"
          }],
          [{
            "Name": "Pirkka luumutomaatteja",
            "Ean": "6408641034011",
            "Amount": "1",
            "AmountInfo": "(500 g)",
            "PackageRecipe": "false",
            "IngredientType": "7124",
            "Unit": "rs",
            "IngredientTypeName": "Tomaatti"
          }],
          [{
            "Name": "valkosipulinkynttä kuorineen",
            "Amount": "5",
            "PackageRecipe": "false",
            "IngredientType": "7262",
            "Unit": "",
            "IngredientTypeName": "Valkosipuli"
          }],
          [{
            "Name": "Pirkka meksikolaista salsa chilpotle -kastiketta",
            "Ean": "6410405150257",
            "Amount": "1",
            "AmountInfo": "(230 g)",
            "PackageRecipe": "false",
            "IngredientType": "6857",
            "Unit": "tlk",
            "IngredientTypeName": "Salsakastike, chipotle"
          }],
          [{
            "Name": "vettä",
            "Amount": "1/2",
            "PackageRecipe": "false",
            "IngredientType": "15792",
            "Unit": "dl",
            "IngredientTypeName": "Vesi"
          }],
          [{
            "Name": "suolaa",
            "Amount": "1",
            "PackageRecipe": "false",
            "IngredientType": "7049",
            "Unit": "tl",
            "IngredientTypeName": "Suola"
          }],
          [{
            "Name": "sipulia",
            "Amount": "2",
            "AmountInfo": "(200 g)",
            "PackageRecipe": "false",
            "IngredientType": "6932",
            "Unit": "",
            "IngredientTypeName": "Sipuli"
          }],
          [{
            "Name": "Pirkka oreganoa",
            "Ean": "6410405055767",
            "Amount": "2",
            "PackageRecipe": "false",
            "IngredientType": "6409",
            "Unit": "tl",
            "IngredientTypeName": "Oregano, kuivattu"
          }],
          [{
            "Name": "juustokuminaa",
            "Amount": "2",
            "PackageRecipe": "false",
            "IngredientType": "5593",
            "Unit": "tl",
            "IngredientTypeName": "Juustokumina"
          }],
          [{
            "Name": "jauhettua maustepippuria",
            "Amount": "1",
            "PackageRecipe": "false",
            "IngredientType": "6174",
            "Unit": "tl",
            "IngredientTypeName": "Maustepippuri, jauhettu"
          }],
          [{
            "Name": "sokeria",
            "Amount": "1",
            "PackageRecipe": "false",
            "IngredientType": "6961",
            "Unit": "tl",
            "IngredientTypeName": "Sokeri"
          }],
          [{
            "Name": "laakerinlehti",
            "Amount": "1",
            "PackageRecipe": "false",
            "IngredientType": "5918",
            "Unit": "",
            "IngredientTypeName": "Laakerinlehti"
          }]
        ]
      }, {
        "SubSectionHeading": "Paistamiseen",
        "SubSectionIngredients": [
          [{
            "Name": "voita tai rypsiöljyä",
            "PackageRecipe": "false",
            "IngredientType": "7343",
            "IngredientTypeName": "Voi"
          }]
        ]
      }, {
        "SubSectionHeading": "Vihreä riisi",
        "SubSectionIngredients": [
          [{
            "Name": "korianteria",
            "Amount": "1 1/2",
            "PackageRecipe": "false",
            "IngredientType": "5852",
            "Unit": "ruukkua",
            "IngredientTypeName": "Korianteri, tuore"
          }],
          [{
            "Name": "Pirkka kevätsipulia",
            "Ean": "6410405093080",
            "Amount": "1/2",
            "AmountInfo": "(à 100 g)",
            "PackageRecipe": "false",
            "IngredientType": "5792",
            "Unit": "ps",
            "IngredientTypeName": "Kevätsipuli"
          }],
          [{
            "Name": "valkosipulinkynsi",
            "Amount": "1",
            "PackageRecipe": "false",
            "IngredientType": "7262",
            "Unit": "",
            "IngredientTypeName": "Valkosipuli"
          }],
          [{
            "Name": "kuumaa vettä",
            "Amount": "4 1/2",
            "PackageRecipe": "false",
            "Unit": "dl"
          }],
          [{
            "Name": "rypsiöljyä",
            "Amount": "1",
            "PackageRecipe": "false",
            "IngredientType": "6807",
            "Unit": "rkl",
            "IngredientTypeName": "Rypsiöljy"
          }],
          [{
            "Name": "Pirkka basmatiriisiä",
            "Ean": "6410402014385",
            "Amount": "3",
            "PackageRecipe": "false",
            "IngredientType": "5272",
            "Unit": "dl",
            "IngredientTypeName": "Basmatiriisi"
          }],
          [{
            "Name": "suolaa",
            "Amount": "1/2",
            "PackageRecipe": "false",
            "IngredientType": "7049",
            "Unit": "tl",
            "IngredientTypeName": "Suola"
          }]
        ]
      }, {
        "SubSectionHeading": "Koristeluun",
        "SubSectionIngredients": [
          [{
            "Name": "korianteria hienonnettuna",
            "Amount": "1/2",
            "PackageRecipe": "false",
            "IngredientType": "5852",
            "Unit": "ruukkua",
            "IngredientTypeName": "Korianteri, tuore"
          }],
          [{
            "Name": "Pirkka mietoa punaista chiliä viipaloituna",
            "Ean": "6410405091857",
            "Amount": "2",
            "PackageRecipe": "false",
            "IngredientType": "5355",
            "Unit": "",
            "IngredientTypeName": "Chili, punainen"
          }]
        ]
      }],
      "Instructions": "# Ota liha huoneenlämpöön noin tunti ennen valmistusta.\r\n# Paahda tomaatteja ja valkosipulinkynsiä valurautapannulla, kunnes ne ovat tummuneet. Anna jäähtyä hetki. Kuori valkosipulit ja lohko tomaatit kuorineen. Siirrä ne monitoimikoneen kulhoon ja lisää chilpotle-kastike. Mittaa joukkoon vesi ja suola. Aja sileäksi soseeksi.\r\n# Hienonna sipuli. Leikkaa liha kuutioiksi.\r\n# Kuumenna rasva padassa ja ruskista siinä lihat muutamassa erässä. Nosta lihat lautaselle odottamaan.\r\n# Lisää rasvaa tarvittaessa ja kuullota padassa sipulia muutama minuutti. Lisää sitten mausteet ja jatka paistamista hetki.\r\n# Kaada tomaattiseos pataan ja lisää lihat. Kuumenna kiehuvaksi, alenna sitten lämpö ja peitä kannella. Anna hautua hiljalleen 1 1/2-2 tuntia, kunnes liha on pehmennyt.\r\n# Valmista sillä aikaa vihreä riisi. Paloittele monitoimikoneen kulhoon korianteri varsineen, kevätsipulit ja valkosipulinkynsi. Aja tasaiseksi ja lisää 2 dl kuumaa vettä. Pyöräytä sileäksi seokseksi.\r\n# Kuumenna öljy pannulla ja lisää riisi. Paista sekoittaen minuutin verran ja lisää korianteriseos, suola sekä loput vedestä (2 1/2 dl). Kuumenna kiehuvaksi, alenna lämpö ja peitä kannella. Hauduta kypsäksi 8-9 minuuttia.\r\n# Viimeistele pata korianterilla ja chilillä. Tarjoa lihapata vihreän riisin kanssa.",
      "EndNote": "Tarjoa loput carne con chilestä seuraavana päivänä tortillojen, ranskankerman ja mustien papujen kanssa. *Annoshinnat on laskettu K-citymarket-ketjun hintatason mukaan 10/2019. Tuoretuotteiden hinnat vaihtelevat sesongeittain.",
      "Description": "Carne con chili on perinteinen meksikolainen ruoka, joka valmistetaan kokolihasta hauduttaen. Lisäkkeeksi tarjotaan korianterilla maustettua riisiä.\r\nNoin 3,30 €/annos*",
      "DateCreated": "2019-04-23T14:46:49",
      "DateModified": "2019-10-07T12:27:16",
      "TvDate": "2019-10-25T00:00:00",
      "Stamp": {
        "Name": "Testattu K-koekeittiössä",
        "Id": "1"
      },
      "RecipeUse": {
        "Name": "k-ruoka.fi",
        "Id": "1"
      },
      "resourceType": "recipe",
      "Url": "https://www.k-ruoka.fi/reseptit/carne-con-chili-eli-meksikolainen-lihapata",
      "UrlSlug": "carne-con-chili-eli-meksikolainen-lihapata",
      "Sort": [1, 1, 10368]
    }, {
      "Id": "10367",
      "Name": "Porkkanafritterit",
      "PieceSize": {
        "Unit": "g",
        "Amount": "280"
      },
      "Portions": {
        "Unit": "annosta",
        "Amount": "4"
      },
      "PreparationTime": {
        "Description": "alle 30 min",
        "TimeRange": {
          "MinTime": 15,
          "MaxTime": 30
        }
      },
      "UserPortions": {
        "$": {
          "Unit": "henkilölle"
        }
      },
      "Categories": [{
        "MainName": "Pääruoat",
        "MainId": "4",
        "SubName": "Kasvisruoat",
        "SubId": "22"
      }, {
        "MainName": "Pääraaka-aine",
        "MainId": "14",
        "SubName": "Kasvis",
        "SubId": "115"
      }, {
        "MainName": "Sesonki",
        "MainId": "1",
        "SubName": "Sadonkorjuu",
        "SubId": "124"
      }, {
        "MainName": "Teema",
        "MainId": "15",
        "SubName": "Brunssi",
        "SubId": "131"
      }, {
        "MainName": "Ruokalaji",
        "MainId": "16",
        "SubName": "Pihvit",
        "SubId": "143"
      }, {
        "MainName": "Teema",
        "MainId": "15",
        "SubName": "Mitä tänään syötäisiin?",
        "SubId": "144"
      }, {
        "MainName": "Asiakassegmentointi (K-asiakkaat)",
        "MainId": "17",
        "SubName": "Innostujat",
        "SubId": "147"
      }, {
        "MainName": "Asiakassegmentointi (K-asiakkaat)",
        "MainId": "17",
        "SubName": "Tiedostavat",
        "SubId": "148"
      }],
      "Pictures": ["PI10_19_porkkanafritterit"],
      "PictureUrls": [{
        "Id": "PI10_19_porkkanafritterit",
        "Normal": "https://k-file-storage-qa.imgix.net/f/k-ruoka/recipe/10367?w=1000&h=1000&fit=clip",
        "Original": "https://k-file-storage-qa.imgix.net/f/k-ruoka/recipe/10367"
      }],
      "VideoUrl": "https://youtu.be/bR72kztH33A",
      "VideoEmbedUrl": "<iframe width=\"1136\" height=\"639\" src=\"https://www.youtube.com/embed/bR72kztH33A\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>",
      "SpecialDiets": [{
        "Id": "6",
        "Name": "Kasvis (lakto-ovo)"
      }],
      "Ingredients": [{
        "SubSectionHeading": "Porkkanafritterit",
        "SubSectionIngredients": [
          [{
            "Name": "porkkanaa",
            "Amount": "4-5",
            "AmountInfo": "(n.400 g)",
            "PackageRecipe": "false",
            "IngredientType": "6582",
            "Unit": "",
            "IngredientTypeName": "Porkkana"
          }],
          [{
            "Name": "iso sipuli",
            "Amount": "1",
            "AmountInfo": "(140 g)",
            "PackageRecipe": "false",
            "IngredientType": "6932",
            "Unit": "",
            "IngredientTypeName": "Sipuli"
          }],
          [{
            "Name": "valkosipulinkynttä",
            "Amount": "2",
            "PackageRecipe": "false",
            "IngredientType": "7262",
            "Unit": "",
            "IngredientTypeName": "Valkosipuli"
          }],
          [{
            "Name": "Pirkka Parhaat fetajuustokuutiota",
            "Ean": "6410405214898",
            "Amount": "1",
            "AmountInfo": "(370 g/150 g)",
            "PackageRecipe": "false",
            "IngredientType": "16154",
            "Unit": "prk",
            "IngredientTypeName": "Fetajuustokuutio"
          }],
          [{
            "Name": "Pirkka Luomu kananmuna",
            "Ean": "6408641114904",
            "Amount": "2",
            "PackageRecipe": "false",
            "IngredientType": "5685",
            "Unit": "",
            "IngredientTypeName": "Kananmuna, luomu"
          }],
          [{
            "Name": "vehnäjauhoja",
            "Amount": "3/4",
            "PackageRecipe": "false",
            "IngredientType": "7293",
            "Unit": "dl",
            "IngredientTypeName": "Vehnäjauho"
          }],
          [{
            "Name": "basilikaa hienonnettuna",
            "Amount": "1/2",
            "AmountInfo": "ruukku",
            "PackageRecipe": "false",
            "IngredientType": "5271",
            "Unit": "",
            "IngredientTypeName": "Basilika, tuore"
          }],
          [{
            "Name": "suolaa",
            "Amount": "1/2",
            "PackageRecipe": "false",
            "IngredientType": "7049",
            "Unit": "tl",
            "IngredientTypeName": "Suola"
          }],
          [{
            "Name": "mustapippuria",
            "Amount": "1/2",
            "PackageRecipe": "false",
            "IngredientType": "6167",
            "Unit": "tl",
            "IngredientTypeName": "Maustemylly, mustapippuri"
          }]
        ]
      }, {
        "SubSectionHeading": "Jogurttikastike",
        "SubSectionIngredients": [
          [{
            "Name": "Arla Lempi turkkilaista jogurttia",
            "Ean": "6413300019117",
            "Amount": "1",
            "AmountInfo": "(300 g)",
            "PackageRecipe": "false",
            "IngredientType": "16103",
            "Unit": "tlk",
            "IngredientTypeName": "Arla Lempi turkkilainen jogurtti"
          }],
          [{
            "Name": "Pirkka arrabbiatapestoa",
            "Ean": "6410405093134",
            "Amount": "2",
            "PackageRecipe": "false",
            "IngredientType": "6537",
            "Unit": "rkl",
            "IngredientTypeName": "Pesto, punainen, arrabbiata"
          }]
        ]
      }, {
        "SubSectionHeading": "Paistamiseen",
        "SubSectionIngredients": [
          [{
            "Name": "Pirkka rypsiöljyä",
            "Ean": "6410405191434",
            "PackageRecipe": "false",
            "IngredientType": "6807",
            "IngredientTypeName": "Rypsiöljy"
          }]
        ]
      }],
      "Instructions": "# Pese, kuori ja raasta porkkanat. Purista porkkanoista enin vesi pois. Hienonna sipuli ja valkosipulinkynnet.\r\n# Murenna kasvisten joukkoon valutetut fetajuustokuutiot. Vatkaa munien rakenne rikki, lisää ne kulhoon yhdessä jauhojen, basilikan ja mausteiden kanssa. Sekoita hyvin.\r\n# Mittaa pannulle n. 1/2 dl öljyä ja kuumenna keskilämmöllä. Nosta taikinaa kahden lusikan avulla pannulle keoiksi, tasoita pinta.\r\n# Paista fritterit erissä molemmin puolin 3-4 minuuttia. Lisää öljyä paistokertojen välissä. Nosta fritterit talouspaperin päälle hetkeksi.\r\n# Sekoita kastikkeen ainekset keskenään ja tarjoa frittereiden kanssa.",
      "EndNote": "*Annoshinnat on laskettu K-citymarket-ketjun hintatason mukaan 10/2019. Tuoretuotteiden hinnat vaihtelevat sesongeittain.",
      "Description": "Porkkanafritterit valmistetaan pannulla öljyssä rapeiksi ja nautitaan arrabiata-pestolla maustetulla kastikkeella. Helppo arkiruoka syntyy nopeasti. Noin 1,90 €/annos*",
      "DateCreated": "2019-04-23T14:42:32",
      "DateModified": "2019-11-03T20:09:26",
      "TvDate": "2019-10-30T00:00:00",
      "Stamp": {
        "Name": "Testattu K-koekeittiössä",
        "Id": "1"
      },
      "RecipeUse": {
        "Name": "k-ruoka.fi",
        "Id": "1"
      },
      "resourceType": "recipe",
      "Url": "https://www.k-ruoka.fi/reseptit/porkkanafritterit",
      "UrlSlug": "porkkanafritterit",
      "Sort": [1, 1, 10367]
    }, {
      "Id": "10364",
      "Name": "Hirviburgeri",
      "PieceSize": {
        "Unit": "g",
        "Amount": "225"
      },
      "Portions": {
        "Unit": "annosta",
        "Amount": "4"
      },
      "PreparationTime": {
        "Description": "alle 30 min",
        "TimeRange": {
          "MinTime": 15,
          "MaxTime": 30
        }
      },
      "UserPortions": {
        "$": {
          "Unit": "henkilölle"
        }
      },
      "Categories": [{
        "MainName": "Pääruoat",
        "MainId": "4",
        "SubName": "Hampurilaiset ja täytetyt leivät",
        "SubId": "86"
      }, {
        "MainName": "Makuja maailmalta",
        "MainId": "2",
        "SubName": "Suomi",
        "SubId": "106"
      }, {
        "MainName": "Pääraaka-aine",
        "MainId": "14",
        "SubName": "Liha",
        "SubId": "114"
      }, {
        "MainName": "Ruokalaji",
        "MainId": "16",
        "SubName": "Hampurilaiset ja täytetyt leivät",
        "SubId": "140"
      }, {
        "MainName": "Teema",
        "MainId": "15",
        "SubName": "Mitä tänään syötäisiin?",
        "SubId": "144"
      }, {
        "MainName": "Asiakassegmentointi (K-asiakkaat)",
        "MainId": "17",
        "SubName": "Nautiskelijat",
        "SubId": "146"
      }, {
        "MainName": "Asiakassegmentointi (K-asiakkaat)",
        "MainId": "17",
        "SubName": "Innostujat",
        "SubId": "147"
      }, {
        "MainName": "Asiakassegmentointi (K-asiakkaat)",
        "MainId": "17",
        "SubName": "Tiedostavat",
        "SubId": "148"
      }],
      "Pictures": ["KR5_19_hirviburgeri"],
      "PictureUrls": [{
        "Id": "KR5_19_hirviburgeri",
        "Normal": "https://k-file-storage-qa.imgix.net/f/k-ruoka/recipe/10364?w=1000&h=1000&fit=clip",
        "Original": "https://k-file-storage-qa.imgix.net/f/k-ruoka/recipe/10364"
      }],
      "VideoUrl": "https://youtu.be/MlfXcwA1zDs",
      "VideoEmbedUrl": "<iframe width=\"1136\" height=\"639\" src=\"https://www.youtube.com/embed/MlfXcwA1zDs\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>",
      "Ingredients": [{
        "SubSectionHeading": "Hirvipihvit",
        "SubSectionIngredients": [
          [{
            "Name": "sipuli",
            "Amount": "1",
            "AmountInfo": "(50 g)",
            "PackageRecipe": "false",
            "IngredientType": "6932",
            "Unit": "",
            "IngredientTypeName": "Sipuli"
          }],
          [{
            "Name": "hirvenlihaa jauhettuna",
            "Amount": "500",
            "PackageRecipe": "false",
            "IngredientType": "8386",
            "Unit": "g",
            "IngredientTypeName": "Hirven jauheliha"
          }],
          [{
            "Name": "Maille Dijon sinappia",
            "Amount": "2",
            "PackageRecipe": "false",
            "IngredientType": "5393",
            "Unit": "tl",
            "IngredientTypeName": "Dijonsinappi"
          }],
          [{
            "Name": "suolaa",
            "Amount": "1",
            "PackageRecipe": "false",
            "IngredientType": "7049",
            "Unit": "tl",
            "IngredientTypeName": "Suola"
          }],
          [{
            "Name": "mustapippuria",
            "Amount": "1/2",
            "PackageRecipe": "false",
            "IngredientType": "6167",
            "Unit": "tl",
            "IngredientTypeName": "Maustemylly, mustapippuri"
          }]
        ]
      }, {
        "SubSectionHeading": "Puolukkamajoneesi",
        "SubSectionIngredients": [
          [{
            "Name": "Hellmann's Luomu majoneesia",
            "Amount": "1",
            "PackageRecipe": "false",
            "IngredientType": "6058",
            "Unit": "dl",
            "IngredientTypeName": "Majoneesi"
          }],
          [{
            "Name": "Pirkka puolukoita",
            "Ean": "6410405086754",
            "Amount": "1/2",
            "PackageRecipe": "false",
            "IngredientType": "6671",
            "Unit": "dl",
            "IngredientTypeName": "Puolukka, pakaste"
          }]
        ]
      }, {
        "SubSectionHeading": "Lisäksi",
        "SubSectionIngredients": [
          [{
            "Name": "Fazer Street Food briossi hampurilaissämpylöitä",
            "Amount": "1",
            "AmountInfo": "(4 kpl/240 g)",
            "PackageRecipe": "false",
            "IngredientType": "7066",
            "Unit": "ps",
            "IngredientTypeName": "Sämpylä"
          }],
          [{
            "Name": "suolakurkkua",
            "Amount": "2",
            "PackageRecipe": "false",
            "IngredientType": "7053",
            "Unit": "",
            "IngredientTypeName": "Suolakurkku"
          }]
        ]
      }],
      "Instructions": "# Hienonna kuorittu sipuli.\r\n# Sekoita jauheliha, sipuli, sinappi ja mausteet kulhossa tasaiseksi massaksi.\r\n# Muotoile massasta 4 pihviä. Paina pihvin keskelle kuoppa, jotta pihvi pysyy tasaisena paistettaessa.\r\n# Grillaa tai paista pihvit kuumalla pannulla noin 8 minuuttia välillä kääntäen.\r\n# Murskaa sulatetut puolukat kevyesti haarukalla kulhossa. Sekoita joukkoon majoneesi.\r\n# Leikkaa suolakurkku pitkittäin neljään osaan.\r\n# Kokoa hampurilaiset sämpylöistä, puolukkamajoneesista, hirvipihvistä ja suolakurkuista.",
      "EndNote": "Vinkki! Korvaa hirvenliha halutessasi naudan jauhelihalla. *Annoshinnat on laskettu K-citymarket-ketjun hintatason mukaan 8/2019. Tuoretuotteiden hinnat vaihtelevat sesongeittain.",
      "Description": "Maistuisiko ronski burgeri? Tässä hirvihampurilaisessa maistuvat mehevä pihvi, kirpsakka puolukkamajoneesi ja suolakurkut. Noin 3,25€/annos",
      "DateCreated": "2019-04-18T14:11:03",
      "DateModified": "2019-10-15T20:14:15",
      "TvDate": "2019-10-17T00:00:00",
      "Stamp": {
        "Name": "Testattu K-koekeittiössä",
        "Id": "1"
      },
      "RecipeUse": {
        "Name": "k-ruoka.fi",
        "Id": "1"
      },
      "resourceType": "recipe",
      "Url": "https://www.k-ruoka.fi/reseptit/hirviburgeri",
      "UrlSlug": "hirviburgeri",
      "Sort": [1, 1, 10364]
    }, {
      "Id": "10363",
      "Name": "Karpalo-kurpitsasalaatti",
      "PieceSize": {
        "Unit": "g"
      },
      "Portions": {
        "Unit": "annosta",
        "Amount": "4"
      },
      "PreparationTime": {
        "Description": "30 - 60 min",
        "TimeRange": {
          "MinTime": 30,
          "MaxTime": 60
        }
      },
      "UserPortions": {
        "$": {
          "Unit": "henkilölle"
        }
      },
      "Categories": [{
        "MainName": "Pääruoat",
        "MainId": "4",
        "SubName": "Salaatit",
        "SubId": "28"
      }, {
        "MainName": "Pääraaka-aine",
        "MainId": "14",
        "SubName": "Kasvis",
        "SubId": "115"
      }, {
        "MainName": "Sesonki",
        "MainId": "1",
        "SubName": "Sadonkorjuu",
        "SubId": "124"
      }, {
        "MainName": "Teema",
        "MainId": "15",
        "SubName": "Mitä tänään syötäisiin?",
        "SubId": "144"
      }],
      "Pictures": ["KR5_19_karpalo_kurpits"],
      "PictureUrls": [{
        "Id": "KR5_19_karpalo_kurpits",
        "Normal": "https://k-file-storage-qa.imgix.net/f/k-ruoka/recipe/10363?w=1000&h=1000&fit=clip",
        "Original": "https://k-file-storage-qa.imgix.net/f/k-ruoka/recipe/10363"
      }],
      "SpecialDiets": [{
        "Id": "6",
        "Name": "Kasvis (lakto-ovo)"
      }],
      "Ingredients": [{
        "SubSectionHeading": "_",
        "SubSectionIngredients": [
          [{
            "Name": "myskikurpitsaa",
            "Amount": "1/2",
            "AmountInfo": "(n. 700 g)",
            "PackageRecipe": "false",
            "IngredientType": "6279",
            "Unit": "",
            "IngredientTypeName": "Myskikurpitsa"
          }],
          [{
            "Name": "Pirkka ekstra-neitsytoliiviöljyä",
            "Ean": "6410402024759",
            "Amount": "2",
            "PackageRecipe": "false",
            "IngredientType": "5405",
            "Unit": "rkl",
            "IngredientTypeName": "Ekstra neitsytoliiviöljy"
          }],
          [{
            "Name": "tuoretta rosmariinia",
            "Amount": "1",
            "PackageRecipe": "false",
            "IngredientType": "6751",
            "Unit": "rkl",
            "IngredientTypeName": "Rosmariini, tuore"
          }],
          [{
            "Name": "suolaa",
            "Amount": "3/4",
            "PackageRecipe": "false",
            "IngredientType": "7049",
            "Unit": "tl",
            "IngredientTypeName": "Suola"
          }],
          [{
            "Name": "lehtikaalia",
            "Amount": "1",
            "AmountInfo": "(200 g)",
            "PackageRecipe": "false",
            "IngredientType": "5952",
            "Unit": "ps",
            "IngredientTypeName": "Lehtikaali"
          }],
          [{
            "Name": "Pirkka Parhaat vuohenjuustoa",
            "Ean": "6410405082602",
            "Amount": "1",
            "AmountInfo": "(200 g)",
            "PackageRecipe": "false",
            "IngredientType": "7362",
            "Unit": "pkt",
            "IngredientTypeName": "Vuohenjuusto"
          }],
          [{
            "Name": "Pirkka pehmeitä karpaloita",
            "Ean": "6410405106179",
            "Amount": "1",
            "PackageRecipe": "false",
            "IngredientType": "5719",
            "Unit": "dl",
            "IngredientTypeName": "Karpalo, kuivattu"
          }]
        ]
      }, {
        "SubSectionHeading": "Karpalokastike",
        "SubSectionIngredients": [
          [{
            "Name": "karpaloita (tuore tai pakaste)",
            "Amount": "1",
            "PackageRecipe": "false",
            "IngredientType": "5720",
            "Unit": "dl",
            "IngredientTypeName": "Karpalo, pakaste"
          }],
          [{
            "Name": "Pirkka ekstra-neitsytoliiviöljyä",
            "Ean": "6410402024759",
            "Amount": "1/2",
            "PackageRecipe": "false",
            "IngredientType": "5405",
            "Unit": "dl",
            "IngredientTypeName": "Ekstra neitsytoliiviöljy"
          }],
          [{
            "Name": "omenaviinietikkaa",
            "Amount": "1/2",
            "PackageRecipe": "false",
            "IngredientType": "6408",
            "Unit": "dl",
            "IngredientTypeName": "Omenaviinietikka"
          }],
          [{
            "Name": "sokeria",
            "Amount": "2",
            "PackageRecipe": "false",
            "IngredientType": "6961",
            "Unit": "rkl",
            "IngredientTypeName": "Sokeri"
          }],
          [{
            "Name": "Pirkka Dijon-sinappia",
            "Ean": "6410405187369",
            "Amount": "1",
            "PackageRecipe": "false",
            "IngredientType": "5393",
            "Unit": "tl",
            "IngredientTypeName": "Dijonsinappi"
          }],
          [{
            "Name": "suolaa",
            "PackageRecipe": "false",
            "IngredientType": "7049",
            "Unit": "ripaus",
            "IngredientTypeName": "Suola"
          }],
          [{
            "Name": "mustapippuria",
            "PackageRecipe": "false",
            "IngredientType": "6167",
            "Unit": "ripaus",
            "IngredientTypeName": "Maustemylly, mustapippuri"
          }]
        ]
      }],
      "Instructions": "# Paloittele kuorittu myskikurpitsa muutaman sentin kokoisiksi kuutioiksi. Levitä kurpitsa leivinpaperin päälle uunipellille ja valuta päälle öljy. Kääntele sekaisin, saksi päälle rosmariinia ja ripottele pinnalle suolaa.\r\n# Paahda kurpitsaa 200-asteisen uunin keskitasolla noin 30 minuuttia, kunnes se pehmenee.\r\n# Huuhtele ja kuivaa lehtikaali, leikkaa pois kova lehtiruoti ja revi lehdet suupaloiksi.\r\n# Mittaa karpalot suureen kulhoon ja murskaa niitä haarukalla. Lisää loput kastikkeen ainekset ja sekoita. Lisää lehtikaalit ja hiero kastiketta niihin noin minuutin ajan, kunnes kaali pehmenee.\r\n# Leikkaa vuohenjuusto kiekoiksi. Paista kiekkoihin kuivalla ja kuumalla pannulla kaunis pinta.\r\n# Levitä tarjoiluvadille marinoitu lehtikaali, kurpitsat ja vuohenjuusto. Viimeistele salaatti kuivatuilla karpaloilla.",
      "EndNote": "*Annoshinnat on laskettu K-citymarket-ketjun hintatason mukaan 8/2019. Tuoretuotteiden hinnat vaihtelevat sesongeittain.",
      "Description": "Kirpsakka karpalokastike kesyttää lehtikaalin herkulliseksi salaattipohjaksi. Paahdettu kurpitsa ja vuohenjuusto tekevät syyssalaatista mukavan ruokaisan. Noin 2,60€/annos",
      "DateCreated": "2019-04-18T13:58:12",
      "DateModified": "2019-09-11T09:32:38",
      "TvDate": "2019-09-27T00:00:00",
      "Stamp": {
        "Name": "Testattu K-koekeittiössä",
        "Id": "1"
      },
      "RecipeUse": {
        "Name": "k-ruoka.fi",
        "Id": "1"
      },
      "resourceType": "recipe",
      "Url": "https://www.k-ruoka.fi/reseptit/karpalo-kurpitsasalaatti",
      "UrlSlug": "karpalo-kurpitsasalaatti",
      "Sort": [1, 1, 10363]
    }, {
      "Id": "10361",
      "Name": "Leipämuru-lehtikaalipasta",
      "PieceSize": {
        "Unit": "g"
      },
      "Portions": {
        "Unit": "annosta",
        "Amount": "4"
      },
      "PreparationTime": {
        "Description": "alle 30 min",
        "TimeRange": {
          "MinTime": 15,
          "MaxTime": 30
        }
      },
      "UserPortions": {
        "$": {
          "Unit": "henkilölle"
        }
      },
      "Categories": [{
        "MainName": "Pääruoat",
        "MainId": "4",
        "SubName": "Kasvisruoat",
        "SubId": "22"
      }, {
        "MainName": "Pääruoat",
        "MainId": "4",
        "SubName": "Pastaruoat",
        "SubId": "31"
      }, {
        "MainName": "Sesonki",
        "MainId": "1",
        "SubName": "Sadonkorjuu",
        "SubId": "124"
      }, {
        "MainName": "Ruokalaji",
        "MainId": "16",
        "SubName": "Pastat",
        "SubId": "138"
      }, {
        "MainName": "Teema",
        "MainId": "15",
        "SubName": "Mitä tänään syötäisiin?",
        "SubId": "144"
      }, {
        "MainName": "Asiakassegmentointi (K-asiakkaat)",
        "MainId": "17",
        "SubName": "Nautiskelijat",
        "SubId": "146"
      }, {
        "MainName": "Asiakassegmentointi (K-asiakkaat)",
        "MainId": "17",
        "SubName": "Innostujat",
        "SubId": "147"
      }, {
        "MainName": "Asiakassegmentointi (K-asiakkaat)",
        "MainId": "17",
        "SubName": "Tiedostavat",
        "SubId": "148"
      }, {
        "MainName": "Teema",
        "MainId": "15",
        "SubName": "K-Marketin valikoima",
        "SubId": "151"
      }, {
        "MainName": "Apukategoriat",
        "MainId": "18",
        "SubName": "K-Marketin valikoima",
        "SubId": "181"
      }],
      "Pictures": ["KR5_19_leipamuru_leht"],
      "PictureUrls": [{
        "Id": "KR5_19_leipamuru_leht",
        "Normal": "https://k-file-storage-qa.imgix.net/f/k-ruoka/recipe/10361?w=1000&h=1000&fit=clip",
        "Original": "https://k-file-storage-qa.imgix.net/f/k-ruoka/recipe/10361"
      }],
      "VideoUrl": "https://youtu.be/NI3ViLnORNM",
      "VideoEmbedUrl": "<iframe width=\"1136\" height=\"639\" src=\"https://www.youtube.com/embed/NI3ViLnORNM\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>",
      "SpecialDiets": [{
        "Id": "5",
        "Name": "Vähemmän sokeria"
      }, {
        "Id": "6",
        "Name": "Kasvis (lakto-ovo)"
      }, {
        "Id": "7",
        "Name": "Kananmunaton"
      }],
      "Ingredients": [{
        "SubSectionHeading": "_",
        "SubSectionIngredients": [
          [{
            "Name": "kuivahtanutta maalaisleipää",
            "Amount": "100",
            "PackageRecipe": "false",
            "IngredientType": "6019",
            "Unit": "g",
            "IngredientTypeName": "Maalaisleipä"
          }],
          [{
            "Name": "valkosipulinkynttä",
            "Amount": "4",
            "PackageRecipe": "false",
            "IngredientType": "7262",
            "Unit": "",
            "IngredientTypeName": "Valkosipuli"
          }],
          [{
            "Name": "lehtikaalia",
            "Amount": "1",
            "AmountInfo": "(200 g)",
            "PackageRecipe": "false",
            "IngredientType": "5952",
            "Unit": "ps",
            "IngredientTypeName": "Lehtikaali"
          }],
          [{
            "Name": "Pirkka Luomu spagettia",
            "Amount": "300",
            "PackageRecipe": "false",
            "IngredientType": "6989",
            "Unit": "g",
            "IngredientTypeName": "Spagetti"
          }],
          [{
            "Name": "Pirkka ekstra-neitsytoliiviöljyä",
            "Ean": "6410402024759",
            "Amount": "6",
            "PackageRecipe": "false",
            "IngredientType": "5405",
            "Unit": "rkl",
            "IngredientTypeName": "Ekstra neitsytoliiviöljy"
          }],
          [{
            "Name": "pastan keitinvettä",
            "Amount": "1",
            "PackageRecipe": "false",
            "IngredientType": "15792",
            "Unit": "dl",
            "IngredientTypeName": "Vesi"
          }],
          [{
            "Name": "sitruunanmehua",
            "Amount": "2",
            "PackageRecipe": "false",
            "IngredientType": "6941",
            "Unit": "rkl",
            "IngredientTypeName": "Sitruuna"
          }],
          [{
            "Name": "Pirkka Parhaat Mozzarella di Bufala -juustoa",
            "Ean": "6410405081261",
            "Amount": "2",
            "AmountInfo": "(à 220 g/125 g)",
            "PackageRecipe": "false",
            "IngredientType": "11521",
            "Unit": "pkt",
            "IngredientTypeName": "Mozzarella, di Bufala"
          }],
          [{
            "Name": "mustapippuria",
            "Amount": "1/2",
            "PackageRecipe": "false",
            "IngredientType": "6271",
            "Unit": "tl",
            "IngredientTypeName": "Mustapippuri, rouhittu"
          }],
          [{
            "Name": "Pirkka Parhaat sormisuolaa",
            "Ean": "6410405076915",
            "PackageRecipe": "false",
            "IngredientType": "6971",
            "Unit": "ripaus",
            "IngredientTypeName": "Sormisuola"
          }]
        ]
      }],
      "Instructions": "# Revi leipä muruksi sormin. Hienonna kuoritut valkosipulinkynnet. Pese ja kuivaa lehtikaali. Poista kova lehtiruoti ja revi lehdet suupaloiksi.\r\n# Keitä spagetti runsaassa suolalla maustetussa vedessä (1 tl suolaa/2 litraa vettä).\r\n# Kuumenna pannulla 2 rkl oliiviöljyä. Lisää leipämuru ja puolet valkosipuleista. Paista sekoitellen, kunnes murut muuttuvat rapeiksi ja saavat kullanruskeaa väriä. Siirrä murut pannulta kulhoon.\r\n# Kuumenna pannulla 2 rkl oliiviöljyä ja lisää loput valkosipulit ja lehtikaalit. Paista, kunnes lehtikaali pehmenee.\r\n# Valuta pasta ja säästä 1 dl keitinvettä. Kaada pasta takaisin kattilaan.\r\n# Kaada spagettikattilaan myös keitinvesi, 2 rkl oliiviöljyä, sitruunanmehu ja lehtikaalit. Kääntele sekaisin. Annostele pasta lautasille ja viimeistele leipämurulla, puolikkaalla juustopallolla, mustapippurilla ja sormisuolalla.",
      "EndNote": "Voit myös kokeilla mozzarellan tilalla pehmeämpää burratina-juustoa. *Annoshinnat on laskettu K-citymarket-ketjun hintatason mukaan 8/2019. Tuoretuotteiden hinnat vaihtelevat sesongeittain.",
      "Description": "Paista kuivahtaneesta leivästä ihanaa rapeaa murua, joka tekee pasta-annoksestakin paremman. Noin 1,80€/annos",
      "DateCreated": "2019-04-18T13:23:36",
      "DateModified": "2019-10-15T20:10:27",
      "TvDate": "2019-10-16T00:00:00",
      "Stamp": {
        "Name": "Testattu K-koekeittiössä",
        "Id": "1"
      },
      "RecipeUse": {
        "Name": "k-ruoka.fi",
        "Id": "1"
      },
      "resourceType": "recipe",
      "Url": "https://www.k-ruoka.fi/reseptit/leipamuru-lehtikaalipasta",
      "UrlSlug": "leipamuru-lehtikaalipasta",
      "Sort": [1, 1, 10361]
    }, {
      "Id": "10359",
      "Name": "Kvinoa-porkkanapyörykät, naurisranskalaiset ja pestodippi",
      "PieceSize": {
        "Unit": "g",
        "Amount": "250"
      },
      "Portions": {
        "Unit": "annosta",
        "Amount": "6"
      },
      "PreparationTime": {
        "Description": "yli 60 min",
        "TimeRange": {
          "MinTime": 60
        }
      },
      "UserPortions": {
        "$": {
          "Unit": "henkilölle"
        }
      },
      "Categories": [{
        "MainName": "Teema",
        "MainId": "15",
        "SubName": "Mitä tänään syötäisiin?",
        "SubId": "144"
      }, {
        "MainName": "Asiakassegmentointi (K-asiakkaat)",
        "MainId": "17",
        "SubName": "Innostujat",
        "SubId": "147"
      }, {
        "MainName": "Asiakassegmentointi (K-asiakkaat)",
        "MainId": "17",
        "SubName": "Tiedostavat",
        "SubId": "148"
      }, {
        "MainName": "Pääruoat",
        "MainId": "4",
        "SubName": "Kasvisruoat",
        "SubId": "22"
      }, {
        "MainName": "Pääraaka-aine",
        "MainId": "14",
        "SubName": "Kasvis",
        "SubId": "115"
      }, {
        "MainName": "Sesonki",
        "MainId": "1",
        "SubName": "Sadonkorjuu",
        "SubId": "124"
      }, {
        "MainName": "Ruokalaji",
        "MainId": "16",
        "SubName": "Uuniruoat",
        "SubId": "141"
      }],
      "Pictures": ["KR5_19_kvinoa_porkkanap"],
      "PictureUrls": [{
        "Id": "KR5_19_kvinoa_porkkanap",
        "Normal": "https://k-file-storage-qa.imgix.net/f/k-ruoka/recipe/10359?w=1000&h=1000&fit=clip",
        "Original": "https://k-file-storage-qa.imgix.net/f/k-ruoka/recipe/10359"
      }],
      "VideoUrl": "https://youtu.be/D-xNZKQxwvg",
      "VideoEmbedUrl": "<iframe width=\"427\" height=\"240\" src=\"https://www.youtube.com/embed/D-xNZKQxwvg\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>",
      "SpecialDiets": [{
        "Id": "8",
        "Name": "Vegaaninen"
      }],
      "Ingredients": [{
        "SubSectionHeading": "Kvinoa-porkkanapyörykät",
        "SubSectionIngredients": [
          [{
            "Name": "Urtekram Luomu kvinoaa",
            "Ean": "5765228112830",
            "Amount": "2",
            "PackageRecipe": "false",
            "IngredientType": "10213",
            "Unit": "dl",
            "IngredientTypeName": "Kvinoa, luomu"
          }],
          [{
            "Name": "vettä",
            "Amount": "4",
            "PackageRecipe": "false",
            "IngredientType": "15792",
            "Unit": "dl",
            "IngredientTypeName": "Vesi"
          }],
          [{
            "Name": "suolaa",
            "Amount": "1",
            "PackageRecipe": "false",
            "IngredientType": "7049",
            "Unit": "tl",
            "IngredientTypeName": "Suola"
          }],
          [{
            "Name": "pieni sipuli",
            "Amount": "1",
            "AmountInfo": "(50 g)",
            "PackageRecipe": "false",
            "IngredientType": "6932",
            "Unit": "",
            "IngredientTypeName": "Sipuli"
          }],
          [{
            "Name": "valkosipulinkynttä",
            "Amount": "4",
            "PackageRecipe": "false",
            "IngredientType": "7262",
            "Unit": "",
            "IngredientTypeName": "Valkosipuli"
          }],
          [{
            "Name": "porkkanaa",
            "Amount": "2",
            "AmountInfo": "(200 g)",
            "PackageRecipe": "false",
            "IngredientType": "6582",
            "Unit": "",
            "IngredientTypeName": "Porkkana"
          }],
          [{
            "Name": "oliiviöljyä",
            "Amount": "2",
            "PackageRecipe": "false",
            "IngredientType": "6386",
            "Unit": "rkl",
            "IngredientTypeName": "Oliiviöljy"
          }],
          [{
            "Name": "garam masala -mausteseosta",
            "Amount": "2",
            "PackageRecipe": "false",
            "IngredientType": "6178",
            "Unit": "tl",
            "IngredientTypeName": "Mausteseos, garam masala"
          }],
          [{
            "Name": "kurkumaa",
            "Amount": "1",
            "PackageRecipe": "false",
            "IngredientType": "10200",
            "Unit": "tl",
            "IngredientTypeName": "Kurkuma, jauhettu"
          }],
          [{
            "Name": "savupaprikajauhetta",
            "Amount": "1",
            "PackageRecipe": "false",
            "IngredientType": "15800",
            "Unit": "tl",
            "IngredientTypeName": "paprika, jauhettu, savustettu"
          }],
          [{
            "Name": "suolaa",
            "Amount": "1/2",
            "PackageRecipe": "false",
            "IngredientType": "7049",
            "Unit": "tl",
            "IngredientTypeName": "Suola"
          }],
          [{
            "Name": "Pirkka salaattisiemensekoitusta",
            "Ean": "6410405127341",
            "Amount": "1",
            "PackageRecipe": "false",
            "IngredientType": "6847",
            "Unit": "dl",
            "IngredientTypeName": "Siemensekoitus"
          }],
          [{
            "Name": "Pirkka ruskeita papuja suolaliemessä",
            "Amount": "1",
            "AmountInfo": "(380 g/230 g)",
            "PackageRecipe": "false",
            "IngredientType": "12303",
            "Unit": "tlk",
            "IngredientTypeName": "Papu, ruskea, säilyke"
          }],
          [{
            "Name": "Pirkka tomaattisosetta",
            "Ean": "6410405093202",
            "Amount": "3",
            "PackageRecipe": "false",
            "IngredientType": "7141",
            "Unit": "rkl",
            "IngredientTypeName": "Tomaattisose"
          }],
          [{
            "Name": "oliiviöljyä",
            "Amount": "3",
            "PackageRecipe": "false",
            "IngredientType": "6386",
            "Unit": "rkl",
            "IngredientTypeName": "Oliiviöljy"
          }]
        ]
      }, {
        "SubSectionHeading": "Pyörittelyyn",
        "SubSectionIngredients": [
          [{
            "Name": "oliiviöljyä",
            "Amount": "3",
            "PackageRecipe": "false",
            "IngredientType": "5405",
            "Unit": "rkl",
            "IngredientTypeName": "Ekstra neitsytoliiviöljy"
          }]
        ]
      }, {
        "SubSectionHeading": "Naurisranskalaiset",
        "SubSectionIngredients": [
          [{
            "Name": "naurista",
            "Amount": "5",
            "AmountInfo": "(600 g)",
            "PackageRecipe": "false",
            "IngredientType": "6337",
            "Unit": "",
            "IngredientTypeName": "Nauris"
          }],
          [{
            "Name": "oliiviöljyä",
            "Amount": "3",
            "PackageRecipe": "false",
            "IngredientType": "6386",
            "Unit": "rkl",
            "IngredientTypeName": "Oliiviöljy"
          }],
          [{
            "Name": "garam masala -mausteseosta",
            "Amount": "1",
            "PackageRecipe": "false",
            "IngredientType": "6178",
            "Unit": "tl",
            "IngredientTypeName": "Mausteseos, garam masala"
          }],
          [{
            "Name": "suolaa",
            "Amount": "3/4",
            "PackageRecipe": "false",
            "IngredientType": "7049",
            "Unit": "tl",
            "IngredientTypeName": "Suola"
          }]
        ]
      }, {
        "SubSectionHeading": "Pestodippi",
        "SubSectionIngredients": [
          [{
            "Name": "makeuttamatonta soijajogurttia (Alpro)",
            "Amount": "1 1/2",
            "PackageRecipe": "false",
            "IngredientType": "14151",
            "Unit": "dl",
            "IngredientTypeName": "Soijajogurtti, maustamaton"
          }],
          [{
            "Name": "Urtekram Luomu basilikapestoa",
            "Amount": "2",
            "PackageRecipe": "false",
            "IngredientType": "6536",
            "Unit": "rkl",
            "IngredientTypeName": "Pesto, vihreä"
          }]
        ]
      }],
      "Instructions": "# Keitä kvinoa pakkauksen ohjeen mukaan suolatussa vedessä.\r\n# Hienonna kuorittu sipuli ja valkosipulinkynnet. Raasta kuoritut porkkanat raastimen hienolla terällä.\r\n# Kuumenna pannulla öljy ja lisää mausteet. Lisää sipulit ja porkkanat ja kuullota, kunnes kasvikset pehmenevät.\r\n# Rouhi siemensekoitus karkeasti veitsellä. Valuta pavuista neste.\r\n# Kaada pavut isoon kulhoon ja murskaa ne perunasurvimella tai haarukalla. Lisää siemenrouhe, kypsä kvinoa, sipuli-porkkanaseos, tomaattisose ja oliiviöljy. Sekoita tasaiseksi taikinaksi.\r\n# Pyörittele taikinasta pieniä pyöryköitä öljytyin käsin leivinpaperin päälle uunipellille. Taikinasta tulee noin 36 pyörykkää.\r\n# Valmista naurisranskalaiset. Kuori nauriit ja leikkaa ne noin puolen sentin paksuisiksi tikuiksi. Kaada nauriit leivinpaperin päälle uunipellille. Sekoita öljy ja mausteet keskenään, valuta seos nauriiden päälle ja pyörittele nauriit kauttaaltaan öljyssä.\r\n# Paahda pyöryköitä ja nauriita 200-asteisessa uunissa kiertoilmalla noin 30 minuuttia. Voit myös kypsentää ne tasalämmöllä 225 asteessa ja vaihtaa peltien paikkoja välissä.\r\n# Valmista kastike sekoittamalla soijajogurtti ja pesto pienessä kulhossa.\r\n# Anna kvinoapyöryköiden jäähtyä hetki ennen tarjoamista, sillä ne ovat juuri uunista tulleina pehmeitä.",
      "EndNote": "*Annoshinnat on laskettu K-citymarket-ketjun hintatason mukaan 8/2019. Tuoretuotteiden hinnat vaihtelevat sesongeittain.",
      "Description": "Kvinoapyörykät maistuvat ihanilta helpon pestodipin ja naurisranskalaisten kanssa. Vegaaniset pallerot maistuvat ihan kaikille, joten niitä kannattaa tehdä kerralla isompi satsi ja pakastaa. Noin 1,55€/annos",
      "DateCreated": "2019-04-18T11:27:39",
      "DateModified": "2019-11-06T11:18:41",
      "TvDate": "2019-09-05T00:00:00",
      "Stamp": {
        "Name": "Testattu K-koekeittiössä",
        "Id": "1"
      },
      "RecipeUse": {
        "Name": "k-ruoka.fi",
        "Id": "1"
      },
      "resourceType": "recipe",
      "Url": "https://www.k-ruoka.fi/reseptit/kvinoa-porkkanapyorykat-naurisranskalaiset-ja-pestodippi",
      "UrlSlug": "kvinoa-porkkanapyorykat-naurisranskalaiset-ja-pestodippi",
      "Sort": [1, 1, 10359]
    }, {
      "Id": "10356",
      "Name": "Savustettu siika tuorejuustotäytteellä",
      "PieceSize": {
        "Unit": "g",
        "Amount": "266"
      },
      "Portions": {
        "Unit": "annosta",
        "Amount": "4"
      },
      "PreparationTime": {
        "Description": "yli 60 min",
        "TimeRange": {
          "MinTime": 60
        }
      },
      "UserPortions": {
        "$": {
          "Unit": "henkilölle"
        }
      },
      "Categories": [{
        "MainName": "Sesonki",
        "MainId": "1",
        "SubName": "Grillaus",
        "SubId": "5"
      }, {
        "MainName": "Sesonki",
        "MainId": "1",
        "SubName": "Kesä",
        "SubId": "121"
      }, {
        "MainName": "Sesonki",
        "MainId": "1",
        "SubName": "Juhannus",
        "SubId": "122"
      }, {
        "MainName": "Asiakassegmentointi (K-asiakkaat)",
        "MainId": "17",
        "SubName": "Innostujat",
        "SubId": "147"
      }, {
        "MainName": "Alkuruoat",
        "MainId": "3",
        "SubName": "Kalat ja äyriäiset",
        "SubId": "16"
      }, {
        "MainName": "Pääruoat",
        "MainId": "4",
        "SubName": "Kalaruoat",
        "SubId": "20"
      }, {
        "MainName": "Pääruoat",
        "MainId": "4",
        "SubName": "Grilliruoat",
        "SubId": "73"
      }, {
        "MainName": "Makuja maailmalta",
        "MainId": "2",
        "SubName": "Skandinavia",
        "SubId": "105"
      }, {
        "MainName": "Makuja maailmalta",
        "MainId": "2",
        "SubName": "Suomi",
        "SubId": "106"
      }, {
        "MainName": "Pääraaka-aine",
        "MainId": "14",
        "SubName": "Kala",
        "SubId": "116"
      }],
      "Pictures": ["KCM_Savustettu_siika_tu"],
      "PictureUrls": [{
        "Id": "KCM_Savustettu_siika_tu",
        "Normal": "https://k-file-storage-qa.imgix.net/f/k-ruoka/recipe/10356?w=1000&h=1000&fit=clip",
        "Original": "https://k-file-storage-qa.imgix.net/f/k-ruoka/recipe/10356"
      }],
      "SpecialDiets": [{
        "Id": "4",
        "Name": "Gluteeniton"
      }],
      "Ingredients": [{
        "SubSectionHeading": "_",
        "SubSectionIngredients": [
          [{
            "Name": "ruodotonta siikafileetä",
            "AmountInfo": "n. 700 g",
            "PackageRecipe": "false",
            "IngredientType": "14014",
            "IngredientTypeName": "Siikafilee, tuore"
          }],
          [{
            "Name": "karkeaa merisuolaa",
            "AmountInfo": "n. 2 rkl",
            "PackageRecipe": "false",
            "IngredientType": "6222",
            "IngredientTypeName": "Merisuola, karkea"
          }],
          [{
            "Name": "Pirkka ruohosipulituorejuustoa",
            "Ean": "6410405156075",
            "Amount": "1/2",
            "AmountInfo": "(à 180 g)",
            "PackageRecipe": "false",
            "IngredientType": "7180",
            "Unit": "rs",
            "IngredientTypeName": "Tuorejuusto, ruohosipuli"
          }],
          [{
            "Name": "Pirkka salaattisiemensekoitusta",
            "Amount": "1/2",
            "AmountInfo": "(à 180 g)",
            "PackageRecipe": "false",
            "IngredientType": "6847",
            "Unit": "ps",
            "IngredientTypeName": "Siemensekoitus"
          }],
          [{
            "Name": "suolaa",
            "Amount": "3/4",
            "PackageRecipe": "false",
            "IngredientType": "7049",
            "Unit": "tl",
            "IngredientTypeName": "Suola"
          }]
        ]
      }, {
        "SubSectionHeading": "Kylmä paprika-nektariinikastike",
        "SubSectionIngredients": [
          [{
            "Name": "isoa punaista paprikaa",
            "Amount": "2",
            "AmountInfo": "(500 g)",
            "PackageRecipe": "false",
            "IngredientType": "6448",
            "Unit": "",
            "IngredientTypeName": "Paprika, punainen"
          }],
          [{
            "Name": "nektariini",
            "Amount": "1",
            "AmountInfo": "(200 g)",
            "PackageRecipe": "false",
            "IngredientType": "6342",
            "Unit": "",
            "IngredientTypeName": "Nektariini"
          }],
          [{
            "Name": "valkosipulinkynttä",
            "Amount": "8",
            "PackageRecipe": "false",
            "IngredientType": "7262",
            "Unit": "",
            "IngredientTypeName": "Valkosipuli"
          }],
          [{
            "Name": "valkoviinietikkaa",
            "Amount": "1/2",
            "PackageRecipe": "false",
            "IngredientType": "7274",
            "Unit": "dl",
            "IngredientTypeName": "Valkoviinietikka"
          }],
          [{
            "Name": "lehtipersiljaa hienonnettuna",
            "Amount": "1/2",
            "PackageRecipe": "false",
            "IngredientType": "5954",
            "Unit": "dl",
            "IngredientTypeName": "Lehtipersilja"
          }],
          [{
            "Name": "sitruunanmehua",
            "Amount": "1 1/2",
            "PackageRecipe": "false",
            "IngredientType": "6941",
            "Unit": "rkl",
            "IngredientTypeName": "Sitruuna"
          }],
          [{
            "Name": "oliiviöljyä",
            "Amount": "1",
            "PackageRecipe": "false",
            "IngredientType": "6386",
            "Unit": "rkl",
            "IngredientTypeName": "Oliiviöljy"
          }],
          [{
            "Name": "suolaa",
            "Amount": "1/2",
            "PackageRecipe": "false",
            "IngredientType": "7049",
            "Unit": "rkl",
            "IngredientTypeName": "Suola"
          }],
          [{
            "Name": "valkopippuria",
            "Amount": "1/4",
            "PackageRecipe": "false",
            "IngredientType": "7259",
            "Unit": "tl",
            "IngredientTypeName": "Valkopippuri, jauhettu"
          }]
        ]
      }, {
        "SubSectionHeading": "Lisäksi",
        "SubSectionIngredients": [
          [{
            "Name": "savustuspurua (esim. leppä)",
            "Amount": "2-3",
            "PackageRecipe": "false",
            "IngredientType": "16076",
            "Unit": "dl",
            "IngredientTypeName": "Savustuspuru"
          }],
          [{
            "Name": "sokeripalaa",
            "Amount": "2",
            "PackageRecipe": "false",
            "IngredientType": "6431",
            "Unit": "",
            "IngredientTypeName": "Palasokeri"
          }]
        ]
      }],
      "Instructions": "# Pyyhi fileiden pinta. Hiero pintaan karkeaa merisuolaa ja kääri kala leivinpaperiin tai folioon. Nosta jääkaappiin noin ½ tunniksi suolautumaan.\r\n# Ota kalat kylmästä. Pyyhi pinnasta ylimääräinen suola. Leikkaa fileet kahtia. Sivele lihan pintaan tuorejuustoa ja ripottele päälle siemeniä. Nosta fileet päällekkäin lihapinnat vastakkain ja halutessasi pistele kiinni hammastikuilla.\r\n# Levitä savustuspuruja tasaisesti savustuslaatikon pohjalle. Lisää purujen pinnalle 2 sokeripalaa. Niillä saat kalalle kauniin värin. Laita laatikon pohjalle lastujen päälle rasvapelti ja nosta kala laatikkoon ritilän päälle. Sulje kansi. Esilämmitä kaasugrilli 200 asteeseen ja nosta laatikko grilliin. Odota 5-10 minuuttia savun muodostumista.\r\n# Savusta siikaa miedolla lämmöllä 30-40 minuuttia, kunnes se on mehevän kypsää. Kala on kypsää, kun nahka irtoaa helposti.\r\n# Valmista kastike. Paahda pestyt paprikat grillissä pehmeiksi, niin että niiden pintaan tulee mustia laikkuja. Nosta kuumat paprikat vahvaan muovipussiin 10 minuutiksi ja poista sitten paprikoista kuoret ja sisukset.\r\n# Kuori ja hienonna valkosipulinkynnet. Lisää monitoimikoneen kulhoon nektariinin palat ja kaikki muut ainekset. Soseuta tasaiseksi ja nosta viileään ennen tarjoilua. Tarjoa kastike kylmänä savustetun siian ja raikkaan salaatin tai uusien perunoiden kanssa.",
      "EndNote": "",
      "Description": "Takuumehevää savukalaa saat, kun täytät siikafileet tuorejuustolla. Tarjoa savustettu siika paprika-nektariinikastikkeen kanssa.",
      "DateCreated": "2019-04-17T15:56:55",
      "DateModified": "2019-08-07T15:31:07",
      "Stamp": {
        "Name": "Testattu K-koekeittiössä",
        "Id": "1"
      },
      "RecipeUse": {
        "Name": "k-ruoka.fi",
        "Id": "1"
      },
      "resourceType": "recipe",
      "Url": "https://www.k-ruoka.fi/reseptit/savustettu-siika-tuorejuustotaytteella",
      "UrlSlug": "savustettu-siika-tuorejuustotaytteella",
      "Sort": [1, 1, 10356]
    }, {
      "Id": "10355",
      "Name": "Savumuikut ja kirsikkawaldorf",
      "PieceSize": {
        "Unit": "g",
        "Amount": "470"
      },
      "Portions": {
        "Unit": "annosta",
        "Amount": "4"
      },
      "PreparationTime": {
        "Description": "yli 60 min",
        "TimeRange": {
          "MinTime": 60
        }
      },
      "UserPortions": {
        "$": {
          "Unit": "henkilölle"
        }
      },
      "Categories": [{
        "MainName": "Sesonki",
        "MainId": "1",
        "SubName": "Grillaus",
        "SubId": "5"
      }, {
        "MainName": "Pääruoat",
        "MainId": "4",
        "SubName": "Kalaruoat",
        "SubId": "20"
      }, {
        "MainName": "Pääraaka-aine",
        "MainId": "14",
        "SubName": "Kala",
        "SubId": "116"
      }, {
        "MainName": "Sesonki",
        "MainId": "1",
        "SubName": "Kesä",
        "SubId": "121"
      }, {
        "MainName": "Sesonki",
        "MainId": "1",
        "SubName": "Juhannus",
        "SubId": "122"
      }, {
        "MainName": "Asiakassegmentointi (K-asiakkaat)",
        "MainId": "17",
        "SubName": "Innostujat",
        "SubId": "147"
      }],
      "Pictures": ["KCM_Savumuikut_ja_kirsi"],
      "PictureUrls": [{
        "Id": "KCM_Savumuikut_ja_kirsi",
        "Normal": "https://k-file-storage-qa.imgix.net/f/k-ruoka/recipe/10355?w=1000&h=1000&fit=clip",
        "Original": "https://k-file-storage-qa.imgix.net/f/k-ruoka/recipe/10355"
      }],
      "SpecialDiets": [{
        "Id": "2",
        "Name": "Laktoositon"
      }, {
        "Id": "3",
        "Name": "Maidoton"
      }, {
        "Id": "4",
        "Name": "Gluteeniton"
      }],
      "Ingredients": [{
        "SubSectionHeading": "_",
        "SubSectionIngredients": [
          [{
            "Name": "tuoreita, keskikokoisia muikkuja",
            "Amount": "1",
            "PackageRecipe": "false",
            "IngredientType": "6247",
            "Unit": "kg",
            "IngredientTypeName": "Muikku"
          }],
          [{
            "Name": "savustuspurua (mieluiten haketettua, kuorittua leppää)",
            "Amount": "2",
            "PackageRecipe": "false",
            "IngredientType": "16076",
            "Unit": "dl",
            "IngredientTypeName": "Savustuspuru"
          }],
          [{
            "Name": "karkeaa merisuolaa",
            "Amount": "1-2",
            "PackageRecipe": "false",
            "IngredientType": "6222",
            "Unit": "rkl",
            "IngredientTypeName": "Merisuola, karkea"
          }],
          [{
            "Name": "sokeripalaa",
            "Amount": "2-4",
            "PackageRecipe": "false",
            "IngredientType": "6431",
            "Unit": "",
            "IngredientTypeName": "Palasokeri"
          }]
        ]
      }, {
        "SubSectionHeading": "Kirsikkawaldorf",
        "SubSectionIngredients": [
          [{
            "Name": "keskikokoista hapanta omenaa (esim. Granny smith)",
            "Amount": "3",
            "AmountInfo": "(600 g)",
            "PackageRecipe": "false",
            "IngredientType": "12050",
            "Unit": "",
            "IngredientTypeName": "Omena, vihreä, Granny Smith"
          }],
          [{
            "Name": "juuriselleriä",
            "Amount": "1/2",
            "AmountInfo": "(225 g)",
            "PackageRecipe": "false",
            "IngredientType": "15836",
            "Unit": "",
            "IngredientTypeName": "Juuriselleri, tuore"
          }],
          [{
            "Name": "tuoreita kirsikoita",
            "Amount": "2",
            "PackageRecipe": "false",
            "IngredientType": "5824",
            "Unit": "dl",
            "IngredientTypeName": "Kirsikka, tuore"
          }, {
            "Name": "Pirkka Parhaat kirsikoita (pakaste)",
            "Ean": "6410405205919",
            "Amount": "1",
            "AmountInfo": "(250 g)",
            "PackageRecipe": "false",
            "IngredientType": "5824",
            "Unit": "pkt",
            "IngredientTypeName": "Kirsikka, tuore"
          }],
          [{
            "Name": "kuorittuja manteleita",
            "Amount": "2",
            "PackageRecipe": "false",
            "IngredientType": "6118",
            "Unit": "rkl",
            "IngredientTypeName": "Manteli"
          }],
          [{
            "Name": "pistaasipähkinöitä",
            "Amount": "2",
            "PackageRecipe": "false",
            "IngredientType": "12788",
            "Unit": "rkl",
            "IngredientTypeName": "Pistaasipähkinä, kuorittu"
          }],
          [{
            "Name": "majoneesia",
            "Amount": "1",
            "PackageRecipe": "false",
            "IngredientType": "6058",
            "Unit": "dl",
            "IngredientTypeName": "Majoneesi"
          }],
          [{
            "Name": "sitruunanmehua",
            "Amount": "1/2",
            "PackageRecipe": "false",
            "IngredientType": "6941",
            "Unit": "dl",
            "IngredientTypeName": "Sitruuna"
          }],
          [{
            "Name": "Pirkka peanut butter smooth -maapähkinävoita",
            "Ean": "6410405170316",
            "Amount": "2",
            "PackageRecipe": "false",
            "IngredientType": "6024",
            "Unit": "rkl",
            "IngredientTypeName": "Maapähkinävoi"
          }],
          [{
            "Name": "kanelia",
            "Amount": "1/2",
            "PackageRecipe": "false",
            "IngredientType": "5689",
            "Unit": "tl",
            "IngredientTypeName": "Kaneli, jauhettu"
          }]
        ]
      }],
      "Instructions": "# Puhko muikut eli poista muikkujen pää, kidukset ja sisälmykset. Ota sormilla tiukka ote kalan kiduskaaren takana olevista evistä ja repäise irti niin, että kalasta irtoavat pään mukana myös sisukset. Huuhdo muikut nopeasti kylmällä vedellä lävikössä ja levitä talouspaperille kuivumaan 15 minuutiksi.\r\n# Asettele kalat savustusritilälle irti toisistaan. Sulje savustuspönttö ja esilämmitä pönttöä 10−15 minuuttia, noin 60 asteeseen. Ripottele sitten pöntön pohjalle leppäpuruja 2−3 dl muikkukiloa kohden. Laita pohjalle lopuksi vielä kaksi sokeripalaa, näin saat kaloille kauniin ruskean värin.\r\n# Sulje pönttö ja anna kalojen savustua 15−20 minuuttia makusi mukaan. Aika alkaa, kun savua alkaa muodostuu pönttöön. Kalan kypsyyden voi todeta irrottamalla vatsaevän. Jos se irtoaa vaivattomasti, kala on kypsää.\r\n# Nosta muikut savustuspöntöstä leivinpaperille, ripottele päälle suolaa. Kääri leivinpaperiin ja sanomalehteen noin kymmeneksi minuutiksi.\r\n# Valmista salaatti. Yhdistä kulhoon pilkotut omenat, selleri, halkaistut siemenettömät kirsikat tai sulatetut, valutetut ja halkaistut pakastekirsikat sekä karkeaksi hienonnetut mantelit ja pistaasipähkinät.\r\n# Sekoita keskenään toisessa kulhossa majoneesi, sitruunanmehu, maapähkinävoi ja kaneli. Sekoita kastike salaatin sekaan.\r\n# Pyyhi savumuikkujen pinnalta pois ylimääräinen suola ja tarjoa muikut kirsikkawaldorfin kanssa.",
      "EndNote": "",
      "Description": "Savustetut muikut ovat oivaa kesäruokaa. Tarjoa muikut  kesäisen kirsikkawaldorfin kanssa.",
      "DateCreated": "2019-04-17T15:07:29",
      "DateModified": "2019-06-12T17:15:50",
      "Stamp": {
        "Name": "Testattu K-koekeittiössä",
        "Id": "1"
      },
      "RecipeUse": {
        "Name": "k-ruoka.fi",
        "Id": "1"
      },
      "resourceType": "recipe",
      "Url": "https://www.k-ruoka.fi/reseptit/savumuikut-ja-kirsikkawaldorf",
      "UrlSlug": "savumuikut-ja-kirsikkawaldorf",
      "Sort": [1, 1, 10355]
    }, {
      "Id": "10354",
      "Name": "Kala-äyriäisvartaat",
      "PieceSize": {
        "Unit": "g",
        "Amount": "147"
      },
      "Portions": {
        "Unit": "kpl",
        "Amount": "10"
      },
      "PreparationTime": {
        "Description": "yli 60 min",
        "TimeRange": {
          "MinTime": 60
        }
      },
      "UserPortions": {
        "$": {
          "Unit": "henkilölle"
        }
      },
      "Categories": [{
        "MainName": "Sesonki",
        "MainId": "1",
        "SubName": "Juhannus",
        "SubId": "122"
      }, {
        "MainName": "Asiakassegmentointi (K-asiakkaat)",
        "MainId": "17",
        "SubName": "Innostujat",
        "SubId": "147"
      }, {
        "MainName": "Alkuruoat",
        "MainId": "3",
        "SubName": "Kalat ja äyriäiset",
        "SubId": "16"
      }, {
        "MainName": "Pääruoat",
        "MainId": "4",
        "SubName": "Grilliruoat",
        "SubId": "73"
      }, {
        "MainName": "Lisäkkeet",
        "MainId": "6",
        "SubName": "Grilliruoan lisäkkeet",
        "SubId": "93"
      }, {
        "MainName": "Pääraaka-aine",
        "MainId": "14",
        "SubName": "Äyriäiset",
        "SubId": "111"
      }, {
        "MainName": "Pääraaka-aine",
        "MainId": "14",
        "SubName": "Kala",
        "SubId": "116"
      }, {
        "MainName": "Sesonki",
        "MainId": "1",
        "SubName": "Kesä",
        "SubId": "121"
      }, {
        "MainName": "Sesonki",
        "MainId": "1",
        "SubName": "Grillaus",
        "SubId": "5"
      }],
      "Pictures": ["KCM_Kala-ayriaisvartaat"],
      "PictureUrls": [{
        "Id": "KCM_Kala-ayriaisvartaat",
        "Normal": "https://k-file-storage-qa.imgix.net/f/k-ruoka/recipe/10354?w=1000&h=1000&fit=clip",
        "Original": "https://k-file-storage-qa.imgix.net/f/k-ruoka/recipe/10354"
      }],
      "SpecialDiets": [{
        "Id": "2",
        "Name": "Laktoositon"
      }, {
        "Id": "3",
        "Name": "Maidoton"
      }, {
        "Id": "4",
        "Name": "Gluteeniton"
      }, {
        "Id": "7",
        "Name": "Kananmunaton"
      }],
      "Ingredients": [{
        "SubSectionHeading": "_",
        "SubSectionIngredients": [
          [{
            "Name": "Pirkka grilliananasta",
            "Amount": "500",
            "PackageRecipe": "false",
            "IngredientType": "16325",
            "Unit": "g",
            "IngredientTypeName": "Grilliananas"
          }],
          [{
            "Name": "Pirkka kuningaskatkarapuja (pakaste)",
            "Ean": "6410405131522",
            "Amount": "1",
            "AmountInfo": "(300 g)",
            "PackageRecipe": "false",
            "IngredientType": "5890",
            "Unit": "pkt",
            "IngredientTypeName": "Kuningaskatkarapu, pakaste"
          }],
          [{
            "Name": "Pirkka kampasimpukoita (pakaste)",
            "Ean": "6410405181848",
            "Amount": "1",
            "AmountInfo": "(150 g)",
            "PackageRecipe": "false",
            "IngredientType": "5681",
            "Unit": "pkt",
            "IngredientTypeName": "Kampasimpukka, pakaste"
          }],
          [{
            "Name": "tuoretta vihreää parsaa",
            "Amount": "225",
            "PackageRecipe": "false",
            "IngredientType": "6472",
            "Unit": "g",
            "IngredientTypeName": "Parsa, vihreä, tuore"
          }],
          [{
            "Name": "tuoretta tonnikalaa (MSC)",
            "Amount": "200",
            "PackageRecipe": "false",
            "IngredientType": "16410",
            "Unit": "g",
            "IngredientTypeName": "Tonnikala, tuore"
          }],
          [{
            "Name": "lohifileetä",
            "Amount": "200",
            "PackageRecipe": "false",
            "IngredientType": "6002",
            "Unit": "g",
            "IngredientTypeName": "Lohifilee"
          }]
        ]
      }, {
        "SubSectionHeading": "Valelukastike",
        "SubSectionIngredients": [
          [{
            "Name": "iso valkosipulinkynsi",
            "Amount": "1",
            "PackageRecipe": "false",
            "IngredientType": "15809",
            "Unit": "",
            "IngredientTypeName": "Valkosipuli, tuore"
          }],
          [{
            "Name": "ananasmehua",
            "Amount": "1",
            "PackageRecipe": "false",
            "IngredientType": "11273",
            "Unit": "dl",
            "IngredientTypeName": "Mehu, ananas"
          }],
          [{
            "Name": "Pirkka soijakastiketta",
            "Ean": "6410402008490",
            "Amount": "1/2",
            "PackageRecipe": "false",
            "IngredientType": "6953",
            "Unit": "dl",
            "IngredientTypeName": "Soijakastike"
          }],
          [{
            "Name": "limetin puristettua mehua",
            "Amount": "1",
            "AmountInfo": "(n. 2 rkl)",
            "PackageRecipe": "false",
            "IngredientType": "5997",
            "Unit": "",
            "IngredientTypeName": "Limetti"
          }],
          [{
            "Name": "sokeria",
            "Amount": "1 1/2",
            "PackageRecipe": "false",
            "IngredientType": "6961",
            "Unit": "rkl",
            "IngredientTypeName": "Sokeri"
          }],
          [{
            "Name": "tuoretta korianteria hienonnettuna",
            "Amount": "1",
            "PackageRecipe": "false",
            "IngredientType": "5852",
            "Unit": "rkl",
            "IngredientTypeName": "Korianteri, tuore"
          }],
          [{
            "Name": "seesamiöljyä",
            "Amount": "1/2",
            "PackageRecipe": "false",
            "IngredientType": "6873",
            "Unit": "rkl",
            "IngredientTypeName": "Seesamiöljy"
          }],
          [{
            "Name": "chilihiutaleita",
            "Amount": "1/4",
            "PackageRecipe": "false",
            "IngredientType": "7468",
            "Unit": "tl",
            "IngredientTypeName": "Chili, hiutale"
          }]
        ]
      }, {
        "SubSectionHeading": "Lisäksi",
        "SubSectionIngredients": [
          [{
            "Name": "puista pitkää grillivarrastikkua",
            "Amount": "5",
            "PackageRecipe": "false",
            "IngredientType": "7288",
            "Unit": "",
            "IngredientTypeName": "Varrastikku"
          }]
        ]
      }],
      "Instructions": "# Leikkaa varrastikut kahtia ja pane ne veteen likoamaan.\r\n# Leikkaa ananas reiluiksi kuutioiksi.\r\n# Sulata kuningaskatkaravut ja kampasimpukat pakkausten ohjeiden mukaan. Poista sulatetuista ravuista kuoret, päät ja suolet tai jätä halutessasi paikoilleen, jolloin liha pysyy grillatessa mehevämpänä.\r\n# Poista parsoista puumainen kantaosa napsauttamalla. Leikkaa parsat kolmeen osaan ja sipaise parsojen pintaan hieman öljyä.\r\n# Leikkaa tonnikala ja lohi 2 cm x 2 cm:n kokoisiksi kuutioiksi.\r\n# Pujota kaikki ainekset vuorotellen varrastikkuihin ja jätä sivuun.\r\n# Valmista valelukastike. Kuori ja hienonna valkosipulinkynsi ja sekoita keskenään kaikki marinadin ainekset.\r\n# Ota erikseen 1/3 valelukastiketta. Grillaa vartaat kypsiksi molemmin puolin grilliparilalla ja valele vartaita kastikkeella. Tarjoa säästetty valelukastike vartaiden kanssa.",
      "EndNote": "",
      "Description": "Yhdistä näyttäviin ja maukkaisiin vartaisiin eri kaloja ja äyriäisiä. Kala-äyriäisvartaat grillaantuvat hetkessä.",
      "DateCreated": "2019-04-17T14:53:13",
      "DateModified": "2019-08-07T13:53:10",
      "Stamp": {
        "Name": "Testattu K-koekeittiössä",
        "Id": "1"
      },
      "RecipeUse": {
        "Name": "k-ruoka.fi",
        "Id": "1"
      },
      "resourceType": "recipe",
      "Url": "https://www.k-ruoka.fi/reseptit/kala-ayriaisvartaat",
      "UrlSlug": "kala-ayriaisvartaat",
      "Sort": [1, 1, 10354]
    }, {
      "Id": "10353",
      "Name": "Nikkarin vuoka",
      "PieceSize": {
        "Unit": "g",
        "Amount": "350"
      },
      "Portions": {
        "Unit": "annosta",
        "Amount": "6"
      },
      "PreparationTime": {
        "Description": "30 - 60 min",
        "TimeRange": {
          "MinTime": 30,
          "MaxTime": 60
        }
      },
      "UserPortions": {
        "$": {
          "Unit": "henkilölle"
        }
      },
      "Categories": [{
        "MainName": "Pääruoat",
        "MainId": "4",
        "SubName": "Liharuoat",
        "SubId": "24"
      }, {
        "MainName": "Pääruoat",
        "MainId": "4",
        "SubName": "Uuniruoat",
        "SubId": "30"
      }, {
        "MainName": "Makuja maailmalta",
        "MainId": "2",
        "SubName": "Suomi",
        "SubId": "106"
      }, {
        "MainName": "Pääraaka-aine",
        "MainId": "14",
        "SubName": "Makkara",
        "SubId": "113"
      }, {
        "MainName": "Ruokalaji",
        "MainId": "16",
        "SubName": "Uuniruoat",
        "SubId": "141"
      }, {
        "MainName": "Teema",
        "MainId": "15",
        "SubName": "Mitä tänään syötäisiin?",
        "SubId": "144"
      }, {
        "MainName": "Asiakassegmentointi (K-asiakkaat)",
        "MainId": "17",
        "SubName": "Vakiintuneet",
        "SubId": "150"
      }, {
        "MainName": "Teema",
        "MainId": "15",
        "SubName": "K-Marketin valikoima",
        "SubId": "151"
      }, {
        "MainName": "Apukategoriat",
        "MainId": "18",
        "SubName": "K-Marketin valikoima",
        "SubId": "181"
      }],
      "Pictures": ["PI9_19_nikkarin_vuoka"],
      "PictureUrls": [{
        "Id": "PI9_19_nikkarin_vuoka",
        "Normal": "https://k-file-storage-qa.imgix.net/f/k-ruoka/recipe/10353?w=1000&h=1000&fit=clip",
        "Original": "https://k-file-storage-qa.imgix.net/f/k-ruoka/recipe/10353"
      }],
      "VideoUrl": "https://youtu.be/Kc-sHH8p__o",
      "VideoEmbedUrl": "<iframe width=\"560\" height=\"315\" src=\"https://www.youtube.com/embed/Kc-sHH8p__o\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>",
      "TrendWords": [{
        "Id": "2",
        "Name": "Kotimaiset ainekset"
      }],
      "SpecialDiets": [{
        "Id": "4",
        "Name": "Gluteeniton"
      }],
      "Ingredients": [{
        "SubSectionHeading": "_",
        "SubSectionIngredients": [
          [{
            "Name": "keitettyä perunaa",
            "Amount": "5-6",
            "AmountInfo": "(800 g)",
            "PackageRecipe": "false",
            "IngredientType": "7368",
            "Unit": "",
            "IngredientTypeName": "Yleisperuna"
          }],
          [{
            "Name": "keskikokoista sipulia",
            "Amount": "2",
            "AmountInfo": "(240 g)",
            "PackageRecipe": "false",
            "IngredientType": "6932",
            "Unit": "",
            "IngredientTypeName": "Sipuli"
          }],
          [{
            "Name": "tomaattia",
            "Amount": "4",
            "AmountInfo": "(500 g)",
            "PackageRecipe": "false",
            "IngredientType": "7124",
            "Unit": "",
            "IngredientTypeName": "Tomaatti"
          }],
          [{
            "Name": "HK Sininen lenkkimakkaraa",
            "Amount": "1",
            "AmountInfo": "(580 g)",
            "PackageRecipe": "false",
            "IngredientType": "5978",
            "Unit": "pkt",
            "IngredientTypeName": "Lenkkimakkara"
          }],
          [{
            "Name": "Pirkka ruokakermaa (15 %)",
            "Ean": "6410405093448",
            "Amount": "1",
            "AmountInfo": "(2 dl)",
            "PackageRecipe": "false",
            "IngredientType": "6775",
            "Unit": "tlk",
            "IngredientTypeName": "Ruokakerma, 15 %, laktoositon"
          }],
          [{
            "Name": "kananmunaa",
            "Amount": "2",
            "PackageRecipe": "false",
            "IngredientType": "5683",
            "Unit": "",
            "IngredientTypeName": "Kananmuna"
          }],
          [{
            "Name": "suolaa",
            "Amount": "1",
            "PackageRecipe": "false",
            "IngredientType": "7049",
            "Unit": "tl",
            "IngredientTypeName": "Suola"
          }],
          [{
            "Name": "mustapippuria",
            "Amount": "1/2",
            "PackageRecipe": "false",
            "IngredientType": "6269",
            "Unit": "tl",
            "IngredientTypeName": "Mustapippuri, kokonainen"
          }],
          [{
            "Name": "Pirkka emmental-mozzarellajuustoraastetta",
            "Ean": "6410405048561",
            "Amount": "1",
            "AmountInfo": "(150 g)",
            "PackageRecipe": "false",
            "IngredientType": "5600",
            "Unit": "ps",
            "IngredientTypeName": "Juustoraaste, emmental-mozzarella"
          }]
        ]
      }],
      "Instructions": "# Kuori ja viipaloi perunat sekä sipulit. Viipaloi myös tomaatit ja kuorittu lenkkimakkara.\r\n# Lado ainekset voideltuun uunivuokaan (noin 35x24 cm) limittäin.\r\n# Sekoita kerman joukkoon munat ja mausteet. Valuta seos vuokaan ja ripottele pinnalle juustoraaste.\r\n# Paista 200 asteisessa uunissa noin 35 minuuttia.",
      "EndNote": "*Annoshinnat on laskettu K-citymarket-ketjun hintatason mukaan 8/2019. Tuoretuotteiden hinnat vaihtelevat sesongeittain.",
      "Description": "Nikkarin vuokaan sopivat edelliseltä päivältä ylijääneet keitetyt perunat. Ruokaisuutta tuo lenkkimakkara. Nikkarin vuoka on klassikko jo syntyessään! Noin 1,45€/annos",
      "DateCreated": "2019-04-17T14:14:47",
      "DateModified": "2019-09-16T08:51:29",
      "TvDate": "2019-08-27T00:00:00",
      "Stamp": {
        "Name": "Testattu K-koekeittiössä",
        "Id": "1"
      },
      "RecipeUse": {
        "Name": "k-ruoka.fi",
        "Id": "1"
      },
      "resourceType": "recipe",
      "Url": "https://www.k-ruoka.fi/reseptit/nikkarin-vuoka",
      "UrlSlug": "nikkarin-vuoka",
      "Sort": [1, 1, 10353]
    }, {
      "Id": "10352",
      "Name": "Sieni-parmesaaniflammkuchen",
      "PieceSize": {
        "Unit": "g",
        "Amount": "146"
      },
      "Portions": {
        "Unit": "annosta",
        "Amount": "5"
      },
      "PreparationTime": {
        "Description": "30 - 60 min",
        "TimeRange": {
          "MinTime": 30,
          "MaxTime": 60
        }
      },
      "UserPortions": {
        "$": {
          "Unit": "henkilölle"
        }
      },
      "Categories": [{
        "MainName": "Makuja maailmalta",
        "MainId": "2",
        "SubName": "Muut maat",
        "SubId": "15"
      }, {
        "MainName": "Pääruoat",
        "MainId": "4",
        "SubName": "Kasvisruoat",
        "SubId": "22"
      }, {
        "MainName": "Suolaiset leivonnaiset",
        "MainId": "8",
        "SubName": "Pizzat",
        "SubId": "56"
      }, {
        "MainName": "Pääruoat",
        "MainId": "4",
        "SubName": "Pizzat",
        "SubId": "87"
      }, {
        "MainName": "Pääraaka-aine",
        "MainId": "14",
        "SubName": "Kasvis",
        "SubId": "115"
      }, {
        "MainName": "Sesonki",
        "MainId": "1",
        "SubName": "Sadonkorjuu",
        "SubId": "124"
      }, {
        "MainName": "Asiakassegmentointi (K-asiakkaat)",
        "MainId": "17",
        "SubName": "Tiedostavat",
        "SubId": "148"
      }, {
        "MainName": "Apukategoriat",
        "MainId": "18",
        "SubName": "Helppo",
        "SubId": "153"
      }, {
        "MainName": "Apukategoriat",
        "MainId": "18",
        "SubName": "Trendikäs ",
        "SubId": "156"
      }, {
        "MainName": "Sesonki",
        "MainId": "1",
        "SubName": "Oktoberfest",
        "SubId": "125"
      }, {
        "MainName": "Teema",
        "MainId": "15",
        "SubName": "Brunssi",
        "SubId": "131"
      }, {
        "MainName": "Ruokalaji",
        "MainId": "16",
        "SubName": "Pizzat",
        "SubId": "139"
      }, {
        "MainName": "Teema",
        "MainId": "15",
        "SubName": "Mitä tänään syötäisiin?",
        "SubId": "144"
      }, {
        "MainName": "Asiakassegmentointi (K-asiakkaat)",
        "MainId": "17",
        "SubName": "Nautiskelijat",
        "SubId": "146"
      }, {
        "MainName": "Asiakassegmentointi (K-asiakkaat)",
        "MainId": "17",
        "SubName": "Innostujat",
        "SubId": "147"
      }],
      "Pictures": ["PI9_19_sieni_parmesaani"],
      "PictureUrls": [{
        "Id": "PI9_19_sieni_parmesaani",
        "Normal": "https://k-file-storage-qa.imgix.net/f/k-ruoka/recipe/10352?w=1000&h=1000&fit=clip",
        "Original": "https://k-file-storage-qa.imgix.net/f/k-ruoka/recipe/10352"
      }],
      "SpecialDiets": [{
        "Id": "7",
        "Name": "Kananmunaton"
      }, {
        "Id": "6",
        "Name": "Kasvis (lakto-ovo)"
      }],
      "Ingredients": [{
        "SubSectionHeading": "Taikina",
        "SubSectionIngredients": [
          [{
            "Name": "Pirkka vehnäjauhoja",
            "Ean": "6410405193414",
            "Amount": "3 3/4",
            "PackageRecipe": "false",
            "IngredientType": "7293",
            "Unit": "dl",
            "IngredientTypeName": "Vehnäjauho"
          }],
          [{
            "Name": "tattarijauhoja",
            "Amount": "1",
            "PackageRecipe": "false",
            "IngredientType": "7094",
            "Unit": "dl",
            "IngredientTypeName": "Tattarijauho"
          }],
          [{
            "Name": "suolaa",
            "Amount": "1",
            "PackageRecipe": "false",
            "IngredientType": "7049",
            "Unit": "tl",
            "IngredientTypeName": "Suola"
          }],
          [{
            "Name": "Pirkka ekstra-neitsytoliiviöljyä",
            "Ean": "6410402024759",
            "Amount": "3",
            "PackageRecipe": "false",
            "IngredientType": "5405",
            "Unit": "rkl",
            "IngredientTypeName": "Ekstra neitsytoliiviöljy"
          }],
          [{
            "Name": "vettä",
            "Amount": "1 1/2",
            "PackageRecipe": "false",
            "IngredientType": "15792",
            "Unit": "dl",
            "IngredientTypeName": "Vesi"
          }]
        ]
      }, {
        "SubSectionHeading": "Täyte",
        "SubSectionIngredients": [
          [{
            "Name": "Pirkka ruskeita herkkusieniä (tuore)",
            "Ean": "6410405123367",
            "Amount": "1",
            "AmountInfo": "(200 g)",
            "PackageRecipe": "false",
            "IngredientType": "5516",
            "Unit": "rs",
            "IngredientTypeName": "Herkkusieni, tuore"
          }],
          [{
            "Name": "Crème Bonjour parmesaanituorejuustoa",
            "Amount": "1",
            "AmountInfo": "(200 g)",
            "PackageRecipe": "false",
            "Unit": "rs"
          }],
          [{
            "Name": "rosmariinin oksaa",
            "Amount": "3",
            "PackageRecipe": "false",
            "IngredientType": "6751",
            "Unit": "",
            "IngredientTypeName": "Rosmariini, tuore"
          }]
        ]
      }, {
        "SubSectionHeading": "Lisäksi",
        "SubSectionIngredients": [
          [{
            "Name": "Pirkka Parhaat parmesaania lastuina",
            "Ean": "6410405102959",
            "Amount": "1",
            "AmountInfo": "(n. 50 g)",
            "PackageRecipe": "false",
            "IngredientType": "6468",
            "Unit": "dl",
            "IngredientTypeName": "Parmesaani"
          }],
          [{
            "Name": "Pirkka Parhaat maustettua ekstra-neitsytoliiviöljyä, tryffeli",
            "Ean": "6410405172853",
            "Amount": "2",
            "PackageRecipe": "false",
            "Unit": "rkl"
          }],
          [{
            "Name": "mustapippuria",
            "PackageRecipe": "false",
            "IngredientType": "6167",
            "IngredientTypeName": "Maustemylly, mustapippuri"
          }]
        ]
      }],
      "Instructions": "# Sekoita isossa kulhossa keskenään jauhot, suola ja öljy. Lisää puolet vedestä haarukalla sekoittaen. Lisää loput vedestä erissä, kunnes rouhea taikina on muodostunut. Kumoa taikina jauhotetulle työpöydälle, lisää myös kulhoon jääneet jauhot. Vaivaa taikinaa noin minuutin verran, kunnes se on sileää ja kimmoisaa. Peitä taikina löyhästi leivinliinalla.\r\n# Pyyhi sienet puhtaiksi talouspaperilla, siisti kanta ja viipaloi sienet.\r\n# Kauli taikina soikeaksi levyksi (20 x 40 cm) ja nosta leivinpaperin päälle uunipellille. Voit myös tehdä kaksi pienempää flammkucheniä.\r\n# Levitä pinnalle tuorejuusto ja sienet. Nypi rosmariinista lehdet ja ripottele ne päälle.\r\n# Paista 250 asteessa noin 20 minuuttia. Lyhennä paistoaikaa, mikäli teet taikinasta kaksi flammkuchenia.\r\n# Viimeistele pinta parmesaanilastuilla ja tryffelillä maustetulla öljyllä. Rouhi päälle mustapippuria. Flammkuchen on parhaimmillaan samana päivänä tarjottuna.",
      "EndNote": "Voit käyttää herkkusienten tilalla myös tuoreita suppilovahveroita. Paista sienet nopeasti pannulla ensin, jotta enin sienissä oleva kosteus haihtuu. *Annoshinnat on laskettu K-citymarket-ketjun hintatason mukaan 8/2019. Tuoretuotteiden hinnat vaihtelevat sesongeittain.",
      "Description": "Flammkuchen on kotoisin Saksasta ja se muistuttaa ohutta pizzaa. Parmesaanituorejuusto ja sienet tekevät tästä herkun, joka viimeistellään tryffeliöljyllä. Noin 1,70€/annos",
      "DateCreated": "2019-04-17T14:12:09",
      "DateModified": "2019-09-16T08:51:28",
      "TvDate": "2019-09-26T00:00:00",
      "Stamp": {
        "Name": "Testattu K-koekeittiössä",
        "Id": "1"
      },
      "RecipeUse": {
        "Name": "k-ruoka.fi",
        "Id": "1"
      },
      "resourceType": "recipe",
      "Url": "https://www.k-ruoka.fi/reseptit/sieni-parmesaaniflammkuchen",
      "UrlSlug": "sieni-parmesaaniflammkuchen",
      "Sort": [1, 1, 10352]
    }, {
      "Id": "10350",
      "Name": "Nyhtökaurapiirakka pellillä",
      "PieceSize": {
        "Unit": "g",
        "Amount": "96"
      },
      "Portions": {
        "Unit": "annosta",
        "Amount": "24"
      },
      "PreparationTime": {
        "Description": "30 - 60 min",
        "TimeRange": {
          "MinTime": 30,
          "MaxTime": 60
        }
      },
      "UserPortions": {
        "$": {
          "Unit": "henkilölle"
        }
      },
      "Categories": [{
        "MainName": "Sesonki",
        "MainId": "1",
        "SubName": "Vappu",
        "SubId": "3"
      }, {
        "MainName": "Pääruoat",
        "MainId": "4",
        "SubName": "Kasvisruoat",
        "SubId": "22"
      }, {
        "MainName": "Suolaiset leivonnaiset",
        "MainId": "8",
        "SubName": "Piirakat ja pasteijat",
        "SubId": "55"
      }, {
        "MainName": "Aamu-,väli- ja iltapalat",
        "MainId": "11",
        "SubName": "Piirakat",
        "SubId": "67"
      }, {
        "MainName": "Sesonki",
        "MainId": "1",
        "SubName": "Kodin juhlat: leivonnaiset",
        "SubId": "76"
      }, {
        "MainName": "Sesonki",
        "MainId": "1",
        "SubName": "Lasten reseptit",
        "SubId": "77"
      }, {
        "MainName": "Asiakassegmentointi (K-asiakkaat)",
        "MainId": "17",
        "SubName": "Innostujat",
        "SubId": "147"
      }, {
        "MainName": "Asiakassegmentointi (K-asiakkaat)",
        "MainId": "17",
        "SubName": "Tiedostavat",
        "SubId": "148"
      }, {
        "MainName": "Asiakassegmentointi (K-asiakkaat)",
        "MainId": "17",
        "SubName": "Mukavuudenhaluiset ",
        "SubId": "149"
      }, {
        "MainName": "Asiakassegmentointi (K-asiakkaat)",
        "MainId": "17",
        "SubName": "Vakiintuneet",
        "SubId": "150"
      }, {
        "MainName": "Sesonki",
        "MainId": "1",
        "SubName": "Uusivuosi",
        "SubId": "81"
      }, {
        "MainName": "Teema",
        "MainId": "15",
        "SubName": "Brunssi",
        "SubId": "131"
      }, {
        "MainName": "Teema",
        "MainId": "15",
        "SubName": "Lasten reseptit",
        "SubId": "133"
      }, {
        "MainName": "Pääraaka-aine",
        "MainId": "14",
        "SubName": "Kasviproteiini",
        "SubId": "135"
      }, {
        "MainName": "Teema",
        "MainId": "15",
        "SubName": "Mitä tänään syötäisiin?",
        "SubId": "144"
      }, {
        "MainName": "Asiakassegmentointi (K-asiakkaat)",
        "MainId": "17",
        "SubName": "Nautiskelijat",
        "SubId": "146"
      }],
      "Pictures": ["KR5_19_nyhtokaurapiir"],
      "PictureUrls": [{
        "Id": "KR5_19_nyhtokaurapiir",
        "Normal": "https://k-file-storage-qa.imgix.net/f/k-ruoka/recipe/10350?w=1000&h=1000&fit=clip",
        "Original": "https://k-file-storage-qa.imgix.net/f/k-ruoka/recipe/10350"
      }],
      "VideoUrl": "https://youtu.be/Iv1XwDmMsHk",
      "VideoEmbedUrl": "<iframe width=\"1136\" height=\"639\" src=\"https://www.youtube.com/embed/Iv1XwDmMsHk\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>",
      "SpecialDiets": [{
        "Id": "6",
        "Name": "Kasvis (lakto-ovo)"
      }],
      "Ingredients": [{
        "SubSectionHeading": "_",
        "SubSectionIngredients": [
          [{
            "Name": "Pirkka piirakkataikinaa (pakaste)",
            "Ean": "6410405215291",
            "Amount": "2",
            "AmountInfo": "(à 400 g)",
            "PackageRecipe": "false",
            "IngredientType": "6549",
            "Unit": "pkt",
            "IngredientTypeName": "Piirakkataikina, pakaste"
          }]
        ]
      }, {
        "SubSectionHeading": "Täyte",
        "SubSectionIngredients": [
          [{
            "Name": "paprikaa",
            "Amount": "2",
            "AmountInfo": "(400 g)",
            "PackageRecipe": "false",
            "IngredientType": "6448",
            "Unit": "",
            "IngredientTypeName": "Paprika, punainen"
          }],
          [{
            "Name": "rypsiöljyä",
            "Amount": "2",
            "PackageRecipe": "false",
            "IngredientType": "6807",
            "Unit": "rkl",
            "IngredientTypeName": "Rypsiöljy"
          }],
          [{
            "Name": "Gold&Green Nyhtökauraa, tomaatti",
            "Ean": "6430061140515",
            "Amount": "2",
            "AmountInfo": "(à 240 g)",
            "PackageRecipe": "false",
            "IngredientType": "16160",
            "Unit": "pkt",
            "IngredientTypeName": "Gold&Green nyhtökaura, tomaatti"
          }],
          [{
            "Name": "Pirkka tacomausteseosta",
            "Ean": "6410405203267",
            "Amount": "1",
            "AmountInfo": "(28 g)",
            "PackageRecipe": "false",
            "IngredientType": "6182",
            "Unit": "ps",
            "IngredientTypeName": "Mausteseos, taco"
          }],
          [{
            "Name": "Arla fraîche kermaa 30 %",
            "Amount": "2",
            "AmountInfo": "(à 2 dl)",
            "PackageRecipe": "false",
            "Unit": "prk"
          }],
          [{
            "Name": "korianteria",
            "Amount": "1",
            "PackageRecipe": "false",
            "IngredientType": "5852",
            "Unit": "ruukku",
            "IngredientTypeName": "Korianteri, tuore"
          }],
          [{
            "Name": "Arla Muhkea mozzarellaraastetta",
            "Amount": "300",
            "PackageRecipe": "false",
            "Unit": "g"
          }]
        ]
      }],
      "Instructions": "# Painele sulanut taikina leivinpaperin päälle korkeareunaisen uunipellin pohjalle ja reunoille. Esikypsennä piirakkapohjaa 200-asteisessa uunissa 20 minuuttia.\r\n# Kuutioi paprikat pieneksi. Kuumenna öljy kasarissa. Kuullota paprikaa muutama minuutti. Lisää nyhtökaura, tacomauste ja kerma joukkoon. Hauduta noin 5 minuuttia, kunnes koostumus paksuuntuu. Hienonna korianteri joukkoon.\r\n# Levitä täyte piirakkapohjalle. Ripottele juusto päälle. Kypsennä uunissa noin 15 minuuttia, kunnes pohja on kypsä ja juusto hieman ruskistunut.\r\n# Paloittele ja tarjoa lämpimänä tai jäähtyneenä.",
      "EndNote": "Vinkki! Voit korvata Arla fraîche kerman 2 purkilla (à 150 g) ranskankermaa. *Annoshinnat on laskettu K-citymarket-ketjun hintatason mukaan 8/2019. Tuoretuotteiden hinnat vaihtelevat sesongeittain.",
      "Description": "Superhelppo suolainen piirakka maistuu jokaiselle vieraalle ja myös lapsille. Täytteenä on meheväksi haudutettua Nyhtökauraa. Isosta peltipiirakasta riittää yli 20 juhlijalle. Noin 0,85€/annos",
      "DateCreated": "2019-04-17T12:22:17",
      "DateModified": "2019-09-29T14:51:04",
      "TvDate": "2019-10-02T00:00:00",
      "Stamp": {
        "Name": "Testattu K-koekeittiössä",
        "Id": "1"
      },
      "RecipeUse": {
        "Name": "k-ruoka.fi",
        "Id": "1"
      },
      "resourceType": "recipe",
      "Url": "https://www.k-ruoka.fi/reseptit/nyhtokaurapiirakka-pellilla",
      "UrlSlug": "nyhtokaurapiirakka-pellilla",
      "Sort": [1, 1, 10350]
    }, {
      "Id": "10349",
      "Name": "Kalapihvit, paistettu pinaatti ja katajanmarjakastike",
      "PieceSize": {
        "Unit": "g",
        "Amount": "363"
      },
      "Portions": {
        "Unit": "annosta",
        "Amount": "4"
      },
      "PreparationTime": {
        "Description": "30 - 60 min",
        "TimeRange": {
          "MinTime": 30,
          "MaxTime": 60
        }
      },
      "UserPortions": {
        "$": {
          "Unit": "henkilölle"
        }
      },
      "Categories": [{
        "MainName": "Asiakassegmentointi (K-asiakkaat)",
        "MainId": "17",
        "SubName": "Vakiintuneet",
        "SubId": "150"
      }, {
        "MainName": "Makuja maailmalta",
        "MainId": "2",
        "SubName": "Suomi",
        "SubId": "106"
      }, {
        "MainName": "Pääraaka-aine",
        "MainId": "14",
        "SubName": "Kala",
        "SubId": "116"
      }, {
        "MainName": "Sesonki",
        "MainId": "1",
        "SubName": "Kesä",
        "SubId": "121"
      }, {
        "MainName": "Teema",
        "MainId": "15",
        "SubName": "Mitä tänään syötäisiin?",
        "SubId": "144"
      }, {
        "MainName": "Asiakassegmentointi (K-asiakkaat)",
        "MainId": "17",
        "SubName": "Tiedostavat",
        "SubId": "148"
      }, {
        "MainName": "Asiakassegmentointi (K-asiakkaat)",
        "MainId": "17",
        "SubName": "Mukavuudenhaluiset ",
        "SubId": "149"
      }, {
        "MainName": "Pääruoat",
        "MainId": "4",
        "SubName": "Kalaruoat",
        "SubId": "20"
      }, {
        "MainName": "Lisäkkeet",
        "MainId": "6",
        "SubName": "Kylmät kastikkeet ja dipit",
        "SubId": "89"
      }, {
        "MainName": "Makuja maailmalta",
        "MainId": "2",
        "SubName": "Skandinavia",
        "SubId": "105"
      }],
      "Pictures": ["KR5_19_kalapihvit_paist"],
      "PictureUrls": [{
        "Id": "KR5_19_kalapihvit_paist",
        "Normal": "https://k-file-storage-qa.imgix.net/f/k-ruoka/recipe/10349?w=1000&h=1000&fit=clip",
        "Original": "https://k-file-storage-qa.imgix.net/f/k-ruoka/recipe/10349"
      }],
      "VideoUrl": "https://youtu.be/HzrVGse4T_o",
      "VideoEmbedUrl": "<iframe width=\"1136\" height=\"639\" src=\"https://www.youtube.com/embed/HzrVGse4T_o\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>",
      "Ingredients": [{
        "SubSectionHeading": "_",
        "SubSectionIngredients": [
          [{
            "Name": "Pirkka saaristolaiskalapihvejä, pinaatti (pakaste)",
            "Amount": "2",
            "AmountInfo": "(à 200 g)",
            "PackageRecipe": "false",
            "Unit": "pkt"
          }],
          [{
            "Name": "oliiviöljyä",
            "Amount": "2",
            "PackageRecipe": "false",
            "IngredientType": "5405",
            "Unit": "rkl",
            "IngredientTypeName": "Ekstra neitsytoliiviöljy"
          }],
          [{
            "Name": "valkosipulinkynttä",
            "Amount": "3",
            "PackageRecipe": "false",
            "IngredientType": "7262",
            "Unit": "",
            "IngredientTypeName": "Valkosipuli"
          }],
          [{
            "Name": "Pirkka Luomu babypinaattia",
            "Amount": "1",
            "AmountInfo": "(65 g)",
            "PackageRecipe": "false",
            "IngredientType": "12738",
            "Unit": "ps",
            "IngredientTypeName": "Pinaatti, baby, luomu"
          }],
          [{
            "Name": "perunoita",
            "Amount": "700",
            "PackageRecipe": "false",
            "IngredientType": "7368",
            "Unit": "g",
            "IngredientTypeName": "Yleisperuna"
          }]
        ]
      }, {
        "SubSectionHeading": "Katajanmarjakastike",
        "SubSectionIngredients": [
          [{
            "Name": "Pirkka laktoositonta kermaviiliä",
            "Ean": "6410405140586",
            "Amount": "1",
            "AmountInfo": "(200 g)",
            "PackageRecipe": "false",
            "IngredientType": "5778",
            "Unit": "prk",
            "IngredientTypeName": "Kermaviili, laktoositon"
          }],
          [{
            "Name": "oliiviöljyä",
            "Amount": "1",
            "PackageRecipe": "false",
            "IngredientType": "5405",
            "Unit": "rkl",
            "IngredientTypeName": "Ekstra neitsytoliiviöljy"
          }],
          [{
            "Name": "sitruunanmehua",
            "Amount": "1/2",
            "PackageRecipe": "false",
            "IngredientType": "6941",
            "Unit": "rkl",
            "IngredientTypeName": "Sitruuna"
          }],
          [{
            "Name": "suolaa",
            "PackageRecipe": "false",
            "IngredientType": "7049",
            "Unit": "ripaus",
            "IngredientTypeName": "Suola"
          }],
          [{
            "Name": "katajanmarjaa",
            "Amount": "7-10",
            "PackageRecipe": "false",
            "IngredientType": "5732",
            "Unit": "",
            "IngredientTypeName": "Katajanmarja"
          }]
        ]
      }],
      "Instructions": "# Keitä perunat kypsiksi, noin 30 minuuttia.\r\n# Tee kastike. Jauha katajanmarjat morttelissa. Yhdistä kastikkeen ainekset. Kastike maustuu voimakkaammaksi, jos on aikaa antaa sen tekeytyä tunti pari jääkaapissa.\r\n# Kuumenna öljy isossa paistinpannussa. Kuori ja halkaise valkosipulinkynnet ja lisää pannuun. Paista kalapihvit kypsiksi, 2-3 minuuttia molemmin puolin. Kasaa pihvit pannun toiseen reunaan. Poista valkosipulit pannusta.\r\n# Lisää tilkka öljyä ja pyöräytä pinaatti nopeasti pannussa. Mausta suolalla.\r\n# Tarjoa kalapihvit perunoiden, pinaatin ja katajanmarjakastikkeen kanssa.",
      "EndNote": "Kastike maustuu voimakkaammaksi, jos annat sen maustua tunnin, pari jääkaapissa. *Annoshinnat on laskettu K-citymarket-ketjun hintatason mukaan 8/2019. Tuoretuotteiden hinnat vaihtelevat sesongeittain.",
      "Description": "Katanmarjalla maustettu kermaviilikastike on yllättävä ja raikas pari kalapihveille. Kun paistat pihvit valkosipuliöljyssä, saat pinnasta rapean ja maukkaan. Noin 2,40€/annos",
      "DateCreated": "2019-04-17T12:10:24",
      "DateModified": "2019-10-16T08:48:07",
      "TvDate": "2019-10-22T00:00:00",
      "Stamp": {
        "Name": "Testattu K-koekeittiössä",
        "Id": "1"
      },
      "RecipeUse": {
        "Name": "k-ruoka.fi",
        "Id": "1"
      },
      "resourceType": "recipe",
      "Url": "https://www.k-ruoka.fi/reseptit/kalapihvit-paistettu-pinaatti-ja-katajanmarjakastike",
      "UrlSlug": "kalapihvit-paistettu-pinaatti-ja-katajanmarjakastike",
      "Sort": [1, 1, 10349]
    }, {
      "Id": "10348",
      "Name": "Kokonainen nieriä grillissä ja grillattu sieni-perunasalaatti",
      "PieceSize": {
        "Unit": "g",
        "Amount": "409"
      },
      "Portions": {
        "Unit": "annosta",
        "Amount": "4"
      },
      "PreparationTime": {
        "Description": "yli 60 min",
        "TimeRange": {
          "MinTime": 60
        }
      },
      "UserPortions": {
        "$": {
          "Unit": "henkilölle"
        }
      },
      "Categories": [{
        "MainName": "Sesonki",
        "MainId": "1",
        "SubName": "Grillaus",
        "SubId": "5"
      }, {
        "MainName": "Pääruoat",
        "MainId": "4",
        "SubName": "Kalaruoat",
        "SubId": "20"
      }, {
        "MainName": "Pääruoat",
        "MainId": "4",
        "SubName": "Grilliruoat",
        "SubId": "73"
      }, {
        "MainName": "Makuja maailmalta",
        "MainId": "2",
        "SubName": "Japani",
        "SubId": "103"
      }, {
        "MainName": "Makuja maailmalta",
        "MainId": "2",
        "SubName": "Suomi",
        "SubId": "106"
      }, {
        "MainName": "Pääraaka-aine",
        "MainId": "14",
        "SubName": "Kala",
        "SubId": "116"
      }, {
        "MainName": "Sesonki",
        "MainId": "1",
        "SubName": "Kesä",
        "SubId": "121"
      }, {
        "MainName": "Sesonki",
        "MainId": "1",
        "SubName": "Juhannus",
        "SubId": "122"
      }, {
        "MainName": "Asiakassegmentointi (K-asiakkaat)",
        "MainId": "17",
        "SubName": "Innostujat",
        "SubId": "147"
      }],
      "Pictures": ["KCM_Kokonainen_nieria_g"],
      "PictureUrls": [{
        "Id": "KCM_Kokonainen_nieria_g",
        "Normal": "https://k-file-storage-qa.imgix.net/f/k-ruoka/recipe/10348?w=1000&h=1000&fit=clip",
        "Original": "https://k-file-storage-qa.imgix.net/f/k-ruoka/recipe/10348"
      }],
      "SpecialDiets": [{
        "Id": "3",
        "Name": "Maidoton"
      }, {
        "Id": "4",
        "Name": "Gluteeniton"
      }, {
        "Id": "7",
        "Name": "Kananmunaton"
      }],
      "Ingredients": [{
        "SubSectionHeading": "_",
        "SubSectionIngredients": [
          [{
            "Name": "kokonainen perattu nieriä",
            "Amount": "1",
            "AmountInfo": "(1,3 kg)",
            "PackageRecipe": "false",
            "IngredientType": "6346",
            "Unit": "",
            "IngredientTypeName": "Nieriä"
          }],
          [{
            "Name": "seesaminsiemeniä koristeluun",
            "PackageRecipe": "false",
            "IngredientType": "6871",
            "IngredientTypeName": "Seesaminsiemen"
          }]
        ]
      }, {
        "SubSectionHeading": "Grillattu sieni-perunasalaatti",
        "SubSectionIngredients": [
          [{
            "Name": "keitettyjä, kylmiä uusia perunoita",
            "Amount": "500",
            "PackageRecipe": "false",
            "IngredientType": "6525",
            "Unit": "g",
            "IngredientTypeName": "Varhaisperuna"
          }],
          [{
            "Name": "portobellosientä",
            "Amount": "2",
            "AmountInfo": "(150 g)",
            "PackageRecipe": "false",
            "IngredientType": "16374",
            "Unit": "",
            "IngredientTypeName": "Sieni, Portobello"
          }],
          [{
            "Name": "Pirkka Luomu babypinaattia",
            "Amount": "1/2",
            "AmountInfo": "(à 65 g)",
            "PackageRecipe": "false",
            "IngredientType": "12738",
            "Unit": "",
            "IngredientTypeName": "Pinaatti, baby, luomu"
          }],
          [{
            "Name": "suolaa",
            "Amount": "1",
            "PackageRecipe": "false",
            "IngredientType": "7049",
            "Unit": "tl",
            "IngredientTypeName": "Suola"
          }],
          [{
            "Name": "mustapippuria",
            "Amount": "1/2",
            "PackageRecipe": "false",
            "IngredientType": "6271",
            "Unit": "tl",
            "IngredientTypeName": "Mustapippuri, rouhittu"
          }]
        ]
      }, {
        "SubSectionHeading": "Yrtti-punasipulikastike",
        "SubSectionIngredients": [
          [{
            "Name": "punasipuli",
            "Amount": "1",
            "AmountInfo": "(60 g)",
            "PackageRecipe": "false",
            "IngredientType": "6667",
            "Unit": "",
            "IngredientTypeName": "Punasipuli"
          }],
          [{
            "Name": "lehtipersiljaa hienonnettuna",
            "Amount": "2",
            "PackageRecipe": "false",
            "IngredientType": "5954",
            "Unit": "rkl",
            "IngredientTypeName": "Lehtipersilja"
          }],
          [{
            "Name": "ruohosipulia hienonnettuna",
            "Amount": "2",
            "PackageRecipe": "false",
            "IngredientType": "6772",
            "Unit": "rkl",
            "IngredientTypeName": "Ruohosipuli, tuore"
          }],
          [{
            "Name": "rosmariinia hienonnettuna",
            "Amount": "1",
            "PackageRecipe": "false",
            "IngredientType": "6751",
            "Unit": "rkl",
            "IngredientTypeName": "Rosmariini, tuore"
          }],
          [{
            "Name": "öljyä",
            "Amount": "2",
            "PackageRecipe": "false",
            "IngredientType": "6807",
            "Unit": "rkl",
            "IngredientTypeName": "Rypsiöljy"
          }],
          [{
            "Name": "sokeria",
            "Amount": "1/2",
            "PackageRecipe": "false",
            "IngredientType": "6961",
            "Unit": "rkl",
            "IngredientTypeName": "Sokeri"
          }],
          [{
            "Name": "riisiviinietikkaa",
            "Amount": "2",
            "PackageRecipe": "false",
            "IngredientType": "6740",
            "Unit": "rkl",
            "IngredientTypeName": "Riisiviinietikka"
          }]
        ]
      }, {
        "SubSectionHeading": "Misokastike",
        "SubSectionIngredients": [
          [{
            "Name": "misotahnaa (misokeittoaineksista)",
            "Amount": "2",
            "PackageRecipe": "false",
            "IngredientType": "7423",
            "Unit": "rkl",
            "IngredientTypeName": "Misokeitto, ainekset"
          }],
          [{
            "Name": "valkoviiniä",
            "Amount": "2",
            "PackageRecipe": "false",
            "IngredientType": "7502",
            "Unit": "rkl",
            "IngredientTypeName": "Alkoholi"
          }],
          [{
            "Name": "mirin-kastiketta",
            "Amount": "2",
            "PackageRecipe": "false",
            "IngredientType": "16053",
            "Unit": "rkl",
            "IngredientTypeName": "Mirini, makea riisiviini"
          }],
          [{
            "Name": "sokeria",
            "Amount": "1",
            "PackageRecipe": "false",
            "IngredientType": "6961",
            "Unit": "rkl",
            "IngredientTypeName": "Sokeri"
          }]
        ]
      }],
      "Instructions": "# Laita nieriä grilliin. Käännä ja jatka grillaamista toiselta puolelta, kunnes kala on kypsää eli kalan nahka irtoaa lihasta helposti.\r\n# Valmista lämmin perunasalaatti. Pilko perunat ja sienet.\r\n# Valmista yrtti-punasipulikastike. Kuori ja hienonna sipuli. Sekoita kastikkeen ainekset keskenään. Jätä sivuun 30 minuutiksi.\r\n# Öljyä puhdas iso parila ja paista siinä perunoita ja sieniä käännellen, kunnes ne ovat saaneet rapsakan pinnan ja kauniin värin. Alenna lämpöä ja kääntele lopuksi sekaan pinaatti. Siirrä laakealle tarjoiluastialle. Lisää pinnalle yrtti-punasipulikastike. Pidä lämpimänä.\r\n# Yhdistä kaikki misokastikkeen ainekset.\r\n# Irrota grillissä kypsän nieriän pinnalta nahka. Sivele kalan pintaan misoseosta. Viimeistele pinta paahdetuilla seesaminsiemenillä. Sulje grilli ja paista muutama minuutti täydellä teholla.\r\n# Tarjoa loppu misokastike kalan ja perunasalaatin kanssa.",
      "EndNote": "",
      "Description": "Kokonainen nieriä on helppo grillata. Tarjoa nieriä grillatun sieni-perunasalaatin ja misokastikkeen kanssa.",
      "DateCreated": "2019-04-17T11:23:55",
      "DateModified": "2019-04-25T12:14:09",
      "Stamp": {
        "Name": "Testattu K-koekeittiössä",
        "Id": "1"
      },
      "RecipeUse": {
        "Name": "k-ruoka.fi",
        "Id": "1"
      },
      "resourceType": "recipe",
      "Url": "https://www.k-ruoka.fi/reseptit/kokonainen-nieria-grillissa-ja-grillattu-sieni-perunasalaatti",
      "UrlSlug": "kokonainen-nieria-grillissa-ja-grillattu-sieni-perunasalaatti",
      "Sort": [1, 1, 10348]
    }, {
      "Id": "10346",
      "Name": "Grillattu lohimedaljonki ja valkoviini-salottisipulivoi",
      "PieceSize": {
        "Unit": "g",
        "Amount": "266"
      },
      "Portions": {
        "Unit": "annosta",
        "Amount": "4"
      },
      "PreparationTime": {
        "Description": "yli 60 min",
        "TimeRange": {
          "MinTime": 60
        }
      },
      "UserPortions": {
        "$": {
          "Unit": "henkilölle"
        }
      },
      "Categories": [{
        "MainName": "Sesonki",
        "MainId": "1",
        "SubName": "Grillaus",
        "SubId": "5"
      }, {
        "MainName": "Pääruoat",
        "MainId": "4",
        "SubName": "Kalaruoat",
        "SubId": "20"
      }, {
        "MainName": "Pääruoat",
        "MainId": "4",
        "SubName": "Grilliruoat",
        "SubId": "73"
      }, {
        "MainName": "Makuja maailmalta",
        "MainId": "2",
        "SubName": "Suomi",
        "SubId": "106"
      }, {
        "MainName": "Pääraaka-aine",
        "MainId": "14",
        "SubName": "Kala",
        "SubId": "116"
      }, {
        "MainName": "Sesonki",
        "MainId": "1",
        "SubName": "Kesä",
        "SubId": "121"
      }, {
        "MainName": "Sesonki",
        "MainId": "1",
        "SubName": "Juhannus",
        "SubId": "122"
      }, {
        "MainName": "Asiakassegmentointi (K-asiakkaat)",
        "MainId": "17",
        "SubName": "Innostujat",
        "SubId": "147"
      }, {
        "MainName": "Asiakassegmentointi (K-asiakkaat)",
        "MainId": "17",
        "SubName": "Mukavuudenhaluiset ",
        "SubId": "149"
      }],
      "Pictures": ["KCM_Grillattu_lohimedal"],
      "PictureUrls": [{
        "Id": "KCM_Grillattu_lohimedal",
        "Normal": "https://k-file-storage-qa.imgix.net/f/k-ruoka/recipe/10346?w=1000&h=1000&fit=clip",
        "Original": "https://k-file-storage-qa.imgix.net/f/k-ruoka/recipe/10346"
      }],
      "Ingredients": [{
        "SubSectionHeading": "_",
        "SubSectionIngredients": [
          [{
            "Name": "lohifileetä",
            "AmountInfo": "n. 700 g",
            "PackageRecipe": "false",
            "IngredientType": "6002",
            "IngredientTypeName": "Lohifilee"
          }],
          [{
            "Name": "suolaa",
            "Amount": "1/2",
            "PackageRecipe": "false",
            "IngredientType": "7049",
            "Unit": "tl",
            "IngredientTypeName": "Suola"
          }],
          [{
            "Name": "mustapippuria",
            "Amount": "1/4",
            "PackageRecipe": "false",
            "IngredientType": "6271",
            "Unit": "tl",
            "IngredientTypeName": "Mustapippuri, rouhittu"
          }]
        ]
      }, {
        "SubSectionHeading": "Soseutettu ja paistettu kyssäkaali",
        "SubSectionIngredients": [
          [{
            "Name": "keskikokoista kyssäkaalia",
            "Amount": "5",
            "AmountInfo": "(1,5 kg)",
            "PackageRecipe": "false",
            "IngredientType": "15791",
            "Unit": "",
            "IngredientTypeName": "Kyssäkaali"
          }],
          [{
            "Name": "sipuli",
            "Amount": "1",
            "AmountInfo": "(100 g)",
            "PackageRecipe": "false",
            "IngredientType": "6932",
            "Unit": "",
            "IngredientTypeName": "Sipuli"
          }],
          [{
            "Name": "lehtipersiljaa hienonnettuna",
            "Amount": "1",
            "PackageRecipe": "false",
            "IngredientType": "5954",
            "Unit": "dl",
            "IngredientTypeName": "Lehtipersilja"
          }],
          [{
            "Name": "voita",
            "Amount": "1",
            "PackageRecipe": "false",
            "IngredientType": "7343",
            "Unit": "rkl",
            "IngredientTypeName": "Voi"
          }],
          [{
            "Name": "vettä",
            "Amount": "8",
            "PackageRecipe": "false",
            "IngredientType": "15792",
            "Unit": "dl",
            "IngredientTypeName": "Vesi"
          }],
          [{
            "Name": "kasvisannosfondi",
            "Amount": "1",
            "PackageRecipe": "false",
            "IngredientType": "5226",
            "Unit": "",
            "IngredientTypeName": "Annosfondi, kasvis"
          }],
          [{
            "Name": "Pirkka laktoositonta kuohukermaa",
            "Ean": "6410405142610",
            "Amount": "1",
            "PackageRecipe": "false",
            "IngredientType": "5892",
            "Unit": "dl",
            "IngredientTypeName": "Kuohukerma, laktoositon"
          }],
          [{
            "Name": "sitruunankuorta raastettuna",
            "Amount": "2",
            "PackageRecipe": "false",
            "IngredientType": "15804",
            "Unit": "tl",
            "IngredientTypeName": "Sitruuna, tuore"
          }],
          [{
            "Name": "suolaa",
            "Amount": "1",
            "PackageRecipe": "false",
            "IngredientType": "7049",
            "Unit": "tl",
            "IngredientTypeName": "Suola"
          }],
          [{
            "Name": "mustapippuria",
            "Amount": "1/4",
            "PackageRecipe": "false",
            "IngredientType": "6271",
            "Unit": "tl",
            "IngredientTypeName": "Mustapippuri, rouhittu"
          }],
          [{
            "Name": "sitruunanmehua",
            "Amount": "2",
            "PackageRecipe": "false",
            "IngredientType": "6941",
            "Unit": "rkl",
            "IngredientTypeName": "Sitruuna"
          }],
          [{
            "Name": "öljyä",
            "Amount": "1/2",
            "PackageRecipe": "false",
            "IngredientType": "6807",
            "Unit": "rkl",
            "IngredientTypeName": "Rypsiöljy"
          }]
        ]
      }, {
        "SubSectionHeading": "Valkoviini-salottisipulivoi",
        "SubSectionIngredients": [
          [{
            "Name": "huoneenlämpöistä voita",
            "Amount": "125",
            "PackageRecipe": "false",
            "IngredientType": "7343",
            "Unit": "g",
            "IngredientTypeName": "Voi"
          }],
          [{
            "Name": "pieni salottisipuli",
            "Amount": "1/2",
            "AmountInfo": "(25 g)",
            "PackageRecipe": "false",
            "IngredientType": "6851",
            "Unit": "",
            "IngredientTypeName": "Salottisipuli"
          }],
          [{
            "Name": "öljyä",
            "Amount": "1",
            "PackageRecipe": "false",
            "IngredientType": "6807",
            "Unit": "rkl",
            "IngredientTypeName": "Rypsiöljy"
          }],
          [{
            "Name": "valkoviiniä",
            "Amount": "1",
            "PackageRecipe": "false",
            "IngredientType": "7502",
            "Unit": "dl",
            "IngredientTypeName": "Alkoholi"
          }],
          [{
            "Name": "Pirkka laktoositonta kuohukermaa",
            "Ean": "6410405142610",
            "Amount": "2",
            "PackageRecipe": "false",
            "IngredientType": "5892",
            "Unit": "rkl",
            "IngredientTypeName": "Kuohukerma, laktoositon"
          }],
          [{
            "Name": "suolaa",
            "Amount": "1/4",
            "PackageRecipe": "false",
            "IngredientType": "7049",
            "Unit": "tl",
            "IngredientTypeName": "Suola"
          }],
          [{
            "Name": "valkopippuria",
            "Amount": "1/4",
            "PackageRecipe": "false",
            "IngredientType": "7259",
            "Unit": "tl",
            "IngredientTypeName": "Valkopippuri, jauhettu"
          }]
        ]
      }],
      "Instructions": "# Valmista maustevoi. Ota voi huoneenlämpöön tuntia ennen valmistusta. Kuullota kuorittu ja hienonnettu salottisipuli pannulla öljyssä keskilämmöllä. Lisää viini ja kerma, anna kiehua, kunnes ne ovat lähes kokonaan haihtuneet. Nosta pannu sivuun ja lisää voita pieninä nokareina pannulle. Mausta suolalla ja pippurilla. Sekoita, kunnes voi on sulaa.\r\n# Kaada voiseos leivinpaperin keskelle. Vedä leivinpaperin yläreuna voin yli itseäsi kohti (voi jää paperin sisään). Pidä alemman paperin reunasta kiinni. Aseta lasta tai iso viivoitin voita vasten päällimmäiselle paperille. Työnnä lastaa itsestäsi poispäin, jolloin voista muodostuu tasainen tanko. Kääri paperi rullalle. Sulje paperin päät tiukasti. Anna kovettua pakastimessa.\r\n# Kuori kyssäkaalit ja sipuli. Hienonna persilja. Leikkaa 4 kyssäkaalia ja sipuli lohkoiksi. Suikaloi loppu kyssäkaali ohuiksi suikaleiksi juustohöylällä tai veitsellä ja leikkaa kaalisiivut vielä ohuemmiksi suikaleiksi.\r\n# Kuumenna kattilassa voi ja kuullota siinä kyssäkaali- ja sipulilohkot. Lisää vesi ja kasvisannosfondi. Keitä pehmeiksi ilman kantta eli vähintään 30 minuuttia. Soseuta seos ensin sauvasekoittimella. Painele sose siivilän läpi takaisin kattilaan. Lisää soseen joukkoon hienonnettu persilja, kuohukerma, 1 tl sitruunankuorta, 3/4 tl suolaa ja mustapippuria. Kiehauta ja pidä lämpimänä.\r\n# Kuullota öljyssä kyssäkaalisuikaleet. Lisää 1/4 tl suolaa, 1/4 tl sitruunankuorta ja sitruunanmehu. Nosta sivuun ja pidä lämpimänä.\r\n# Leikkaa lohifilee noin 4 cm:n paksuisiksi viipaleiksi. Taita palat medaljongeiksi leikkaamalla viilto jokaisen palan keskelle nahkaan asti. Taita viipaleet auki siten, että nahka jää alapuolelle. Paista medaljongit kuumalla öljytyllä parilalla grillissä molemmin puolin kauniin ruskeiksi ja kypsiksi noin 2 minuuttia per puoli. Mausta suolalla ja pippurilla.\r\n# Tarjoa lohimedaljongit maustevoin ja kyssäkaalisoseen ja -suikaleiden kanssa.",
      "EndNote": "",
      "Description": "Lohimedaljongit valmistuvat grillissä nopeasti. Tarjoa medaljongit maustevoin ja kyssäkaalisoseen ja -suikaleiden kanssa.",
      "DateCreated": "2019-04-17T10:48:02",
      "DateModified": "2019-04-25T13:40:18",
      "Stamp": {
        "Name": "Testattu K-koekeittiössä",
        "Id": "1"
      },
      "RecipeUse": {
        "Name": "k-ruoka.fi",
        "Id": "1"
      },
      "resourceType": "recipe",
      "Url": "https://www.k-ruoka.fi/reseptit/grillattu-lohimedaljonki-ja-valkoviini-salottisipulivoi",
      "UrlSlug": "grillattu-lohimedaljonki-ja-valkoviini-salottisipulivoi",
      "Sort": [1, 1, 10346]
    }, {
      "Id": "10345",
      "Name": "Possunkyljykset ja siideririsotto",
      "PieceSize": {
        "Unit": "g",
        "Amount": "395"
      },
      "Portions": {
        "Unit": "annosta",
        "Amount": "4"
      },
      "PreparationTime": {
        "Description": "30 - 60 min",
        "TimeRange": {
          "MinTime": 30,
          "MaxTime": 60
        }
      },
      "UserPortions": {
        "$": {
          "Unit": "henkilölle"
        }
      },
      "Categories": [{
        "MainName": "Teema",
        "MainId": "15",
        "SubName": "Mitä tänään syötäisiin?",
        "SubId": "144"
      }, {
        "MainName": "Asiakassegmentointi (K-asiakkaat)",
        "MainId": "17",
        "SubName": "Nautiskelijat",
        "SubId": "146"
      }, {
        "MainName": "Asiakassegmentointi (K-asiakkaat)",
        "MainId": "17",
        "SubName": "Vakiintuneet",
        "SubId": "150"
      }, {
        "MainName": "Pääruoat",
        "MainId": "4",
        "SubName": "Liharuoat",
        "SubId": "24"
      }, {
        "MainName": "Pääraaka-aine",
        "MainId": "14",
        "SubName": "Liha",
        "SubId": "114"
      }, {
        "MainName": "Sesonki",
        "MainId": "1",
        "SubName": "Sadonkorjuu",
        "SubId": "124"
      }, {
        "MainName": "Sesonki",
        "MainId": "1",
        "SubName": "Oktoberfest",
        "SubId": "125"
      }, {
        "MainName": "Sesonki",
        "MainId": "1",
        "SubName": "Isänpäivä",
        "SubId": "127"
      }, {
        "MainName": "Ruokalaji",
        "MainId": "16",
        "SubName": "Pihvit",
        "SubId": "143"
      }, {
        "MainName": "Makuja maailmalta",
        "MainId": "2",
        "SubName": "Italia",
        "SubId": "10"
      }],
      "Pictures": ["PI9_19_possunkyljykset"],
      "PictureUrls": [{
        "Id": "PI9_19_possunkyljykset",
        "Normal": "https://k-file-storage-qa.imgix.net/f/k-ruoka/recipe/10345?w=1000&h=1000&fit=clip",
        "Original": "https://k-file-storage-qa.imgix.net/f/k-ruoka/recipe/10345"
      }],
      "VideoUrl": "https://youtu.be/h0AK5rgkNrg",
      "VideoEmbedUrl": "<iframe width=\"560\" height=\"315\" src=\"https://www.youtube.com/embed/h0AK5rgkNrg\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>",
      "Ingredients": [{
        "SubSectionHeading": "_",
        "SubSectionIngredients": [
          [{
            "Name": "luullista possunkyljystä",
            "Amount": "4",
            "AmountInfo": "(n. 400 g)",
            "PackageRecipe": "false",
            "IngredientType": "12934",
            "Unit": "",
            "IngredientTypeName": "Porsaan kyljys, maustamaton"
          }],
          [{
            "Name": "suolaa",
            "Amount": "1",
            "PackageRecipe": "false",
            "Unit": "tl"
          }],
          [{
            "Name": "mustapippuria",
            "Amount": "1/2",
            "PackageRecipe": "false",
            "IngredientType": "6269",
            "Unit": "tl",
            "IngredientTypeName": "Mustapippuri, kokonainen"
          }],
          [{
            "Name": "rypsiöljyä",
            "Amount": "1",
            "PackageRecipe": "false",
            "IngredientType": "6807",
            "Unit": "rkl",
            "IngredientTypeName": "Rypsiöljy"
          }],
          [{
            "Name": "salvianlehteä",
            "Amount": "15",
            "PackageRecipe": "false",
            "IngredientType": "6859",
            "Unit": "",
            "IngredientTypeName": "Salvia, tuore"
          }],
          [{
            "Name": "voita",
            "Amount": "2",
            "PackageRecipe": "false",
            "Unit": "rkl"
          }],
          [{
            "Name": "hapahko omena (kotimainen tai Jonagold)",
            "Amount": "1",
            "AmountInfo": "(200 g)",
            "PackageRecipe": "false",
            "IngredientType": "12045",
            "Unit": "",
            "IngredientTypeName": "Omena, punainen, kotimainen"
          }],
          [{
            "Name": "Pirkka Parhaat Aito Omena kuivaa omenasiideriä",
            "Ean": "6410405232564",
            "Amount": "1",
            "PackageRecipe": "false",
            "IngredientType": "6888",
            "Unit": "dl",
            "IngredientTypeName": "Siideri, omena, kuiva"
          }]
        ]
      }, {
        "SubSectionHeading": "Siideririsotto",
        "SubSectionIngredients": [
          [{
            "Name": "sipuli",
            "Amount": "1",
            "AmountInfo": "(80 g)",
            "PackageRecipe": "false",
            "IngredientType": "6932",
            "Unit": "",
            "IngredientTypeName": "Sipuli"
          }],
          [{
            "Name": "valkosipulinkynttä",
            "Amount": "2",
            "PackageRecipe": "false",
            "IngredientType": "7262",
            "Unit": "",
            "IngredientTypeName": "Valkosipuli"
          }],
          [{
            "Name": "rypsiöljyä",
            "Amount": "2",
            "PackageRecipe": "false",
            "IngredientType": "6807",
            "Unit": "rkl",
            "IngredientTypeName": "Rypsiöljy"
          }],
          [{
            "Name": "Pirkka risottoriisiä",
            "Amount": "3",
            "PackageRecipe": "false",
            "IngredientType": "6745",
            "Unit": "dl",
            "IngredientTypeName": "Risottoriisi"
          }],
          [{
            "Name": "Pirkka Parhaat Aito Omena kuivaa omenasiideriä",
            "Ean": "6410405232564",
            "Amount": "2 1/3",
            "PackageRecipe": "false",
            "IngredientType": "6888",
            "Unit": "dl",
            "IngredientTypeName": "Siideri, omena, kuiva"
          }],
          [{
            "Name": "vettä",
            "Amount": "6-7",
            "PackageRecipe": "false",
            "IngredientType": "15792",
            "Unit": "dl",
            "IngredientTypeName": "Vesi"
          }],
          [{
            "Name": "kanafondia",
            "Amount": "2 1/2",
            "PackageRecipe": "false",
            "IngredientType": "5439",
            "Unit": "rkl",
            "IngredientTypeName": "Fondi, kana"
          }],
          [{
            "Name": "Pirkka Parhaat parmesaania",
            "Ean": "6410405102959",
            "Amount": "50",
            "PackageRecipe": "false",
            "IngredientType": "6468",
            "Unit": "g",
            "IngredientTypeName": "Parmesaani"
          }]
        ]
      }],
      "Instructions": "# Ota kyljykset huoneenlämpöön 1 tunti ennen kypsennystä.\r\n# Valmista risotto. Tee kasvisliemi valmiiksi: kiehauta vesi ja mausta fondilla. Hienonna sipuli ja valkosipuli.\r\n# Kuumenna öljy kasarissa. Kuullota sipuleita muutama minuutti. Lisää riisi ja jatka kuullottamista pari minuuttia. Lisää siideri ja anna sen imeytyä riisiin. Lisää kuumaa kanalientä vähän kerrallaan. Anna edellisen erän imeytyä ennen kuin lisäät nestettä. Kypsennä riisi napakankypsäksi. Risoton kuuluu jäädä löysäksi.\r\n# Raasta parmesaani ja sekoita risottoon.\r\n# Risoton kypsyessä aloita kyljysten valmistus. Kuutioi omena. Mausta kyljykset reilusti suolalla ja pippurilla molemmin puolin. Työnnä paistolämpömittari yhteen kyljyksistä.\r\n# Kuumenna öljy paistinpannussa. Paista kyljyksiä 1 minuutti. Käännä ja paista 1 minuutti. Jatka näin pari minuuttia. Laske lämpöä ja lisää voi ja kokonaiset salvianlehdet pannuun. Jatka paistamista käännellen, kunnes kyljysten sisälämpötila on 66 astetta. Nosta kyljykset lautaselle lepäämään 5 minuutiksi. Sisälämpötila nousee 70 asteeseen, jolloin liha on kypsää. Voit lusikoida lautaselle valuneen nesteen annoksille.\r\n# Lisää kuutioitu omena pannuun ja paista pari minuuttia sekoitellen. Kaada siideri pannuun ja hauduta muutama minuutti.\r\n# Tarjoa kyljykset risoton ja omenoiden kanssa.",
      "EndNote": "*Annoshinnat on laskettu K-citymarket-ketjun hintatason mukaan 8/2019. Tuoretuotteiden hinnat vaihtelevat sesongeittain.",
      "Description": "Herkulliseen risottoon ei tarvita viiniä, sillä sopivasti syvyyttä saa myös siiderillä. Omenaiset possunkyljykset valmistuvat paistinpannussa pikana. Noin 4,05€/annos",
      "DateCreated": "2019-04-16T16:17:41",
      "DateModified": "2019-09-16T08:51:30",
      "TvDate": "2019-09-13T00:00:00",
      "Stamp": {
        "Name": "Testattu K-koekeittiössä",
        "Id": "1"
      },
      "RecipeUse": {
        "Name": "k-ruoka.fi",
        "Id": "1"
      },
      "resourceType": "recipe",
      "Url": "https://www.k-ruoka.fi/reseptit/possunkyljykset-ja-siideririsotto",
      "UrlSlug": "possunkyljykset-ja-siideririsotto",
      "Sort": [1, 1, 10345]
    }, {
      "Id": "10344",
      "Name": "Karamellisoitu sipuli-bratwurstipasta",
      "PieceSize": {
        "Unit": "g"
      },
      "Portions": {
        "Unit": "annosta",
        "Amount": "4"
      },
      "PreparationTime": {
        "Description": "30 - 60 min",
        "TimeRange": {
          "MinTime": 30,
          "MaxTime": 60
        }
      },
      "UserPortions": {
        "$": {
          "Unit": "henkilölle"
        }
      },
      "Categories": [{
        "MainName": "Pääruoat",
        "MainId": "4",
        "SubName": "Makkararuoat",
        "SubId": "25"
      }, {
        "MainName": "Pääruoat",
        "MainId": "4",
        "SubName": "Pastaruoat",
        "SubId": "31"
      }, {
        "MainName": "Pääraaka-aine",
        "MainId": "14",
        "SubName": "Makkara",
        "SubId": "113"
      }, {
        "MainName": "Sesonki",
        "MainId": "1",
        "SubName": "Sadonkorjuu",
        "SubId": "124"
      }, {
        "MainName": "Sesonki",
        "MainId": "1",
        "SubName": "Oktoberfest",
        "SubId": "125"
      }, {
        "MainName": "Ruokalaji",
        "MainId": "16",
        "SubName": "Pastat",
        "SubId": "138"
      }, {
        "MainName": "Asiakassegmentointi (K-asiakkaat)",
        "MainId": "17",
        "SubName": "Nautiskelijat",
        "SubId": "146"
      }, {
        "MainName": "Asiakassegmentointi (K-asiakkaat)",
        "MainId": "17",
        "SubName": "Mukavuudenhaluiset ",
        "SubId": "149"
      }, {
        "MainName": "Asiakassegmentointi (K-asiakkaat)",
        "MainId": "17",
        "SubName": "Vakiintuneet",
        "SubId": "150"
      }],
      "Pictures": ["PI9_19_karam_sipu_brat"],
      "PictureUrls": [{
        "Id": "PI9_19_karam_sipu_brat",
        "Normal": "https://k-file-storage-qa.imgix.net/f/k-ruoka/recipe/10344?w=1000&h=1000&fit=clip",
        "Original": "https://k-file-storage-qa.imgix.net/f/k-ruoka/recipe/10344"
      }],
      "Ingredients": [{
        "SubSectionHeading": "_",
        "SubSectionIngredients": [
          [{
            "Name": "Pirkka bratwurstia",
            "Ean": "6410402014323",
            "Amount": "1",
            "AmountInfo": "(250 g)",
            "PackageRecipe": "false",
            "IngredientType": "5276",
            "Unit": "pkt",
            "IngredientTypeName": "Bratwursti"
          }],
          [{
            "Name": "sipulia",
            "Amount": "4",
            "AmountInfo": "n. 400 g)",
            "PackageRecipe": "false",
            "IngredientType": "6932",
            "Unit": "",
            "IngredientTypeName": "Sipuli"
          }],
          [{
            "Name": "rypsiöljyä",
            "Amount": "2",
            "PackageRecipe": "false",
            "IngredientType": "6807",
            "Unit": "rkl",
            "IngredientTypeName": "Rypsiöljy"
          }],
          [{
            "Name": "tuoretta timjamia hienonnettuna",
            "Amount": "1/2",
            "PackageRecipe": "false",
            "IngredientType": "7119",
            "Unit": "dl",
            "IngredientTypeName": "Timjami, tuore"
          }],
          [{
            "Name": "sokeria",
            "Amount": "1",
            "PackageRecipe": "false",
            "IngredientType": "6961",
            "Unit": "tl",
            "IngredientTypeName": "Sokeri"
          }],
          [{
            "Name": "suolaa",
            "Amount": "1/2",
            "PackageRecipe": "false",
            "IngredientType": "7049",
            "Unit": "tl",
            "IngredientTypeName": "Suola"
          }],
          [{
            "Name": "mustapippuria",
            "Amount": "1/2",
            "PackageRecipe": "false",
            "IngredientType": "6271",
            "Unit": "tl",
            "IngredientTypeName": "Mustapippuri, rouhittu"
          }],
          [{
            "Name": "Pirkka spagettia",
            "Amount": "350",
            "PackageRecipe": "false",
            "IngredientType": "6989",
            "Unit": "g",
            "IngredientTypeName": "Spagetti"
          }],
          [{
            "Name": "Pirkka Parhaat cheddaria",
            "Ean": "6410405230904",
            "Amount": "1/2",
            "AmountInfo": "(à 200 g)",
            "PackageRecipe": "false",
            "IngredientType": "5349",
            "Unit": "pkt",
            "IngredientTypeName": "Cheddarjuusto"
          }]
        ]
      }],
      "Instructions": "# Viipaloi bratwurstit. Kuori ja viipaloi sipulit.\r\n# Kuumenna öljy suuressa paistinpannussa. Ruskista bratwursti. Lisää sipuli, timjami, sokeri, suola ja pippuri. Paista noin 20 minuuttia miedolla lämmöllä, kunnes sipuli on karamellisoitunut kullanruskeaksi. Älä päästä sipulia palamaan.\r\n# Keitä pasta suolalla maustetussa vedessä. Raasta cheddar, jätä 1 dl koristelua varten.\r\n# Lisää 2 dl pastan keitinvettä sipulipaistokseen. Valuta pasta ja sekoita sipulin joukkoon. Kääntele cheddar joukkoon.\r\n# Tarjoa pasta cheddarin kanssa ja rouhi pippuria päälle.",
      "EndNote": "*Annoshinnat on laskettu K-citymarket-ketjun hintatason mukaan 8/2019. Tuoretuotteiden hinnat vaihtelevat sesongeittain.",
      "Description": "Uskoisitko, spagetinkastikkeeseen voi upottaa liki puoli kiloa sipulia! Karamellimaisen makeaksi paistetut sipulit saavat suolaisiksi sattumiksi bratwurstia. Noin 1,40€/annos",
      "DateCreated": "2019-04-16T16:08:43",
      "DateModified": "2019-08-06T12:59:35",
      "Stamp": {
        "Name": "Testattu K-koekeittiössä",
        "Id": "1"
      },
      "RecipeUse": {
        "Name": "k-ruoka.fi",
        "Id": "1"
      },
      "resourceType": "recipe",
      "Url": "https://www.k-ruoka.fi/reseptit/karamellisoitu-sipuli-bratwurstipasta",
      "UrlSlug": "karamellisoitu-sipuli-bratwurstipasta",
      "Sort": [1, 1, 10344]
    }, {
      "Id": "10340",
      "Name": "Pestokalkkuna, sellerimuusi ja rucolaöljy",
      "PieceSize": {
        "Unit": "g"
      },
      "Portions": {
        "Unit": "annosta",
        "Amount": "4"
      },
      "PreparationTime": {
        "Description": "30 - 60 min",
        "TimeRange": {
          "MinTime": 30,
          "MaxTime": 60
        }
      },
      "UserPortions": {
        "$": {
          "Unit": "henkilölle"
        }
      },
      "Categories": [{
        "MainName": "Pääruoat",
        "MainId": "4",
        "SubName": "Broileri- ja linturuoat",
        "SubId": "21"
      }, {
        "MainName": "Lisäkkeet",
        "MainId": "6",
        "SubName": "Kasvislisäkkeet",
        "SubId": "42"
      }, {
        "MainName": "Pääraaka-aine",
        "MainId": "14",
        "SubName": "Broileri ja linnut",
        "SubId": "118"
      }, {
        "MainName": "Sesonki",
        "MainId": "1",
        "SubName": "Sadonkorjuu",
        "SubId": "124"
      }, {
        "MainName": "Teema",
        "MainId": "15",
        "SubName": "Mitä tänään syötäisiin?",
        "SubId": "144"
      }, {
        "MainName": "Asiakassegmentointi (K-asiakkaat)",
        "MainId": "17",
        "SubName": "Nautiskelijat",
        "SubId": "146"
      }, {
        "MainName": "Asiakassegmentointi (K-asiakkaat)",
        "MainId": "17",
        "SubName": "Innostujat",
        "SubId": "147"
      }, {
        "MainName": "Asiakassegmentointi (K-asiakkaat)",
        "MainId": "17",
        "SubName": "Vakiintuneet",
        "SubId": "150"
      }],
      "Pictures": ["KR5_19_pestokalkk_selle"],
      "PictureUrls": [{
        "Id": "KR5_19_pestokalkk_selle",
        "Normal": "https://k-file-storage-qa.imgix.net/f/k-ruoka/recipe/10340?w=1000&h=1000&fit=clip",
        "Original": "https://k-file-storage-qa.imgix.net/f/k-ruoka/recipe/10340"
      }],
      "VideoUrl": "https://youtu.be/2gWm5ZlK_uU",
      "VideoEmbedUrl": "<iframe width=\"560\" height=\"315\" src=\"https://www.youtube.com/embed/2gWm5ZlK_uU\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>",
      "SpecialDiets": [{
        "Id": "4",
        "Name": "Gluteeniton"
      }, {
        "Id": "7",
        "Name": "Kananmunaton"
      }],
      "Ingredients": [{
        "SubSectionHeading": "_",
        "SubSectionIngredients": [
          [{
            "Name": "Kariniemen kalkkunan sisäfileetä, pesto",
            "Amount": "1",
            "AmountInfo": "(n. 600 g)",
            "PackageRecipe": "false",
            "Unit": "pkt"
          }],
          [{
            "Name": "öljyä",
            "Amount": "2",
            "PackageRecipe": "false",
            "Unit": "rkl"
          }]
        ]
      }, {
        "SubSectionHeading": "Juurisellerimuusi",
        "SubSectionIngredients": [
          [{
            "Name": "juuriselleri",
            "Amount": "1",
            "AmountInfo": "(800 g)",
            "PackageRecipe": "false",
            "IngredientType": "15836",
            "Unit": "",
            "IngredientTypeName": "Juuriselleri, tuore"
          }],
          [{
            "Name": "Pirkka kuohukermaa",
            "Ean": "6410405142597",
            "Amount": "1",
            "AmountInfo": "(2 dl)",
            "PackageRecipe": "false",
            "IngredientType": "5891",
            "Unit": "tlk",
            "IngredientTypeName": "Kuohukerma"
          }],
          [{
            "Name": "Pirkka voita",
            "Ean": "6410405083197",
            "Amount": "25",
            "PackageRecipe": "false",
            "IngredientType": "7343",
            "Unit": "g",
            "IngredientTypeName": "Voi"
          }],
          [{
            "Name": "suolaa",
            "Amount": "1/4",
            "PackageRecipe": "false",
            "IngredientType": "7049",
            "Unit": "tl",
            "IngredientTypeName": "Suola"
          }]
        ]
      }, {
        "SubSectionHeading": "Rucolaöljy",
        "SubSectionIngredients": [
          [{
            "Name": "Pirkka Luomu rucolaa",
            "Amount": "1",
            "AmountInfo": "(65 g)",
            "PackageRecipe": "false",
            "IngredientType": "6753",
            "Unit": "ps",
            "IngredientTypeName": "Rucola"
          }],
          [{
            "Name": "valkosipulinkynsi",
            "Amount": "1",
            "PackageRecipe": "false",
            "IngredientType": "7262",
            "Unit": "",
            "IngredientTypeName": "Valkosipuli"
          }],
          [{
            "Name": "oliiviöljyä",
            "Amount": "3/4",
            "PackageRecipe": "false",
            "IngredientType": "6386",
            "Unit": "dl",
            "IngredientTypeName": "Oliiviöljy"
          }],
          [{
            "Name": "sitruunanmehua",
            "Amount": "1",
            "PackageRecipe": "false",
            "IngredientType": "6941",
            "Unit": "rkl",
            "IngredientTypeName": "Sitruuna"
          }],
          [{
            "Name": "sokeria",
            "Amount": "1",
            "PackageRecipe": "false",
            "IngredientType": "6961",
            "Unit": "tl",
            "IngredientTypeName": "Sokeri"
          }],
          [{
            "Name": "suolaa",
            "Amount": "1/4",
            "PackageRecipe": "false",
            "IngredientType": "7049",
            "Unit": "tl",
            "IngredientTypeName": "Suola"
          }]
        ]
      }],
      "Instructions": "# Tee muusi. Kuori selleri ja leikkaa muutaman sentin paloiksi. Keitä palat vähässä, suolalla maustetussa vedessä, kannen alla aivan pehmeiksi, 15-20 minuuttia. Valuta sellerinpalat.\r\n# Lisää kerma ja voi kattilaan sellerin joukkoon ja kuumenna kerma. Nosta kattila liedeltä. Soseuta selleri sauvasekoittimella aivan samettiseksi muusiksi. Mausta suolalla.\r\n# Valmista rucolaöljy soseuttamalla ainekset sauvasekoittimella, jätä kourallinen rucolaa annosten koristeeksi.\r\n# Kuumenna öljy paistinpannussa ja ruskista kalkkunafileet. Laske lämpö ja paista fileet kypsiksi, välillä käännellen noin 10 minuuttia. Leikkaa kalkkuna viipaleiksi tarjolle.\r\n# Tarjoa kalkkuna sellerimuusin ja rucolaöljyn kanssa.",
      "EndNote": "*Annoshinnat on laskettu K-citymarket-ketjun hintatason mukaan 8/2019. Tuoretuotteiden hinnat vaihtelevat sesongeittain.",
      "Description": "Kalkkunan sisäfilee tyylikäs lisuke on mukulaselleristä valmistettu muusi. Kermaisen muusin raikastaa kunnolla sitruunainen rucolapesto. Noin 4,60€/annos",
      "DateCreated": "2019-04-16T14:31:16",
      "DateModified": "2019-09-14T17:22:47",
      "TvDate": "2019-09-19T00:00:00",
      "Stamp": {
        "Name": "Testattu K-koekeittiössä",
        "Id": "1"
      },
      "RecipeUse": {
        "Name": "k-ruoka.fi",
        "Id": "1"
      },
      "resourceType": "recipe",
      "Url": "https://www.k-ruoka.fi/reseptit/pestokalkkuna-sellerimuusi-ja-rucolaoljy",
      "UrlSlug": "pestokalkkuna-sellerimuusi-ja-rucolaoljy",
      "Sort": [1, 1, 10340]
    }, {
      "Id": "10338",
      "Name": "Sitruunainen savulohipasta",
      "PieceSize": {
        "Unit": "g",
        "Amount": "380"
      },
      "Portions": {
        "Unit": "annosta",
        "Amount": "4"
      },
      "PreparationTime": {
        "Description": "alle 30 min",
        "TimeRange": {
          "MinTime": 15,
          "MaxTime": 30
        }
      },
      "UserPortions": {
        "$": {
          "Unit": "henkilölle"
        }
      },
      "Categories": [{
        "MainName": "Pääruoat",
        "MainId": "4",
        "SubName": "Kalaruoat",
        "SubId": "20"
      }, {
        "MainName": "Pääruoat",
        "MainId": "4",
        "SubName": "Pastaruoat",
        "SubId": "31"
      }, {
        "MainName": "Makuja maailmalta",
        "MainId": "2",
        "SubName": "Etelä- ja Väli-Amerikka",
        "SubId": "102"
      }, {
        "MainName": "Ruokalaji",
        "MainId": "16",
        "SubName": "Pastat",
        "SubId": "138"
      }],
      "Pictures": ["sitruunainensavu_19"],
      "PictureUrls": [{
        "Id": "sitruunainensavu_19",
        "Normal": "https://k-file-storage-qa.imgix.net/f/k-ruoka/recipe/10338?w=1000&h=1000&fit=clip",
        "Original": "https://k-file-storage-qa.imgix.net/f/k-ruoka/recipe/10338"
      }],
      "Ingredients": [{
        "SubSectionHeading": "_",
        "SubSectionIngredients": [
          [{
            "Name": "Pirkka täysjyväspagettia",
            "Ean": "6410405189554",
            "Amount": "250",
            "PackageRecipe": "false",
            "IngredientType": "6990",
            "Unit": "g",
            "IngredientTypeName": "Spagetti, täysjyvä"
          }],
          [{
            "Name": "kesäkurpitsaa",
            "Amount": "1/2",
            "AmountInfo": "(n. 200 g)",
            "PackageRecipe": "false",
            "IngredientType": "5781",
            "Unit": "",
            "IngredientTypeName": "Kesäkurpitsa"
          }],
          [{
            "Name": "sipuli",
            "Amount": "1",
            "AmountInfo": "(100 g)",
            "PackageRecipe": "false",
            "IngredientType": "6932",
            "Unit": "",
            "IngredientTypeName": "Sipuli"
          }],
          [{
            "Name": "valkosipulinkynsi",
            "Amount": "1",
            "PackageRecipe": "false",
            "IngredientType": "7262",
            "Unit": "",
            "IngredientTypeName": "Valkosipuli"
          }],
          [{
            "Name": "Pirkka savustettua kirjolohifileepalaa",
            "Ean": "6410405186843",
            "Amount": "1",
            "AmountInfo": "(250 g)",
            "PackageRecipe": "false",
            "IngredientType": "6868",
            "Unit": "pkt",
            "IngredientTypeName": "Savustettu kirjolohifileepala"
          }],
          [{
            "Name": "öljyä",
            "Amount": "1/2",
            "PackageRecipe": "false",
            "IngredientType": "6807",
            "Unit": "rkl",
            "IngredientTypeName": "Rypsiöljy"
          }],
          [{
            "Name": "sitruunanmehua",
            "Amount": "2",
            "PackageRecipe": "false",
            "IngredientType": "6941",
            "Unit": "rkl",
            "IngredientTypeName": "Sitruuna"
          }],
          [{
            "Name": "sitruunan raastettua kuorta",
            "Amount": "1 1/2",
            "PackageRecipe": "false",
            "IngredientType": "6941",
            "Unit": "tl",
            "IngredientTypeName": "Sitruuna"
          }],
          [{
            "Name": "suolaa",
            "Amount": "3/4",
            "PackageRecipe": "false",
            "Unit": "tl"
          }],
          [{
            "Name": "mustapippuria",
            "Amount": "1/2",
            "PackageRecipe": "false",
            "IngredientType": "6269",
            "Unit": "tl",
            "IngredientTypeName": "Mustapippuri, kokonainen"
          }],
          [{
            "Name": "Pirkka herneitä (pakaste)",
            "Amount": "1/2",
            "AmountInfo": "(à 200 g)",
            "PackageRecipe": "false",
            "IngredientType": "5524",
            "Unit": "ps",
            "IngredientTypeName": "Herne, pakaste"
          }],
          [{
            "Name": "Pirkka ruokakermaa",
            "Amount": "1",
            "AmountInfo": "(2 dl)",
            "PackageRecipe": "false",
            "IngredientType": "6774",
            "Unit": "tlk",
            "IngredientTypeName": "Ruokakerma, 15 %"
          }],
          [{
            "Name": "pastan keitinlientä",
            "Amount": "1",
            "PackageRecipe": "false",
            "Unit": "dl"
          }]
        ]
      }, {
        "SubSectionHeading": "Lisäksi",
        "SubSectionIngredients": [
          [{
            "Name": "Pirkka Parhaat parmesaania raastettuna",
            "Ean": "6410405102959",
            "PackageRecipe": "false",
            "IngredientType": "6468",
            "IngredientTypeName": "Parmesaani"
          }]
        ]
      }],
      "Instructions": "# Pane pastavesi kiehumaan. Keitä pasta pakkauksen ohjeen mukaan juuri ja juuri kypsäksi.\r\n# Leikkaa huuhdottu kesäkurpitsa pieniksi kuutioiksi. Hienonna kuorittu sipuli ja valkosipuli. Poista kirjolohesta nahka ja leikkaa kala reiluiksi kuutioiksi.\r\n# Kuullota sipulia ja valkosipulia öljyssä 2-3 minuuttia. Lisää joukkoon kesäkurpitsat ja sekoittele hetki.\r\n# Mausta sitruunanmehulla, -raasteella, suolalla ja pippurilla.\r\n# Lisää herneet, ruokakerma ja kalapalat. Kuumenna, mutta älä keitä. Ohenna kastike pastan keitinliemellä (noin 1 dl).\r\n# Siivilöi pasta ja sekoita kastikkeeseen. Ripottele pinnalle parmesaaniraastetta. Tarjoa lisäksi salaattia.",
      "EndNote": "",
      "Description": "Sitruunainen kirjolohipasta on väriltään kauniin keväinen. Mukavan maun pastaan tuovat herneet. Tällä lähtee isompikin nälkä.",
      "DateCreated": "2019-04-16T09:57:46",
      "DateModified": "2019-05-31T12:07:37",
      "Stamp": {
        "Name": "Testattu K-koekeittiössä",
        "Id": "1"
      },
      "RecipeUse": {
        "Name": "k-ruoka.fi",
        "Id": "1"
      },
      "resourceType": "recipe",
      "Url": "https://www.k-ruoka.fi/reseptit/sitruunainen-savulohipasta",
      "UrlSlug": "sitruunainen-savulohipasta",
      "Sort": [1, 1, 10338]
    }, {
      "Id": "10329",
      "Name": "Lehtikaali-perunapannukakut",
      "PieceSize": {
        "Unit": "g",
        "Amount": "210"
      },
      "Portions": {
        "Unit": "annosta",
        "Amount": "4"
      },
      "PreparationTime": {
        "Description": "30 - 60 min",
        "TimeRange": {
          "MinTime": 30,
          "MaxTime": 60
        }
      },
      "UserPortions": {
        "$": {
          "Unit": "henkilölle"
        }
      },
      "Categories": [{
        "MainName": "Pääruoat",
        "MainId": "4",
        "SubName": "Kasvisruoat",
        "SubId": "22"
      }, {
        "MainName": "Makuja maailmalta",
        "MainId": "2",
        "SubName": "Suomi",
        "SubId": "106"
      }, {
        "MainName": "Pääraaka-aine",
        "MainId": "14",
        "SubName": "Kasvis",
        "SubId": "115"
      }, {
        "MainName": "Teema",
        "MainId": "15",
        "SubName": "Mitä tänään syötäisiin?",
        "SubId": "144"
      }, {
        "MainName": "Asiakassegmentointi (K-asiakkaat)",
        "MainId": "17",
        "SubName": "Tiedostavat",
        "SubId": "148"
      }, {
        "MainName": "Asiakassegmentointi (K-asiakkaat)",
        "MainId": "17",
        "SubName": "Mukavuudenhaluiset ",
        "SubId": "149"
      }],
      "Pictures": ["KR5_19_lehtikaali_perun"],
      "PictureUrls": [{
        "Id": "KR5_19_lehtikaali_perun",
        "Normal": "https://k-file-storage-qa.imgix.net/f/k-ruoka/recipe/10329?w=1000&h=1000&fit=clip",
        "Original": "https://k-file-storage-qa.imgix.net/f/k-ruoka/recipe/10329"
      }],
      "VideoUrl": "https://youtu.be/PSdm9Vs8ZRQ",
      "VideoEmbedUrl": "<iframe width=\"560\" height=\"315\" src=\"https://www.youtube.com/embed/PSdm9Vs8ZRQ\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>",
      "SpecialDiets": [{
        "Id": "6",
        "Name": "Kasvis (lakto-ovo)"
      }],
      "Ingredients": [{
        "SubSectionHeading": "_",
        "SubSectionIngredients": [
          [{
            "Name": "Pirkka perunamuusijauhetta",
            "Ean": "6410400034248",
            "Amount": "1",
            "AmountInfo": "(105 g)",
            "PackageRecipe": "false",
            "IngredientType": "6532",
            "Unit": "ps",
            "IngredientTypeName": "Perunasoseaines, pakaste"
          }],
          [{
            "Name": "vettä",
            "Amount": "4 1/2",
            "PackageRecipe": "false",
            "IngredientType": "15792",
            "Unit": "dl",
            "IngredientTypeName": "Vesi"
          }],
          [{
            "Name": "Pirkka rypsiöljyä",
            "Amount": "4",
            "PackageRecipe": "false",
            "IngredientType": "6807",
            "Unit": "rkl",
            "IngredientTypeName": "Rypsiöljy"
          }],
          [{
            "Name": "Pirkka lehtikaalisuikaleita",
            "Ean": "6410405146786",
            "Amount": "1",
            "AmountInfo": "(125 g)",
            "PackageRecipe": "false",
            "IngredientType": "10442",
            "Unit": "ps",
            "IngredientTypeName": "Lehtikaali, suikaloitu"
          }],
          [{
            "Name": "Pirkka Luomu kananmuna",
            "Ean": "6408641114904",
            "Amount": "1",
            "PackageRecipe": "false",
            "IngredientType": "5685",
            "Unit": "",
            "IngredientTypeName": "Kananmuna, luomu"
          }],
          [{
            "Name": "vehnäjauhoja",
            "Amount": "3/4",
            "PackageRecipe": "false",
            "IngredientType": "7293",
            "Unit": "dl",
            "IngredientTypeName": "Vehnäjauho"
          }],
          [{
            "Name": "Pirkka Parhaat cheddarjuustoa raastettuna",
            "Amount": "100",
            "PackageRecipe": "false",
            "IngredientType": "5349",
            "Unit": "g",
            "IngredientTypeName": "Cheddarjuusto"
          }],
          [{
            "Name": "lehtipersijaa hienonnettuna",
            "Amount": "1/2",
            "PackageRecipe": "false",
            "Unit": "dl"
          }]
        ]
      }, {
        "SubSectionHeading": "Kermaviili-tahinikastike",
        "SubSectionIngredients": [
          [{
            "Name": "Pirkka laktoositonta kermaviiliä 10%",
            "Ean": "6410405140586",
            "Amount": "1",
            "AmountInfo": "(200 g)",
            "PackageRecipe": "false",
            "IngredientType": "5778",
            "Unit": "tlk",
            "IngredientTypeName": "Kermaviili, laktoositon"
          }],
          [{
            "Name": "Urtekram Luomu tahinia",
            "Amount": "2",
            "PackageRecipe": "false",
            "IngredientType": "14498",
            "Unit": "rkl",
            "IngredientTypeName": "Tahini, seesamitahna, luomu"
          }],
          [{
            "Name": "Pirkka ekstra-neitsytoliiviöljyä",
            "Ean": "6410402024759",
            "Amount": "2",
            "PackageRecipe": "false",
            "IngredientType": "5405",
            "Unit": "rkl",
            "IngredientTypeName": "Ekstra neitsytoliiviöljy"
          }],
          [{
            "Name": "(luomu)sitruunan kuorta raastettuna",
            "Amount": "1",
            "PackageRecipe": "false",
            "IngredientType": "6942",
            "Unit": "tl",
            "IngredientTypeName": "Sitruuna, luomu"
          }],
          [{
            "Name": "pieni valkosipulinkynsi raastettuna",
            "Amount": "1",
            "PackageRecipe": "false",
            "IngredientType": "7262",
            "Unit": "",
            "IngredientTypeName": "Valkosipuli"
          }],
          [{
            "Name": "Pirkka Luomu juoksevaa hunajaa",
            "Amount": "1",
            "PackageRecipe": "false",
            "IngredientType": "5550",
            "Unit": "tl",
            "IngredientTypeName": "Hunaja, juokseva"
          }],
          [{
            "Name": "ripaus suolaa",
            "PackageRecipe": "false",
            "IngredientType": "7049",
            "IngredientTypeName": "Suola"
          }]
        ]
      }],
      "Instructions": "# Valmista perunamuusi. Vatkaa jauhe kiehuvaan veteen, siirrä kattila liedeltä ja peitä kannella. Anna vetäytyä muutama minuutti ja sekoita sileäksi. Jäähdytä.\r\n# Sekoita kastikkeen ainekset keskenään ja siirrä jääkaappiin maustumaan.\r\n# Kuumenna öljy (1 rkl) pannulla ja lisää lehtikaalisuikaleet. Poista suikaleiden joukosta mahdolliset kovat kannat. Paista kaalit aivan pehmeiksi ja jäähdytä.\r\n# Sekoita jäähtyneen perunasoseen joukkoon muna, jauhot, kaalit ja juustoraaste. Lisää lopuksi lehtipersilja.\r\n# Kuumenna loppu öljy pannulla ja nostele kahden lusikan avulla massasta pannukakkuja pannulle. Paista keskilämmöllä noin 3 minuuttia/puoli. Tarjoa peruna-lehtikaalipannukakut kastikkeen kanssa.",
      "EndNote": "Jos käytät edelliseltä päivältä jäänyttä perunasosetta, niin lisää 1/2 l:n perunasosetta 1 dl vehnäjauhoja. *Annoshinnat on laskettu K-citymarket-ketjun hintatason mukaan 8/2019. Tuoretuotteiden hinnat vaihtelevat sesongeittain.",
      "Description": "Pehmeät peruna-lehtikaalipannukakut valmistetaan perunasoseesta. Voit käyttää ohjeeseen edelliseltä päivältä jäänyttä sosetta. Tarjoa raikkaan tahinikastikkeen kanssa. Noin 1,75€/annos",
      "DateCreated": "2019-04-15T10:41:53",
      "DateModified": "2019-09-14T17:21:29",
      "TvDate": "2019-09-18T00:00:00",
      "Stamp": {
        "Name": "Testattu K-koekeittiössä",
        "Id": "1"
      },
      "RecipeUse": {
        "Name": "k-ruoka.fi",
        "Id": "1"
      },
      "resourceType": "recipe",
      "Url": "https://www.k-ruoka.fi/reseptit/lehtikaali-perunapannukakut",
      "UrlSlug": "lehtikaali-perunapannukakut",
      "Sort": [1, 1, 10329]
    }, {
      "Id": "10317",
      "Name": "Nopea tonnikalapasta",
      "PieceSize": {
        "Unit": "g",
        "Amount": "325"
      },
      "Portions": {
        "Unit": "annosta",
        "Amount": "4"
      },
      "PreparationTime": {
        "Description": "alle 30 min",
        "TimeRange": {
          "MinTime": 15,
          "MaxTime": 30
        }
      },
      "UserPortions": {
        "$": {
          "Unit": "henkilölle"
        }
      },
      "Categories": [{
        "MainName": "Pääruoat",
        "MainId": "4",
        "SubName": "Pastaruoat",
        "SubId": "31"
      }, {
        "MainName": "Pääraaka-aine",
        "MainId": "14",
        "SubName": "Kala",
        "SubId": "116"
      }, {
        "MainName": "Ruokalaji",
        "MainId": "16",
        "SubName": "Pastat",
        "SubId": "138"
      }, {
        "MainName": "Teema",
        "MainId": "15",
        "SubName": "Mitä tänään syötäisiin?",
        "SubId": "144"
      }, {
        "MainName": "Asiakassegmentointi (K-asiakkaat)",
        "MainId": "17",
        "SubName": "Mukavuudenhaluiset ",
        "SubId": "149"
      }, {
        "MainName": "Asiakassegmentointi (K-asiakkaat)",
        "MainId": "17",
        "SubName": "Vakiintuneet",
        "SubId": "150"
      }, {
        "MainName": "Teema",
        "MainId": "15",
        "SubName": "K-Marketin valikoima",
        "SubId": "151"
      }, {
        "MainName": "Apukategoriat",
        "MainId": "18",
        "SubName": "K-Marketin valikoima",
        "SubId": "181"
      }],
      "Pictures": ["PI8_19_tonnikalapasta"],
      "PictureUrls": [{
        "Id": "PI8_19_tonnikalapasta",
        "Normal": "https://k-file-storage-qa.imgix.net/f/k-ruoka/recipe/10317?w=1000&h=1000&fit=clip",
        "Original": "https://k-file-storage-qa.imgix.net/f/k-ruoka/recipe/10317"
      }],
      "VideoUrl": "https://youtu.be/9RWHXXlp6co",
      "VideoEmbedUrl": "<iframe width=\"560\" height=\"315\" src=\"https://www.youtube.com/embed/9RWHXXlp6co\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>",
      "Ingredients": [{
        "SubSectionHeading": "_",
        "SubSectionIngredients": [
          [{
            "Name": "Rummo penne rigate -pastaa",
            "Amount": "350",
            "PackageRecipe": "false",
            "Unit": "g"
          }],
          [{
            "Name": "Pirkka tonnikalapaloja öljyssä",
            "Ean": "6410402010318",
            "Amount": "2",
            "AmountInfo": "(à 200 g/150 g)",
            "PackageRecipe": "false",
            "IngredientType": "7151",
            "Unit": "prk",
            "IngredientTypeName": "Tonnikala, öljyssä, säilyke"
          }],
          [{
            "Name": "kapriksia",
            "Amount": "1",
            "AmountInfo": "(50 g/35 g)",
            "PackageRecipe": "false",
            "IngredientType": "5695",
            "Unit": "prk",
            "IngredientTypeName": "Kapris"
          }],
          [{
            "Name": "valkosipulinkynsi",
            "Amount": "1",
            "PackageRecipe": "false",
            "IngredientType": "7262",
            "Unit": "",
            "IngredientTypeName": "Valkosipuli"
          }],
          [{
            "Name": "(lehti)persiljaa",
            "Amount": "1",
            "PackageRecipe": "false",
            "IngredientType": "5954",
            "Unit": "ruukku",
            "IngredientTypeName": "Lehtipersilja"
          }],
          [{
            "Name": "Pirkka Grana Padano -juustoraastetta",
            "Ean": "6410405124272",
            "Amount": "1",
            "AmountInfo": "(100 g)",
            "PackageRecipe": "false",
            "IngredientType": "5601",
            "Unit": "ps",
            "IngredientTypeName": "Juustoraaste, grana padano"
          }],
          [{
            "Name": "Pirkka ekstra-neitsytoliiviöljyä",
            "Ean": "6410402024759",
            "Amount": "1/2",
            "PackageRecipe": "false",
            "IngredientType": "5405",
            "Unit": "dl",
            "IngredientTypeName": "Ekstra neitsytoliiviöljy"
          }],
          [{
            "Name": "sitruunanmehua",
            "Amount": "1/2",
            "PackageRecipe": "false",
            "IngredientType": "6941",
            "Unit": "dl",
            "IngredientTypeName": "Sitruuna"
          }],
          [{
            "Name": "suolaa",
            "Amount": "1/2",
            "PackageRecipe": "false",
            "IngredientType": "7049",
            "Unit": "tl",
            "IngredientTypeName": "Suola"
          }],
          [{
            "Name": "mustapippuria",
            "Amount": "1/4",
            "PackageRecipe": "false",
            "IngredientType": "6271",
            "Unit": "tl",
            "IngredientTypeName": "Mustapippuri, rouhittu"
          }]
        ]
      }],
      "Instructions": "# Keitä pasta suolalla maustetussa vedessä.\r\n# Valmista kastike pastan kiehuessa. Hienonna kaprikset, valkosipuli ja persilja kulhoon. Lisää juustoraaste ja jätä hieman annosten päälle. Lisää öljy, sitruunanmehu, suola ja pippuri ja sekoita. Kääntele tonnikala öljyineen joukkoon.\r\n# Valuta pasta, säästä 1/2 dl keitinvettä ja sekoita se tonnikalaseoksen joukkoon. Kääntele kastike pastaan kattilassa kuumalla liedellä.",
      "EndNote": "*Annoshinnat on laskettu K-citymarket-ketjun hintatason mukaan 8/2019. Tuoretuotteiden hinnat vaihtelevat sesongeittain.",
      "Description": "Kun kaipaat helpoista helpointa pastaa, tämä resepti on sinulle. Tonnikala, grana padano ja muut kastikkeen ainekset vain sekoitetaan pastan kiehuessa. Noin 3,20 €/annos*",
      "DateCreated": "2019-04-01T13:18:33",
      "DateModified": "2019-08-26T17:53:02",
      "TvDate": "2019-08-26T00:00:00",
      "Stamp": {
        "Name": "Testattu K-koekeittiössä",
        "Id": "1"
      },
      "RecipeUse": {
        "Name": "k-ruoka.fi",
        "Id": "1"
      },
      "resourceType": "recipe",
      "Url": "https://www.k-ruoka.fi/reseptit/nopea-tonnikalapasta",
      "UrlSlug": "nopea-tonnikalapasta",
      "Sort": [1, 1, 10317]
    }, {
      "Id": "10316",
      "Name": "Kesäkurpitsapaistos",
      "PieceSize": {
        "Unit": "g",
        "Amount": "367"
      },
      "Portions": {
        "Unit": "annosta",
        "Amount": "6"
      },
      "PreparationTime": {
        "Description": "30 - 60 min",
        "TimeRange": {
          "MinTime": 30,
          "MaxTime": 60
        }
      },
      "UserPortions": {
        "$": {
          "Unit": "henkilölle"
        }
      },
      "Categories": [{
        "MainName": "Pääruoat",
        "MainId": "4",
        "SubName": "Kasvisruoat",
        "SubId": "22"
      }, {
        "MainName": "Pääraaka-aine",
        "MainId": "14",
        "SubName": "Kasvis",
        "SubId": "115"
      }, {
        "MainName": "Sesonki",
        "MainId": "1",
        "SubName": "Kesä",
        "SubId": "121"
      }, {
        "MainName": "Sesonki",
        "MainId": "1",
        "SubName": "Sadonkorjuu",
        "SubId": "124"
      }, {
        "MainName": "Pääraaka-aine",
        "MainId": "14",
        "SubName": "Kasviproteiini",
        "SubId": "135"
      }, {
        "MainName": "Ruokalaji",
        "MainId": "16",
        "SubName": "Pataruoat",
        "SubId": "142"
      }, {
        "MainName": "Teema",
        "MainId": "15",
        "SubName": "Mitä tänään syötäisiin?",
        "SubId": "144"
      }, {
        "MainName": "Asiakassegmentointi (K-asiakkaat)",
        "MainId": "17",
        "SubName": "Tiedostavat",
        "SubId": "148"
      }, {
        "MainName": "Teema",
        "MainId": "15",
        "SubName": "K-Marketin valikoima",
        "SubId": "151"
      }, {
        "MainName": "Apukategoriat",
        "MainId": "18",
        "SubName": "K-Marketin valikoima",
        "SubId": "181"
      }],
      "Pictures": ["PI8_19_kesakurpitsapais"],
      "PictureUrls": [{
        "Id": "PI8_19_kesakurpitsapais",
        "Normal": "https://k-file-storage-qa.imgix.net/f/k-ruoka/recipe/10316?w=1000&h=1000&fit=clip",
        "Original": "https://k-file-storage-qa.imgix.net/f/k-ruoka/recipe/10316"
      }],
      "VideoUrl": "https://youtu.be/J8mmFRJ2d4I",
      "VideoEmbedUrl": "<iframe width=\"560\" height=\"315\" src=\"https://www.youtube.com/embed/J8mmFRJ2d4I\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>",
      "SpecialDiets": [{
        "Id": "2",
        "Name": "Laktoositon"
      }, {
        "Id": "3",
        "Name": "Maidoton"
      }, {
        "Id": "6",
        "Name": "Kasvis (lakto-ovo)"
      }, {
        "Id": "7",
        "Name": "Kananmunaton"
      }, {
        "Id": "8",
        "Name": "Vegaaninen"
      }],
      "Ingredients": [{
        "SubSectionHeading": "_",
        "SubSectionIngredients": [
          [{
            "Name": "kesäkurpitsa",
            "Amount": "1",
            "AmountInfo": "(250 g)",
            "PackageRecipe": "false",
            "IngredientType": "5781",
            "Unit": "",
            "IngredientTypeName": "Kesäkurpitsa"
          }],
          [{
            "Name": "Pirkka perunaa (keltainen pussi)",
            "Amount": "800",
            "PackageRecipe": "false",
            "IngredientType": "7368",
            "Unit": "g",
            "IngredientTypeName": "Yleisperuna"
          }],
          [{
            "Name": "tomaatteja",
            "Amount": "800",
            "PackageRecipe": "false",
            "IngredientType": "7124",
            "Unit": "g",
            "IngredientTypeName": "Tomaatti"
          }],
          [{
            "Name": "sipuli",
            "Amount": "1",
            "AmountInfo": "(100 g)",
            "PackageRecipe": "false",
            "IngredientType": "6932",
            "Unit": "",
            "IngredientTypeName": "Sipuli"
          }],
          [{
            "Name": "valkosipulinkynttä",
            "Amount": "3",
            "PackageRecipe": "false",
            "IngredientType": "7262",
            "Unit": "",
            "IngredientTypeName": "Valkosipuli"
          }],
          [{
            "Name": "oliiviöljyä",
            "Amount": "1",
            "PackageRecipe": "false",
            "IngredientType": "5405",
            "Unit": "dl",
            "IngredientTypeName": "Ekstra neitsytoliiviöljy"
          }],
          [{
            "Name": "Gold&Green Nyhtökauraa, nude",
            "Amount": "1",
            "AmountInfo": "(240 g)",
            "PackageRecipe": "false",
            "IngredientType": "11957",
            "Unit": "rs",
            "IngredientTypeName": "Nyhtökaura, maustamaton"
          }],
          [{
            "Name": "Pirkka tomaattisosetta",
            "Ean": "6410405197382",
            "Amount": "3",
            "PackageRecipe": "false",
            "IngredientType": "7141",
            "Unit": "rkl",
            "IngredientTypeName": "Tomaattisose"
          }],
          [{
            "Name": "mustapippuria",
            "Amount": "1/2",
            "PackageRecipe": "false",
            "IngredientType": "6271",
            "Unit": "tl",
            "IngredientTypeName": "Mustapippuri, rouhittu"
          }],
          [{
            "Name": "persiljaa",
            "Amount": "1",
            "PackageRecipe": "false",
            "IngredientType": "6517",
            "Unit": "ruukku",
            "IngredientTypeName": "Persilja, tuore"
          }],
          [{
            "Name": "korianteria",
            "Amount": "1",
            "PackageRecipe": "false",
            "IngredientType": "5852",
            "Unit": "ruukku",
            "IngredientTypeName": "Korianteri, tuore"
          }],
          [{
            "Name": "suolaa",
            "Amount": "1",
            "PackageRecipe": "false",
            "IngredientType": "7049",
            "Unit": "tl",
            "IngredientTypeName": "Suola"
          }],
          [{
            "Name": "vettä",
            "Amount": "1 1/2",
            "PackageRecipe": "false",
            "IngredientType": "15792",
            "Unit": "dl",
            "IngredientTypeName": "Vesi"
          }]
        ]
      }],
      "Instructions": "# Viipaloi kesäkurpitsa ohuiksi kiekoiksi. Kuori ja viipaloi perunat. Kuutioi puolet tomaateista ja viipaloi loput. Hienonna sipuli ja valkosipuli.\r\n# Kuumenna öljy padassa tai kannellisessa kasarissa. Kuullota sipuleita muutama minuutti. Lisää kuutioitu tomaatti, Nyhtökaura, tomaattisose ja pippuri. Kypsennä 5 minuuttia. Hienonna suurin osa yrteistä varsineen joukkoon, jätä loput koristelua varten. Siirrä seos lautaselle odottamaan.\r\n# Kasaa perunoita, Nyhtökauraa, kesäkurpitsaa ja tomaatteja kerroksittain pataan, ripottele suolaa väleihin. Paina kerrokset tiiviiksi.\r\n# Kaada vesi pataan. Peitä pata kannella ja kypsennä 30-45 minuuttia liedellä, kunnes peruna on kypsää. Anna ruoan levähtää hetki ennen tarjoamista.",
      "EndNote": "*Annoshinnat on laskettu K-citymarket-ketjun hintatason mukaan 8/2019. Tuoretuotteiden hinnat vaihtelevat sesongeittain.",
      "Description": "Saat kesäkurpitsapaistokseen huikean paljon makua, kun valmistat sen vähän tavallisesta poiketen: kokoa kasvisviipaleet ja Nyhtökaura pannuun ja hauduta liedellä. Noin 1,85 €/annos",
      "DateCreated": "2019-04-01T13:07:09",
      "DateModified": "2019-09-02T14:49:32",
      "TvDate": "2019-08-28T00:00:00",
      "Stamp": {
        "Name": "Testattu K-koekeittiössä",
        "Id": "1"
      },
      "RecipeUse": {
        "Name": "k-ruoka.fi",
        "Id": "1"
      },
      "resourceType": "recipe",
      "Url": "https://www.k-ruoka.fi/reseptit/kesakurpitsapaistos",
      "UrlSlug": "kesakurpitsapaistos",
      "Sort": [1, 1, 10316]
    }, {
      "Id": "10313",
      "Name": "Silakkapihvit",
      "PieceSize": {
        "Unit": "g",
        "Amount": "158"
      },
      "Portions": {
        "Unit": "annosta",
        "Amount": "4"
      },
      "PreparationTime": {
        "Description": "30 - 60 min",
        "TimeRange": {
          "MinTime": 30,
          "MaxTime": 60
        }
      },
      "UserPortions": {
        "$": {
          "Unit": "henkilölle"
        }
      },
      "Categories": [{
        "MainName": "Pääruoat",
        "MainId": "4",
        "SubName": "Kalaruoat",
        "SubId": "20"
      }, {
        "MainName": "Makuja maailmalta",
        "MainId": "2",
        "SubName": "Suomi",
        "SubId": "106"
      }, {
        "MainName": "Pääraaka-aine",
        "MainId": "14",
        "SubName": "Kala",
        "SubId": "116"
      }, {
        "MainName": "Asiakassegmentointi (K-asiakkaat)",
        "MainId": "17",
        "SubName": "Tiedostavat",
        "SubId": "148"
      }, {
        "MainName": "Asiakassegmentointi (K-asiakkaat)",
        "MainId": "17",
        "SubName": "Vakiintuneet",
        "SubId": "150"
      }, {
        "MainName": "Apukategoriat",
        "MainId": "18",
        "SubName": "Helppo",
        "SubId": "153"
      }, {
        "MainName": "Apukategoriat",
        "MainId": "18",
        "SubName": "Perinteinen ",
        "SubId": "155"
      }, {
        "MainName": "Teema",
        "MainId": "15",
        "SubName": "Arkiruoka",
        "SubId": "159"
      }, {
        "MainName": "Teema",
        "MainId": "15",
        "SubName": "Halpa",
        "SubId": "161"
      }],
      "Pictures": ["KR5_19_silakkapihvi"],
      "PictureUrls": [{
        "Id": "KR5_19_silakkapihvi",
        "Normal": "https://k-file-storage-qa.imgix.net/f/k-ruoka/recipe/10313?w=1000&h=1000&fit=clip",
        "Original": "https://k-file-storage-qa.imgix.net/f/k-ruoka/recipe/10313"
      }],
      "TrendWords": [{
        "Id": "2",
        "Name": "Kotimaiset ainekset"
      }],
      "SpecialDiets": [{
        "Id": "2",
        "Name": "Laktoositon"
      }, {
        "Id": "3",
        "Name": "Maidoton"
      }, {
        "Id": "5",
        "Name": "Vähemmän sokeria"
      }, {
        "Id": "7",
        "Name": "Kananmunaton"
      }],
      "Ingredients": [{
        "SubSectionHeading": "_",
        "SubSectionIngredients": [
          [{
            "Name": "silakkafileitä",
            "Amount": "600",
            "PackageRecipe": "false",
            "IngredientType": "14023",
            "Unit": "g",
            "IngredientTypeName": "Silakkafilee, tuore"
          }],
          [{
            "Name": "suolaa",
            "Amount": "1",
            "PackageRecipe": "false",
            "IngredientType": "7049",
            "Unit": "tl",
            "IngredientTypeName": "Suola"
          }],
          [{
            "Name": "mustapippuria",
            "Amount": "1/4",
            "PackageRecipe": "false",
            "IngredientType": "6167",
            "Unit": "tl",
            "IngredientTypeName": "Maustemylly, mustapippuri"
          }],
          [{
            "Name": "tilliä hienonnettuna",
            "Amount": "1",
            "PackageRecipe": "false",
            "IngredientType": "14766",
            "Unit": "dl",
            "IngredientTypeName": "Tilli, tuore"
          }],
          [{
            "Name": "ruohosipulia hienonnettuna",
            "Amount": "3/4",
            "PackageRecipe": "false",
            "IngredientType": "6772",
            "Unit": "dl",
            "IngredientTypeName": "Ruohosipuli, tuore"
          }],
          [{
            "Name": "ruiskorppujauhoja",
            "Amount": "1",
            "PackageRecipe": "false",
            "IngredientType": "5873",
            "Unit": "dl",
            "IngredientTypeName": "Korppujauho, ruis"
          }]
        ]
      }, {
        "SubSectionHeading": "Paistamiseen",
        "SubSectionIngredients": [
          [{
            "Name": "voita",
            "Amount": "50",
            "PackageRecipe": "false",
            "IngredientType": "7343",
            "Unit": "g",
            "IngredientTypeName": "Voi"
          }]
        ]
      }],
      "Instructions": "# Leikkaa saksilla fileistä selkäevä pois. Levitä fileet leikkuulaudalle tai voipaperin päälle siten, että samankokoiset fileet muodostavat parin. Mausta suolalla ja pippurilla.\r\n# Lisää hienonnettuja yrttejä joka toiselle fileelle ja paina toinen filee kanneksi.\r\n# Pyörittele silakkapihvit korppujauhoissa.\r\n# Kuumenna voi pannussa ja paista kerrallaan 4-5 pihviä. Paista pihvit keskilämmöllä molemmin puolin. Pyyhi pannu puhtaaksi paistokertojen välissä.",
      "EndNote": "Silakkapihvien valmistus onnistuu myös uunissa. Voitele fileiden pinta rypsiöljyllä ja ripottele päälle korppujauhoja. Paista pihvit 225 asteessa noin 15 minuuttia. Variaatiota silakkapihveihin saat lisäämällä pihvien väliin yrttien lisäksi maustamatonta tuorejuustoa. *Annoshinnat on laskettu K-citymarket-ketjun hintatason mukaan 8/2019. Tuoretuotteiden hinnat vaihtelevat sesongeittain.",
      "Description": "Perinteiset silakkapihvit täytetään tillillä ja ruohosipulilla ja pyöritellään ruisjauhoissa ennen paistamista. Voit kypsentää silakkapihvit myös uunissa. Noin 1,80€/annos",
      "DateCreated": "2019-04-01T09:31:11",
      "DateModified": "2019-07-16T12:52:33",
      "Stamp": {
        "Name": "Testattu K-koekeittiössä",
        "Id": "1"
      },
      "RecipeUse": {
        "Name": "k-ruoka.fi",
        "Id": "1"
      },
      "resourceType": "recipe",
      "Url": "https://www.k-ruoka.fi/reseptit/silakkapihvit",
      "UrlSlug": "silakkapihvit",
      "Sort": [1, 1, 10313]
    }, {
      "Id": "10311",
      "Name": "Possuvartaat ja grillattu kaali",
      "PieceSize": {
        "Unit": "g",
        "Amount": "317"
      },
      "Portions": {
        "Unit": "annosta",
        "Amount": "4"
      },
      "PreparationTime": {
        "Description": "alle 30 min",
        "TimeRange": {
          "MinTime": 15,
          "MaxTime": 30
        }
      },
      "UserPortions": {
        "$": {
          "Unit": "henkilölle"
        }
      },
      "Categories": [{
        "MainName": "Sesonki",
        "MainId": "1",
        "SubName": "Grillaus",
        "SubId": "5"
      }, {
        "MainName": "Makuja maailmalta",
        "MainId": "2",
        "SubName": "Aasia",
        "SubId": "7"
      }, {
        "MainName": "Asiakassegmentointi (K-asiakkaat)",
        "MainId": "17",
        "SubName": "Tiedostavat",
        "SubId": "148"
      }, {
        "MainName": "Pääruoat",
        "MainId": "4",
        "SubName": "Liharuoat",
        "SubId": "24"
      }, {
        "MainName": "Pääruoat",
        "MainId": "4",
        "SubName": "Grilliruoat",
        "SubId": "73"
      }, {
        "MainName": "Pääraaka-aine",
        "MainId": "14",
        "SubName": "Liha",
        "SubId": "114"
      }, {
        "MainName": "Sesonki",
        "MainId": "1",
        "SubName": "Kesä",
        "SubId": "121"
      }, {
        "MainName": "Sesonki",
        "MainId": "1",
        "SubName": "Juhannus",
        "SubId": "122"
      }, {
        "MainName": "Asiakassegmentointi (K-asiakkaat)",
        "MainId": "17",
        "SubName": "Innostujat",
        "SubId": "147"
      }],
      "Pictures": ["KSM_19_possuvartaat"],
      "PictureUrls": [{
        "Id": "KSM_19_possuvartaat",
        "Normal": "https://k-file-storage-qa.imgix.net/f/k-ruoka/recipe/10311?w=1000&h=1000&fit=clip",
        "Original": "https://k-file-storage-qa.imgix.net/f/k-ruoka/recipe/10311"
      }],
      "SpecialDiets": [{
        "Id": "3",
        "Name": "Maidoton"
      }, {
        "Id": "7",
        "Name": "Kananmunaton"
      }, {
        "Id": "2",
        "Name": "Laktoositon"
      }],
      "Ingredients": [{
        "SubSectionHeading": "_",
        "SubSectionIngredients": [
          [{
            "Name": "Snellman Maatiaispossun fileepihviä",
            "Amount": "3",
            "AmountInfo": "(à 4 kpl/460 g)",
            "PackageRecipe": "false",
            "IngredientType": "16398",
            "Unit": "pkt",
            "IngredientTypeName": "Snellman Maatiaispossun fileepihvi"
          }],
          [{
            "Name": "Pirkka Hoisin-kastiketta",
            "Ean": "6410405192677",
            "Amount": "1 1/2",
            "PackageRecipe": "false",
            "IngredientType": "8414",
            "Unit": "dl",
            "IngredientTypeName": "Hoi Sin -kastike"
          }],
          [{
            "Name": "varhaiskaalia",
            "Amount": "n. 400",
            "PackageRecipe": "false",
            "IngredientType": "7286",
            "Unit": "g",
            "IngredientTypeName": "Varhaiskaali"
          }],
          [{
            "Name": "rypsiöljyä",
            "Amount": "3",
            "PackageRecipe": "false",
            "IngredientType": "6807",
            "Unit": "rkl",
            "IngredientTypeName": "Rypsiöljy"
          }],
          [{
            "Name": "suolaa",
            "PackageRecipe": "false",
            "IngredientType": "7049",
            "Unit": "ripaus",
            "IngredientTypeName": "Suola"
          }]
        ]
      }, {
        "SubSectionHeading": "Lisäksi",
        "SubSectionIngredients": [
          [{
            "Name": "kevätsipulia koristeluun",
            "PackageRecipe": "false",
            "IngredientType": "5792",
            "IngredientTypeName": "Kevätsipuli"
          }],
          [{
            "Name": "puisia tai metallisia varrastikkuja",
            "PackageRecipe": "false",
            "IngredientType": "7288",
            "IngredientTypeName": "Varrastikku"
          }]
        ]
      }],
      "Instructions": "# Sekoita fileepihvit ja Hoisin-kastike. Marinoi kylmässä kelmun alla tai pussissa 1-2 tuntia. Nosta lihat huoneenlämpöön ½ tuntia ennen grillausta.\r\n# Pujota fileet vartaisiin, 3 fileetä/varras. Leikkaa varhaiskaalista 4 reilua lohkoa, sivele leikkuupinnoille rypsiöljyä ja lisää pinnoille ripaus suolaa.\r\n# Grillaa vartaita ja kaalilohkoja kovalla lämmöllä noin 4 minuuttia. Käännä vartaat ja kaalit, sivele fileiden pintaan vielä Hoisin-kastiketta. Jatka grillausta vielä noin 4 minuuttia kunnes pinnoissa on hyvä väri ja fileet ovat kypsiä. Anna lihojen levätä pari minuuttia ennen tarjoamista.",
      "EndNote": "",
      "Description": "Marinoi possun fileepihvit Hoisin-kastikkeessa, laita vartaisiin ja grillaa meheviksi. Tarjoa isoina lohkoina grillattu varhaiskaali possuvartaiden lisäkkeenä.",
      "DateCreated": "2019-03-29T15:49:33",
      "DateModified": "2019-04-30T12:41:05",
      "Stamp": {
        "Name": "Kumppani",
        "Id": "6"
      },
      "RecipeUse": {
        "Name": "k-ruoka.fi",
        "Id": "1"
      },
      "resourceType": "recipe",
      "Url": "https://www.k-ruoka.fi/reseptit/possuvartaat-ja-grillattu-kaali",
      "UrlSlug": "possuvartaat-ja-grillattu-kaali",
      "Sort": [1, 1, 10311]
    }, {
      "Id": "10310",
      "Name": "Grillattu pestokana ja varsiparsakaali",
      "PieceSize": {
        "Unit": "g",
        "Amount": "232"
      },
      "Portions": {
        "Unit": "annosta",
        "Amount": "4"
      },
      "PreparationTime": {
        "Description": "alle 30 min",
        "TimeRange": {
          "MinTime": 15,
          "MaxTime": 30
        }
      },
      "UserPortions": {
        "$": {
          "Unit": "henkilölle"
        }
      },
      "Categories": [{
        "MainName": "Asiakassegmentointi (K-asiakkaat)",
        "MainId": "17",
        "SubName": "Tiedostavat",
        "SubId": "148"
      }, {
        "MainName": "Pääruoat",
        "MainId": "4",
        "SubName": "Broileri- ja linturuoat",
        "SubId": "21"
      }, {
        "MainName": "Pääruoat",
        "MainId": "4",
        "SubName": "Grilliruoat",
        "SubId": "73"
      }, {
        "MainName": "Pääraaka-aine",
        "MainId": "14",
        "SubName": "Broileri ja linnut",
        "SubId": "118"
      }, {
        "MainName": "Sesonki",
        "MainId": "1",
        "SubName": "Kesä",
        "SubId": "121"
      }, {
        "MainName": "Sesonki",
        "MainId": "1",
        "SubName": "Juhannus",
        "SubId": "122"
      }, {
        "MainName": "Asiakassegmentointi (K-asiakkaat)",
        "MainId": "17",
        "SubName": "Innostujat",
        "SubId": "147"
      }, {
        "MainName": "Sesonki",
        "MainId": "1",
        "SubName": "Grillaus",
        "SubId": "5"
      }, {
        "MainName": "Makuja maailmalta",
        "MainId": "2",
        "SubName": "Italia",
        "SubId": "10"
      }],
      "Pictures": ["KSM_19_pestokana"],
      "PictureUrls": [{
        "Id": "KSM_19_pestokana",
        "Normal": "https://k-file-storage-qa.imgix.net/f/k-ruoka/recipe/10310?w=1000&h=1000&fit=clip",
        "Original": "https://k-file-storage-qa.imgix.net/f/k-ruoka/recipe/10310"
      }],
      "VideoUrl": "https://www.youtube.com/watch?v=KCNDvCOYomI",
      "VideoEmbedUrl": "<iframe width=\"560\" height=\"315\" src=\"https://www.youtube.com/embed/KCNDvCOYomI\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>",
      "Ingredients": [{
        "SubSectionHeading": "_",
        "SubSectionIngredients": [
          [{
            "Name": "Kariniemen kananpojan minuuttipihvejä, maustamaton",
            "Amount": "1",
            "AmountInfo": "(à 700 g)",
            "PackageRecipe": "false",
            "IngredientType": "5299",
            "Unit": "rs",
            "IngredientTypeName": "Broilerin minuuttipihvi, maustamaton"
          }],
          [{
            "Name": "Pirkka vihreää pestoa",
            "Ean": "6410402013210",
            "Amount": "2",
            "PackageRecipe": "false",
            "IngredientType": "6536",
            "Unit": "rkl",
            "IngredientTypeName": "Pesto, vihreä"
          }],
          [{
            "Name": "varsiparsakaalia",
            "Amount": "1",
            "AmountInfo": "(250 g)",
            "PackageRecipe": "false",
            "IngredientType": "15396",
            "Unit": "pkt",
            "IngredientTypeName": "Varsiparsakaali"
          }],
          [{
            "Name": "suolaa",
            "PackageRecipe": "false",
            "IngredientType": "7049",
            "Unit": "ripaus",
            "IngredientTypeName": "Suola"
          }]
        ]
      }, {
        "SubSectionHeading": "Lisäksi",
        "SubSectionIngredients": [
          [{
            "Name": "puisia tai metallisia varrastikkuja",
            "PackageRecipe": "false",
            "IngredientType": "7288",
            "IngredientTypeName": "Varrastikku"
          }]
        ]
      }],
      "Instructions": "# Levitä pesto fileiden toiselle puolelle. Rullaa fileet niin, että niiden ohuimmat päät jäävät rullien sisään. Sulje rullat varrastikuilla ja mausta pintasuolalla. Jos käytät puutikkuja, liota niitä ennakkoon vedessä.\r\n# Grillaa rullia keskilämmöllä kolmelta sivulta, 3 minuuttia/sivu.\r\n# Pirskota rypsiöljyä ja suolaa varsiparsakaalien päälle ja grillaa noin minuutti/puoli.",
      "EndNote": "",
      "Description": "Pestokana onnistuu myös grillissä. Täytä kanafileet pestolla, rullaa ja grillaa. Tarjoa mehevä pestokana grillattujen varsiparsakaalien kanssa.",
      "DateCreated": "2019-03-29T15:41:05",
      "DateModified": "2019-05-22T14:36:46",
      "Stamp": {
        "Name": "Kumppani",
        "Id": "6"
      },
      "RecipeUse": {
        "Name": "k-ruoka.fi",
        "Id": "1"
      },
      "resourceType": "recipe",
      "Url": "https://www.k-ruoka.fi/reseptit/grillattu-pestokana-ja-varsiparsakaali",
      "UrlSlug": "grillattu-pestokana-ja-varsiparsakaali",
      "Sort": [1, 1, 10310]
    }, {
      "Id": "10309",
      "Name": "Grillattu hauki, perunavartaat ja munavoi",
      "PieceSize": {
        "Unit": "g",
        "Amount": "306"
      },
      "Portions": {
        "Unit": "annosta",
        "Amount": "4"
      },
      "PreparationTime": {
        "Description": "30 - 60 min",
        "TimeRange": {
          "MinTime": 30,
          "MaxTime": 60
        }
      },
      "UserPortions": {
        "$": {
          "Unit": "henkilölle"
        }
      },
      "Categories": [{
        "MainName": "Asiakassegmentointi (K-asiakkaat)",
        "MainId": "17",
        "SubName": "Innostujat",
        "SubId": "147"
      }, {
        "MainName": "Asiakassegmentointi (K-asiakkaat)",
        "MainId": "17",
        "SubName": "Tiedostavat",
        "SubId": "148"
      }, {
        "MainName": "Pääruoat",
        "MainId": "4",
        "SubName": "Grilliruoat",
        "SubId": "73"
      }, {
        "MainName": "Makuja maailmalta",
        "MainId": "2",
        "SubName": "Skandinavia",
        "SubId": "105"
      }, {
        "MainName": "Makuja maailmalta",
        "MainId": "2",
        "SubName": "Suomi",
        "SubId": "106"
      }, {
        "MainName": "Pääraaka-aine",
        "MainId": "14",
        "SubName": "Kala",
        "SubId": "116"
      }, {
        "MainName": "Sesonki",
        "MainId": "1",
        "SubName": "Kesä",
        "SubId": "121"
      }, {
        "MainName": "Sesonki",
        "MainId": "1",
        "SubName": "Juhannus",
        "SubId": "122"
      }, {
        "MainName": "Sesonki",
        "MainId": "1",
        "SubName": "Grillaus",
        "SubId": "5"
      }, {
        "MainName": "Pääruoat",
        "MainId": "4",
        "SubName": "Kalaruoat",
        "SubId": "20"
      }],
      "Pictures": ["KSM_grillattu_hauki"],
      "PictureUrls": [{
        "Id": "KSM_grillattu_hauki",
        "Normal": "https://k-file-storage-qa.imgix.net/f/k-ruoka/recipe/10309?w=1000&h=1000&fit=clip",
        "Original": "https://k-file-storage-qa.imgix.net/f/k-ruoka/recipe/10309"
      }],
      "SpecialDiets": [{
        "Id": "5",
        "Name": "Vähemmän sokeria"
      }, {
        "Id": "4",
        "Name": "Gluteeniton"
      }],
      "Ingredients": [{
        "SubSectionHeading": "Perunavartaat",
        "SubSectionIngredients": [
          [{
            "Name": "pientä uutta perunaa",
            "Amount": "12",
            "AmountInfo": "(450 g)",
            "PackageRecipe": "false",
            "IngredientType": "6525",
            "Unit": "",
            "IngredientTypeName": "Varhaisperuna"
          }],
          [{
            "Name": "rypsiöljyä",
            "Amount": "1",
            "PackageRecipe": "false",
            "IngredientType": "6807",
            "Unit": "rkl",
            "IngredientTypeName": "Rypsiöljy"
          }],
          [{
            "Name": "suolaa",
            "Amount": "1",
            "PackageRecipe": "false",
            "IngredientType": "7049",
            "Unit": "tl",
            "IngredientTypeName": "Suola"
          }],
          [{
            "Name": "puu- tai metallivarrasta",
            "Amount": "4",
            "PackageRecipe": "false",
            "IngredientType": "7288",
            "Unit": "",
            "IngredientTypeName": "Varrastikku"
          }]
        ]
      }, {
        "SubSectionHeading": "Munavoi",
        "SubSectionIngredients": [
          [{
            "Name": "kananmunaa keitettynä",
            "Amount": "3",
            "PackageRecipe": "false",
            "IngredientType": "5683",
            "Unit": "",
            "IngredientTypeName": "Kananmuna"
          }],
          [{
            "Name": "suolaa",
            "Amount": "1/2",
            "PackageRecipe": "false",
            "IngredientType": "7049",
            "Unit": "tl",
            "IngredientTypeName": "Suola"
          }],
          [{
            "Name": "voita",
            "Amount": "50",
            "PackageRecipe": "false",
            "IngredientType": "7343",
            "Unit": "g",
            "IngredientTypeName": "Voi"
          }],
          [{
            "Name": "tuoretta tilliä hienonnettuna",
            "Amount": "1/2",
            "PackageRecipe": "false",
            "IngredientType": "14766",
            "Unit": "ruukkua",
            "IngredientTypeName": "Tilli, tuore"
          }]
        ]
      }, {
        "SubSectionHeading": "Hauki",
        "SubSectionIngredients": [
          [{
            "Name": "nahaton tai nahallinen suomustettu haukifilee",
            "Amount": "1",
            "AmountInfo": "(500-600 g)",
            "PackageRecipe": "false",
            "IngredientType": "5503",
            "Unit": "",
            "IngredientTypeName": "Haukifilee, tuore"
          }],
          [{
            "Name": "rypsiöljyä",
            "Amount": "1",
            "PackageRecipe": "false",
            "IngredientType": "6807",
            "Unit": "rkl",
            "IngredientTypeName": "Rypsiöljy"
          }],
          [{
            "Name": "suolaa",
            "Amount": "1/2-1",
            "PackageRecipe": "false",
            "IngredientType": "7049",
            "Unit": "tl",
            "IngredientTypeName": "Suola"
          }],
          [{
            "Name": "(Luomu) sitruuna",
            "Amount": "1",
            "PackageRecipe": "false",
            "IngredientType": "6941",
            "Unit": "",
            "IngredientTypeName": "Sitruuna"
          }]
        ]
      }],
      "Instructions": "# Pese ja kuivaa perunat. Sekoita rypsiöljy ja suola perunoihin ja pujota ne 4 vartaaseen. Jos käytät puuvartaita, liota niitä ennakkoon vedessä. Grillaa vartaita keskilämmöllä 20-30 minuuttia tai kunnes ne ovat kypsiä. Käännä vartaita 2-3 kertaa grillauksen aikana.\r\n# Keitä kananmunia 9 minuuttia ja jäähdytä ne. Riko kananmunien rakenne haarukalla ja sekoita niihin suola, huoneenlämpöinen voi ja hienonnettu tilli.\r\n# Sivele haukifileen kummallekin pinnalle runsaasti rypsiöljyä ja suolaa filee kummaltakin puolelta. Viipaloi sitruunat ja poista siemenet. Grillaa haukifileetä ja sitruunaviipaleita maksimilämmöllä ensin 4 minuuttia. Käännä filee varovasti esim. kahta lastaa apuna käyttäen. Jatka kypsennystä vielä 1-2 minuuttia tai kunnes filee on haluamassasi kypsyydessä. Tarjoa koko annos esimerkiksi isolta lankulta tai leikkuulautaselta.",
      "EndNote": "Grillaa ritilällä kypsennettävä kala aina mahdollisimman kuumassa lämmössä, jolloin se ei tartu niin helposti ritilään. Ruodottoman kalan ystävän kannattaa pyytää apua ruotojen poistoon palvelutiskin kalamestarilta.",
      "Description": "Hauki on parhaimmillaan nopeasti grillattuna ja kevyesti maustettuna. Grillaa fileetä muutama \r\nminuutti, tarjoa perunavartaiden ja kesäisen munavoin kanssa.",
      "DateCreated": "2019-03-29T15:32:29",
      "DateModified": "2019-05-03T16:59:28",
      "Stamp": {
        "Name": "Kumppani",
        "Id": "6"
      },
      "RecipeUse": {
        "Name": "k-ruoka.fi",
        "Id": "1"
      },
      "resourceType": "recipe",
      "Url": "https://www.k-ruoka.fi/reseptit/grillattu-hauki-perunavartaat-ja-munavoi",
      "UrlSlug": "grillattu-hauki-perunavartaat-ja-munavoi",
      "Sort": [1, 1, 10309]
    }, {
      "Id": "10305",
      "Name": "Grillatut vietnamilaiset kevätrullat tofusta",
      "PieceSize": {
        "Unit": "g",
        "Amount": "241"
      },
      "Portions": {
        "Unit": "annosta",
        "Amount": "4"
      },
      "PreparationTime": {
        "Description": "30 - 60 min",
        "TimeRange": {
          "MinTime": 30,
          "MaxTime": 60
        }
      },
      "UserPortions": {
        "$": {
          "Unit": "henkilölle"
        }
      },
      "Categories": [{
        "MainName": "Sesonki",
        "MainId": "1",
        "SubName": "Kesä",
        "SubId": "121"
      }, {
        "MainName": "Sesonki",
        "MainId": "1",
        "SubName": "Juhannus",
        "SubId": "122"
      }, {
        "MainName": "Pääraaka-aine",
        "MainId": "14",
        "SubName": "Kasviproteiini",
        "SubId": "135"
      }, {
        "MainName": "Asiakassegmentointi (K-asiakkaat)",
        "MainId": "17",
        "SubName": "Innostujat",
        "SubId": "147"
      }, {
        "MainName": "Asiakassegmentointi (K-asiakkaat)",
        "MainId": "17",
        "SubName": "Tiedostavat",
        "SubId": "148"
      }, {
        "MainName": "Sesonki",
        "MainId": "1",
        "SubName": "Grillaus",
        "SubId": "5"
      }, {
        "MainName": "Makuja maailmalta",
        "MainId": "2",
        "SubName": "Aasia",
        "SubId": "7"
      }, {
        "MainName": "Alkuruoat",
        "MainId": "3",
        "SubName": "Muut alkuruoat",
        "SubId": "19"
      }, {
        "MainName": "Pääruoat",
        "MainId": "4",
        "SubName": "Kasvisruoat",
        "SubId": "22"
      }, {
        "MainName": "Lisäkkeet",
        "MainId": "6",
        "SubName": "Kasvislisäkkeet",
        "SubId": "42"
      }, {
        "MainName": "Lisäkkeet",
        "MainId": "6",
        "SubName": "Grilliruoan lisäkkeet",
        "SubId": "93"
      }],
      "Pictures": ["KSM_19_tofurullat"],
      "PictureUrls": [{
        "Id": "KSM_19_tofurullat",
        "Normal": "https://k-file-storage-qa.imgix.net/f/k-ruoka/recipe/10305?w=1000&h=1000&fit=clip",
        "Original": "https://k-file-storage-qa.imgix.net/f/k-ruoka/recipe/10305"
      }],
      "SpecialDiets": [{
        "Id": "4",
        "Name": "Gluteeniton"
      }, {
        "Id": "6",
        "Name": "Kasvis (lakto-ovo)"
      }, {
        "Id": "7",
        "Name": "Kananmunaton"
      }, {
        "Id": "2",
        "Name": "Laktoositon"
      }, {
        "Id": "3",
        "Name": "Maidoton"
      }],
      "Ingredients": [{
        "SubSectionHeading": "_",
        "SubSectionIngredients": [
          [{
            "Name": "porkkana",
            "Amount": "1",
            "AmountInfo": "(n. 90 g)",
            "PackageRecipe": "false",
            "IngredientType": "6582",
            "Unit": "",
            "IngredientTypeName": "Porkkana"
          }],
          [{
            "Name": "retikkaa",
            "Amount": "100",
            "PackageRecipe": "false",
            "IngredientType": "16067",
            "Unit": "g",
            "IngredientTypeName": "Retikka"
          }],
          [{
            "Name": "sokeriherneitä",
            "Amount": "100",
            "PackageRecipe": "false",
            "IngredientType": "6966",
            "Unit": "g",
            "IngredientTypeName": "Sokeriherne, pakaste"
          }],
          [{
            "Name": "kevätsipulin vartta",
            "Amount": "4",
            "PackageRecipe": "false",
            "IngredientType": "5792",
            "Unit": "",
            "IngredientTypeName": "Kevätsipuli"
          }],
          [{
            "Name": "Pirkka mieto punainen chili",
            "Amount": "1",
            "PackageRecipe": "false",
            "IngredientType": "5355",
            "Unit": "",
            "IngredientTypeName": "Chili, punainen"
          }],
          [{
            "Name": "Go-Tan riisinuudelia",
            "Amount": "1/2",
            "AmountInfo": "(à 250 g)",
            "PackageRecipe": "false",
            "IngredientType": "6735",
            "Unit": "pkt",
            "IngredientTypeName": "Riisinuudeli"
          }],
          [{
            "Name": "Spice Up Sataykastiketta",
            "Amount": "1/2",
            "AmountInfo": "(à 100 g)",
            "PackageRecipe": "false",
            "IngredientType": "11140",
            "Unit": "ps",
            "IngredientTypeName": "Maustekastike, satay"
          }],
          [{
            "Name": "Blue Dragon tofua",
            "Amount": "1/2",
            "AmountInfo": "(à 349 g)",
            "PackageRecipe": "false",
            "IngredientType": "16112",
            "Unit": "pkt",
            "IngredientTypeName": "Blue Dragon tofu"
          }],
          [{
            "Name": "Blue Dragon Spring Roll riisipaperiarkkia",
            "Amount": "10",
            "PackageRecipe": "false",
            "IngredientType": "16111",
            "Unit": "",
            "IngredientTypeName": "Blue Dragon Spring Roll riisipaperiarkki"
          }],
          [{
            "Name": "rypsiöljyä",
            "Amount": "2",
            "PackageRecipe": "false",
            "IngredientType": "6807",
            "Unit": "rkl",
            "IngredientTypeName": "Rypsiöljy"
          }],
          [{
            "Name": "oksaa tuoretta korianteria",
            "Amount": "10",
            "PackageRecipe": "false",
            "IngredientType": "5852",
            "Unit": "",
            "IngredientTypeName": "Korianteri, tuore"
          }],
          [{
            "Name": "muutama chilirengas",
            "PackageRecipe": "false",
            "IngredientType": "5355",
            "IngredientTypeName": "Chili, punainen"
          }],
          [{
            "Name": "seesaminsiemeniä",
            "Amount": "1",
            "PackageRecipe": "false",
            "IngredientType": "6871",
            "Unit": "tl",
            "IngredientTypeName": "Seesaminsiemen"
          }]
        ]
      }, {
        "SubSectionHeading": "Lisäksi",
        "SubSectionIngredients": [
          [{
            "Name": "Pirkka makeaa chilikastiketta",
            "Ean": "6410405186553",
            "PackageRecipe": "false",
            "IngredientType": "5358",
            "IngredientTypeName": "Chilikastike, makea"
          }]
        ]
      }],
      "Instructions": "# Kuori porkkana ja retikka. Leikkaa porkkana, retikka ja sokeriherneet ohuiksi tikuiksi. Hienonna kevätsipuli ja chili (leikkaa pieni osa chilistä renkaiksi ja jätä koristeluun). Poista chilin siemenet ja valkoiset osat, jos haluat vähentää tulisuutta. Kypsennä riisinuudeli pakkauksen ohjeen mukaan. Sekoita riisinuudeli ja sataykastike ja leikkaa tofu 10 tangoksi. Jaa rullien sisään tulevat raaka-aineet valmiiksi 10 saman kokoiseen osaan.\r\n# Upota riisipaperit yksi kerrallaan kädenlämpöiseen veteen noin 10 sekunniksi. Nosta pehmennyt riisipaperi puhtaan keittiöliinan päälle, joka imee ylimääräisen kosteuden. Varo ettei riisipaperi kostu liikaa, jolloin sitä on vaikeata käsitellä.\r\n# Asettele kasvikset, tofu ja nuudelit riisipaperin keskelle suorakaiteen muotoiseksi keoksi noin 8 cm:n leveydeltä. Taita kolme reunoista keon päälle ja rullaa mahdollisimman tiiviiksi paketiksi. Valmista loput rullat samalla tavalla. Sivele rullien pinnat kauttaaltaan ohuesti rypsiöljyllä ennen grillausta.\r\n# Grillaa kevätrullat maksimilämmöllä kahdelta puolelta 1-1 ½ minuuttia/ puoli tai kunnes rullissa on hyvä väri ja ne ovat pinnalta rapeita.\r\n# Koristele kevätrullat korianterilla, chilirenkailla ja seesaminsiemenillä. Tarjoa yhden tai useamman dippikastikkeen kanssa.",
      "EndNote": "Kokeile kevätrullien kanssa erilaisia dippejä. Tumma kastike kesärullille ja maapähkinäkastike kesärullille ohjeet löydät K-ruoka.fi/reseptit.",
      "Description": "Vietnamilaiset kevätrullat saavat grillissä rapean pinnan. Täytä kevätrullat sesongin kasviksilla ja tofulla ja tarjoa dippien kanssa.",
      "DateCreated": "2019-03-28T16:49:14",
      "DateModified": "2019-06-12T17:15:26",
      "Stamp": {
        "Name": "Kumppani",
        "Id": "6"
      },
      "RecipeUse": {
        "Name": "k-ruoka.fi",
        "Id": "1"
      },
      "resourceType": "recipe",
      "Url": "https://www.k-ruoka.fi/reseptit/grillatut-vietnamilaiset-kevatrullat-tofusta",
      "UrlSlug": "grillatut-vietnamilaiset-kevatrullat-tofusta",
      "Sort": [1, 1, 10305]
    }, {
      "Id": "10304",
      "Name": "Linssikeitto",
      "PieceSize": {
        "Unit": "g",
        "Amount": "390"
      },
      "Portions": {
        "Unit": "annosta",
        "Amount": "3-4"
      },
      "PreparationTime": {
        "Description": "alle 30 min",
        "TimeRange": {
          "MinTime": 15,
          "MaxTime": 30
        }
      },
      "UserPortions": {
        "$": {
          "Unit": "henkilölle"
        }
      },
      "Categories": [{
        "MainName": "Pääruoat",
        "MainId": "4",
        "SubName": "Keitot",
        "SubId": "27"
      }, {
        "MainName": "Ruokalaji",
        "MainId": "16",
        "SubName": "Keitot",
        "SubId": "136"
      }, {
        "MainName": "Asiakassegmentointi (K-asiakkaat)",
        "MainId": "17",
        "SubName": "Nautiskelijat",
        "SubId": "146"
      }, {
        "MainName": "Asiakassegmentointi (K-asiakkaat)",
        "MainId": "17",
        "SubName": "Innostujat",
        "SubId": "147"
      }, {
        "MainName": "Asiakassegmentointi (K-asiakkaat)",
        "MainId": "17",
        "SubName": "Tiedostavat",
        "SubId": "148"
      }],
      "Pictures": ["PI10_19_maukas_linssik"],
      "PictureUrls": [{
        "Id": "PI10_19_maukas_linssik",
        "Normal": "https://k-file-storage-qa.imgix.net/f/k-ruoka/recipe/10304?w=1000&h=1000&fit=clip",
        "Original": "https://k-file-storage-qa.imgix.net/f/k-ruoka/recipe/10304"
      }],
      "SpecialDiets": [{
        "Id": "2",
        "Name": "Laktoositon"
      }, {
        "Id": "3",
        "Name": "Maidoton"
      }, {
        "Id": "4",
        "Name": "Gluteeniton"
      }, {
        "Id": "6",
        "Name": "Kasvis (lakto-ovo)"
      }, {
        "Id": "7",
        "Name": "Kananmunaton"
      }, {
        "Id": "8",
        "Name": "Vegaaninen"
      }],
      "Ingredients": [{
        "SubSectionHeading": "_",
        "SubSectionIngredients": [
          [{
            "Name": "sipuli",
            "Amount": "1",
            "AmountInfo": "(100 g)",
            "PackageRecipe": "false",
            "IngredientType": "6932",
            "Unit": "",
            "IngredientTypeName": "Sipuli"
          }],
          [{
            "Name": "valkosipulinkynttä",
            "Amount": "2",
            "PackageRecipe": "false",
            "IngredientType": "7262",
            "Unit": "",
            "IngredientTypeName": "Valkosipuli"
          }],
          [{
            "Name": "öljyä",
            "Amount": "1 1/2",
            "PackageRecipe": "false",
            "IngredientType": "6807",
            "Unit": "rkl",
            "IngredientTypeName": "Rypsiöljy"
          }],
          [{
            "Name": "Pirkka currya",
            "Ean": "6410405055569",
            "Amount": "2",
            "PackageRecipe": "false",
            "IngredientType": "5382",
            "Unit": "tl",
            "IngredientTypeName": "Curry"
          }],
          [{
            "Name": "juustokuminaa",
            "Amount": "2",
            "PackageRecipe": "false",
            "IngredientType": "5593",
            "Unit": "tl",
            "IngredientTypeName": "Juustokumina"
          }],
          [{
            "Name": "Pirkka inkivääriä",
            "Ean": "6410405055606",
            "Amount": "1",
            "PackageRecipe": "false",
            "IngredientType": "5566",
            "Unit": "tl",
            "IngredientTypeName": "Inkivääri, jauhettu"
          }],
          [{
            "Name": "punaisia linssejä (kuivattu)",
            "Amount": "2",
            "PackageRecipe": "false",
            "IngredientType": "5998",
            "Unit": "dl",
            "IngredientTypeName": "Linssi, punainen"
          }],
          [{
            "Name": "Pirkka kookosmaitoa",
            "Ean": "6410402020669",
            "Amount": "1",
            "AmountInfo": "(400 ml)",
            "PackageRecipe": "false",
            "IngredientType": "5847",
            "Unit": "tlk",
            "IngredientTypeName": "Kookosmaito"
          }],
          [{
            "Name": "K-Menu kuorittuja tomaatteja tomaattimehussa",
            "Amount": "2",
            "AmountInfo": "(à 400 g/240 g)",
            "PackageRecipe": "false",
            "Unit": "tlk"
          }],
          [{
            "Name": "vettä",
            "Amount": "2",
            "PackageRecipe": "false",
            "IngredientType": "15792",
            "Unit": "dl",
            "IngredientTypeName": "Vesi"
          }],
          [{
            "Name": "Pirkka tomaattisosetta",
            "Ean": "6410405197382",
            "Amount": "2",
            "PackageRecipe": "false",
            "IngredientType": "7141",
            "Unit": "rkl",
            "IngredientTypeName": "Tomaattisose"
          }],
          [{
            "Name": "suolaa",
            "Amount": "1",
            "PackageRecipe": "false",
            "IngredientType": "7049",
            "Unit": "tl",
            "IngredientTypeName": "Suola"
          }],
          [{
            "Name": "mustapippuria",
            "PackageRecipe": "false",
            "IngredientType": "6269",
            "Unit": "ripaus",
            "IngredientTypeName": "Mustapippuri, kokonainen"
          }],
          [{
            "Name": "sitruunanmehua tai valkoviinietikkaa",
            "Amount": "1",
            "PackageRecipe": "false",
            "IngredientType": "6941",
            "Unit": "rkl",
            "IngredientTypeName": "Sitruuna"
          }],
          [{
            "Name": "sokeria",
            "Amount": "1-2",
            "PackageRecipe": "false",
            "IngredientType": "6961",
            "Unit": "tl",
            "IngredientTypeName": "Sokeri"
          }]
        ]
      }],
      "Instructions": "# Hienonna sipulit ja valkosipulit. Kuullota sipulit ja mausteet öljyssä isossa kattilassa. Huuhtele linssit lävikössä.\r\n# Lisää linssit, kookosmaito ja kuoritut tomaatit kattilaan. Huuhtele tomaattien tölkki vedellä (2 dl) ja lisää vesi kattilaan. Lisää myös tomaattisose, suola ja mustapippuri\r\n# Keitä kannen alla 15-20 minuuttia. Nosta kattila liedeltä ja soseuta keitto sauvaskoittimella. Lisää sitruunanmehu ja sokeri. Tarjoa linssikeitto tuoreen leivän kanssa.",
      "EndNote": "*Annoshinnat on laskettu K-citymarket-ketjun hintatason mukaan 10/2019. Tuoretuotteiden hinnat vaihtelevat sesongeittain.",
      "Description": "Linssikeitto on täyttävä ja helppo kasvisruoka, joka maistuu koko perheelle. Noin 1,65 €/annos*",
      "DateCreated": "2019-03-28T15:07:05",
      "DateModified": "2019-09-18T09:52:29",
      "Stamp": {
        "Name": "Testattu K-koekeittiössä",
        "Id": "1"
      },
      "RecipeUse": {
        "Name": "k-ruoka.fi",
        "Id": "1"
      },
      "resourceType": "recipe",
      "Url": "https://www.k-ruoka.fi/reseptit/linssikeitto",
      "UrlSlug": "linssikeitto",
      "Sort": [1, 1, 10304]
    }, {
      "Id": "10300",
      "Name": "Grillattu kana-caesarsalaatti",
      "PieceSize": {
        "Unit": "g",
        "Amount": "259"
      },
      "Portions": {
        "Unit": "annosta",
        "Amount": "4"
      },
      "PreparationTime": {
        "Description": "alle 30 min",
        "TimeRange": {
          "MinTime": 15,
          "MaxTime": 30
        }
      },
      "UserPortions": {
        "$": {
          "Unit": "henkilölle"
        }
      },
      "Categories": [{
        "MainName": "Sesonki",
        "MainId": "1",
        "SubName": "Grillaus",
        "SubId": "5"
      }, {
        "MainName": "Makuja maailmalta",
        "MainId": "2",
        "SubName": "Italia",
        "SubId": "10"
      }, {
        "MainName": "Alkuruoat",
        "MainId": "3",
        "SubName": "Salaatit",
        "SubId": "18"
      }, {
        "MainName": "Pääruoat",
        "MainId": "4",
        "SubName": "Salaatit",
        "SubId": "28"
      }, {
        "MainName": "Lisäkkeet",
        "MainId": "6",
        "SubName": "Kasvislisäkkeet",
        "SubId": "42"
      }, {
        "MainName": "Ruokalaji",
        "MainId": "16",
        "SubName": "Salaatit",
        "SubId": "137"
      }, {
        "MainName": "Asiakassegmentointi (K-asiakkaat)",
        "MainId": "17",
        "SubName": "Innostujat",
        "SubId": "147"
      }, {
        "MainName": "Asiakassegmentointi (K-asiakkaat)",
        "MainId": "17",
        "SubName": "Tiedostavat",
        "SubId": "148"
      }, {
        "MainName": "Lisäkkeet",
        "MainId": "6",
        "SubName": "Salaatit",
        "SubId": "44"
      }, {
        "MainName": "Aamu-,väli- ja iltapalat",
        "MainId": "11",
        "SubName": "Salaatit",
        "SubId": "70"
      }, {
        "MainName": "Lisäkkeet",
        "MainId": "6",
        "SubName": "Grilliruoan lisäkkeet",
        "SubId": "93"
      }, {
        "MainName": "Pääraaka-aine",
        "MainId": "14",
        "SubName": "Broileri ja linnut",
        "SubId": "118"
      }, {
        "MainName": "Sesonki",
        "MainId": "1",
        "SubName": "Kesä",
        "SubId": "121"
      }, {
        "MainName": "Sesonki",
        "MainId": "1",
        "SubName": "Juhannus",
        "SubId": "122"
      }],
      "Pictures": ["KSM_19_kanaceasar"],
      "PictureUrls": [{
        "Id": "KSM_19_kanaceasar",
        "Normal": "https://k-file-storage-qa.imgix.net/f/k-ruoka/recipe/10300?w=1000&h=1000&fit=clip",
        "Original": "https://k-file-storage-qa.imgix.net/f/k-ruoka/recipe/10300"
      }],
      "VideoUrl": "https://www.youtube.com/watch?v=0GJ_wW54oGs",
      "VideoEmbedUrl": "<iframe width=\"560\" height=\"315\" src=\"https://www.youtube.com/embed/0GJ_wW54oGs\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>",
      "Ingredients": [{
        "SubSectionHeading": "_",
        "SubSectionIngredients": [
          [{
            "Name": "Pirkka cosmopolitan-salaattia",
            "Ean": "6410405198259",
            "Amount": "2",
            "AmountInfo": "(à 250 g)",
            "PackageRecipe": "false",
            "IngredientType": "5368",
            "Unit": "ps",
            "IngredientTypeName": "Cosmopolitan salaatti"
          }],
          [{
            "Name": "sitruunaa",
            "Amount": "2",
            "PackageRecipe": "false",
            "IngredientType": "6941",
            "Unit": "",
            "IngredientTypeName": "Sitruuna"
          }],
          [{
            "Name": "Pirkka pekonia",
            "Ean": "6410405148674",
            "Amount": "2",
            "AmountInfo": "(à 140 g)",
            "PackageRecipe": "false",
            "IngredientType": "6498",
            "Unit": "pkt",
            "IngredientTypeName": "Pekoni"
          }],
          [{
            "Name": "rypsiöljyä",
            "Amount": "4",
            "PackageRecipe": "false",
            "IngredientType": "6807",
            "Unit": "rkl",
            "IngredientTypeName": "Rypsiöljy"
          }],
          [{
            "Name": "maalaisleipää",
            "Amount": "4",
            "PackageRecipe": "false",
            "IngredientType": "6019",
            "Unit": "viipaletta",
            "IngredientTypeName": "Maalaisleipä"
          }],
          [{
            "Name": "oliiviöljyä",
            "Amount": "2",
            "PackageRecipe": "false",
            "IngredientType": "6386",
            "Unit": "rkl",
            "IngredientTypeName": "Oliiviöljy"
          }],
          [{
            "Name": "Pirkka Parhaat caesarsalaattikastiketta",
            "Amount": "1 1/4",
            "PackageRecipe": "false",
            "IngredientType": "6830",
            "Unit": "dl",
            "IngredientTypeName": "Salaattikastike, caesar"
          }],
          [{
            "Name": "Pirkka Parhaat parmesaania lastuina",
            "Amount": "2",
            "AmountInfo": "(n. 100 g)",
            "PackageRecipe": "false",
            "IngredientType": "6468",
            "Unit": "dl",
            "IngredientTypeName": "Parmesaani"
          }],
          [{
            "Name": "mustapippuria",
            "PackageRecipe": "false",
            "IngredientType": "6269",
            "Unit": "ripaus",
            "IngredientTypeName": "Mustapippuri, kokonainen"
          }]
        ]
      }, {
        "SubSectionHeading": "Pestokana",
        "SubSectionIngredients": [
          [{
            "Name": "Kariniemen kananpojan minuuttipihvejä, maustamaton",
            "Amount": "1",
            "AmountInfo": "(700 g)",
            "PackageRecipe": "false",
            "IngredientType": "5299",
            "Unit": "rs",
            "IngredientTypeName": "Broilerin minuuttipihvi, maustamaton"
          }],
          [{
            "Name": "Pirkka vihreää pestoa",
            "Amount": "2",
            "PackageRecipe": "false",
            "IngredientType": "6536",
            "Unit": "rkl",
            "IngredientTypeName": "Pesto, vihreä"
          }],
          [{
            "Name": "suolaa",
            "PackageRecipe": "false",
            "IngredientType": "7049",
            "Unit": "ripaus",
            "IngredientTypeName": "Suola"
          }]
        ]
      }, {
        "SubSectionHeading": "Lisäksi",
        "SubSectionIngredients": [
          [{
            "Name": "puisia tai metallisia varrastikkuja",
            "PackageRecipe": "false",
            "IngredientType": "7288",
            "IngredientTypeName": "Varrastikku"
          }]
        ]
      }],
      "Instructions": "# Puolita salaatit pituussuunnassa, jätä salaatin kannat paikoilleen. Huuhtele salaatti ja anna kuivua. Puolita sitruunat.\r\n# Grillaa sitruunoita keskilämmöllä, leikkuupinta alaspäin 6-8 minuuttia. Lisää pekoniviipaleet ja grillaa niitä 3 minuuttia/puoli tai kunnes ne ovat rapeita. Nosta pekoni hetkeksi talouspaperin päälle.\r\n# Nosta grillin lämpötila maksimiin. Sivele halkaistujen salaattien leikkuupinnoille rypsiöljyä ja grillaa niitä leikkuupinta alaspäin 3 minuuttia tai kunnes leikkuupinnoissa on hyvä väri. Sivele leipien leikkuupinnat oliiviöljyllä ja grillaa nopeasti kummaltakin puolelta, kunnes ne ovat rapeita.\r\n# Sivele pullasudilla caesarkastiketta salaatin väleihin ja leikkuupinnan päälle. Hienonna pekoni. Lisää pekoni ja parmesaanilastut tasaisesti salaattien päälle. Mausta mustapippurilla. Tarjoa grillatun maalaisleivän ja sitruunan kanssa.\r\n# Levitä pesto fileiden toiselle puolelle. Rullaa fileet niin, että niiden ohuimmat päät jäävät rullien sisään. Sulje rullat varrastikuilla ja mausta pintasuolalla. Joskäytät puutikkuja, liota niitä ennakkoon vedessä.\r\n# Grillaa rullia keskilämmöllä kolmelta sivulta, 3 minuuttia/sivu.",
      "EndNote": "",
      "Description": "Rapeat salaatit sopivat loistavasti myös kevyesti grillattaviksi. Viimeistele salaatti caesarkastikkeella, pekonimurulla ja parmesaanilla.",
      "DateCreated": "2019-03-27T14:59:18",
      "DateModified": "2019-06-11T15:18:51",
      "Stamp": {
        "Name": "Kumppani",
        "Id": "6"
      },
      "RecipeUse": {
        "Name": "k-ruoka.fi",
        "Id": "1"
      },
      "resourceType": "recipe",
      "Url": "https://www.k-ruoka.fi/reseptit/grillattu-kana-caesarsalaatti",
      "UrlSlug": "grillattu-kana-caesarsalaatti",
      "Sort": [1, 1, 10300]
    }, {
      "Id": "10299",
      "Name": "Kesäkeitto grillatuista kasviksista",
      "PieceSize": {
        "Unit": "g",
        "Amount": "597"
      },
      "Portions": {
        "Unit": "annosta",
        "Amount": "4"
      },
      "PreparationTime": {
        "Description": "30 - 60 min",
        "TimeRange": {
          "MinTime": 30,
          "MaxTime": 60
        }
      },
      "UserPortions": {
        "$": {
          "Unit": "henkilölle"
        }
      },
      "Categories": [{
        "MainName": "Sesongin raaka-aineet",
        "MainId": "19",
        "SubName": "Varhaisperunat",
        "SubId": "169"
      }, {
        "MainName": "Sesongin raaka-aineet",
        "MainId": "19",
        "SubName": "Herne",
        "SubId": "171"
      }, {
        "MainName": "Sesongin raaka-aineet",
        "MainId": "19",
        "SubName": "Kukkakaali",
        "SubId": "174"
      }, {
        "MainName": "Sesonki",
        "MainId": "1",
        "SubName": "Juhannus",
        "SubId": "122"
      }, {
        "MainName": "Ruokalaji",
        "MainId": "16",
        "SubName": "Keitot",
        "SubId": "136"
      }, {
        "MainName": "Asiakassegmentointi (K-asiakkaat)",
        "MainId": "17",
        "SubName": "Innostujat",
        "SubId": "147"
      }, {
        "MainName": "Asiakassegmentointi (K-asiakkaat)",
        "MainId": "17",
        "SubName": "Tiedostavat",
        "SubId": "148"
      }, {
        "MainName": "Apukategoriat",
        "MainId": "18",
        "SubName": "Helppo",
        "SubId": "153"
      }, {
        "MainName": "Teema",
        "MainId": "15",
        "SubName": "Arkiruoka",
        "SubId": "159"
      }, {
        "MainName": "Pääruoat",
        "MainId": "4",
        "SubName": "Kasvisruoat",
        "SubId": "22"
      }, {
        "MainName": "Pääruoat",
        "MainId": "4",
        "SubName": "Keitot",
        "SubId": "27"
      }, {
        "MainName": "Makuja maailmalta",
        "MainId": "2",
        "SubName": "Skandinavia",
        "SubId": "105"
      }, {
        "MainName": "Makuja maailmalta",
        "MainId": "2",
        "SubName": "Suomi",
        "SubId": "106"
      }, {
        "MainName": "Pääraaka-aine",
        "MainId": "14",
        "SubName": "Kasvis",
        "SubId": "115"
      }, {
        "MainName": "Sesonki",
        "MainId": "1",
        "SubName": "Kesä",
        "SubId": "121"
      }, {
        "MainName": "Sesonki",
        "MainId": "1",
        "SubName": "Grillaus",
        "SubId": "5"
      }, {
        "MainName": "Alkuruoat",
        "MainId": "3",
        "SubName": "Keitot",
        "SubId": "17"
      }],
      "Pictures": ["KSM_19_kesakeitto"],
      "PictureUrls": [{
        "Id": "KSM_19_kesakeitto",
        "Normal": "https://k-file-storage-qa.imgix.net/f/k-ruoka/recipe/10299?w=1000&h=1000&fit=clip",
        "Original": "https://k-file-storage-qa.imgix.net/f/k-ruoka/recipe/10299"
      }],
      "SpecialDiets": [{
        "Id": "4",
        "Name": "Gluteeniton"
      }, {
        "Id": "6",
        "Name": "Kasvis (lakto-ovo)"
      }, {
        "Id": "7",
        "Name": "Kananmunaton"
      }],
      "Ingredients": [{
        "SubSectionHeading": "_",
        "SubSectionIngredients": [
          [{
            "Name": "pientä uutta perunaa",
            "Amount": "12-14",
            "AmountInfo": "(500 g)",
            "PackageRecipe": "false",
            "IngredientType": "6525",
            "Unit": "",
            "IngredientTypeName": "Varhaisperuna"
          }],
          [{
            "Name": "naattiporkkanaa",
            "Amount": "4-5",
            "AmountInfo": "(300 g)",
            "PackageRecipe": "false",
            "IngredientType": "5783",
            "Unit": "",
            "IngredientTypeName": "Kesäporkkana"
          }],
          [{
            "Name": "kukkakaali",
            "Amount": "1",
            "AmountInfo": "(400 g)",
            "PackageRecipe": "false",
            "IngredientType": "5885",
            "Unit": "",
            "IngredientTypeName": "Kukkakaali"
          }],
          [{
            "Name": "salottisipulia",
            "Amount": "2",
            "AmountInfo": "(140 g)",
            "PackageRecipe": "false",
            "IngredientType": "6932",
            "Unit": "",
            "IngredientTypeName": "Sipuli"
          }],
          [{
            "Name": "herneenpalkoja",
            "Amount": "3",
            "PackageRecipe": "false",
            "IngredientType": "5526",
            "Unit": "dl",
            "IngredientTypeName": "Herne, tuore"
          }],
          [{
            "Name": "rypsiöljyä",
            "Amount": "1",
            "PackageRecipe": "false",
            "IngredientType": "6807",
            "Unit": "rkl",
            "IngredientTypeName": "Rypsiöljy"
          }],
          [{
            "Name": "Pirkka täysmaitoa",
            "Ean": "6410405113559",
            "Amount": "1",
            "PackageRecipe": "false",
            "IngredientType": "7218",
            "Unit": "l",
            "IngredientTypeName": "Täysmaito"
          }],
          [{
            "Name": "kasvisfondia",
            "Amount": "1",
            "PackageRecipe": "false",
            "IngredientType": "5440",
            "Unit": "tl",
            "IngredientTypeName": "Fondi, kasvis"
          }],
          [{
            "Name": "Valio Koskenlaskija Koskenlaskija sulatejuustoa",
            "Amount": "3",
            "PackageRecipe": "false",
            "IngredientType": "16418",
            "Unit": "rkl",
            "IngredientTypeName": "Sulatejuusto, Valio Koskenlaskija"
          }],
          [{
            "Name": "rouhaisua valkopippuria myllystä",
            "Amount": "2",
            "PackageRecipe": "false",
            "IngredientType": "11162",
            "Unit": "",
            "IngredientTypeName": "Maustemylly, valkopippuri"
          }],
          [{
            "Name": "suolaa",
            "Amount": "1",
            "PackageRecipe": "false",
            "Unit": "tl"
          }],
          [{
            "Name": "voita",
            "Amount": "2",
            "PackageRecipe": "false",
            "Unit": "rkl"
          }],
          [{
            "Name": "kevätsipulinvartta hienonnettuna",
            "Amount": "2-3",
            "PackageRecipe": "false",
            "IngredientType": "5792",
            "Unit": "",
            "IngredientTypeName": "Kevätsipuli"
          }]
        ]
      }],
      "Instructions": "# Pese kasvikset kuorineen ja anna kuivua. Puolita perunat, halkaise porkkanat pituussuunnassa, irrota kukkakaalista kukinnot ja jätä ne kokonaisen pienen perunan kokoisiksi. Leikkaa kuoritut salottisipulit ohuiksi renkaiksi ja irrota herneet paloista.\r\n# Esikeitä perunoita 10-12 minuuttia kiehuvassa, suolalla maustetussa vedessä. Lisää porkkanat ja kukkakaali ja jatka keittämistä 2 minuuttia. Kaada vihannekset siivilään ja anna kosteuden haihtua. Pirskottele päälle rypsiöljyä, sekoita ja siirrä odottamaan grillausta.\r\n# Laita maito ja salottisipuli kattilaan ja keitä miedolla lämmöllä 6 minuuttia. Lisää kasvisfondi ja sulatejuusto. Mausta valkopippurilla, suolalla ja voilla. Jätä kattila liedelle matalalle lämmölle. Lisää herneet 2-3 minuuttia ennen tarjoamista.\r\n# Grillaa esikeitettyjä kasviksia maksimilämmöllä 4-6 minuuttia tai kunnes ne ovat saaneet kauniin värin. Kasvikset saavat jäädä hiukan napakoiksi. (Kasvikset voi kypsentää kokonaan myös grillissä. Sekoita öljy raakojen kasvisten sekaan ja asettele ne tasaisesti kahden reilun kokoisen leivinpaperin päälle. Perunat ja muut kasvikset erikseen. Sulje paketit ja kääri ne vielä folioon tiiviiksi nyytiksi. Kypsennä perunoita noin 20 minuuttia ja muita noin 15 minuuttia keskilämmöllä tai kunnes kasvikset ovat kypsiä.)\r\n# Nosta grillatut kasvikset lautasille ja kaada liemi lautasen reunaa pitkin sen pohjalle. Koristele annokset kevätsipulilla. Tarjoa grillatun ruisleivän ja voin kanssa.",
      "EndNote": "",
      "Description": "Kesäkeitto on parhaimmillaan, kun se tehdään sesongin tuoreista kasviksista. Grillaus tuo Koskenlaskijalla viimeisteltyyn keittoon ihanan kesän maun.",
      "DateCreated": "2019-03-27T14:45:36",
      "DateModified": "2019-08-05T13:15:06",
      "Stamp": {
        "Name": "Kumppani",
        "Id": "6"
      },
      "RecipeUse": {
        "Name": "k-ruoka.fi",
        "Id": "1"
      },
      "resourceType": "recipe",
      "Url": "https://www.k-ruoka.fi/reseptit/kesakeitto-grillatuista-kasviksista",
      "UrlSlug": "kesakeitto-grillatuista-kasviksista",
      "Sort": [1, 1, 10299]
    }, {
      "Id": "10296",
      "Name": "Helppo kalapata",
      "PieceSize": {
        "Unit": "g"
      },
      "Portions": {
        "Unit": "annosta",
        "Amount": "4"
      },
      "PreparationTime": {
        "Description": "30 - 60 min",
        "TimeRange": {
          "MinTime": 30,
          "MaxTime": 60
        }
      },
      "UserPortions": {
        "$": {
          "Unit": "henkilölle"
        }
      },
      "Categories": [{
        "MainName": "Pääruoat",
        "MainId": "4",
        "SubName": "Kalaruoat",
        "SubId": "20"
      }, {
        "MainName": "Pääraaka-aine",
        "MainId": "14",
        "SubName": "Kala",
        "SubId": "116"
      }, {
        "MainName": "Teema",
        "MainId": "15",
        "SubName": "Mitä tänään syötäisiin?",
        "SubId": "144"
      }, {
        "MainName": "Asiakassegmentointi (K-asiakkaat)",
        "MainId": "17",
        "SubName": "Tiedostavat",
        "SubId": "148"
      }, {
        "MainName": "Asiakassegmentointi (K-asiakkaat)",
        "MainId": "17",
        "SubName": "Vakiintuneet",
        "SubId": "150"
      }],
      "Pictures": ["PI8_19_helppo_kalapata"],
      "PictureUrls": [{
        "Id": "PI8_19_helppo_kalapata",
        "Normal": "https://k-file-storage-qa.imgix.net/f/k-ruoka/recipe/10296?w=1000&h=1000&fit=clip",
        "Original": "https://k-file-storage-qa.imgix.net/f/k-ruoka/recipe/10296"
      }],
      "VideoUrl": "https://youtu.be/ncLlueguI_E",
      "VideoEmbedUrl": "<iframe width=\"560\" height=\"315\" src=\"https://www.youtube.com/embed/ncLlueguI_E\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>",
      "Ingredients": [{
        "SubSectionHeading": "_",
        "SubSectionIngredients": [
          [{
            "Name": "Pirkka alaskanseitiä annospaloina (pakaste)",
            "Ean": "6410405098672",
            "Amount": "1",
            "AmountInfo": "(400 g)",
            "PackageRecipe": "false",
            "IngredientType": "5207",
            "Unit": "pkt",
            "IngredientTypeName": "Alaskanseiti, annospala, pakaste"
          }],
          [{
            "Name": "Meira kalamaustetta, savuinen pippuri",
            "Amount": "2",
            "PackageRecipe": "false",
            "IngredientType": "16218",
            "Unit": "rkl",
            "IngredientTypeName": "Meira kalamauste, savuinen pippuri"
          }],
          [{
            "Name": "suolaa",
            "Amount": "1 1/2",
            "PackageRecipe": "false",
            "Unit": "tl"
          }],
          [{
            "Name": "punasipuli",
            "Amount": "1",
            "AmountInfo": "(100 g)",
            "PackageRecipe": "false",
            "IngredientType": "6667",
            "Unit": "",
            "IngredientTypeName": "Punasipuli"
          }],
          [{
            "Name": "valkosipulinkynttä",
            "Amount": "2",
            "PackageRecipe": "false",
            "IngredientType": "7262",
            "Unit": "",
            "IngredientTypeName": "Valkosipuli"
          }],
          [{
            "Name": "öljyä",
            "Amount": "1",
            "PackageRecipe": "false",
            "Unit": "rkl"
          }],
          [{
            "Name": "Pirkka tomaattimurskaa",
            "Ean": "6410402011865",
            "Amount": "2",
            "AmountInfo": "(à 500 g)",
            "PackageRecipe": "false",
            "IngredientType": "7136",
            "Unit": "tlk",
            "IngredientTypeName": "Tomaattimurska, chili"
          }],
          [{
            "Name": "sokeria",
            "PackageRecipe": "false",
            "IngredientType": "6961",
            "Unit": "ripaus",
            "IngredientTypeName": "Sokeri"
          }],
          [{
            "Name": "(lehti)persiljaa hienonnettuna",
            "Amount": "1/2",
            "PackageRecipe": "false",
            "IngredientType": "5954",
            "Unit": "dl",
            "IngredientTypeName": "Lehtipersilja"
          }]
        ]
      }],
      "Instructions": "# Ota seiti lautaselle sulamaan. Leikkaa kohmeiset kalapalat neljään osaan ja pyörittele mausteseoksessa ja suolassa (1/2 tl). Anna maustua sen aikaa, kun teet tomaattikastikkeen.\r\n# Hienonna kuorittu punasipuli ja valkosipulinkynnet. Kuullota sipulit padassa öljyssä, lisää joukkoon tomaattimurska. Mausta suolalla (1 tl) ja sokerilla. Anna kastikkeen kiehua kannen alla noin 10 minuuttia.\r\n# Nosta kalapalat kastikkeen joukkoon. Peitä pata kannella ja kypsennä vielä 10-15 minuuttia, kunnes kala on kypsää. Lisää persilja juuri ennen tarjoamista. Tarjoa kalapata tuoreen leivän tai keitetyn riisin kanssa.",
      "EndNote": "*Annoshinnat on laskettu K-citymarket-ketjun hintatason mukaan 8/2019. Tuoretuotteiden hinnat vaihtelevat sesongeittain.",
      "Description": "Helppo kalapata on maukas tomaattinen kalaruoka. Mausta kalat savupaprikaisella mausteseoksella. Tarjoa maukas kalapata tuoreen leivän kanssa tai riisin kanssa. Noin 1,35 €/annos*",
      "DateCreated": "2019-03-22T14:42:00",
      "DateModified": "2019-08-26T17:54:18",
      "TvDate": "2019-08-29T00:00:00",
      "Stamp": {
        "Name": "Testattu K-koekeittiössä",
        "Id": "1"
      },
      "RecipeUse": {
        "Name": "k-ruoka.fi",
        "Id": "1"
      },
      "resourceType": "recipe",
      "Url": "https://www.k-ruoka.fi/reseptit/helppo-kalapata",
      "UrlSlug": "helppo-kalapata",
      "Sort": [1, 1, 10296]
    }, {
      "Id": "10295",
      "Name": "Karitsaflatbread",
      "PieceSize": {
        "Unit": "g",
        "Amount": "150"
      },
      "Portions": {
        "Unit": "annosta",
        "Amount": "8"
      },
      "PreparationTime": {
        "Description": "yli 60 min",
        "TimeRange": {
          "MinTime": 60
        }
      },
      "UserPortions": {
        "$": {
          "Unit": "henkilölle"
        }
      },
      "Categories": [{
        "MainName": "Sesonki",
        "MainId": "1",
        "SubName": "Grillaus",
        "SubId": "5"
      }, {
        "MainName": "Pääruoat",
        "MainId": "4",
        "SubName": "Grilliruoat",
        "SubId": "73"
      }, {
        "MainName": "Lisäkkeet",
        "MainId": "6",
        "SubName": "Grilliruoan lisäkkeet",
        "SubId": "93"
      }, {
        "MainName": "Pääraaka-aine",
        "MainId": "14",
        "SubName": "Liha",
        "SubId": "114"
      }, {
        "MainName": "Sesonki",
        "MainId": "1",
        "SubName": "Kesä",
        "SubId": "121"
      }, {
        "MainName": "Iltapalat",
        "MainId": "21",
        "SubName": "Leivät ja sämpylät",
        "SubId": "198"
      }, {
        "MainName": "Asiakassegmentointi (K-asiakkaat)",
        "MainId": "17",
        "SubName": "Innostujat",
        "SubId": "147"
      }, {
        "MainName": "Asiakassegmentointi (K-asiakkaat)",
        "MainId": "17",
        "SubName": "Tiedostavat",
        "SubId": "148"
      }, {
        "MainName": "Apukategoriat",
        "MainId": "18",
        "SubName": "Haastavampi",
        "SubId": "154"
      }, {
        "MainName": "Apukategoriat",
        "MainId": "18",
        "SubName": "Trendikäs ",
        "SubId": "156"
      }, {
        "MainName": "Sesongin raaka-aineet",
        "MainId": "19",
        "SubName": "Herne",
        "SubId": "171"
      }, {
        "MainName": "Aamupalat",
        "MainId": "20",
        "SubName": "Leivät ja sämpylät",
        "SubId": "191"
      }],
      "Pictures": ["KCM_Karitsaflatbread"],
      "PictureUrls": [{
        "Id": "KCM_Karitsaflatbread",
        "Normal": "https://k-file-storage-qa.imgix.net/f/k-ruoka/recipe/10295?w=1000&h=1000&fit=clip",
        "Original": "https://k-file-storage-qa.imgix.net/f/k-ruoka/recipe/10295"
      }],
      "Ingredients": [{
        "SubSectionHeading": "_",
        "SubSectionIngredients": [
          [{
            "Name": "karitsan paahtopaistia",
            "Amount": "4",
            "AmountInfo": "(n. 800 g)",
            "PackageRecipe": "false",
            "IngredientType": "5706",
            "Unit": "",
            "IngredientTypeName": "Karitsan paahtopaisti"
          }],
          [{
            "Name": "suolaa",
            "Amount": "1",
            "PackageRecipe": "false",
            "IngredientType": "7049",
            "Unit": "tl",
            "IngredientTypeName": "Suola"
          }],
          [{
            "Name": "mustapippuria",
            "PackageRecipe": "false",
            "IngredientType": "6271",
            "IngredientTypeName": "Mustapippuri, rouhittu"
          }],
          [{
            "Name": "pieni kesäkurpitsa",
            "Amount": "1",
            "AmountInfo": "(350 g)",
            "PackageRecipe": "false",
            "IngredientType": "5781",
            "Unit": "",
            "IngredientTypeName": "Kesäkurpitsa"
          }],
          [{
            "Name": "kukkakaali",
            "Amount": "1/2",
            "AmountInfo": "(350 g)",
            "PackageRecipe": "false",
            "IngredientType": "5885",
            "Unit": "",
            "IngredientTypeName": "Kukkakaali"
          }],
          [{
            "Name": "fetajuustoa",
            "Amount": "100",
            "PackageRecipe": "false",
            "IngredientType": "5432",
            "Unit": "g",
            "IngredientTypeName": "Fetajuusto"
          }],
          [{
            "Name": "pinjansiemeniä",
            "Amount": "1/2",
            "PackageRecipe": "false",
            "IngredientType": "6558",
            "Unit": "dl",
            "IngredientTypeName": "Pinjansiemen"
          }],
          [{
            "Name": "minttua",
            "PackageRecipe": "false",
            "IngredientType": "15789",
            "IngredientTypeName": "Minttu"
          }]
        ]
      }, {
        "SubSectionHeading": "Hernehummus",
        "SubSectionIngredients": [
          [{
            "Name": "Pirkka suomalaisia herneitä (pakaste)",
            "Amount": "1",
            "AmountInfo": "(200 g)",
            "PackageRecipe": "false",
            "IngredientType": "5524",
            "Unit": "ps",
            "IngredientTypeName": "Herne, pakaste"
          }],
          [{
            "Name": "oliiviöljyä",
            "Amount": "2",
            "PackageRecipe": "false",
            "IngredientType": "6386",
            "Unit": "rkl",
            "IngredientTypeName": "Oliiviöljy"
          }],
          [{
            "Name": "tahinia",
            "Amount": "1",
            "PackageRecipe": "false",
            "IngredientType": "6872",
            "Unit": "rkl",
            "IngredientTypeName": "Tahini, seesamitahna"
          }],
          [{
            "Name": "valkosipulinkynsi",
            "Amount": "1",
            "PackageRecipe": "false",
            "IngredientType": "7262",
            "Unit": "",
            "IngredientTypeName": "Valkosipuli"
          }],
          [{
            "Name": "minttua",
            "Amount": "1/2",
            "PackageRecipe": "false",
            "IngredientType": "15789",
            "Unit": "ruukku",
            "IngredientTypeName": "Minttu"
          }],
          [{
            "Name": "suolaa",
            "Amount": "1/2",
            "PackageRecipe": "false",
            "IngredientType": "7049",
            "Unit": "tl",
            "IngredientTypeName": "Suola"
          }],
          [{
            "Name": "mustapippuria",
            "AmountInfo": "ripaus",
            "PackageRecipe": "false",
            "IngredientType": "6271",
            "IngredientTypeName": "Mustapippuri, rouhittu"
          }]
        ]
      }, {
        "SubSectionHeading": "Leivät",
        "SubSectionIngredients": [
          [{
            "Name": "vehnäjauhoja",
            "Amount": "3",
            "PackageRecipe": "false",
            "IngredientType": "7293",
            "Unit": "dl",
            "IngredientTypeName": "Vehnäjauho"
          }],
          [{
            "Name": "suolaa",
            "Amount": "1",
            "PackageRecipe": "false",
            "IngredientType": "6221",
            "Unit": "tl",
            "IngredientTypeName": "Merisuola"
          }],
          [{
            "Name": "rypsiöljyä",
            "Amount": "1",
            "PackageRecipe": "false",
            "IngredientType": "6807",
            "Unit": "rkl",
            "IngredientTypeName": "Rypsiöljy"
          }],
          [{
            "Name": "vettä",
            "Amount": "n. 1 1/2",
            "PackageRecipe": "false",
            "IngredientType": "15792",
            "Unit": "dl",
            "IngredientTypeName": "Vesi"
          }]
        ]
      }],
      "Instructions": "# Ota liha huoneenlämpöön noin tuntia ennen grillausta.\r\n# Sulata herneet ja laita kulhoon. Lisää joukkoon muut hernehummuksen aineet ja soseuta sauvasekoittimella tasaiseksi.\r\n# Paloittele kesäkurpitsa ja kukkakaali.\r\n# Sekoita jauhot ja suola kulhossa. Lisää ensin öljy, sitten vähitellen vettä ja sekoita kimmoisaksi taikinaksi. Jätä taikina lepäämään kelmuttuna.\r\n# Mausta paahtopaistit kauttaaltaan suolalla ja pippurilla. Grillaa lihan pinnat ensin suoralla lämmöllä ja jatka grillausta epäsuoralla lämmöllä välillä käännellen, yhteensä 15-20 minuuttia. Voit käyttää lihalämpömittaria lihan kypsyyden arvioimiseen. Sisälämmön tulisi olla 55-58 astetta, jolloin liha jää punertavaksi. Kun paahtopaistit ovat valmiita, kääri ne folioon ja anna vetäytyä 10 minuuttia.\r\n# Grillaa kasvikset molemmin puolin, siten että saat niihin kauniin pintavärin.\r\n# Jaa leipätaikina 8 osaan ja kauli ohuiksi soikeiksi leipäsiksi. Grillaa leivät nopeasti molemmin puolin.\r\n# Viipaloi paistit. Kokoa annokset levittämällä leiville hernehummusta, asettelemalla paahtopaistia ja kasviksia, ripottelemalla fetaa, pinjansiemeniä ja mintunlehtiä.",
      "EndNote": "",
      "Description": "Trendikkäät flatbreadit höystetään grillatulla karitsan paahtopaistilla, hernehummuksella ja fetalla.",
      "DateCreated": "2019-03-22T13:21:38",
      "DateModified": "2019-08-05T12:57:27",
      "Stamp": {
        "Name": "Testattu K-koekeittiössä",
        "Id": "1"
      },
      "RecipeUse": {
        "Name": "k-ruoka.fi",
        "Id": "1"
      },
      "resourceType": "recipe",
      "Url": "https://www.k-ruoka.fi/reseptit/karitsaflatbread",
      "UrlSlug": "karitsaflatbread",
      "Sort": [1, 1, 10295]
    }, {
      "Id": "10293",
      "Name": "Ribsit ja kirsikkainen BBQ-kastike",
      "PieceSize": {
        "Unit": "g",
        "Amount": "445"
      },
      "Portions": {
        "Unit": "annosta",
        "Amount": "4"
      },
      "PreparationTime": {
        "Description": "yli 60 min",
        "TimeRange": {
          "MinTime": 60
        }
      },
      "UserPortions": {
        "$": {
          "Unit": "henkilölle"
        }
      },
      "Categories": [{
        "MainName": "Sesonki",
        "MainId": "1",
        "SubName": "Grillaus",
        "SubId": "5"
      }, {
        "MainName": "Pääruoat",
        "MainId": "4",
        "SubName": "Grilliruoat",
        "SubId": "73"
      }, {
        "MainName": "Lisäkkeet",
        "MainId": "6",
        "SubName": "Marinadit ja maustesekoitukset",
        "SubId": "92"
      }, {
        "MainName": "Pääraaka-aine",
        "MainId": "14",
        "SubName": "Liha",
        "SubId": "114"
      }],
      "Pictures": ["KCM_Ribsit_ja_kirsikkai"],
      "PictureUrls": [{
        "Id": "KCM_Ribsit_ja_kirsikkai",
        "Normal": "https://k-file-storage-qa.imgix.net/f/k-ruoka/recipe/10293?w=1000&h=1000&fit=clip",
        "Original": "https://k-file-storage-qa.imgix.net/f/k-ruoka/recipe/10293"
      }],
      "SpecialDiets": [{
        "Id": "4",
        "Name": "Gluteeniton"
      }, {
        "Id": "3",
        "Name": "Maidoton"
      }],
      "Ingredients": [{
        "SubSectionHeading": "_",
        "SubSectionIngredients": [
          [{
            "Name": "naudan ribsejä",
            "Amount": "2",
            "PackageRecipe": "false",
            "IngredientType": "16302",
            "Unit": "kg",
            "IngredientTypeName": "Naudan ribs, maustamaton"
          }],
          [{
            "Name": "colajuomaa (ei light)",
            "Amount": "1,4",
            "PackageRecipe": "false",
            "IngredientType": "7335",
            "Unit": "l",
            "IngredientTypeName": "Virvoitusjuoma, cola"
          }],
          [{
            "Name": "sipulia",
            "Amount": "2",
            "AmountInfo": "(200 g)",
            "PackageRecipe": "false",
            "IngredientType": "6932",
            "Unit": "",
            "IngredientTypeName": "Sipuli"
          }],
          [{
            "Name": "valkosipulinkynttä",
            "Amount": "4",
            "PackageRecipe": "false",
            "IngredientType": "7262",
            "Unit": "",
            "IngredientTypeName": "Valkosipuli"
          }]
        ]
      }, {
        "SubSectionHeading": "Kirsikka-bbq-kastike",
        "SubSectionIngredients": [
          [{
            "Name": "Pirkka Parhaat kirsikoita (pakaste)",
            "Ean": "6410405205919",
            "Amount": "1",
            "AmountInfo": "(250 g)",
            "PackageRecipe": "false",
            "IngredientType": "5824",
            "Unit": "pkt",
            "IngredientTypeName": "Kirsikka, tuore"
          }],
          [{
            "Name": "pieni sipuli",
            "Amount": "1",
            "AmountInfo": "(50 g)",
            "PackageRecipe": "false",
            "IngredientType": "6932",
            "Unit": "",
            "IngredientTypeName": "Sipuli"
          }],
          [{
            "Name": "valkosipulinkynttä",
            "Amount": "2",
            "PackageRecipe": "false",
            "IngredientType": "7262",
            "Unit": "",
            "IngredientTypeName": "Valkosipuli"
          }],
          [{
            "Name": "rypsiöljyä",
            "Amount": "2",
            "PackageRecipe": "false",
            "IngredientType": "6807",
            "Unit": "rkl",
            "IngredientTypeName": "Rypsiöljy"
          }],
          [{
            "Name": "colajuomaa (ei light)",
            "Amount": "1",
            "PackageRecipe": "false",
            "IngredientType": "7335",
            "Unit": "dl",
            "IngredientTypeName": "Virvoitusjuoma, cola"
          }],
          [{
            "Name": "balsamiviinietikkaa",
            "Amount": "1/2",
            "PackageRecipe": "false",
            "IngredientType": "5257",
            "Unit": "dl",
            "IngredientTypeName": "Balsamietikka, tumma"
          }],
          [{
            "Name": "ketsuppia",
            "Amount": "1/2",
            "PackageRecipe": "false",
            "IngredientType": "5784",
            "Unit": "dl",
            "IngredientTypeName": "Ketsuppi"
          }],
          [{
            "Name": "muscovadosokeria",
            "Amount": "2",
            "PackageRecipe": "false",
            "IngredientType": "13632",
            "Unit": "rkl",
            "IngredientTypeName": "Ruokosokeri, tumma, muscovado"
          }],
          [{
            "Name": "suolaa",
            "Amount": "1",
            "PackageRecipe": "false",
            "IngredientType": "7049",
            "Unit": "tl",
            "IngredientTypeName": "Suola"
          }],
          [{
            "Name": "savuaromia",
            "Amount": "1",
            "PackageRecipe": "false",
            "IngredientType": "15980",
            "Unit": "tl",
            "IngredientTypeName": "Savuaromi"
          }]
        ]
      }],
      "Instructions": "# Leikkaa ribsit isoon kattilaan sopiviksi paloiksi. Kaada joukkoon colajuoma, paloiteltu sipuli ja valkosipulinkynnet. Keitä ribsejä 1-1,5 tuntia, kunnes liha on niin kypsää, että se irtoaa helposti luista. Jäähdytä ribsit liemessään.\r\n# Kuori ja hienonna sipuli ja valkosipulinkynnet. Kuullota sipuleita öljyssä kasarissa. Lisää muut aineet ja keitä ilman kantta keskilämmöllä noin 20 minuuttia. Soseuta kastike sileäksi.\r\n# Sivele bbq-kastiketta ribsien pintoihin. Grillaa ribseihin rapea pinta kuumassa grillissä. Sivele kypsennyksen aikana lisää bbq-kastiketta.",
      "EndNote": "",
      "Description": "Itsetehty kirsikkainen BBQ-kastike viimeistelee mehevät naudan ribsit.",
      "DateCreated": "2019-03-22T13:11:42",
      "DateModified": "2019-04-25T12:10:27",
      "Stamp": {
        "Name": "Testattu K-koekeittiössä",
        "Id": "1"
      },
      "RecipeUse": {
        "Name": "k-ruoka.fi",
        "Id": "1"
      },
      "resourceType": "recipe",
      "Url": "https://www.k-ruoka.fi/reseptit/ribsit-ja-kirsikkainen-bbq-kastike",
      "UrlSlug": "ribsit-ja-kirsikkainen-bbq-kastike",
      "Sort": [1, 1, 10293]
    }, {
      "Id": "10292",
      "Name": "Meksikolaiset ribsit ja avokado-coleslaw",
      "PieceSize": {
        "Unit": "g",
        "Amount": "630"
      },
      "Portions": {
        "Unit": "annosta",
        "Amount": "4"
      },
      "PreparationTime": {
        "Description": "yli 60 min",
        "TimeRange": {
          "MinTime": 60
        }
      },
      "UserPortions": {
        "$": {
          "Unit": "henkilölle"
        }
      },
      "Categories": [{
        "MainName": "Sesonki",
        "MainId": "1",
        "SubName": "Grillaus",
        "SubId": "5"
      }, {
        "MainName": "Makuja maailmalta",
        "MainId": "2",
        "SubName": "Meksiko ja texmex",
        "SubId": "12"
      }, {
        "MainName": "Pääruoat",
        "MainId": "4",
        "SubName": "Grilliruoat",
        "SubId": "73"
      }, {
        "MainName": "Lisäkkeet",
        "MainId": "6",
        "SubName": "Grilliruoan lisäkkeet",
        "SubId": "93"
      }, {
        "MainName": "Pääraaka-aine",
        "MainId": "14",
        "SubName": "Liha",
        "SubId": "114"
      }],
      "Pictures": ["KCM_Meksikolaiset_ribsi"],
      "PictureUrls": [{
        "Id": "KCM_Meksikolaiset_ribsi",
        "Normal": "https://k-file-storage-qa.imgix.net/f/k-ruoka/recipe/10292?w=1000&h=1000&fit=clip",
        "Original": "https://k-file-storage-qa.imgix.net/f/k-ruoka/recipe/10292"
      }],
      "SpecialDiets": [{
        "Id": "2",
        "Name": "Laktoositon"
      }, {
        "Id": "3",
        "Name": "Maidoton"
      }],
      "Ingredients": [{
        "SubSectionHeading": "_",
        "SubSectionIngredients": [
          [{
            "Name": "HK Viljaporsaan Spare ribsejä",
            "Amount": "1",
            "AmountInfo": "(n. 2 kg)",
            "PackageRecipe": "false",
            "IngredientType": "6622",
            "Unit": "pkt",
            "IngredientTypeName": "Porsaan ribs, maustamaton"
          }],
          [{
            "Name": "BBQ-kastiketta",
            "Amount": "1",
            "PackageRecipe": "false",
            "IngredientType": "5466",
            "Unit": "dl",
            "IngredientTypeName": "Grillauskastike, BBQ"
          }]
        ]
      }, {
        "SubSectionHeading": "Mausteseos",
        "SubSectionIngredients": [
          [{
            "Name": "ruokosokeria",
            "Amount": "3",
            "PackageRecipe": "false",
            "IngredientType": "6795",
            "Unit": "rkl",
            "IngredientTypeName": "Ruokokidesokeri"
          }],
          [{
            "Name": "suolaa",
            "Amount": "1",
            "PackageRecipe": "false",
            "IngredientType": "7049",
            "Unit": "rkl",
            "IngredientTypeName": "Suola"
          }],
          [{
            "Name": "juustokuminaa",
            "Amount": "1",
            "PackageRecipe": "false",
            "IngredientType": "5593",
            "Unit": "rkl",
            "IngredientTypeName": "Juustokumina"
          }],
          [{
            "Name": "jauhettua korianteria",
            "Amount": "1",
            "PackageRecipe": "false",
            "IngredientType": "5850",
            "Unit": "rkl",
            "IngredientTypeName": "Korianteri, jauhettu"
          }],
          [{
            "Name": "oreganoa",
            "Amount": "1",
            "PackageRecipe": "false",
            "IngredientType": "6410",
            "Unit": "rkl",
            "IngredientTypeName": "Oregano, tuore"
          }],
          [{
            "Name": "savupaprikajauhetta",
            "Amount": "2",
            "PackageRecipe": "false",
            "IngredientType": "15800",
            "Unit": "tl",
            "IngredientTypeName": "paprika, jauhettu, savustettu"
          }],
          [{
            "Name": "valkosipulijauhetta",
            "Amount": "2",
            "PackageRecipe": "false",
            "IngredientType": "7430",
            "Unit": "tl",
            "IngredientTypeName": "Valkosipulijauhe"
          }]
        ]
      }, {
        "SubSectionHeading": "Avokado-coleslaw",
        "SubSectionIngredients": [
          [{
            "Name": "kaalia",
            "Amount": "500",
            "PackageRecipe": "false",
            "IngredientType": "7256",
            "Unit": "g",
            "IngredientTypeName": "Valkokaali"
          }],
          [{
            "Name": "porkkanaa",
            "Amount": "2",
            "AmountInfo": "(200 g)",
            "PackageRecipe": "false",
            "IngredientType": "6582",
            "Unit": "",
            "IngredientTypeName": "Porkkana"
          }],
          [{
            "Name": "Pirkka maissia (pakaste)",
            "Ean": "6410405045874",
            "Amount": "1",
            "AmountInfo": "(200 g)",
            "PackageRecipe": "false",
            "IngredientType": "6028",
            "Unit": "ps",
            "IngredientTypeName": "Maissi, pakaste"
          }],
          [{
            "Name": "korianteria",
            "Amount": "1",
            "PackageRecipe": "false",
            "IngredientType": "5852",
            "Unit": "ruukku",
            "IngredientTypeName": "Korianteri, tuore"
          }]
        ]
      }, {
        "SubSectionHeading": "Kastike",
        "SubSectionIngredients": [
          [{
            "Name": "avokadoa",
            "Amount": "2",
            "AmountInfo": "(300 g)",
            "PackageRecipe": "false",
            "IngredientType": "7605",
            "Unit": "",
            "IngredientTypeName": "Avokado, tuore"
          }],
          [{
            "Name": "limetin mehua",
            "Amount": "2",
            "PackageRecipe": "false",
            "IngredientType": "5997",
            "Unit": "rkl",
            "IngredientTypeName": "Limetti"
          }],
          [{
            "Name": "suolaa",
            "Amount": "3/4",
            "PackageRecipe": "false",
            "IngredientType": "7049",
            "Unit": "tl",
            "IngredientTypeName": "Suola"
          }],
          [{
            "Name": "mustapippuria",
            "AmountInfo": "ripaus",
            "PackageRecipe": "false",
            "IngredientType": "6271",
            "IngredientTypeName": "Mustapippuri, rouhittu"
          }]
        ]
      }],
      "Instructions": "# Sekoita mausteseoksen aineet keskenään.\r\n# Siirrä ribsit isoon uunivuokaan ja hiero mausteseos ribsien pintaan. Anna maustua huoneenlämmössä tunnin ajan.\r\n# Kypsennä ribsejä kannella tai foliolla peitettynä 150-asteisessa uunissa noin 3 tuntia, kunnes liha on niin kypsää, että se irtoaa helposti luista.\r\n# Suikaloi kaali. Kuori ja raasta porkkanat karkeaksi raasteeksi. Kiehauta ja valuta maissi. Kuori avokadot ja kaavi hedelmälihat kulhoon. Murskaa avokadot tasaiseksi ja lisää kastikkeen muut aineet. Sekoita kastike kaaliin, porkkanaan ja maissiin. Sekoita joukkoon myös hienonnettu korianteri.\r\n# Grillaa ribseihin rapea pinta nopeasti kuumassa grillissä. Sivele kypsennyksen aikana BBQ-kastikkeella. Tarjoa ribsit avokado-coleslawn kanssa.",
      "EndNote": "",
      "Description": "Ribsit maustetaan meksikolaisella savuisella dry rubilla ja seurakseen ribsit saavat avokadolla höystettyä, vegaanista coleslaw -salaattia.",
      "DateCreated": "2019-03-22T13:07:42",
      "DateModified": "2019-04-25T12:11:36",
      "Stamp": {
        "Name": "Testattu K-koekeittiössä",
        "Id": "1"
      },
      "RecipeUse": {
        "Name": "k-ruoka.fi",
        "Id": "1"
      },
      "resourceType": "recipe",
      "Url": "https://www.k-ruoka.fi/reseptit/meksikolaiset-ribsit-ja-avokado-coleslaw",
      "UrlSlug": "meksikolaiset-ribsit-ja-avokado-coleslaw",
      "Sort": [1, 1, 10292]
    }, {
      "Id": "10291",
      "Name": "Maissi-makkaranyytit",
      "PieceSize": {
        "Unit": "g",
        "Amount": "350"
      },
      "Portions": {
        "Unit": "annosta",
        "Amount": "4"
      },
      "PreparationTime": {
        "Description": "30 - 60 min",
        "TimeRange": {
          "MinTime": 30,
          "MaxTime": 60
        }
      },
      "UserPortions": {
        "$": {
          "Unit": "henkilölle"
        }
      },
      "Categories": [{
        "MainName": "Sesonki",
        "MainId": "1",
        "SubName": "Grillaus",
        "SubId": "5"
      }, {
        "MainName": "Pääruoat",
        "MainId": "4",
        "SubName": "Grilliruoat",
        "SubId": "73"
      }, {
        "MainName": "Pääraaka-aine",
        "MainId": "14",
        "SubName": "Makkara",
        "SubId": "113"
      }],
      "Pictures": ["KCM_Maissi-makkaranyyti"],
      "PictureUrls": [{
        "Id": "KCM_Maissi-makkaranyyti",
        "Normal": "https://k-file-storage-qa.imgix.net/f/k-ruoka/recipe/10291?w=1000&h=1000&fit=clip",
        "Original": "https://k-file-storage-qa.imgix.net/f/k-ruoka/recipe/10291"
      }],
      "SpecialDiets": [{
        "Id": "3",
        "Name": "Maidoton"
      }, {
        "Id": "4",
        "Name": "Gluteeniton"
      }],
      "Ingredients": [{
        "SubSectionHeading": "_",
        "SubSectionIngredients": [
          [{
            "Name": "Atria Artesaani Krouvi grillimakkaraa",
            "Amount": "2",
            "AmountInfo": "(á 250 g)",
            "PackageRecipe": "false",
            "IngredientType": "16105",
            "Unit": "pkt",
            "IngredientTypeName": "Atria Artesaani Krouvi grillimakkara"
          }],
          [{
            "Name": "iso bataatti",
            "Amount": "1",
            "AmountInfo": "(600 g)",
            "PackageRecipe": "false",
            "IngredientType": "5273",
            "Unit": "",
            "IngredientTypeName": "Bataatti"
          }],
          [{
            "Name": "Pirkka ohuita vihreitä papuja (pakaste)",
            "Ean": "6410405045713",
            "Amount": "200",
            "PackageRecipe": "false",
            "IngredientType": "12308",
            "Unit": "g",
            "IngredientTypeName": "Papu, vihreä, pakaste"
          }],
          [{
            "Name": "Pirkka grillimaissipaloja",
            "Amount": "1",
            "AmountInfo": "(400 g)",
            "PackageRecipe": "false",
            "IngredientType": "16326",
            "Unit": "pkt",
            "IngredientTypeName": "Grillimaissi"
          }],
          [{
            "Name": "rypsiöljyä",
            "Amount": "1/2",
            "PackageRecipe": "false",
            "IngredientType": "6807",
            "Unit": "dl",
            "IngredientTypeName": "Rypsiöljy"
          }],
          [{
            "Name": "lehtipersiljaa hienonnettuna",
            "Amount": "2",
            "PackageRecipe": "false",
            "IngredientType": "5954",
            "Unit": "rkl",
            "IngredientTypeName": "Lehtipersilja"
          }],
          [{
            "Name": "valkosipulinkynttä hienonnettuna",
            "Amount": "2",
            "PackageRecipe": "false",
            "IngredientType": "7262",
            "Unit": "",
            "IngredientTypeName": "Valkosipuli"
          }],
          [{
            "Name": "suolaa",
            "AmountInfo": "ripaus",
            "PackageRecipe": "false"
          }],
          [{
            "Name": "mustapippuria",
            "AmountInfo": "ripaus",
            "PackageRecipe": "false",
            "IngredientType": "6271",
            "IngredientTypeName": "Mustapippuri, rouhittu"
          }]
        ]
      }, {
        "SubSectionHeading": "Savupaprikamajoneesi",
        "SubSectionIngredients": [
          [{
            "Name": "majoneesia",
            "Amount": "1",
            "PackageRecipe": "false",
            "IngredientType": "6058",
            "Unit": "dl",
            "IngredientTypeName": "Majoneesi"
          }],
          [{
            "Name": "savupaprikajauhetta",
            "Amount": "1/2",
            "PackageRecipe": "false",
            "IngredientType": "15800",
            "Unit": "tl",
            "IngredientTypeName": "paprika, jauhettu, savustettu"
          }],
          [{
            "Name": "suolaa",
            "AmountInfo": "ripaus",
            "PackageRecipe": "false"
          }],
          [{
            "Name": "mustapippuria",
            "AmountInfo": "ripaus",
            "PackageRecipe": "false",
            "IngredientType": "6271",
            "IngredientTypeName": "Mustapippuri, rouhittu"
          }]
        ]
      }],
      "Instructions": "# Kuori, paloittele ja keitä bataatti juuri ja juuri kypsäksi. Keitä papuja pari minuuttia ja valuta.\r\n# Paloittele makkarat. Paloittele myös maissit terävällä veitsellä muutamaan osaan. Sekoita makkarat, maissit, bataatit ja pavut kulhossa. Sekoita joukkoon öljy ja mausteet.\r\n# Jaa maustetut ainekset neljälle leivinpaperilla suojatulle folionpalalle. Sulje nyytit. Grillaa nyyttejä noin 15 minuuttia.\r\n# Sekoita majoneesin ainekset keskenään. Tarjoa nyytit majoneesin kanssa.",
      "EndNote": "",
      "Description": "Makkara-maissinyytit valmistuvat helposti grillissä. Bataatti ja pavut antavat ruokaisuutta nyytteihin.",
      "DateCreated": "2019-03-22T12:56:11",
      "DateModified": "2019-06-06T15:32:49",
      "Stamp": {
        "Name": "Testattu K-koekeittiössä",
        "Id": "1"
      },
      "RecipeUse": {
        "Name": "k-ruoka.fi",
        "Id": "1"
      },
      "resourceType": "recipe",
      "Url": "https://www.k-ruoka.fi/reseptit/maissi-makkaranyytit",
      "UrlSlug": "maissi-makkaranyytit",
      "Sort": [1, 1, 10291]
    }, {
      "Id": "10290",
      "Name": "Grillattu flank steak ja ratatouille-vartaat",
      "PieceSize": {
        "Unit": "g",
        "Amount": "312"
      },
      "Portions": {
        "Unit": "annosta",
        "Amount": "4"
      },
      "PreparationTime": {
        "Description": "yli 60 min",
        "TimeRange": {
          "MinTime": 60
        }
      },
      "UserPortions": {
        "$": {
          "Unit": "henkilölle"
        }
      },
      "Categories": [{
        "MainName": "Pääraaka-aine",
        "MainId": "14",
        "SubName": "Liha",
        "SubId": "114"
      }, {
        "MainName": "Sesonki",
        "MainId": "1",
        "SubName": "Grillaus",
        "SubId": "5"
      }, {
        "MainName": "Pääruoat",
        "MainId": "4",
        "SubName": "Grilliruoat",
        "SubId": "73"
      }, {
        "MainName": "Lisäkkeet",
        "MainId": "6",
        "SubName": "Grilliruoan lisäkkeet",
        "SubId": "93"
      }],
      "Pictures": ["KCM_Grillattu_flank_ste"],
      "PictureUrls": [{
        "Id": "KCM_Grillattu_flank_ste",
        "Normal": "https://k-file-storage-qa.imgix.net/f/k-ruoka/recipe/10290?w=1000&h=1000&fit=clip",
        "Original": "https://k-file-storage-qa.imgix.net/f/k-ruoka/recipe/10290"
      }],
      "SpecialDiets": [{
        "Id": "4",
        "Name": "Gluteeniton"
      }],
      "Ingredients": [{
        "SubSectionHeading": "_",
        "SubSectionIngredients": [
          [{
            "Name": "flank steakia eli naudan kuvetta",
            "AmountInfo": "n. 700 g",
            "PackageRecipe": "false",
            "IngredientType": "6305",
            "IngredientTypeName": "Naudan flank steak"
          }],
          [{
            "Name": "suolaa",
            "Amount": "1",
            "PackageRecipe": "false",
            "IngredientType": "7049",
            "Unit": "tl",
            "IngredientTypeName": "Suola"
          }],
          [{
            "Name": "mustapippuria",
            "Amount": "1/2",
            "PackageRecipe": "false",
            "IngredientType": "6271",
            "Unit": "tl",
            "IngredientTypeName": "Mustapippuri, rouhittu"
          }],
          [{
            "Name": "rucolaa",
            "Amount": "1",
            "PackageRecipe": "false",
            "IngredientType": "6753",
            "Unit": "ruukku",
            "IngredientTypeName": "Rucola"
          }],
          [{
            "Name": "tuoretta basilikaa",
            "Amount": "1",
            "PackageRecipe": "false",
            "IngredientType": "5271",
            "Unit": "ruukku",
            "IngredientTypeName": "Basilika, tuore"
          }],
          [{
            "Name": "parmesaania lastuina",
            "Amount": "1",
            "PackageRecipe": "false",
            "IngredientType": "6468",
            "Unit": "dl",
            "IngredientTypeName": "Parmesaani"
          }],
          [{
            "Name": "auringonkukansiemeniä",
            "Amount": "1/2",
            "PackageRecipe": "false",
            "IngredientType": "5250",
            "Unit": "dl",
            "IngredientTypeName": "Auringonkukansiemen"
          }],
          [{
            "Name": "oliiviöljyä",
            "Amount": "2",
            "PackageRecipe": "false",
            "IngredientType": "6386",
            "Unit": "rkl",
            "IngredientTypeName": "Oliiviöljy"
          }]
        ]
      }, {
        "SubSectionHeading": "Yrttiöljy",
        "SubSectionIngredients": [
          [{
            "Name": "oliiviöljyä",
            "Amount": "1",
            "PackageRecipe": "false",
            "IngredientType": "6386",
            "Unit": "dl",
            "IngredientTypeName": "Oliiviöljy"
          }],
          [{
            "Name": "valkosipulinkynttä",
            "Amount": "2",
            "PackageRecipe": "false",
            "IngredientType": "7262",
            "Unit": "",
            "IngredientTypeName": "Valkosipuli"
          }],
          [{
            "Name": "yrtinlehtiä (esim. basilika, lehtipersilja)",
            "Amount": "1",
            "PackageRecipe": "false",
            "IngredientType": "5271",
            "Unit": "dl",
            "IngredientTypeName": "Basilika, tuore"
          }],
          [{
            "Name": "suolaa",
            "Amount": "1",
            "PackageRecipe": "false",
            "IngredientType": "7049",
            "Unit": "tl",
            "IngredientTypeName": "Suola"
          }],
          [{
            "Name": "mustapippuria",
            "AmountInfo": "ripaus",
            "PackageRecipe": "false",
            "IngredientType": "6271",
            "IngredientTypeName": "Mustapippuri, rouhittu"
          }]
        ]
      }, {
        "SubSectionHeading": "Ratatouille-vartaat",
        "SubSectionIngredients": [
          [{
            "Name": "pieni kesäkurpitsa",
            "Amount": "1",
            "AmountInfo": "(200 g)",
            "PackageRecipe": "false",
            "IngredientType": "5781",
            "Unit": "",
            "IngredientTypeName": "Kesäkurpitsa"
          }],
          [{
            "Name": "pieni munakoiso",
            "Amount": "1",
            "AmountInfo": "(250 g)",
            "PackageRecipe": "false",
            "IngredientType": "6248",
            "Unit": "",
            "IngredientTypeName": "Munakoiso"
          }],
          [{
            "Name": "punainen paprika",
            "Amount": "1",
            "AmountInfo": "(150 g)",
            "PackageRecipe": "false",
            "IngredientType": "6448",
            "Unit": "",
            "IngredientTypeName": "Paprika, punainen"
          }],
          [{
            "Name": "punasipulia",
            "Amount": "2",
            "AmountInfo": "(200 g)",
            "PackageRecipe": "false",
            "IngredientType": "6667",
            "Unit": "",
            "IngredientTypeName": "Punasipuli"
          }],
          [{
            "Name": "kirsikkatomaatteja",
            "Amount": "200",
            "PackageRecipe": "false",
            "IngredientType": "5826",
            "Unit": "g",
            "IngredientTypeName": "Kirsikkatomaatti"
          }]
        ]
      }],
      "Instructions": "# Ota liha huoneenlämpöön tunti ennen grillausta.\r\n# Soseuta yrttiöljyn ainekset kapeassa kulhossa sauvasekoittimella.\r\n# Kuutioi kesäkurpitsa ja munakoiso. Paloittele paprika. Kuori ja lohko sipulit. Sekoita kasvikset ja yrttiöljy kulhossa. Pujottele kasvikset vartaisiin.\r\n# Suolaa ja pippuroi flank steak. Paista lihaan ensin kuumassa grillissä pintaväri ja siirrä sitten epäsuoralle lämmölle. Aseta lihalämpömittarin anturi keskelle lihaa. Jätä flank steak keskeltä punaiseksi (55 astetta), sillä kypsä flank steak muuttuu sitkeäksi. Anna grillatun lihan tekeytyä folioon käärittynä 10 minuuttia.\r\n# Grillaa kasvisvartaat keskilämmöllä grillissä, kunnes ne ovat saaneet väriä ja kasvikset ovat sopivan kypsiä.\r\n# Sekoita rucolasta, basilikanlehdistä, parmesaanilastuista ja auringonkukansiemenistä salaatti ja lorauta kastikkeeksi tilkka öljyä\r\n# Viipaloi flank steak poikkisyin ja tarjoa vartaiden ja salaatin kanssa.",
      "EndNote": "",
      "Description": "Grillattu flank steak tarjotaan värikkäiden ratatouille-vartaiden ja basilikaisen salaatin kanssa.",
      "DateCreated": "2019-03-22T12:46:24",
      "DateModified": "2019-08-06T12:32:41",
      "Stamp": {
        "Name": "Testattu K-koekeittiössä",
        "Id": "1"
      },
      "RecipeUse": {
        "Name": "k-ruoka.fi",
        "Id": "1"
      },
      "resourceType": "recipe",
      "Url": "https://www.k-ruoka.fi/reseptit/grillattu-flank-steak-ja-ratatouille-vartaat",
      "UrlSlug": "grillattu-flank-steak-ja-ratatouille-vartaat",
      "Sort": [1, 1, 10290]
    }]
  }