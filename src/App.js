import React from 'react';
import logo from './logo.svg';
import './App.css';
import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import Paper from '@material-ui/core/Paper';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import clsx from 'clsx';
import CardHeader from '@material-ui/core/CardHeader';
import CardMedia from '@material-ui/core/CardMedia';
import { CardActionArea } from '@material-ui/core';
import Collapse from '@material-ui/core/Collapse';
import IconButton from '@material-ui/core/IconButton';
import { red } from '@material-ui/core/colors';
import * as backend from './backend.js'

const useStyles = makeStyles(theme => ({
  button: {
    marginTop:"20vh",
   margin: theme.spacing(1),
 },
 input: {
   display: 'none',
 },
  card: {
    width: "20vw",
    marginLeft:20,
    marginTop:40,
    marginBottom:40,
  },
  decriptionCard:{
    width: "20vw",
    marginLeft:10,
    marginTop:40,
    marginBottom:40,
  },
  descriptionCardContent:{
    marginTop:10,
  },
  media: {
    height: 0,
    paddingTop: '100%', // 16:9
  },
}));
function App() {

    const classes = useStyles();
    const bull = <span className={classes.bullet}>•</span>;


    const demoSet = backend.getMealSets()['sets'][0]
    let totalCO2 = 0
    let totalKcal = 0
    for (const meal in demoSet['meals'].slice(0,5)) {
      totalCO2 += demoSet['meals'][meal]['co2']
      totalKcal += demoSet['meals'][meal]['kcal']
    }
    totalKcal = totalKcal/5;

  return (
    <div className="App">
      <header className="App-header"></header>
      <span className="menu-text">Stores </span>
      <a href="/scrapes/frontpage.html"><span className="menu-text">Products </span></a>
      <span className="menu-text">Recipes </span>
      <span className="menu-text">Seasonal</span>
      <span className="menu-text" id="meal-sub">Subscription</span>
      <div className="banner"></div>
      <Paper className="paper">

        <Card className={classes.decriptionCard}>
        <CardHeader
          title={demoSet['title']}
      titleTypographyProps={{variant:'h6' }}
        />
      <Typography variant="body" component="p">
      {demoSet['description']}
    </Typography>
        <CardContent>
        <Typography variant="body2" component="p">
          </Typography>
          <Typography variant="body2" color="textSecondary" component="p">
            <Paper className={classes.descriptionCardContent}>
            CO2 emission: {totalCO2} kg - <span className="goodChoice"> Great!</span>
            </Paper>
            <Paper className={classes.descriptionCardContent}>
  Average kcal per meal: {totalKcal} kcal
            </Paper>
            <Paper className={classes.descriptionCardContent}>
              Your meals are: <span className="goodChoice"> Healthy!</span>
            </Paper>
          </Typography>
          <Button variant="contained" color="primary" className={classes.button}>
        Subscribe 50€/kk
      </Button>
        </CardContent>
      </Card>




      <Card className={classes.card}>
      <a href="/scrapes/firstfood.html">

      <CardHeader
        className="cardHeader"
        title={demoSet['meals'][0]['Name']}
    titleTypographyProps={{variant:'h7' }}
      />
      <CardMedia
        className={classes.media}
        image={demoSet['meals'][0]['image']}
        title="Paella dish"
      />
      <CardContent>
        <Typography className="descriptionText" variant="body2" color="textSecondary" component="p">
        {demoSet['meals'][0]['description']}
        </Typography>
        <Paper className={classes.descriptionCardContent}>
          <Typography variant="body2" color="textSecondary" component="p">
        Kcal: {demoSet['meals'][0]['kcal']}
      </Typography>
        </Paper>
        <Paper className={classes.descriptionCardContent}>
      <Typography variant="body2" color="textSecondary" component="p">
    CO2: {demoSet['meals'][0]['co2']}
  </Typography>
    </Paper>
      </CardContent>
      </a>

    </Card>



    <Card className={classes.card}>
  <CardHeader
    titleTypographyProps={{variant:'h7' }}
    title={demoSet['meals'][1]['Name']}
    className="cardHeader"
  />
  <CardMedia
    className={classes.media}
    image={demoSet['meals'][1]['image']}
    title="Paella dish"
  />
  <CardContent>
    <Typography className="descriptionText" variant="body2" color="textSecondary" component="p">
    {demoSet['meals'][1]['description']}
    </Typography>
    <Paper className={classes.descriptionCardContent}>
      <Typography variant="body2" color="textSecondary" component="p">
    Kcal: {demoSet['meals'][1]['kcal']}
  </Typography>
    </Paper>
    <Paper className={classes.descriptionCardContent}>
      <Typography variant="body2" color="textSecondary" component="p">
    CO2: {demoSet['meals'][1]['co2']}
  </Typography>
    </Paper>
  </CardContent>
</Card>
<Card className={classes.card}>
<CardHeader
  className="cardHeader"
title={demoSet['meals'][2]['Name']}
titleTypographyProps={{variant:'h7' }}
/>
<CardMedia
className={classes.media}
image={demoSet['meals'][2]['image']}
title="Paella dish"
/>
<CardContent>
<Typography className="descriptionText" variant="body2" color="textSecondary" component="p">
{demoSet['meals'][2]['description']}
</Typography>
<Paper className={classes.descriptionCardContent}>
  <Typography variant="body2" color="textSecondary" component="p">
Kcal: {demoSet['meals'][2]['kcal']}
</Typography>
</Paper>
<Paper className={classes.descriptionCardContent}>
      <Typography variant="body2" color="textSecondary" component="p">
    CO2: {demoSet['meals'][2]['co2']}
  </Typography>
    </Paper>
</CardContent>
</Card>
<Card className={classes.card}>
<CardHeader
  className="cardHeader"
title={demoSet['meals'][3]['Name']}
titleTypographyProps={{variant:'h7' }}
/>
<CardMedia
className={classes.media}
image={demoSet['meals'][3]['image']}
title="Paella dish"
/>
<CardContent>
<Typography className="descriptionText" variant="body2" color="textSecondary" component="p">
{demoSet['meals'][3]['description']}
</Typography>
<Paper className={classes.descriptionCardContent}>
  <Typography variant="body2" color="textSecondary" component="p">
Kcal: {demoSet['meals'][3]['kcal']}
</Typography>
</Paper>
<Paper className={classes.descriptionCardContent}>
      <Typography variant="body2" color="textSecondary" component="p">
    CO2: {demoSet['meals'][3]['co2']}
  </Typography>
    </Paper>
</CardContent>
</Card>
<Card className={classes.card}>
<CardHeader
  className="cardHeader"
title={demoSet['meals'][4]['Name']}
titleTypographyProps={{variant:'h7' }}
/>
<CardMedia
className={classes.media}
image={demoSet['meals'][4]['image']}
title="Paella dish"
/>
<CardContent>
<Typography className="descriptionText" variant="body2" color="textSecondary" component="p">
{demoSet['meals'][4]['description']}
</Typography>
<Paper className={classes.descriptionCardContent}>
  <Typography variant="body2" color="textSecondary" component="p">
Kcal: {demoSet['meals'][4]['kcal']}
</Typography>
</Paper>
<Paper className={classes.descriptionCardContent}>
      <Typography variant="body2" color="textSecondary" component="p">
    CO2: {demoSet['meals'][5]['co2']}
  </Typography>
    </Paper>
</CardContent>
</Card>



</Paper>

    </div>
  );
}

export default App;
